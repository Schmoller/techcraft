#include "techcraft/generation/base.hpp"

namespace Techcraft::Generation {

ChunkGenerator::ChunkGenerator() {
}

void ChunkGenerator::addBiome(std::unique_ptr<Biome> &&biome) {
    biomeBoundaries.push_back(maxWeight);
    maxWeight += biome->weight;
    halfWeight = maxWeight / 2;
    
    biomes.push_back(std::move(biome));
}

}