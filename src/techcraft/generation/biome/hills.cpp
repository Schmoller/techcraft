#include "techcraft/generation/biome/hills.hpp"
#include "techcraft/tiles/registry.hpp"

namespace Techcraft::Generation {

HillsBiome::HillsBiome() {
    minimumElevation = 1;
    maximumElevation = 90;

    lowFreqFactor = 1;
    medFreqFactor = 0.8;
    highFreqFactor = 0.5;

    weight = 0.3;
}

}