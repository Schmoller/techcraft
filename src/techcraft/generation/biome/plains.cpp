#include "techcraft/generation/biome/plains.hpp"
#include "techcraft/tiles/registry.hpp"
#include "techcraft/dimension/chunk.hpp"

namespace Techcraft::Generation {

PlainsBiome::PlainsBiome() {
    minimumElevation = 1;
    maximumElevation = 30;

    lowFreqFactor = 1;
    medFreqFactor = 0.1;
    highFreqFactor = 0.3;
}

void PlainsBiome::decorateColumn(const std::shared_ptr<Chunk> &chunk, const ChunkPosition &chunkCoord, int x, int y, int realX, int realY, tcsize terrainHeight) {
    int dirtLen = ((realX ^ ~realY) & 0b11) + 2;
    
    auto top = chunk->getTop(x, y);
    if (top == NOT_PRESENT) {
        return;
    }

    for (int z = top; z >= 0; --z) {
        auto realZ = chunkCoord.z << ChunkBits | z;

        if (!chunk->getTile(x, y, z).isEmpty()) {
            if (realZ == terrainHeight) {
                if (realZ > 0) {
                    chunk->setTile(x, y, z, {TileTypes::Grass});
                } else {
                    chunk->setTile(x, y, z, {TileTypes::Dirt});
                }
            } else if (realZ > terrainHeight - dirtLen) {
                chunk->setTile(x, y, z, {TileTypes::Dirt});
            } else {
                break;
            }
        }
    }
}

}