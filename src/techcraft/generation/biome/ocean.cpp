#include "techcraft/generation/biome/ocean.hpp"
#include "techcraft/tiles/registry.hpp"
#include "techcraft/dimension/chunk.hpp"

namespace Techcraft::Generation {

OceanBiome::OceanBiome() {
    minimumElevation = -40;
    maximumElevation = 2;

    lowFreqFactor = 1;
    medFreqFactor = 0.4;
    highFreqFactor = 0.3;
}

void OceanBiome::decorateColumn(const std::shared_ptr<Chunk> &chunk, const ChunkPosition &chunkCoord, int x, int y, int realX, int realY, tcsize terrainHeight) {
    int dirtLen = ((realX ^ ~realY) & 0b11) + 2;
    
    auto top = chunk->getTop(x, y);
    if (top == NOT_PRESENT) {
        return;
    }

    for (int z = top; z >= 0; --z) {
        auto realZ = chunkCoord.z << ChunkBits | z;

        if (!chunk->getTile(x, y, z).isEmpty()) {
            if (realZ == terrainHeight) {
                if (realZ > 0) {
                    // TODO: Sand
                    chunk->setTile(x, y, z, {TileTypes::Dirt});
                } else {
                    // TODO: Gravel
                    chunk->setTile(x, y, z, {TileTypes::Dirt});
                }
            } else if (realZ > terrainHeight - dirtLen) {
                chunk->setTile(x, y, z, {TileTypes::Dirt});
            } else {
                break;
            }
        }
    }
}

}