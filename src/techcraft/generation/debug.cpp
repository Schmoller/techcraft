#include "techcraft/generation/debug.hpp"
#include "techcraft/tiles/registry.hpp"
#include "techcraft/dimension/chunk.hpp"

namespace Techcraft::Generation {

void DebugChunkGenerator::populate(const ChunkPosition &coord, Chunk *chunk) {
    // Dirt floor
    for (auto x = 0; x < ChunkSize; x++) {
        for (auto y = 0; y < ChunkSize; y++) {
            chunk->setTile(x, y, 0, {TileTypes::Dirt});
        }
    }

    // Useful markers to indicate where the chunk is
    // chunk->setTile(0, 0, 0, {TileTypes::Stone});
    // chunk->setTile(ChunkSize - 1, 0, 0, {TileTypes::Stone});
    // chunk->setTile(0, ChunkSize - 1, 0, {TileTypes::Stone});
    // chunk->setTile(0, 0, ChunkSize - 1, {TileTypes::Stone});
    // chunk->setTile(ChunkSize - 1, ChunkSize - 1, 0, {TileTypes::Stone});
    // chunk->setTile(0, ChunkSize - 1, ChunkSize - 1, {TileTypes::Stone});
    // chunk->setTile(ChunkSize - 1, 0, ChunkSize - 1, {TileTypes::Stone});
    // chunk->setTile(ChunkSize - 1, ChunkSize - 1, ChunkSize - 1, {TileTypes::Stone});

    for (int x = 0; x < ChunkSize; ++x) {
        chunk->setTile(x, x, 1, {TileTypes::Stone});
    }
    // for (int x = 1; x < ChunkSize; x += 2) {
    //     chunk->setTile(x, 4, 1, {TileTypes::Stone});
    // }
}

}