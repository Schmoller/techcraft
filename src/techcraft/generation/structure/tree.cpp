#include "techcraft/generation/structure/tree.hpp"
#include "techcraft/tiles/registry.hpp"
#include "utilities/lsystem.hpp"

#include "engine/subsystem/debug.hpp"

#include <iostream>
#include <random>
#include <vector>
#include <assert.h>
#include <functional>

namespace Techcraft::Generation {

using namespace Utilities::LSystem;

// Constants
const char Up = 'u';
const char North = 'n';
const char South = 's';
const char East = 'e';
const char West = 'w';

const char BranchStart = '[';
const char BranchEnd = ']';

// Variables
const char Log = 'W';
const char Leaf = 'L';
const char BranchLog = 'B';
const char BranchLeaf = 'K';


TreeGenerator::TreeGenerator() {
    // FIXME DEBUG: This is just to get us going
    trunkType = TileTypes::Log1;
    branchType = TileTypes::Log1;
    leafType = TileTypes::Leaves1;

    assert(trunkType);
    assert(branchType);
    assert(leafType);
}

void TreeGenerator::generateAt(Dimension &dimension, const TilePosition &position) {
    static auto debug = Engine::Subsystem::DebugSubsystem::instance();

    std::cout << "Place a tree" << std::endl;

    /*
    How do we want to do this?

    Parameters:
    - trunk min height
    - trunk max height
    - trunk height variability
    - trunk min width
    - trunk max width
    - root min width
    - root max width
    - root width taper
    - branch min height
    - min branches
    - max branches
    - branch no. variability
    - branch min length
    - branch max length
    - branch length variability
    - branch shape?(some branches flattern out, some curve up)
    - leaves min thickness
    - leaves max thickness

    Algorithm:
    1. Determine height of trunk first
    2. 
    */

    uint32_t seed = std::hash<TilePosition>()(position);

    std::mt19937 gen(seed);

    LSystem system;
    system.setRand(gen);

    // system.addRule(std::make_shared<BasicRule>('W', "W"));
    //system.addRule(std::make_shared<ProbabilisticRule>('W', 0.1, "WW"));
    //system.addRule(std::make_shared<ProbabilisticRule>('W', 0.1, "W[W]"));
    // system.addRule(std::make_shared<ContextualRule>('W', "WWW", "", "WB"));

    
    // Rules
    // system.addRule('L', "WL");
    // system.addRule('L', 0.4, "WWL");
    // system.addRule('W', 0.1, "W[nK]");
    // system.addRule('W', 0.1, "W[sK]");
    // system.addRule('W', 0.1, "W[eK]");
    // system.addRule('W', 0.1, "W[wK]");
    
    // system.addRule('K', "BK");
    // system.addRule('K', 0.3, "BBK");
    // system.addRule('B', 0.1, "B[uK]");
    // system.addRule('B', 0.1, "B[nK]");
    // system.addRule('B', 0.1, "B[sK]");
    // system.addRule('B', 0.1, "B[eK]");
    // system.addRule('B', 0.1, "B[wK]");
    // system.addRule('B', 0.7, "BuB");
    // system.addRule('B', 0.6, "BnB");
    // system.addRule('B', 0.6, "BsB");
    // system.addRule('B', 0.6, "BeB");
    // system.addRule('B', 0.6, "BwB");

    system.addRule('L', "W[tL]");
    system.addRule('W', "WW");
    // system.addRule('L', "WWL", "", "W[eL][wL]");

    std::cout << system.toString() << std::endl;

    std::cout << "S: " << seed << " R: " << gen() << std::endl;

    // LSystem
    auto result = system.run("L", 5);
  
    std::cout << "Output: " << std::string(result.data(), result.size()) << std::endl;

    // First attempt at generation

    placeBy(result, 0, dimension, position, TileDirection::Up);



    // // Make a trunk
    // int trunkWidth = trunkSize.randomValue(gen);
    // int maxHeight = height.randomValue(gen);

    // for (int z = 0; z < maxHeight; ++z) {
    //     dimension.setTile(position + TilePosition{0, 0, z}, { trunkType });
    // }
}

size_t TreeGenerator::placeBy(const std::string &dna, size_t offset, Dimension &dimension, TilePosition startPos, TileDirection startDir) {
    size_t i = offset;
    TileDirection dir = startDir;
    TilePosition pos = startPos;

    while (i < dna.size()) {
        auto ch = dna[i];

        if (ch == BranchStart) {
            i = placeBy(dna, i+1, dimension, pos, dir);
            continue;
        } else {
            ++i;
        }

        switch (ch) {
            case BranchEnd:
                return i;
            case Log:
            case BranchLog:
                dimension.setTile(pos, { trunkType });
                pos = pos + TileDirectionOffset::from(dir);
                break;
            case Leaf:
            case BranchLeaf:
                dimension.setTile(pos, { leafType });
                pos = pos + TileDirectionOffset::from(dir);
                break;
            case 't':
                switch (dir) {
                case TileDirection::Up:
                    dir = TileDirection::West;
                    break;
                case TileDirection::North:
                case TileDirection::South:
                case TileDirection::East:
                case TileDirection::West:
                    dir = TileDirection::Up;
                    break;
                }
                break;
            case Up:
                dir = TileDirection::Up;
                break;
            case North:
                dir = TileDirection::North;
                break;
            case South:
                dir = TileDirection::South;
                break;
            case East:
                dir = TileDirection::East;
                break;
            case West:
                dir = TileDirection::West;
                break;
        }
    }

    return i;
}

}