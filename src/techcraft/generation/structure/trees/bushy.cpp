#include "techcraft/generation/structure/trees/bushy.hpp"
#include "techcraft/tiles/registry.hpp"

#include <assert.h>
#include <random>


namespace Techcraft::Generation {
BushyTreeGenerator::BushyTreeGenerator() {
    // FIXME DEBUG: This is just to get us going
    trunkType = TileTypes::Log1;
    branchType = TileTypes::Log1;
    leafType = TileTypes::Leaves1;

    assert(trunkType);
    assert(branchType);
    assert(leafType);
}
    
void BushyTreeGenerator::generateAt(Dimension &dimension, const TilePosition &position, Random &random) {
    int height = random.randRange(9, 17);
    int leanHeight = height - random.randRange(0, 4);

    TileDirectionExtended lean = random.randIn(ExtendedHorizontal, 8);

    tcsize x = position.x;
    tcsize y = position.y;
    tcsize z = position.z;

    tcsize trunkTop = z + height - 1;

    // Roots?
    if (random.randChance(10, 15)) {
        auto rootStyle = random.randRange(1, 15);

        if (random.randChance(2)) {
            // North-south roots
            placeLogAt(dimension, {x, y + 1, z});
            placeLogAt(dimension, {x, y - 1, z});

            switch (rootStyle) {
                case 0:
                    placeLogAt(dimension, {x, y - 1, z + 1});
                    break;
                case 1:
                    placeLogAt(dimension, {x, y + 1, z + 1});
                    break;
                case 2:
                    placeLogAt(dimension, {x - 1, y, z});
                    placeLogAt(dimension, {x - 1, y, z + 1});
                    break;
                default:
                    // No augments
                    break;
            }

            if (random.randChance(3)) {
                placeLogAt(dimension, {x + 1, y, z});
            } else if (random.randChance(3)) {
                placeLogAt(dimension, {x - 1, y, z});
            }
        } else {
            // East-west roots
            placeLogAt(dimension, {x + 1, y, z});
            placeLogAt(dimension, {x - 1, y, z});

            switch (rootStyle) {
                case 0:
                    placeLogAt(dimension, {x - 1, y, z + 1});
                    break;
                case 1:
                    placeLogAt(dimension, {x + 1, y, z + 1});
                    break;
                case 2:
                    placeLogAt(dimension, {x, y - 1, z});
                    placeLogAt(dimension, {x, y - 1, z + 1});
                    break;
                default:
                    // No augments
                    break;
            }

            if (random.randChance(3)) {
                placeLogAt(dimension, {x, y + 1, z});
            } else if (random.randChance(3)) {
                placeLogAt(dimension, {x, y - 1, z});
            }
        }
    }

    // Trunk
    bool hasLeant = false;
    for (tcsize trunkOff = 0; trunkOff < height; ++trunkOff) {
        // if (trunkOff >= leanHeight && !hasLeant) {
        //     hasLeant = true;
        //     auto offset = TileDirectionOffset::from(lean);;
        //     x += offset.x;
        //     y += offset.y;
        // }

        placeLogAt(dimension, {x, y, z + trunkOff});
    }

    // Branches
    placeLogAt(dimension, {x - 1, y, z + height - 3});
    placeLogAt(dimension, {x - 2, y, z + height - 3});
    placeLogAt(dimension, {x + 1, y, z + height - 3});
    placeLogAt(dimension, {x + 2, y, z + height - 3});
    placeLogAt(dimension, {x, y - 1, z + height - 3});
    placeLogAt(dimension, {x, y - 2, z + height - 3});
    placeLogAt(dimension, {x, y + 1, z + height - 3});
    placeLogAt(dimension, {x, y + 2, z + height - 3});

    placeLogAt(dimension, {x - 2, y, z + height - 1});
    placeLogAt(dimension, {x + 2, y, z + height - 1});
    placeLogAt(dimension, {x, y - 2, z + height - 1});
    placeLogAt(dimension, {x, y + 2, z + height - 1});

    placeLogAt(dimension, {x - 3, y, z + height - 2});
    placeLogAt(dimension, {x + 3, y, z + height - 2});
    placeLogAt(dimension, {x, y - 3, z + height - 2});
    placeLogAt(dimension, {x, y + 3, z + height - 2});
    placeLogAt(dimension, {x - 4, y, z + height - 2});
    placeLogAt(dimension, {x + 4, y, z + height - 2});
    placeLogAt(dimension, {x, y - 4, z + height - 2});
    placeLogAt(dimension, {x, y + 4, z + height - 2});

    placeLogAt(dimension, {x - 5, y + 1, z + height - 3});
    placeLogAt(dimension, {x + 5, y - 1, z + height - 3});
    placeLogAt(dimension, {x - 1, y - 5, z + height - 3});
    placeLogAt(dimension, {x + 1, y + 5, z + height - 3});

    // Leaves
    const tcsize leafSize = 1;
    placeLeafAt(dimension, {x + 2, y, trunkTop + 1});
    placeLeafAt(dimension, {x - 2, y, trunkTop + 1});
    placeLeafAt(dimension, {x, y + 2, trunkTop + 1});
    placeLeafAt(dimension, {x, y - 2, trunkTop + 1});
    
    for (tcsize lx = -leafSize; lx <= leafSize; ++lx) {
        for (tcsize ly = -leafSize; ly <= leafSize; ++ly) {
            placeLeafAt(dimension, {x + lx, y + ly, trunkTop + 1});
            for (tcsize lz = 0; lz <= 3; ++lz) {
                placeLeafAt(dimension, {x + lx, y + ly, trunkTop + lz - 3});
            }
        }
    }

    // if (random.randChance(2)) {
    //     // DEBUG: Marker log
    //     placeLogAt(dimension, { x, y - 3, z + 3 });

    //     // Branch S
    //     // placeLeafHanging(dimension, { x, y - 1, trunkTop - 3 }, 3);
    //     // placeLeafHanging(dimension, { x, y - 2, trunkTop - 0 }, 3);
    //     // placeLeafHanging(dimension, { x, y - 3, trunkTop + 1 }, 3);
    //     // placeLeafHanging(dimension, { x, y - 4, trunkTop - 0 }, 5);
    //     placeLeafHanging(dimension, { x, y - 5, trunkTop - 1 }, 2);
    // } else {
    for (tcsize lz = 0; lz < 3; ++lz) {
        placeLeafAt(dimension, { x, y - 1, trunkTop + lz - 6 });
        placeLeafAt(dimension, { x, y - 2, trunkTop + lz - 3 });
        placeLeafAt(dimension, { x, y - 3, trunkTop + lz - 2 });
        placeLeafAt(dimension, { x, y - 4, trunkTop + lz - 5 });
        placeLeafAt(dimension, { x, y - 4, trunkTop + lz - 3 });

        if (lz <= 2) {
            placeLeafAt(dimension, { x, y - 5, trunkTop + lz - 3});
            placeLeafAt(dimension, { x - 1, y - 2, trunkTop + lz - 2 });
            placeLeafAt(dimension, { x + 2, y - 4, trunkTop + lz - 5 });
            placeLeafAt(dimension, { x + 1, y - 3, trunkTop + lz - 3 });
            placeLeafAt(dimension, { x + 1, y - 2, trunkTop + lz - 2 });
            placeLeafAt(dimension, { x + 2, y - 1, trunkTop + lz - 2 });

            placeLeafAt(dimension, { x + 3, y, trunkTop + lz - 2 });
            placeLeafAt(dimension, { x + 4, y, trunkTop + lz - 2 });
            placeLeafAt(dimension, { x + 5, y, trunkTop + lz - 3 });

            placeLeafAt(dimension, { x + 6, y - 1, trunkTop + lz - 4 });
            placeLeafAt(dimension, { x + 5, y - 1, trunkTop + lz - 5 });
            placeLeafAt(dimension, { x + 1, y + 3, trunkTop + lz - 2 });
            placeLeafAt(dimension, { x + 1, y + 4, trunkTop + lz - 3 });

            placeLeafAt(dimension, { x + 1, y + 6, trunkTop + lz - 4 });

            placeLeafAt(dimension, { x - 1, y + 2, trunkTop + lz - 2 });

            placeLeafAt(dimension, { x - 4, y + 1, trunkTop + lz - 2 });
            placeLeafAt(dimension, { x - 4, y, trunkTop + lz - 2 });
            placeLeafAt(dimension, { x - 2, y + 1, trunkTop + lz - 2 });

            placeLeafAt(dimension, { x - 5, y + 2, trunkTop + lz - 4 });
            placeLeafAt(dimension, { x - 2, y - 1, trunkTop - lz });
            placeLeafAt(dimension, { x - 3, y - 2, trunkTop - lz - 2 });

            placeLeafAt(dimension, { x + 4, y - 3, trunkTop - lz - 2 });
        }
        if (lz <= 1) {
            placeLeafAt(dimension, { x - 1, y - 4, trunkTop + lz - 2 });
            placeLeafAt(dimension, { x - 1, y - 3, trunkTop + lz - 1 });

            placeLeafAt(dimension, {x - 1, y - 6, trunkTop + lz - 3});
            placeLeafAt(dimension, {x + 3, y + 1, trunkTop + lz - 2});
            placeLeafAt(dimension, {x + 3, y + 2, trunkTop + lz - 2});
            placeLeafAt(dimension, {x + 2, y + 5, trunkTop + lz - 3});
            placeLeafAt(dimension, {x, y + 5, trunkTop + lz - 2});

            placeLeafAt(dimension, {x - 2, y + 2, trunkTop + lz - 1});
        }

        placeLeafAt(dimension, {x + 2, y, trunkTop + lz - 3});
        placeLeafAt(dimension, {x + 5, y - 2, trunkTop + lz - 4});
        placeLeafAt(dimension, {x + 5, y - 2, trunkTop + lz - 6});
        placeLeafAt(dimension, {x + 4, y - 1, trunkTop + lz - 3});
        placeLeafAt(dimension, {x + 4, y - 1, trunkTop + lz - 4});

        placeLeafAt(dimension, {x + 1, y + 5, trunkTop + lz - 4});
        placeLeafAt(dimension, {x + 1, y + 5, trunkTop + lz - 6});

        placeLeafAt(dimension, {x, y + 3, trunkTop + lz - 3});
        placeLeafAt(dimension, {x, y + 4, trunkTop + lz - 3});

        placeLeafAt(dimension, {x - 1, y + 3, trunkTop + lz - 4});
        placeLeafAt(dimension, {x - 1, y + 3, trunkTop + lz - 5});

        placeLeafAt(dimension, {x - 3, y, trunkTop + lz - 3});
        placeLeafAt(dimension, {x - 2, y, trunkTop + lz - 3});
        placeLeafAt(dimension, {x - 1, y, trunkTop + lz - 5});

        placeLeafAt(dimension, {x - 6, y + 2, trunkTop + lz - 6});

        placeLeafAt(dimension, {x + 1, y - 5, trunkTop - 2});
        placeLeafAt(dimension, {x + 1, y - 4, trunkTop - 1});
        placeLeafAt(dimension, {x + 1, y - 4, trunkTop - 3});

        placeLeafAt(dimension, {x + 2, y - 3, trunkTop - 2});
        placeLeafAt(dimension, {x + 2, y - 3, trunkTop});
        placeLeafAt(dimension, {x + 2, y - 2, trunkTop - 1});
        placeLeafAt(dimension, {x + 5, y - 1, trunkTop - 3});
        placeLeafAt(dimension, {x + 5, y - 1, trunkTop - 1});
        placeLeafAt(dimension, {x + 3, y - 1, trunkTop - 1});

        placeLeafAt(dimension, {x + 6, y - 2, trunkTop - 2});
        placeLeafAt(dimension, {x + 4, y - 2, trunkTop - 2});
        placeLeafAt(dimension, {x + 3, y - 2, trunkTop - 2});
        placeLeafAt(dimension, {x + 1, y - 2, trunkTop - 2});

        placeLeafAt(dimension, {x + 1, y - 2, trunkTop - 2});
        placeLeafAt(dimension, {x + 3, y - 2, trunkTop - 1});
        placeLeafAt(dimension, {x + 2, y - 2, trunkTop});

        placeLeafAt(dimension, {x + 5, y + 1, trunkTop});

        placeLeafAt(dimension, {x - 2, y - 5, trunkTop + lz - 5});
        placeLeafAt(dimension, {x - 1, y - 5, trunkTop + lz - 4});

        placeLeafAt(dimension, {x + 1, y + 2, trunkTop + lz - 3});

        placeLeafAt(dimension, {x - 2, y - 6, trunkTop - 2});

        placeLeafAt(dimension, {x + 4, y + 1, trunkTop - 1});
        placeLeafAt(dimension, {x + 4, y + 2, trunkTop - 1});
        placeLeafAt(dimension, {x + 3, y + 2, trunkTop});
        placeLeafAt(dimension, {x + 2, y + 1, trunkTop});
        placeLeafAt(dimension, {x + 2, y + 3, trunkTop});

        placeLeafAt(dimension, {x + 2, y + 2, trunkTop - 1});
        placeLeafAt(dimension, {x + 2, y + 4, trunkTop - 1});

        placeLeafAt(dimension, {x + 2, y + 3, trunkTop - 2});
        placeLeafAt(dimension, {x + 2, y + 1, trunkTop - 2});

        placeLeafAt(dimension, {x - 1, y + 4, trunkTop - 1});
        placeLeafAt(dimension, {x - 1, y + 4, trunkTop - 3});

        placeLeafAt(dimension, {x, y + 2, trunkTop - 1});
        placeLeafAt(dimension, {x, y + 2, trunkTop - 3});

        placeLeafAt(dimension, {x - 3, y + 2, trunkTop + 1});
        placeLeafAt(dimension, {x - 3, y + 1, trunkTop + 1});
        placeLeafAt(dimension, {x - 3, y + 1, trunkTop - 1});
        placeLeafAt(dimension, {x - 3, y + 1, trunkTop});


        placeLeafAt(dimension, {x - 3, y + 2, trunkTop - 1});
        placeLeafAt(dimension, {x - 3, y + 2, trunkTop - 2});
        placeLeafAt(dimension, {x - 3, y + 3, trunkTop - 1});
        placeLeafAt(dimension, {x - 2, y + 3, trunkTop});
        placeLeafAt(dimension, {x - 2, y + 3, trunkTop - 2});

        placeLeafAt(dimension, {x - 5, y, trunkTop - 1});
        placeLeafAt(dimension, {x - 5, y, trunkTop - 2});

        placeLeafAt(dimension, {x - 4, y - 1, trunkTop - 1});
        placeLeafAt(dimension, {x - 3, y - 1, trunkTop - 1});
        placeLeafAt(dimension, {x - 3, y - 1, trunkTop - 2});


        placeLeafAt(dimension, {x - 5, y + 1, trunkTop - 1});
        placeLeafAt(dimension, {x - 6, y + 1, trunkTop - 2});
        placeLeafAt(dimension, {x - 5, y + 1, trunkTop - 3});

        placeLeafAt(dimension, {x - 3, y - 2, trunkTop});
        placeLeafAt(dimension, {x - 2, y - 2, trunkTop});
        placeLeafAt(dimension, {x - 2, y - 2, trunkTop - 1});
        placeLeafAt(dimension, {x - 2, y - 3, trunkTop - 1});
    }
}

void BushyTreeGenerator::placeLogAt(Dimension &dimension, const TilePosition &position) {
    auto existing = dimension.getTile(position);

    if (!existing.isEmpty() && existing.type != leafType) {
        return;
    }

    dimension.setTile(position, { trunkType });
}

void BushyTreeGenerator::placeLeafAt(Dimension &dimension, const TilePosition &position) {
    auto existing = dimension.getTile(position);

    if (!existing.isEmpty()) {
        return;
    }

    dimension.setTile(position, { leafType });
}

void BushyTreeGenerator::placeLeafHanging(Dimension &dimension, const TilePosition &position, tcsize length) {
    for (tcsize z = position.z; z >= position.z - length; --z) {
        placeLeafAt(dimension, TilePosition{position.x, position.y, z});
    }
}

}