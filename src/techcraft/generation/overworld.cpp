#include "techcraft/generation/overworld.hpp"
#include "techcraft/lighting.hpp"
#include "techcraft/game.hpp"
#include "techcraft/tiles/registry.hpp"
#include "techcraft/fluid/registry.hpp"
#include "utilities/binarysearch.hpp"

#include "techcraft/generation/biome/plains.hpp"
#include "techcraft/generation/biome/hills.hpp"
#include "techcraft/generation/biome/ocean.hpp"
#include "techcraft/dimension/chunk.hpp"

const double WorldScale = 100000.0;
const double BiomeBlendDist = 0.05;

namespace Techcraft::Generation {

OverworldGenerator::OverworldGenerator(int seed) {
    highFrequencyGen.SetSeed(seed);
    mediumFrequencyGen.SetSeed(seed ^ 0x71ccd75d);
    lowFrequencyGen.SetSeed(seed ^ 0x13bb4780);
    biomeGen.SetSeed(seed ^ 0x6c33896e);

    highFrequencyGen.SetOctaveCount(3);
    mediumFrequencyGen.SetOctaveCount(1);
    lowFrequencyGen.SetOctaveCount(5);

    highFrequencyGen.SetFrequency(1000);
    mediumFrequencyGen.SetFrequency(300);
    lowFrequencyGen.SetFrequency(20);
    biomeGen.SetFrequency(100);
    biomeGen.SetOctaveCount(3);

    // Register biomes
    addBiome(std::make_unique<PlainsBiome>());
    addBiome(std::make_unique<HillsBiome>());
    addBiome(std::make_unique<OceanBiome>());
}

void OverworldGenerator::populate(const ChunkPosition &coord, const std::shared_ptr<Chunk> &chunk) {
    for (auto x = 0; x < ChunkSize; ++x) {
        for (auto y = 0; y < ChunkSize; ++y) {
            auto realX = (coord.x << ChunkBits | x);
            auto realY = (coord.y << ChunkBits | y);

            auto scaleX = realX / WorldScale;
            auto scaleY = realY / WorldScale;

            // Determine the current biome
            auto biomeRaw = std::min(maxWeight, std::max(0.0, biomeGen.GetValue(scaleX, scaleY, 0) * halfWeight + halfWeight));
            auto biomeIndex = Utilities::binarySearchLowest(biomeBoundaries, biomeRaw);
            auto boundaryLeft = biomeBoundaries[biomeIndex];
            auto boundaryRight = (biomeIndex + 1 < biomeBoundaries.size() ? biomeBoundaries[biomeIndex+1] : std::numeric_limits<double>::max());

            auto &biome = biomes[biomeIndex];

            double terrainHeightFloat = sampleTerrainHeight(*biome, scaleX, scaleY);

            // const TileType *topType;
            const TileType *topType;
            auto boundaryBlur = BiomeBlendDist * biome->weight;

            if (biomeRaw < boundaryLeft + boundaryBlur && biomeIndex > 0) {
                // Blending with the left biome
                double blend = (biomeRaw - boundaryLeft) / (boundaryBlur * 2) + 0.5;

                auto &leftBiome = biomes[biomeIndex - 1];
                double otherHeight = sampleTerrainHeight(*leftBiome, scaleX, scaleY);
                terrainHeightFloat = otherHeight * (1-blend) + terrainHeightFloat * blend;
                topType = TileTypes::Grass;

            } else if (biomeRaw >= boundaryRight - boundaryBlur) {
                // Blending with the right biome
                double blend = (biomeRaw - (boundaryRight - boundaryBlur)) / (boundaryBlur * 2);

                auto &rightBiome = biomes[biomeIndex + 1];
                double otherHeight = sampleTerrainHeight(*rightBiome, scaleX, scaleY);
                terrainHeightFloat = terrainHeightFloat * (1-blend) + otherHeight * blend;

                topType = TileTypes::Dirt;
            } else {
                topType = TileTypes::Stone;
            }

            tcsize terrainHeight = std::floor(terrainHeightFloat);

            for (auto z = 0; z < ChunkSize; ++z) {
                auto realZ = coord.z << ChunkBits | z;
                if (realZ <= terrainHeight) {
                    chunk->setTile(x, y, z, {TileTypes::Stone});
                } else {
                    chunk->setTileLight(x, y, z, TileLighting{
                        {},
                        toLightBaked(31, TileDirection::Self)
                    });

                    if (realZ <= 0) {
                        chunk->setFluid(x, y, z, {FluidTypes::Water, FluidTypes::Water->getSpread()});
                    }
                }
            }

            biome->decorateColumn(chunk, coord, x, y, realX, realY, terrainHeight);
        }
    }
}

double OverworldGenerator::sampleTerrainHeight(const Biome &biome, double x, double y) const {
    double value = 0;
    if (biome.lowFreqFactor != 0) {
        value += biome.lowFreqFactor * lowFrequencyGen.GetValue(x, y, 0);
    }
    if (biome.medFreqFactor != 0) {
        value += biome.medFreqFactor * mediumFrequencyGen.GetValue(x, y, 0);
    }
    if (biome.highFreqFactor != 0) {
        value += biome.highFreqFactor * highFrequencyGen.GetValue(x, y, 0);
    }

    return (value + 1) / 2 * (biome.maximumElevation - biome.minimumElevation) + biome.minimumElevation;
}

}