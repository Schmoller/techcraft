#include "techcraft/console.hpp"
#include "techcraft/game.hpp"

#include <cwctype>

namespace Techcraft {

Console::Console(Commands::Executor &executor)
    : executor(executor)
{
    // The current command line
    commandHistory.push_back({});
}

bool Console::wasModifiedSince() {
    if (isModified) {
        isModified = false;
        return true;
    }

    return false;
}

void Console::setMaxHistoryLines(uint32_t maxLines) {
    maxHistoryLines = maxLines;
    while (commandHistory.size() > maxHistoryLines + 1) {
        commandHistory.pop_front();
    }
    isModified = true;
}

void Console::clearHistory() {
    commandHistory.clear();
    isModified = true;
}

void Console::setCursorPos(uint32_t pos) {
    if (pos > commandLine().size()) {
        cursorPos = commandLine().size();
    } else {
        cursorPos = pos;
    }
    isModified = true;
}

void Console::setCommandLine(const std::wstring &commandLine) {
    commandHistory[commandHistory.size()-1] = commandLine;
    isModified = true;
}

void Console::setSuggestions(const std::vector<std::wstring> &suggestions) {
    this->suggestions = suggestions;
    isModified = true;
}

void Console::setMaxScrollback(uint32_t maxScrollback) {
    this->maxScrollback = maxScrollback;
    while (scrollback.size() > maxScrollback) {
        scrollback.pop_front();
    }
}

void Console::type(wchar_t ch) {
    if (!std::iswprint(ch)) {
        return;
    }

    // if you try to modify a command line that was historical, it will make a copy as your current one
    if (isLookingAtHistory) {
        commandHistory[commandHistory.size()-1] = commandLine();
        currentHistoryLine = commandHistory.size()-1;
        isLookingAtHistory = false;
    }

    commandLine().insert(cursorPos, 1, ch);
    ++cursorPos;
    isModified = true;
}

void Console::backspace(bool word) {
    if (cursorPos > 0) {
        size_t delStart = cursorPos-1;

        if (word) {
            // Find word break
            bool isWhitespace = std::iswspace(commandLine()[cursorPos-1]);
            bool foundTarget = false;

            for (size_t i = cursorPos-1; i != 0; --i) {
                wchar_t ch = commandLine()[i];
                if (isWhitespace) {
                    // Find first non-whitespace
                    if (!std::iswspace(ch)) {
                        isWhitespace = false;
                    }
                } else {
                    // Find first whitespace
                    if (std::iswspace(ch)) {
                        delStart = i + 1;
                        foundTarget = true;
                        break;
                    }
                }
            }

            if (!foundTarget) {
                delStart = 0;
            }
        }

        // if you try to modify a command line that was historical, it will make a copy as your current one
        if (isLookingAtHistory) {
            commandHistory[commandHistory.size()-1] = commandLine();
            currentHistoryLine = commandHistory.size()-1;
            isLookingAtHistory = false;
        }

        commandLine().erase(delStart, cursorPos - delStart);
        cursorPos = delStart;
        isModified = true;
    }
}

void Console::del(bool word) {
    if (cursorPos < commandLine().size()) {
        size_t delEnd = cursorPos+1;

        if (word) {
            // Find word break
            bool isWhitespace = std::iswspace(commandLine()[cursorPos]);
            bool foundTarget = false;

            for (size_t i = cursorPos+1; i < commandLine().size(); ++i) {
                wchar_t ch = commandLine()[i];
                if (isWhitespace) {
                    // Find first non-whitespace
                    if (!std::iswspace(ch)) {
                        isWhitespace = false;
                    }
                } else {
                    // Find first whitespace
                    if (std::iswspace(ch)) {
                        delEnd = i + 1;
                        foundTarget = true;
                        break;
                    }
                }
            }

            if (!foundTarget) {
                delEnd = commandLine().size();
            }
        }

        // if you try to modify a command line that was historical, it will make a copy as your current one
        if (isLookingAtHistory) {
            commandHistory[commandHistory.size()-1] = commandLine();
            currentHistoryLine = commandHistory.size()-1;
            isLookingAtHistory = false;
        }

        commandLine().erase(cursorPos, delEnd - cursorPos);
        isModified = true;
    }
}

void Console::submit(Commands::CommandSender &sender) {
    // TODO: Submit command

    std::wstring commandLineToExecute = commandLine();

    if (commandLineToExecute.empty()) {
        return;
    }

    if (isLookingAtHistory) {
        // When executing on an item in history (without modifications).
        // Pull it to the end of the history. (replace incomplete line)
        commandHistory[commandHistory.size() - 1] = commandLine();
        commandHistory.erase(commandHistory.begin() + currentHistoryLine);
    }

    commandHistory.push_back({});
    while (commandHistory.size() > maxHistoryLines + 1) {
        commandHistory.pop_front();
    }

    // TODO: It should not be printed exactly like this. It should be made clear that its a command line
    print(L"> " + commandLineToExecute);

    // TODO: State if the command doesnt exist
    if (!executor.execute(sender, commandLineToExecute)) {
        print(L"Failed to execute command");
    }

    currentHistoryLine = commandHistory.size() - 1;
    cursorPos = 0;
    isModified = true;
}

void Console::cursorLeft() {
    if (cursorPos > 0) {
        --cursorPos;
    }
    isModified = true;
}

void Console::cursorRight() {
    if (cursorPos < commandLine().size()) {
        ++cursorPos;
    }
    isModified = true;
}

void Console::tabComplete() {
    // TODO: Tab compl5ete
    isModified = true;
}

void Console::historyPrevious() {
    if (currentHistoryLine > 0) {
        --currentHistoryLine;
        cursorPos = commandLine().size();

        isLookingAtHistory = true;
        isModified = true;
    }
}

void Console::historyNext() {
    if (currentHistoryLine < commandHistory.size() - 1) {
        ++currentHistoryLine;
        cursorPos = commandLine().size();

        isLookingAtHistory = (currentHistoryLine < commandHistory.size() - 1);
        isModified = true;
    }
}

void Console::print(const std::wstring &string) {
    scrollback.push_back(string);
    while (scrollback.size() > maxScrollback) {
        scrollback.pop_front();
    }
}


}