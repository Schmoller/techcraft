#include "techcraft/entities/common.hpp"
#include "techcraft/dimension/dimension.hpp"

namespace Techcraft {
// Entity
Entity::Entity()
    : position(), bounds(), dimension(nullptr), id(0xFFFFFFFF)
{}

const Position &Entity::getPosition() const {
    return position;
}

void Entity::setPosition(const Position &position) {
    this->position = position;
}

const BoundingBox &Entity::getBounds() const {
    return bounds;
}

void Entity::setBounds(const BoundingBox &bounds) {
    this->bounds = bounds;
}

void Entity::takeOwnership(Dimension *dimension, EntityID id) {
    this->id = id;
    this->dimension = dimension;
}

// Rotatable
Rotatable::Rotatable()
    : yaw(0), pitch(0)
{}

void Rotatable::setYaw(float yaw) {
    this->yaw = fmod(yaw, 360);
}

float Rotatable::getYaw() const {
    return yaw;
}

void Rotatable::setPitch(float pitch) {
    if (pitch > 89) {
        this->pitch = 89;
    } else if (pitch < -89) {
        this->pitch = -89;
    } else {
        this->pitch = pitch;
    }
}

float Rotatable::getPitch() const {
    return pitch;
}

glm::vec3 Rotatable::getForward() const {
    return glm::vec3(
        cosf(glm::radians(pitch)) * sinf(glm::radians(yaw)),
        cosf(glm::radians(pitch)) * cosf(glm::radians(yaw)),
        sinf(glm::radians(pitch))
    );
}

}