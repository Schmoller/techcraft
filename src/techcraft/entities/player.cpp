#include "techcraft/entities/player.hpp"
#include "techcraft/dimension/dimension.hpp"
#include "techcraft/default.hpp"
#include "techcraft/game.hpp"
#include "techcraft/common/raycast.hpp"
#include "techcraft/tiles/registry.hpp"

#include "techcraft/gui/player_inventory.hpp"

#include <iostream>
#include <glm/glm.hpp>

#define EYE_HEIGHT 1.6

namespace Techcraft {

// LocalPlayer
const LocalPlayer::LocalPlayerType LocalPlayer::Type;

LocalPlayer::LocalPlayer(BindingManager &bindingManager, Engine::InputManager &input)
    : input(input)
{
    setBounds(BoundingBox(0.7, 0.7, 1.8).offsetZ(0.9f));
    setCollidable(true);
    setStepUp(0.5);

    // Key bindings    
    bindMoveForward = bindingManager.getBinding(Default::KeyBindings::MOVE_FORWARD);
    bindMoveBackward = bindingManager.getBinding(Default::KeyBindings::MOVE_BACKWARD);
    bindStrafeLeft = bindingManager.getBinding(Default::KeyBindings::STRAFE_LEFT);
    bindStrafeRight = bindingManager.getBinding(Default::KeyBindings::STRAFE_RIGHT);

    bindCrouch = bindingManager.getBinding(Default::KeyBindings::CROUCH);
    bindJump = bindingManager.getBinding(Default::KeyBindings::JUMP);

    bindSwitchSlot0 = bindingManager.getBinding(Default::KeyBindings::SWITCH_SLOT_0);
    bindSwitchSlot1 = bindingManager.getBinding(Default::KeyBindings::SWITCH_SLOT_1);
    bindSwitchSlot2 = bindingManager.getBinding(Default::KeyBindings::SWITCH_SLOT_2);
    bindSwitchSlot3 = bindingManager.getBinding(Default::KeyBindings::SWITCH_SLOT_3);
    bindSwitchSlot4 = bindingManager.getBinding(Default::KeyBindings::SWITCH_SLOT_4);
    bindSwitchSlot5 = bindingManager.getBinding(Default::KeyBindings::SWITCH_SLOT_5);
    bindSwitchSlot6 = bindingManager.getBinding(Default::KeyBindings::SWITCH_SLOT_6);
    bindSwitchSlot7 = bindingManager.getBinding(Default::KeyBindings::SWITCH_SLOT_7);
    bindSwitchSlot8 = bindingManager.getBinding(Default::KeyBindings::SWITCH_SLOT_8);
    bindSwitchSlot9 = bindingManager.getBinding(Default::KeyBindings::SWITCH_SLOT_9);
    bindOpenBackpack = bindingManager.getBinding(Default::KeyBindings::OPEN_BACKPACK);

    bindBreakTile = bindingManager.getBinding(Default::KeyBindings::BREAK_TILE);
    bindPlaceTile = bindingManager.getBinding(Default::KeyBindings::PLACE_TILE);
}

const EntityType &LocalPlayer::getType() const {
    return LocalPlayer::Type;
}

void LocalPlayer::setPosition(const Position &position) {
    Entity::setPosition(position);
}

void LocalPlayer::setCrouching(bool crouch) {
    crouching = crouch;
    setPreventDropoff(crouch);
}

bool LocalPlayer::isCrouching() const {
    return crouching;
}

void LocalPlayer::setFlying(bool flying) {
    this->flying = flying;
    setGravityEnable(!flying);
}

Position LocalPlayer::getEyePosition() const {
    return getPosition() + Position{0.0, 0.0, EYE_HEIGHT};
}

void LocalPlayer::processInputs() {
    // Camera Rotation
    auto mouseDelta = input.getMouseDelta();

    setYaw(getYaw() + mouseDelta.x * confSensitiviy);
    setPitch(getPitch() - mouseDelta.y * confSensitiviy);

    // Movement

    glm::highp_dvec3 forwardPlane = {
        sin(glm::radians(getYaw())),
        cos(glm::radians(getYaw())),
        0
    };
    glm::highp_dvec3 rightPlane = {
        cos(glm::radians(getYaw())),
        -sin(glm::radians(getYaw())),
        0
    };

    // Get movement input
    glm::highp_dvec3 inputVector = {};
    bool allow3DInput = false;

    hasMovementInput = false;
    if (bindMoveForward->isPressed()) {
        inputVector = forwardPlane;
        hasMovementInput = true;
    } else if (bindMoveBackward->isPressed()) {
        inputVector = -forwardPlane;
        hasMovementInput = true;
    }

    if (bindStrafeRight->isPressed()) {
        inputVector += rightPlane;
        hasMovementInput = true;
    } else if (bindStrafeLeft->isPressed()) {
        inputVector += -rightPlane;
        hasMovementInput = true;
    }

    if (bindCrouch->isPressed()) {
        setCrouching(true);
    } else {
        setCrouching(false);
    }

    auto velocity = getVelocity();

    // Change gravity infuence to very slowly fall. Unless we are on the surface of the fluid
    bool atSurface = false;
    if (isInFluid()) {
        auto &eyeFluid = getDimension()->getFluid(getEyePosition());
        auto &chestFluid = getDimension()->getFluid(getPosition() + Position{0.0, 0.0, getBounds().height() * 0.6});
        if (eyeFluid.isEmpty()) {
            atSurface = true;
            // We only want to hover around the chest height
            if (chestFluid.isEmpty()) {
                setGravityInfuence(1);
            } else {
                // We are on the surface. Slight boyant force to push us to chest height
                setGravityInfuence(-0.1);
            }
        } else {
            setGravityInfuence(0.1);
        }
    } else {
        setGravityInfuence(1);
    }

    if (isFlying()) {
        allow3DInput = true;
        if (bindJump->isPressed()) {
            inputVector.z = 1;
            hasMovementInput = true;
        }
        if (bindCrouch->isPressed()) {
            inputVector.z = -1;
            hasMovementInput = true;
        }
    } else {
        if (isInFluid()) {
            if (isOnGround() && atSurface) {
                // Allow player to jump out
                if (bindJump->wasPressed()) {
                    velocity.z += 5;
                }
            } else {
                // Handle swimming
                if (bindJump->isPressed()) {
                    allow3DInput = true;
                    inputVector.z = 1;
                    hasMovementInput = true;
                }
                if (bindCrouch->isPressed()) {
                    allow3DInput = true;
                    inputVector.z = -1;
                    hasMovementInput = true;
                }
            }

        } else {
            // Handle jumping
            if (bindJump->wasPressed()) {
                if (isOnGround()) {
                    velocity.z += 9;
                }
            }
        }
    }

    // Calculate new velocity
    double speed;
    if (isFlying()) {
        speed = static_cast<double>(confFlySpeed);
    } else {
        if (isInFluid()) {
            if (isOnGround() && isCrouching()) {
                speed = static_cast<double>(confFluidCrouchSpeed);
            } else {
                speed = static_cast<double>(confSwimSpeed);
            }
        } else {
            if (isCrouching()) {
                speed = static_cast<double>(confCrouchSpeed);
            } else {
                speed = static_cast<double>(confSpeed);
            }
        }
    }

    const double airSpeed = 0.1;

    double currentSpeed = 0;

    if (glm::length(inputVector) > 0) {
        double amount = 0;
        if (isFlying()) {
            amount = 1;
            if (inputVector.x != 0 || inputVector.y != 0) {
                glm::highp_dvec3 flatVec(inputVector);
                flatVec.z = 0;

                auto inputZ = inputVector.z;

                inputVector = glm::normalize(flatVec) * speed;
                inputVector.z = inputZ * speed;
            } else {
                inputVector.z *= speed;
            }
        } else if (isInFluid()) {
            amount = 1;
            inputVector = glm::normalize(inputVector) * speed;
        } else {
            if (isOnGround()) {
                // Walking, crouching, running, etc
                amount = 0.5;
                inputVector = glm::normalize(inputVector) * speed;

            } else {
                // Falling, dont alter speed, only change direction
                amount = airSpeed / speed;
                inputVector = glm::normalize(inputVector) * speed;
            }

        }
        
        if (!allow3DInput) {
            inputVector.z = velocity.z;
        }
        velocity = velocity * (1-amount) + inputVector * amount;
    }

    setVelocity(velocity);

    // Inventory slots
    if (bindSwitchSlot1->wasPressed()) {
        inventory.setSelected(0);
    } else if (bindSwitchSlot2->wasPressed()) {
        inventory.setSelected(1);
    } else if (bindSwitchSlot3->wasPressed()) {
        inventory.setSelected(2);
    } else if (bindSwitchSlot4->wasPressed()) {
        inventory.setSelected(3);
    } else if (bindSwitchSlot5->wasPressed()) {
        inventory.setSelected(4);
    } else if (bindSwitchSlot6->wasPressed()) {
        inventory.setSelected(5);
    } else if (bindSwitchSlot7->wasPressed()) {
        inventory.setSelected(6);
    } else if (bindSwitchSlot8->wasPressed()) {
        inventory.setSelected(7);
    } else if (bindSwitchSlot9->wasPressed()) {
        inventory.setSelected(8);
    } else if (bindSwitchSlot0->wasPressed()) {
        inventory.setSelected(9);
    }

    // Block placement etc.
    if (bindBreakTile->wasPressed()) {
        // Left click breaks
        Raycast raycast(getEyePosition(), getForward(), confReach);
        
        auto result = raycast
            .ignoreFluid()
            .cast(*getDimension());

        if (result.result() == HitType::Subtile) {
            getDimension()->setSubtile(result.tilePosition(), result.slot(), {});
        } else if (result.result() == HitType::Tile) {
            getDimension()->setTile(result.tilePosition(), {
                TileTypes::Air
            });
        }

    } else if (bindPlaceTile->wasPressed()) {
        // Right click places
        auto selectedItem = inventory.getSelectedItem();

        if (selectedItem) {
            if (selectedItem->getType().doPlace(*selectedItem, *this, *this->getDimension())) {
                // TODO: This means the place was successful. Not sure what to do here yet.
            }
        }
    }

    if (bindOpenBackpack->wasPressed()) {
        auto currentGui = Game::getInstance().getCurrentGUI();
        
        if (currentGui) {
            auto currentInvGui = std::dynamic_pointer_cast<GuiPlayerInventory>(currentGui);
            if (currentInvGui) {
                // Close it
                Game::getInstance().setCurrentGUI({});
            }
        } else {
            auto invGui = std::make_shared<GuiPlayerInventory>(inventory);
            Game::getInstance().setCurrentGUI(invGui);
        }
    }
}

AgentType LocalPlayer::getTicketAgentId() {
    return 0;
}

ChunkLoadCause LocalPlayer::getTicketReason() {
    return ChunkLoadCause::PlayerNear;
}

PlayerInventory &LocalPlayer::getInventory() {
    return inventory;
}

RaycastResult LocalPlayer::getLookAtTile() const {
    Raycast raycast(getEyePosition(), getForward(), confReach);
    
    return raycast
        .ignoreFluid()
        .ignoreEntities()
        .cast(*getDimension());
}

glm::highp_dvec3 LocalPlayer::getDrag() const {
    float friction = 0.8;
    float vfriction = 0.99;

    if (!getDimension()->getChunkAt(getPosition())) {
        // Disallow movement while the chunk is not loaded
        friction = 0;
        vfriction = 0;
    } else {

        if (isFlying()) {
            if (hasMovementInput) {
                friction = 1;
                vfriction = 1;
            } else {
                friction = 0.6;
                vfriction = 0.6;
            }
        } else if (isInFluid()) {
            // vfriction = 0.8;
            if (isOnGround()) {
                // Wading
                friction = 0.65;
            } else {
                // Swimming
                friction = 0.9;
                vfriction = friction;
            }
        } else {
            if (hasMovementInput) {
                friction = 1;
            } else {
                if (isOnGround()) {
                    // Walking
                    friction = 0.8;
                } else {
                    // Falling
                    friction = 0.995;
                }
            }
        }
    }

    return {
        friction,
        friction,
        vfriction
    };
}

void LocalPlayer::printLine(const std::wstring_view &line) {
    auto &console = Game::getInstance().getUniverse()->getConsole();
    console.print(std::wstring(line));
}

}