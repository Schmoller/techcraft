#include "techcraft/fluid/base.hpp"
#include "techcraft/rendering/fluid/basicfluid.hpp"

namespace Techcraft {

// FluidType
void FluidType::setTextureAll(const TileTexture *texture) {
    for (size_t i = 0; i < 6; ++i) {
        faceTextures[i] = texture;
    }
}

void FluidType::setTexture(const TileTexture *texture, TileDirection direction) {
    faceTextures[static_cast<int>(direction)] = texture;
}

void FluidType::setLightEmission(TileLightPallet pallet, uint8_t intensity) {
    tileLight[static_cast<int>(pallet)] = intensity;
    if (intensity > 0) {
        emitsLight = true;
    }
}

bool FluidType::operator==(const FluidType &other) const {
    return slotId == other.slotId;
}

bool FluidType::operator!=(const FluidType &other) const {
    return slotId != other.slotId;
}

Rendering::FluidRenderer &FluidType::getRenderer() const {
    return Rendering::BasicFluidRenderer::instance();
}

// FluidTile
std::optional<BoundingBox> FluidTile::getBounds() const {
    if (type) {
        float height = static_cast<float>(amount) / static_cast<float>(type->getSpread());

        return BoundingBox{1, 1, height, false};
    } else {
        return {};
    }
}

}