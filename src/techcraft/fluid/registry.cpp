#include "techcraft/fluid/registry.hpp"

namespace Techcraft {

std::unique_ptr<FluidRegistry> FluidTypes::fluidRegistry;

// Common liquid
const FluidType *FluidTypes::Water;
const FluidType *FluidTypes::Lava;

void FluidTypes::initialize(ResourceManager &manager) {
    FluidRegistryBuilder builder(manager);
    
    FluidTypeDefinition water("water");
    water.setTextureAll({"fluid.water"});
    water.setSpread(10);
    water.setViscosity(3);
    Water = builder.add(water);
    
    FluidTypeDefinition lava("lava");
    lava.setTextureAll({"fluid.lava"});
    lava.setSpread(3);
    lava.setViscosity(10);
    Lava = builder.add(lava);
    
    fluidRegistry = builder.build();
}


// FluidTypeDefinition
void FluidTypeDefinition::setTextureAll(const Resource &resource) {
    for (size_t i = 0; i < 6; ++i) {
        faceTextures[i] = resource;
    }
}

void FluidTypeDefinition::setTexture(const Resource &resource, TileDirection direction) {
    faceTextures[static_cast<int>(direction)] = resource;
}

void FluidTypeDefinition::setLightEmission(TileLightPallet pallet, uint8_t intensity) {
    tileLight[static_cast<int>(pallet)] = intensity;
}

// BasicFluidType
BasicFluidType::BasicFluidType(
    const std::string &id,
    uint32_t spread,
    uint32_t viscosity,
    const TileTexture *faceTextures[6],
    const uint8_t tileLight[TILE_LIGHT_PALLET_COUNT]
) : FluidType(id)
{
    setSpread(spread);
    setViscosity(viscosity);

    for (auto pallet : AllPallets) {
        setLightEmission(pallet, tileLight[static_cast<int>(pallet)]);
    }

    for (auto dir : AllDirections) {
        setTexture(faceTextures[static_cast<int>(dir)], dir);
    }
}

void BasicFluidType::initialize(ResourceManager &manager) {
    // No-op
}

// FluidRegistryBuilder
FluidRegistryBuilder::FluidRegistryBuilder(ResourceManager &manager)
    : manager(manager) 
{
}

const FluidType *FluidRegistryBuilder::add(const FluidTypeDefinition &definition) {
    const TileTexture *textures[6];

    for (size_t i = 0; i < 6; ++i) {
        textures[i] = manager.asTileTexture(definition.faceTextures[i]);
    }

    auto basicType = std::make_shared<BasicFluidType>(
        definition.name,
        definition.spread,
        definition.viscosity,
        textures,
        definition.tileLight
    );

    return add(basicType);
}

std::unique_ptr<FluidRegistry> FluidRegistryBuilder::build() {
    return std::unique_ptr<FluidRegistry>(new FluidRegistry(
        byName,
        slots
    ));
}

void FluidRegistryBuilder::initializeItem(FluidType *item, size_t slotId) {
    item->initialize(manager);
    item->slotId = slotId;

    // Ensure no unset textures
    for (int i = 0; i < 6; ++i) {
        if (!item->faceTextures[i]) {
            item->faceTextures[i] = manager.asTileTexture({});
        }
    }
}

// FluidRegistry
FluidRegistry::FluidRegistry(
    std::unordered_map<std::string, std::shared_ptr<FluidType>> byName,
    std::vector<std::shared_ptr<FluidType>> slots
) : Registry(byName, slots)
{}

}