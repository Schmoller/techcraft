#include "techcraft/commands/fly.hpp"
#include "techcraft/entities/player.hpp"

namespace Techcraft::Commands {

bool FlyCommand::execute(CommandSender &sender, const std::vector<std::wstring> &args) {
    auto player = dynamic_cast<LocalPlayer*>(&sender);

    if (!player) {
        sender.printLine(L"Only players may use this command");
        return false;
    }

    player->setFlying(!player->isFlying());

    if (player->isFlying()) {
        sender.printLine(L"Flight is now enabled");
    } else {
        sender.printLine(L"Flight is now disabled");
    }
    
    return true;
}

std::vector<std::wstring> FlyCommand::tabComplete(CommandSender &sender, const std::vector<std::wstring> &args, size_t index) {
    return {};
}

}