#include "techcraft/commands/executor.hpp"

namespace Techcraft::Commands {

std::vector<std::wstring> parseCommandLine(const std::wstring &commandLine, std::wstring &outCommandName);

void Executor::addCommand(const std::shared_ptr<Command> &command) {
    std::wstring lowerName = command->getName();
    std::transform(lowerName.begin(), lowerName.end(), lowerName.begin(), [](wchar_t ch) {
        return std::tolower(ch);
    });

    commandMap[lowerName] = command;

    for (auto &alias : command->getAliases()) {
        std::wstring lowerAlias = alias;
        std::transform(lowerAlias.begin(), lowerAlias.end(), lowerAlias.begin(), [](wchar_t ch) {
            return std::tolower(ch);
        });

        aliasMap[lowerAlias] = command;
    }
}

bool Executor::execute(CommandSender &sender, const std::wstring &commandLine) {
    std::wstring command;

    std::vector<std::wstring> args = parseCommandLine(commandLine, command);

    return execute(sender, command, args);
}

bool Executor::execute(CommandSender &sender, const std::wstring &command, const std::vector<std::wstring> &args) {
    auto it = commandMap.find(command);
    if (it == commandMap.end()) {
        it = aliasMap.find(command);
        if (it == aliasMap.end()) {
            return false;
        }
    }

    return it->second->execute(sender, args);
}

std::vector<std::wstring> Executor::autoComplete(CommandSender &sender, const std::wstring &commandLine, size_t cursor) {
    std::wstring command;

    std::vector<std::wstring> args = parseCommandLine(commandLine, command);

    if (args.empty()) {
        // Tab complete available commands.
    } else {
        // Tab complete command arguments
    }

    // TODO: Lots to do here
    return {};
}

std::vector<std::wstring> Executor::autoComplete(CommandSender &sender, const std::wstring &command, const std::vector<std::wstring> &commandLine, size_t arg) {
    // TODO: Lots to do here
    return {};
}

std::vector<std::wstring> parseCommandLine(const std::wstring &commandLine, std::wstring &outCommandName) {
    enum class State {
        Name,
        Arg,
        SingleString,
        DoubleString
    };

    State current = State::Name;

    std::vector<std::wstring> args;
    std::wstring temp;

    for (size_t i = 0; i < commandLine.size(); ++i) {
        wchar_t ch = commandLine[i];
        
        switch (current) {
            case State::Name: {
                if (std::isspace(ch)) {
                    outCommandName = temp;
                    temp.clear();
                    current = State::Arg;
                } else {
                    temp.push_back(std::tolower(ch));
                }
                break;
            }
            case State::Arg: {
                if (ch == L'"') {
                    current = State::DoubleString;
                } else if (ch == L'\'') {
                    current = State::SingleString;
                } else if (std::isspace(ch)) {
                    args.push_back(temp);
                    temp.clear();
                } else {
                    temp.push_back(ch);
                }
                break;
            }
            case State::DoubleString: {
                if (ch == L'"') {
                    current = State::Arg;
                    args.push_back(temp);
                    temp.clear();
                } else {
                    temp.push_back(ch);
                }
                break;
            }
            case State::SingleString: {
                if (ch == L'\'') {
                    current = State::Arg;
                    args.push_back(temp);
                    temp.clear();
                } else {
                    temp.push_back(ch);
                }
                break;
            }
        }
    }

    if (temp.size() > 0) {
        if (current == State::Name) {
            outCommandName = temp;
        } else {
            args.push_back(temp);
        }
    }

    return args;
}

}