#include "techcraft/commands/test.hpp"

namespace Techcraft::Commands {

bool TestCommand::execute(CommandSender &sender, const std::vector<std::wstring> &args) {
    sender.printLine(L"Hello from test command");
    return true;
}

std::vector<std::wstring> TestCommand::tabComplete(CommandSender &sender, const std::vector<std::wstring> &args, size_t index) {
    return {};
}

}