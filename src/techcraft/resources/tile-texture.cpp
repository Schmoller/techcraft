#include "techcraft/resources/tile-texture.hpp"
#include "utilities/random.hpp"

namespace Techcraft {

TileTexture::TileTexture(const Engine::Texture *initialTexture, const TextureInfo &info) {
    standardTextures.push_back({initialTexture, info});
}

const Engine::Texture *TileTexture::asTexture(const TilePosition &pos, TextureInfo *info) const {
    return selectTexture(standardTextures, pos, info);
}

const Engine::Texture *TileTexture::asTexture(const TilePosition &pos, Connectivity connectivity, TextureInfo *info) const {
    const Engine::Texture *texture;
    
    if (!connectivity.horizontal) {
        if (!connectivity.vertical) {
            // End 
            texture = selectTexture(standardTextures, pos, info);
        } else {
            // Vertical
            texture = selectTexture(verticalTextures, pos, info);
        }
    } else if (!connectivity.vertical) {
        // Horizontal
        texture = selectTexture(horizontalTextures, pos, info);
    } else if (!connectivity.diagonal) {
        // Cross
        texture = selectTexture(crossTextures, pos, info);
    } else {
        // Middle
        texture = selectTexture(middleTextures, pos, info);
    }

    if (!connectivity.fullVertical) {
        // Adjust coordinates for subface
        double midV = (info->v1 + info->v2) / 2;

        switch (connectivity.subFace) {
            case SubFace::TopLeft:
                info->v2 = midV;
                break;
            case SubFace::TopRight:
                info->v2 = midV;
                break;
            case SubFace::BottomLeft:
                info->v1 = midV;
                break;
            case SubFace::BottomRight:
                info->v1 = midV;
                break;
        }
    }

    if (!connectivity.fullHorizontal) {
        // Adjust coordinates for subface
        double midU = (info->u1 + info->u2) / 2;

        switch (connectivity.subFace) {
            case SubFace::TopLeft:
                info->u2 = midU;
                break;
            case SubFace::TopRight:
                info->u1 = midU;
                break;
            case SubFace::BottomLeft:
                info->u2 = midU;
                break;
            case SubFace::BottomRight:
                info->u1 = midU;
                break;
        }
    }

    return texture;
}

const Engine::Texture *TileTexture::selectTexture(const TextureVector &textures, const TilePosition &pos, TextureInfo *info) const {
    static Utilities::LehmerRandom rand;

    // TODO: Need logic to select which texture in the vector to use

    *info = textures[0].second;

    if (flip != TextureFlipMode::None) {
        rand.seed(std::hash<TilePosition>()(pos));
        auto testVal = rand.random();

        switch (flip) {
            case TextureFlipMode::Horizontal:
                if (testVal & 0b1) {
                    std::swap(info->u1, info->u2);
                }
                break;
            case TextureFlipMode::Vertical:
                if (testVal & 0b1) {
                    std::swap(info->v1, info->v2);
                }
                break;
            case TextureFlipMode::Both:
                if (testVal & 0b01) {
                    std::swap(info->u1, info->u2);
                }
                if (testVal & 0b10) {
                    std::swap(info->v1, info->v2);
                }
                break;
        }
    }
    return standardTextures[0].first;
}

bool TileTexture::isConnectedTexture() const {
    return usingConnected;
}

void TileTexture::updateTexture(const Engine::Texture *texture, const TextureInfo &info) {
    standardTextures.clear();
    standardTextures.push_back({texture, info});
    usingConnected = false;
}

void TileTexture::updateTextures(const std::vector<std::pair<const Engine::Texture *, TextureInfo>> &textures) {
    standardTextures = textures;
    usingConnected = false;
}

void TileTexture::setFlipMode(TextureFlipMode mode) {
    this->flip = mode;
}

void TileTexture::updateConnectedTextures(
    const TextureVector &end,
    const TextureVector &middle,
    const TextureVector &vertical,
    const TextureVector &horizontal,
    const TextureVector &cross
) {
    standardTextures = end;
    middleTextures = middle;
    verticalTextures = vertical;
    horizontalTextures = horizontal;
    crossTextures = cross;

    usingConnected = true;
}

}