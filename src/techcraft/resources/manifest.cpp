#include "techcraft/resources/manifest.hpp"

namespace Techcraft {

NLOHMANN_JSON_SERIALIZE_ENUM( TileTextureManifestDefinition::Mode, {
    {TileTextureManifestDefinition::Mode::Standard, "standard"},
    {TileTextureManifestDefinition::Mode::Connected, "connected"},
})

NLOHMANN_JSON_SERIALIZE_ENUM( TextureFlipMode, {
    {TextureFlipMode::None, "none"},
    {TextureFlipMode::Vertical, "vertical"},
    {TextureFlipMode::Horizontal, "horizontal"},
    {TextureFlipMode::Both, "both"},
})

NLOHMANN_JSON_SERIALIZE_ENUM( Engine::FontStyle, {
    {Engine::FontStyle::Regular, "regular"},
    {Engine::FontStyle::Bold, "bold"},
    {Engine::FontStyle::Italic, "italic"},
    {Engine::FontStyle::BoldItalic, "bold-italic"},
})

// Manifest
Manifest::Manifest(std::istream &input, const std::filesystem::path &path)
    : filePath(path)
{
    load(input);
}

Manifest::SharedDefinition Manifest::get(const std::string &name) const {
    return definitions.at(name);
}

Manifest::SharedDefinition Manifest::get() const {
    return rootDefinition;
}

std::vector<std::string> Manifest::ids() const {
    std::vector<std::string> keys;
    keys.reserve(definitions.size());
    for (auto &pair : definitions) {
        keys.push_back(pair.first);
    }
    return keys;
}

Manifest::SharedDefinition parseDefinition(const nlohmann::json &raw) {
    auto type = raw["type"].get<std::string>();

    if (type == "tile-texture") {
        return std::make_shared<TileTextureManifestDefinition>(raw);
    } else if (type == "texture") {
        return std::make_shared<TextureManifestDefinition>(raw);
    } else if (type == "model") {
        return std::make_shared<ModelManifestDefinition>(raw);
    } else if (type == "font") {
        return std::make_shared<FontManifestDefinition>(raw);
    } else {
        throw InvalidManifestError("Invalid asset type");
    }
}

std::pair<Manifest::SharedDefinition, std::string> parseDefinitionWithId(const nlohmann::json &raw) {
    auto id = raw["id"].get<std::string>();
    auto definition = parseDefinition(raw);

    return {definition, id};
}

void Manifest::load(std::istream &input) {
    nlohmann::json rawManifest;

    try {
        input >> rawManifest;

        if (rawManifest.is_array()) {
            // Manifest of manifests
            for (auto &rawDefinition : rawManifest) {
                SharedDefinition definition;
                std::string name;

                std::tie(definition, name) = parseDefinitionWithId(rawDefinition);

                definitions[name] = definition;
            }
        } else {
            // Single root manifest
            rootDefinition = parseDefinition(rawManifest);
        }
    } catch (nlohmann::json::parse_error &error) {
        throw InvalidManifestError(std::string(error.what()));
    } catch (nlohmann::json::type_error &error) {
        throw InvalidManifestError(std::string(error.what()));
    } catch (nlohmann::json::other_error &error) {
        throw InvalidManifestError(std::string(error.what()));
    }
}

// TileTextureManifestDefinition
TileTextureManifestDefinition::TileTextureManifestDefinition(const nlohmann::json &raw) {
    if (raw.contains("mode")) {
        mode = raw["mode"].get<Mode>();
    } else {
        mode = Mode::Standard;
    }
    
    if (raw.contains("random-flip")) {
        flip = raw["random-flip"].get<TextureFlipMode>();
    } else {
        flip = TextureFlipMode::None;
    }

    if (raw.contains("width")) {
        width = raw["width"].get<uint32_t>();
    }
    if (raw.contains("height")) {
        height = raw["height"].get<uint32_t>();
    }

    if (mode == Mode::Standard) {
        auto imageSection = raw["image"];
        parseImages(imageSection, images);
    } else if (mode == Mode::Connected) {
        auto connectedSection = raw["connected"];
        auto endSection = connectedSection["end"];
        parseImages(endSection["image"], connectedTextures.end);
        auto middleSection = connectedSection["middle"];
        parseImages(middleSection["image"], connectedTextures.middle);
        auto verticalSection = connectedSection["vertical"];
        parseImages(verticalSection["image"], connectedTextures.vertical);
        auto horizontalSection = connectedSection["horizontal"];
        parseImages(horizontalSection["image"], connectedTextures.horizontal);
        auto crossSection = connectedSection["cross"];
        parseImages(crossSection["image"], connectedTextures.cross);
    }
}

TileTextureManifestDefinition::Image TileTextureManifestDefinition::parseImage(const nlohmann::json &raw) const {
    if (raw.is_string()) {
        return {
            raw.get<std::string>(),
            0
        };
    }

    auto file = raw["file"].get<std::string>();
    uint32_t index = 0;
    if (raw.contains("index")) {
        index = raw["index"].get<uint32_t>();
    }

    return {
        file,
        index
    };
}

void TileTextureManifestDefinition::parseImages(const nlohmann::json &raw, std::vector<Image> &images) const {
    if (raw.is_array()) {
        // Multiple images
        images.reserve(raw.size());
        for (auto &imageRaw : raw) {
            images.push_back(parseImage(imageRaw));
        }
    } else {
        // Single image
        images.push_back(parseImage(raw));
    }
}


TextureManifestDefinition::TextureManifestDefinition(const nlohmann::json &raw) {
    file = raw["image"].get<std::string>();
}

ModelManifestDefinition::ModelManifestDefinition(const nlohmann::json &raw) {
    file = raw["file"].get<std::string>();
    texture = raw["texture"].get<std::string>();

    if (raw.contains("object")) {
        object = raw["object"].get<std::string>();
    } else {
        object = "";
    }
}

FontManifestDefinition::FontManifestDefinition(const nlohmann::json &raw) {
    auto styleSection = raw["styles"];
    styles.reserve(styleSection.size());

    for (auto &styleRaw : styleSection) {
        styles.push_back(parseStyle(styleRaw));
    }

    if (raw.contains("size")) {
        sizes.push_back(raw["size"].get<uint32_t>());
    } else {
        auto sizesSection = raw["sizes"];
        sizes.resize(sizesSection.size());
        for (auto &sizeRaw : sizesSection) {
            sizes.push_back(sizeRaw.get<uint32_t>());
        }
    }
}

FontManifestDefinition::Style FontManifestDefinition::parseStyle(const nlohmann::json &raw) const {
    auto file = raw["file"].get<std::string>();
    auto style = raw["style"].get<Engine::FontStyle>();

    return {
        file,
        style
    };
}

}