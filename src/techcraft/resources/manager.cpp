#include "techcraft/resources/manager.hpp"

#include <fstream>
#include <iostream>
#include <filesystem>

namespace Techcraft {

ResourceManager::ResourceManager(Engine::TextureManager &textureManager)
    : textureManager(textureManager)
{}

const TileTexture *ResourceManager::asTileTexture(const Resource &resource) {
    auto it = tileTextureResources.find(resource);
    if (it != tileTextureResources.end()) {
        return it->second.get();
    }

    auto loadingTexture = textureManager.getTexture("internal.loading");

    // New texture resource
    auto texture = std::make_unique<TileTexture>(loadingTexture, TextureInfo{0, 0, 1, 1});

    auto texturePtr = texture.get();

    tileTextureResources[resource] = std::move(texture);

    return texturePtr;
}

void ResourceManager::finaliseResources() {
    loadAllTextures();
}

void ResourceManager::loadAllTextures() {
    std::unordered_map<std::string, std::shared_ptr<Manifest>> manifestsByPath;
    std::unordered_map<std::string, const Engine::Texture *> texturesByFile;

    auto errorTexture = textureManager.getTexture("internal.error");

    for (auto &pair : tileTextureResources) {
        if (!pair.first.isValid()) {
            pair.second->updateTexture(errorTexture, {0, 0, 1, 1});
            continue;
        }

        // Resolve the manifest definition
        auto it = manifestsByPath.find(pair.first.path);
        std::shared_ptr<Manifest> manifest;
        try {
            if (it == manifestsByPath.end()) {
                manifest = loadManifest("textures", pair.first.path);
                manifestsByPath[pair.first.path] = manifest;
            } else {
                manifest = manifestsByPath[pair.first.path];
            }
        } catch (InvalidManifestError &error) {
            std::cerr << "Failed to load manifest for " << pair.first << ": " << error.what() << std::endl;
            pair.second->updateTexture(errorTexture, {0, 0, 1, 1});
            continue;
        }

        Manifest::SharedDefinition definition;
        if (pair.first.subResource.empty()) {
            definition = manifest->get();
        } else {
            definition = manifest->get(pair.first.subResource);
        }

        if (!definition) {
            std::cerr << "Failed to find resource definition " << pair.first << std::endl;
            std::cerr << " Manifest does not contain definition " << pair.first.subResource << std::endl;

            pair.second->updateTexture(errorTexture, {0, 0, 1, 1});
            continue;
        }

        if (definition->type() != ManifestDefinition::AssetType::TileTexture) {
            std::cerr << "Invalid asset type for " << pair.first << ". requires as tile-texture" << std::endl;

            pair.second->updateTexture(errorTexture, {0, 0, 1, 1});
            continue;
        }

        // Resolve the texture information
        auto textureDefinition = std::static_pointer_cast<TileTextureManifestDefinition>(definition);

        pair.second->setFlipMode(textureDefinition->flip);

        switch (textureDefinition->mode) {
            case TileTextureManifestDefinition::Mode::Standard: {
                // Basic texture, nothing special
                TileTexture::TextureVector textures;
                getAllTextures(
                    textureDefinition->images,
                    manifest->path().parent_path(),
                    *textureDefinition,
                    pair.first,
                    textures
                );
                
                pair.second->updateTextures(textures);
                break;
            }
            case TileTextureManifestDefinition::Mode::Connected: {
                TileTexture::TextureVector end;
                TileTexture::TextureVector middle;
                TileTexture::TextureVector vertical;
                TileTexture::TextureVector horizontal;
                TileTexture::TextureVector cross;

                getAllTextures(
                    textureDefinition->connectedTextures.end,
                    manifest->path().parent_path(),
                    *textureDefinition,
                    pair.first,
                    end
                );

                getAllTextures(
                    textureDefinition->connectedTextures.middle,
                    manifest->path().parent_path(),
                    *textureDefinition,
                    pair.first,
                    middle
                );

                getAllTextures(
                    textureDefinition->connectedTextures.vertical,
                    manifest->path().parent_path(),
                    *textureDefinition,
                    pair.first,
                    vertical
                );

                getAllTextures(
                    textureDefinition->connectedTextures.horizontal,
                    manifest->path().parent_path(),
                    *textureDefinition,
                    pair.first,
                    horizontal
                );

                getAllTextures(
                    textureDefinition->connectedTextures.cross,
                    manifest->path().parent_path(),
                    *textureDefinition,
                    pair.first,
                    cross
                );

                pair.second->updateConnectedTextures(
                    end,
                    middle,
                    vertical,
                    horizontal,
                    cross
                );
                break;
            }
            default: {
                std::cerr << "Unknown texture mode for resource " << pair.first << "." << std::endl;

                pair.second->updateTexture(errorTexture, {0, 0, 1, 1});
                continue;
            }
        }
    }
}

const Engine::Texture *ResourceManager::getTextureByFilename(const std::string &name) {
    auto it = texturesByFile.find(name);
    if (it == texturesByFile.end()) {
        try {
            auto texture = textureManager.createTexture(name)
                .fromFile(name)
                .build();
            texturesByFile[name] = texture;

            return texture;
        } catch (Engine::TextureLoadError &error) {
            return nullptr;
        }
    } else {
        return texturesByFile[name];
    }
}

std::shared_ptr<Manifest> ResourceManager::loadManifest(const std::string_view &base, const std::string_view &path) {
    // Work out the manifest file
    std::string manifestFilename(path);
    std::replace(manifestFilename.begin(), manifestFilename.end(), '.', '/');
    manifestFilename.append(".meta");

    std::filesystem::path manifestPath("assets");
    manifestPath.append(base);
    manifestPath.append(manifestFilename);

    std::ifstream inputFile(manifestPath);

    if (!inputFile.is_open()) {
        return {};
    }

    return std::make_shared<Manifest>(inputFile, manifestPath);
}

void ResourceManager::getAllTextures(
    const std::vector<TileTextureManifestDefinition::Image> &images,
    const std::filesystem::path &basePath,
    const TileTextureManifestDefinition &definition,
    const Resource &resource,
    TileTexture::TextureVector &textures
) {
    textures.reserve(images.size());

    for (auto &image : images) {
        // Try by full path first
        const Engine::Texture *texture = getTextureByFilename(image.file);

        if (!texture) {
            // Try by relative path
            auto relativePath = basePath;
            relativePath.append(image.file);

            texture = getTextureByFilename(relativePath.string());
        }

        if (texture) {
            // Compute the coordinates
            TextureInfo coords;
            if (definition.width && definition.height) {
                if (texture->width != definition.width || texture->height != definition.height) {
                    int cols = std::ceil(texture->width / static_cast<double>(definition.width));
                    
                    int x = image.index % cols;
                    int y = image.index / cols;

                    coords = {
                        (x * definition.width) / static_cast<double>(texture->width),
                        (y * definition.height) / static_cast<double>(texture->height),
                        ((x + 1) * definition.width) / static_cast<double>(texture->width),
                        ((y + 1) * definition.height) / static_cast<double>(texture->height),
                    };
                }
            } else {
                coords = {0, 0, 1, 1};
                if (image.index > 0) {
                    std::cerr << "Warn: Texture resource " << resource << " requested image index " << image.index;
                    std::cerr << " on file " << image.file << " but did not have width and height set" << std::endl;
                }
            }

            textures.push_back({texture, coords});
        } else {
            std::cerr << "Failed to load texture for resource " << resource << ": " << image.file << std::endl;
            auto errorTexture = textureManager.getTexture("internal.error");
            textures.push_back({errorTexture, {0, 0, 1, 1}});
        }
    }
}

}