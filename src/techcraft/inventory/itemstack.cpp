#include "techcraft/inventory/itemstack.hpp"

namespace Techcraft {

// ItemStack
ItemStack::ItemStack(const ItemType &type, uint16_t quantity)
    : type(&type), quantity(quantity)
{}

ItemStack::ItemStack(const ItemStack &other)
    : type(other.type), quantity(other.quantity)
{}

bool ItemStack::canStackWith(const ItemStack &other) const {
    return type == other.type;
}

uint16_t ItemStack::getMaximumStackSize() const {
    return 99_u16;
}

void ItemStack::setQuantity(uint16_t quantity) {
    this->quantity = quantity;
}

std::optional<ItemStack> ItemStack::splitAfter(uint16_t amount) {
    if (amount > quantity) {
        uint16_t remain = quantity - amount;
        quantity = amount;

        return copy(remain);
    } else {
        return {};
    }
}

ItemStack ItemStack::splitOff(uint16_t amount) {
    if (amount >= quantity) {
        uint16_t oldQuantity = quantity;
        quantity = 0;
        return copy(oldQuantity);
    } else {
        uint16_t remain = quantity - amount;
        quantity = remain;
        return copy(amount);
    }
}

bool ItemStack::merge(ItemStack &other) {
    if (!canStackWith(other)) {
        return false;
    }

    uint16_t amountToMerge = std::min(other.quantity, static_cast<uint16_t>(getMaximumStackSize() - quantity));
    if (amountToMerge > 0) {
        quantity += amountToMerge;
        other.quantity -= amountToMerge;

        return true;
    }

    return false;
}

uint16_t ItemStack::combine(const ItemStack &other) {
    if (!canStackWith(other)) {
        return false;
    }

    uint16_t amountToMerge = std::min(other.quantity, static_cast<uint16_t>(getMaximumStackSize() - quantity));
    if (amountToMerge > 0) {
        quantity += amountToMerge;
        return other.quantity - amountToMerge;
    }

    return false;
}

ItemStack ItemStack::copy(uint16_t quantity) {
    return ItemStack(*type, quantity);
}

bool ItemStack::isSimilar(const ItemStack &other) const {
    return other.type == type;
}

bool ItemStack::operator==(const ItemStack &other) const {
    return (
        other.type == type &&
        other.quantity == quantity
    );
}

}