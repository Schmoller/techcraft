#include "techcraft/inventory/player.hpp"

namespace Techcraft {

// PlayerInventory
PlayerInventory::PlayerInventory()
    : Inventory(40), selectedSlot(0)
{}

uint8_t PlayerInventory::getSelected() const {
    return selectedSlot;
}

void PlayerInventory::setSelected(uint8_t slot) {
    selectedSlot = slot % 10;
}

OptionalItemStack PlayerInventory::getSelectedItem() const {
    return (*this)[selectedSlot];
}

}