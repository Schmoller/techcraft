#include "techcraft/inventory/base.hpp"

namespace Techcraft {


// Inventory
Inventory::Inventory(uint16_t slotCount)
    : slots(slotCount, OptionalItemStack{})
{}

uint16_t Inventory::getSize() const {
    return static_cast<uint16_t>(slots.size());
}

OptionalItemStack Inventory::insert(const ItemStack &stack, bool simulate) {
    ItemStack temp = stack;

    // Try to merge first
    for (size_t slot = 0; slot < slots.size(); ++slot) {
        auto stack = slots[slot];
        
        if (stack) {
            if (temp.canStackWith(*stack)) {
                stack->merge(temp);
                if (temp.getQuantity() <= 0) {
                    return {};
                }

                if (!simulate) {
                    slots.assign(slot, stack);
                }
            }
        }
    }

    // Check for empty slots next
    for (size_t slot = 0; slot < slots.size(); ++slot) {
        auto stack = slots[slot];
        
        if (!stack) {
            auto newStack = temp.splitOff(temp.getMaximumStackSize());

            if (!simulate) {
                slots.assign(slot, newStack);
            }

            if (temp.getQuantity() <= 0) {
                return {};
            }
        }
    }

    // Failed to insert all
    return temp;
}

OptionalItemStack Inventory::operator[](uint16_t slot) const {
    return slots[slot];
}

OptionalItemStack &Inventory::operator[](uint16_t slot) {
    return slots[slot];
}

}