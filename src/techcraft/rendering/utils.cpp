#include "techcraft/rendering/utils.hpp"
#include "techcraft/lighting.hpp"

namespace Techcraft {

#define OCCLUSION_VALUE 0.8f

glm::vec3 mergeTile(const TileLighting &self, const TileLighting &tl1, const Tile &t1, const TileLighting &tl2, const Tile &t2, const TileLighting &tl3, const Tile &t3) {
    glm::vec3 output = blendTileLighting(self.tileIntensity);
    int count = 1;
    if (t1.type->isTransparent() || t1.type->hasLight()) {
        output += blendTileLighting(tl1.tileIntensity);
        ++count;
    }
    if (t2.type->isTransparent() || t2.type->hasLight()) {
        output += blendTileLighting(tl2.tileIntensity);
        ++count;
    }
    if (t3.type->isTransparent() || t3.type->hasLight()) {
        output += blendTileLighting(tl3.tileIntensity);
        ++count;
    }

    return output / static_cast<float>(count);
}

glm::vec3 mergeSky(const TileLighting &self, const TileLighting &tl1, const Tile &t1, const TileLighting &tl2, const Tile &t2, const TileLighting &tl3, const Tile &t3) {
    glm::vec3 output = blendSkyLighting(self.skyIntensity);
    int count = 1;
    if (t1.type->isTransparent()) {
        output += blendSkyLighting(tl1.skyIntensity);
        ++count;
    }
    if (t2.type->isTransparent()) {
        output += blendSkyLighting(tl2.skyIntensity);
        ++count;
    }
    if (t3.type->isTransparent()) {
        output += blendSkyLighting(tl3.skyIntensity);
        ++count;
    }

    return output / static_cast<float>(count);
}

glm::vec3 occlude(const Tile &t1, const Tile &t2, const Tile &t3, const glm::vec3 &tint) {
    if (!t1.type->isTransparent() || !t2.type->isTransparent() || !t3.type->isTransparent()) {
        return tint * OCCLUSION_VALUE;
    } else {
        return tint;
    }
}

}