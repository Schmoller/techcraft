#include "techcraft/rendering/tile/slab.hpp"
#include "techcraft/rendering/utils.hpp"
#include "techcraft/lighting.hpp"

#include "techcraft/tileentities/slab.hpp"

namespace Techcraft::Rendering {

// SlabTileRenderer
SlabTileRenderer SlabTileRenderer::inst;
SlabTileRenderer &SlabTileRenderer::instance() {
    return inst;
}

void SlabTileRenderer::render(tcsize x, tcsize y, tcsize z, const Tile &tile, StaticRenderContext &renderer) {
    SlabTileEntity *entity = static_cast<SlabTileEntity*>(tile.getTileEntity().get());

    auto bounds = entity->getBounds().value();

    // Tiles

    // Corners
    auto &tilepXpYpZ = renderer.getTile({x+1, y+1, z+1});
    auto &tilepXpYnZ = renderer.getTile({x+1, y+1, z-1});
    auto &tilepXnYpZ = renderer.getTile({x+1, y-1, z+1});
    auto &tilepXnYnZ = renderer.getTile({x+1, y-1, z-1});
    auto &tilenXpYpZ = renderer.getTile({x-1, y+1, z+1});
    auto &tilenXpYnZ = renderer.getTile({x-1, y+1, z-1});
    auto &tilenXnYpZ = renderer.getTile({x-1, y-1, z+1});
    auto &tilenXnYnZ = renderer.getTile({x-1, y-1, z-1});

    // Edges
    auto &tileXpYpZ = renderer.getTile({x, y+1, z+1});
    auto &tileXpYnZ = renderer.getTile({x, y+1, z-1});
    auto &tileXnYpZ = renderer.getTile({x, y-1, z+1});
    auto &tileXnYnZ = renderer.getTile({x, y-1, z-1});

    auto &tilepXYpZ = renderer.getTile({x+1, y, z+1});
    auto &tilepXYnZ = renderer.getTile({x+1, y, z-1});
    auto &tilenXYpZ = renderer.getTile({x-1, y, z+1});
    auto &tilenXYnZ = renderer.getTile({x-1, y, z-1});

    auto &tilepXpYZ = renderer.getTile({x+1, y+1, z});
    auto &tilepXnYZ = renderer.getTile({x+1, y-1, z});
    auto &tilenXpYZ = renderer.getTile({x-1, y+1, z});
    auto &tilenXnYZ = renderer.getTile({x-1, y-1, z});

    // Lighting

    // Corners
    auto &lightpXpYpZ = renderer.getTileLight({x+1, y+1, z+1});
    auto &lightpXpYnZ = renderer.getTileLight({x+1, y+1, z-1});
    auto &lightpXnYpZ = renderer.getTileLight({x+1, y-1, z+1});
    auto &lightpXnYnZ = renderer.getTileLight({x+1, y-1, z-1});
    auto &lightnXpYpZ = renderer.getTileLight({x-1, y+1, z+1});
    auto &lightnXpYnZ = renderer.getTileLight({x-1, y+1, z-1});
    auto &lightnXnYpZ = renderer.getTileLight({x-1, y-1, z+1});
    auto &lightnXnYnZ = renderer.getTileLight({x-1, y-1, z-1});

    // Edges
    auto &lightXpYpZ = renderer.getTileLight({x, y+1, z+1});
    auto &lightXpYnZ = renderer.getTileLight({x, y+1, z-1});
    auto &lightXnYpZ = renderer.getTileLight({x, y-1, z+1});
    auto &lightXnYnZ = renderer.getTileLight({x, y-1, z-1});

    auto &lightpXYpZ = renderer.getTileLight({x+1, y, z+1});
    auto &lightpXYnZ = renderer.getTileLight({x+1, y, z-1});
    auto &lightnXYpZ = renderer.getTileLight({x-1, y, z+1});
    auto &lightnXYnZ = renderer.getTileLight({x-1, y, z-1});

    auto &lightpXpYZ = renderer.getTileLight({x+1, y+1, z});
    auto &lightpXnYZ = renderer.getTileLight({x+1, y-1, z});
    auto &lightnXpYZ = renderer.getTileLight({x-1, y+1, z});
    auto &lightnXnYZ = renderer.getTileLight({x-1, y-1, z});

    for (auto face : AllDirections) {
        const auto &faceOffset = TileDirectionOffset::from(face);
        auto &adjacent = renderer.getTile({x + faceOffset.x, y + faceOffset.y, z + faceOffset.z});

        bool render = face == opposite(entity->getAttached());
        if (adjacent.isEmpty()) {
            render = true;
        } else if (adjacent.type != tile.type && !adjacent.isSolidOnSide(opposite(face))) {
            render = true;
        } else if (adjacent.type == tile.type) {
            auto adjEntity = static_cast<SlabTileEntity*>(adjacent.getTileEntity().get());

            if (adjEntity->getAttached() == entity->getAttached()) {
                if (face == entity->getAttached()) {
                    render = true;
                }
            } else {
                if (!adjacent.getTileEntity()->isSolidOn(opposite(face))) {
                    render = true;
                }
            }
        }

        // Tiles may render their sides if there is a transparent material adjacent
        if (render) {
            auto &light = renderer.getTileLight({x + faceOffset.x, y + faceOffset.y, z + faceOffset.z});

            glm::vec3 sideTint = glm::vec3(DirectionalOcclusionShade[static_cast<int>(face)]);

            Engine::Subsystem::TileVertex tlVertex, trVertex, blVertex, brVertex;
            
            switch (face) {
                case TileDirection::North:
                    tlVertex = {
                        {
                            (x + bounds.xMax),
                            (y + bounds.yMax),
                            (z + bounds.zMax),
                        },
                        mergeTile(light, lightpXpYpZ, tilepXpYpZ, lightpXpYZ, tilepXpYZ, lightXpYpZ, tileXpYpZ), // Tile light
                        mergeSky(light, lightpXpYpZ, tilepXpYpZ, lightpXpYZ, tilepXpYZ, lightXpYpZ, tileXpYpZ), // Sky tint
                        occlude(tilepXpYpZ, tilepXpYZ, tileXpYpZ, sideTint), // Occlusion
                    };
                    trVertex = {
                        {
                            (x + bounds.xMin), 
                            (y + bounds.yMax), 
                            (z + bounds.zMax), 
                        },
                        mergeTile(light, lightXpYpZ, tileXpYpZ, lightnXpYpZ, tilenXpYpZ, lightnXpYZ, tilenXpYZ), // Tile light
                        mergeSky(light, lightXpYpZ, tileXpYpZ, lightnXpYpZ, tilenXpYpZ, lightnXpYZ, tilenXpYZ), // Sky tint
                        occlude(tileXpYpZ, tilenXpYpZ, tilenXpYZ, sideTint), // Occlusion
                    };
                    brVertex = {
                        {
                            (x + bounds.xMin), 
                            (y + bounds.yMax), 
                            (z + bounds.zMin), 
                        },
                        mergeTile(light, lightnXpYZ, tilenXpYZ, lightnXpYnZ, tilenXpYnZ, lightXpYnZ, tileXpYnZ), // Tile light
                        mergeSky(light, lightnXpYZ, tilenXpYZ, lightnXpYnZ, tilenXpYnZ, lightXpYnZ, tileXpYnZ), // Sky tint
                        occlude(tilenXpYZ, tilenXpYnZ, tileXpYnZ, sideTint), // Occlusion
                    };
                    blVertex = {
                        {
                            (x + bounds.xMax), 
                            (y + bounds.yMax), 
                            (z + bounds.zMin), 
                        },
                        mergeTile(light, lightXpYnZ, tileXpYnZ, lightpXpYnZ, tilepXpYnZ, lightpXpYZ, tilepXpYZ), // Tile light
                        mergeSky(light, lightXpYnZ, tileXpYnZ, lightpXpYnZ, tilepXpYnZ, lightpXpYZ, tilepXpYZ), // Sky tint
                        occlude(tileXpYnZ, tilepXpYnZ, tilepXpYZ, sideTint), // Occlusion
                    };

                    tlVertex.texCoord = {1 - bounds.xMax, 1 - bounds.zMax, 0};
                    trVertex.texCoord = {1 - bounds.xMin, 1 - bounds.zMax, 0};
                    brVertex.texCoord = {1 - bounds.xMin, 1 - bounds.zMin, 0};
                    blVertex.texCoord = {1 - bounds.xMax, 1 - bounds.zMin, 0};
                    break;
                case TileDirection::South:
                    tlVertex = {
                        {
                            (x + bounds.xMin), 
                            (y + bounds.yMin), 
                            (z + bounds.zMax), 
                        },
                        mergeTile(light, lightnXnYZ, tilenXnYZ, lightnXnYpZ, tilenXnYpZ, lightXnYpZ, tileXnYpZ), // Tile light
                        mergeSky(light, lightnXnYZ, tilenXnYZ, lightnXnYpZ, tilenXnYpZ, lightXnYpZ, tileXnYpZ), // Sky tint
                        occlude(tilenXnYZ, tilenXnYpZ, tileXnYpZ, sideTint), // Occlusion
                    };
                    trVertex = {
                        {
                            (x + bounds.xMax),
                            (y + bounds.yMin),
                            (z + bounds.zMax),
                        },
                        mergeTile(light, lightXnYpZ, tileXnYpZ, lightpXnYpZ, tilepXnYpZ, lightpXnYZ, tilepXnYZ), // Tile light
                        mergeSky(light, lightXnYpZ, tileXnYpZ, lightpXnYpZ, tilepXnYpZ, lightpXnYZ, tilepXnYZ), // Sky tint
                        occlude(tileXnYpZ, tilepXnYpZ, tilepXnYZ, sideTint), // Occlusion
                    };
                    brVertex = {
                        {
                            (x + bounds.xMax),
                            (y + bounds.yMin),
                            (z + bounds.zMin),
                        },
                        mergeTile(light, lightpXnYZ, tilepXnYZ, lightpXnYnZ, tilepXnYnZ, lightXnYnZ, tileXnYnZ), // Tile light
                        mergeSky(light, lightpXnYZ, tilepXnYZ, lightpXnYnZ, tilepXnYnZ, lightXnYnZ, tileXnYnZ), // Sky tint
                        occlude(tilepXnYZ, tilepXnYnZ, tileXnYnZ, sideTint), // Occlusion
                    };
                    blVertex = {
                        {
                            (x + bounds.xMin), 
                            (y + bounds.yMin), 
                            (z + bounds.zMin), 
                        },
                        mergeTile(light, lightXnYnZ, tileXnYnZ, lightnXnYnZ, tilenXnYnZ, lightnXnYZ, tilenXnYZ), // Tile light
                        mergeSky(light, lightXnYnZ, tileXnYnZ, lightnXnYnZ, tilenXnYnZ, lightnXnYZ, tilenXnYZ), // Sky tint
                        occlude(tileXnYnZ, tilenXnYnZ, tilenXnYZ, sideTint), // Occlusion
                    };

                    tlVertex.texCoord = {bounds.xMin, 1 - bounds.zMax, 0};
                    trVertex.texCoord = {bounds.xMax, 1 - bounds.zMax, 0};
                    brVertex.texCoord = {bounds.xMax, 1 - bounds.zMin, 0};
                    blVertex.texCoord = {bounds.xMin, 1 - bounds.zMin, 0};
                    break;
                case TileDirection::Up:
                    tlVertex = {
                        {
                            (x + bounds.xMin), 
                            (y + bounds.yMax), 
                            (z + bounds.zMax), 
                        },
                        mergeTile(light, lightnXYpZ, tilenXYpZ, lightnXpYpZ, tilenXpYpZ, lightXpYpZ, tileXpYpZ), // Tile light
                        mergeSky(light, lightnXYpZ, tilenXYpZ, lightnXpYpZ, tilenXpYpZ, lightXpYpZ, tileXpYpZ), // Sky tint
                        occlude(tilenXYpZ, tilenXpYpZ, tileXpYpZ, sideTint), // Occlusion
                    };
                    trVertex = {
                        {
                            (x + bounds.xMax), 
                            (y + bounds.yMax), 
                            (z + bounds.zMax), 
                        },
                        mergeTile(light, lightXpYpZ, tileXpYpZ, lightpXpYpZ, tilepXpYpZ, lightpXYpZ, tilepXYpZ), // Tile light
                        mergeSky(light, lightXpYpZ, tileXpYpZ, lightpXpYpZ, tilepXpYpZ, lightpXYpZ, tilepXYpZ), // Sky tint
                        occlude(tileXpYpZ, tilepXpYpZ, tilepXYpZ, sideTint), // Occlusion
                    };
                    brVertex = {
                        {
                            (x + bounds.xMax), 
                            (y + bounds.yMin), 
                            (z + bounds.zMax), 
                        },
                        mergeTile(light, lightpXYpZ, tilepXYpZ, lightpXnYpZ, tilepXnYpZ, lightXnYpZ, tileXnYpZ), // Tile light
                        mergeSky(light, lightpXYpZ, tilepXYpZ, lightpXnYpZ, tilepXnYpZ, lightXnYpZ, tileXnYpZ), // Sky tint
                        occlude(tilepXYpZ, tilepXnYpZ, tileXnYpZ, sideTint), // Occlusion
                    };
                    blVertex = {
                        {
                            (x + bounds.xMin), 
                            (y + bounds.yMin), 
                            (z + bounds.zMax), 
                        },
                        mergeTile(light, lightXnYpZ, tileXnYpZ, lightnXnYpZ, tilenXnYpZ, lightnXYpZ, tilenXYpZ), // Tile light
                        mergeSky(light, lightXnYpZ, tileXnYpZ, lightnXnYpZ, tilenXnYpZ, lightnXYpZ, tilenXYpZ), // Sky tint
                        occlude(tileXnYpZ, tilenXnYpZ, tilenXYpZ, sideTint), // Occlusion
                    };

                    tlVertex.texCoord = {bounds.xMin, 1 - bounds.yMax, 0};
                    trVertex.texCoord = {bounds.xMax, 1 - bounds.yMax, 0};
                    brVertex.texCoord = {bounds.xMax, 1 - bounds.yMin, 0};
                    blVertex.texCoord = {bounds.xMin, 1 - bounds.yMin, 0};
                    break;
                case TileDirection::Down:
                    tlVertex = {
                        {
                            (x + bounds.xMin), 
                            (y + bounds.yMin), 
                            (z + bounds.zMin), 
                        },
                        mergeTile(light, lightnXYnZ, tilenXYnZ, lightnXnYnZ, tilenXnYnZ, lightXnYnZ, tileXnYnZ), // Tile light
                        mergeSky(light, lightnXYnZ, tilenXYnZ, lightnXnYnZ, tilenXnYnZ, lightXnYnZ, tileXnYnZ), // Sky tint
                        occlude(tilenXYnZ, tilenXnYnZ, tileXnYnZ, sideTint), // Occlusion
                    };
                    trVertex = {
                        {
                            (x + bounds.xMax), 
                            (y + bounds.yMin), 
                            (z + bounds.zMin), 
                        },
                        mergeTile(light, lightXnYnZ, tileXnYnZ, lightpXnYnZ, tilepXnYnZ, lightpXYnZ, tilepXYnZ), // Tile light
                        mergeSky(light, lightXnYnZ, tileXnYnZ, lightpXnYnZ, tilepXnYnZ, lightpXYnZ, tilepXYnZ), // Sky tint
                        occlude(tileXnYnZ, tilepXnYnZ, tilepXYnZ, sideTint), // Occlusion
                    };
                    brVertex = {
                        {
                            (x + bounds.xMax), 
                            (y + bounds.yMax), 
                            (z + bounds.zMin), 
                        },
                        mergeTile(light, lightpXYnZ, tilepXYnZ, lightpXpYnZ, tilepXpYnZ, lightXpYnZ, tileXpYnZ), // Tile light
                        mergeSky(light, lightpXYnZ, tilepXYnZ, lightpXpYnZ, tilepXpYnZ, lightXpYnZ, tileXpYnZ), // Sky tint
                        occlude(tilepXYnZ, tilepXpYnZ, tileXpYnZ, sideTint), // Occlusion
                    };
                    blVertex = {
                        {
                            (x + bounds.xMin), 
                            (y + bounds.yMax), 
                            (z + bounds.zMin), 
                        },
                        mergeTile(light, lightXpYnZ, tileXpYnZ, lightnXpYnZ, tilenXpYnZ, lightnXYnZ, tilenXYnZ), // Tile light
                        mergeSky(light, lightXpYnZ, tileXpYnZ, lightnXpYnZ, tilenXpYnZ, lightnXYnZ, tilenXYnZ), // Sky tint
                        occlude(tileXpYnZ, tilenXpYnZ, tilenXYnZ, sideTint), // Occlusion
                    };

                    tlVertex.texCoord = {bounds.xMin, bounds.yMin, 0};
                    trVertex.texCoord = {bounds.xMax, bounds.yMin, 0};
                    brVertex.texCoord = {bounds.xMax, bounds.yMax, 0};
                    blVertex.texCoord = {bounds.xMin, bounds.yMax, 0};
                    break;
                case TileDirection::East:
                    tlVertex = {
                        {
                            (x + bounds.xMax), 
                            (y + bounds.yMin), 
                            (z + bounds.zMax), 
                        },
                        mergeTile(light, lightpXnYZ, tilepXnYZ, lightpXnYpZ, tilepXnYpZ, lightpXYpZ, tilepXYpZ), // Tile light
                        mergeSky(light, lightpXnYZ, tilepXnYZ, lightpXnYpZ, tilepXnYpZ, lightpXYpZ, tilepXYpZ), // Sky tint
                        occlude(tilepXnYZ, tilepXnYpZ, tilepXYpZ, sideTint), // Occlusion
                    };
                    trVertex = {
                        {
                            (x + bounds.xMax), 
                            (y + bounds.yMax), 
                            (z + bounds.zMax), 
                        },
                        mergeTile(light, lightpXYpZ, tilepXYpZ, lightpXpYpZ, tilepXpYpZ, lightpXpYZ, tilepXpYZ), // Tile light
                        mergeSky(light, lightpXYpZ, tilepXYpZ, lightpXpYpZ, tilepXpYpZ, lightpXpYZ, tilepXpYZ), // Sky tint
                        occlude(tilepXYpZ, tilepXpYpZ, tilepXpYZ, sideTint), // Occlusion
                    };
                    brVertex = {
                        {
                            (x + bounds.xMax), 
                            (y + bounds.yMax), 
                            (z + bounds.zMin), 
                        },
                        mergeTile(light, lightpXpYZ, tilepXpYZ, lightpXpYnZ, tilepXpYnZ, lightpXYnZ, tilepXYnZ), // Tile light
                        mergeSky(light, lightpXpYZ, tilepXpYZ, lightpXpYnZ, tilepXpYnZ, lightpXYnZ, tilepXYnZ), // Sky tint
                        occlude(tilepXpYZ, tilepXpYnZ, tilepXYnZ, sideTint), // Occlusion
                    };
                    blVertex = {
                        {
                            (x + bounds.xMax), 
                            (y + bounds.yMin), 
                            (z + bounds.zMin), 
                        },
                        mergeTile(light, lightpXYnZ, tilepXYnZ, lightpXnYnZ, tilepXnYnZ, lightpXnYZ, tilepXnYZ), // Tile light
                        mergeSky(light, lightpXYnZ, tilepXYnZ, lightpXnYnZ, tilepXnYnZ, lightpXnYZ, tilepXnYZ), // Sky tint
                        occlude(tilepXYnZ, tilepXnYnZ, tilepXnYZ, sideTint), // Occlusion
                    };

                    tlVertex.texCoord = {bounds.yMin, 1 - bounds.zMax, 0};
                    trVertex.texCoord = {bounds.yMax, 1 - bounds.zMax, 0};
                    brVertex.texCoord = {bounds.yMax, 1 - bounds.zMin, 0};
                    blVertex.texCoord = {bounds.yMin, 1 - bounds.zMin, 0};
                    break;
                case TileDirection::West:
                    tlVertex = {
                        {
                            (x + bounds.xMin), 
                            (y + bounds.yMax), 
                            (z + bounds.zMax), 
                        },
                        mergeTile(light, lightnXpYZ, tilenXpYZ, lightnXpYpZ, tilenXpYpZ, lightnXYpZ, tilenXYpZ), // Tile light
                        mergeSky(light, lightnXpYZ, tilenXpYZ, lightnXpYpZ, tilenXpYpZ, lightnXYpZ, tilenXYpZ), // Sky tint
                        occlude(tilenXpYZ, tilenXpYpZ, tilenXYpZ, sideTint), // Occlusion
                    };
                    trVertex = {
                        {
                            (x + bounds.xMin), 
                            (y + bounds.yMin), 
                            (z + bounds.zMax), 
                        },
                        mergeTile(light, lightnXYpZ, tilenXYpZ, lightnXnYpZ, tilenXnYpZ, lightnXnYZ, tilenXnYZ), // Tile light
                        mergeSky(light, lightnXYpZ, tilenXYpZ, lightnXnYpZ, tilenXnYpZ, lightnXnYZ, tilenXnYZ), // Sky tint
                        occlude(tilenXYpZ, tilenXnYpZ, tilenXnYZ, sideTint), // Occlusion
                    };
                    brVertex = {
                        {
                            (x + bounds.xMin), 
                            (y + bounds.yMin), 
                            (z + bounds.zMin), 
                        },
                        mergeTile(light, lightnXnYZ, tilenXnYZ, lightnXnYnZ, tilenXnYnZ, lightnXYnZ, tilenXYnZ), // Tile light
                        mergeSky(light, lightnXnYZ, tilenXnYZ, lightnXnYnZ, tilenXnYnZ, lightnXYnZ, tilenXYnZ), // Sky tint
                        occlude(tilenXnYZ, tilenXnYnZ, tilenXYnZ, sideTint), // Occlusion
                    };
                    blVertex = {
                        {
                            (x + bounds.xMin), 
                            (y + bounds.yMax), 
                            (z + bounds.zMin), 
                        },
                        mergeTile(light, lightnXYnZ, tilenXYnZ, lightnXpYnZ, tilenXpYnZ, lightnXpYZ, tilenXpYZ), // Tile light
                        mergeSky(light, lightnXYnZ, tilenXYnZ, lightnXpYnZ, tilenXpYnZ, lightnXpYZ, tilenXpYZ), // Sky tint
                        occlude(tilenXYnZ, tilenXpYnZ, tilenXpYZ, sideTint), // Occlusion
                    };

                    tlVertex.texCoord = {1 - bounds.yMax, 1 - bounds.zMax, 0};
                    trVertex.texCoord = {1 - bounds.yMin, 1 - bounds.zMax, 0};
                    brVertex.texCoord = {1 - bounds.yMin, 1 - bounds.zMin, 0};
                    blVertex.texCoord = {1 - bounds.yMax, 1 - bounds.zMin, 0};
                    break;
                default:
                    assert(false);
            }
           
            renderer.outputSolidFace(face, tile.type->textureOf(face), tlVertex, trVertex, brVertex, blVertex, false);
        }
    }
}

}