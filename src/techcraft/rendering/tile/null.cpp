#include "techcraft/rendering/tile/null.hpp"

namespace Techcraft::Rendering {

NullTileRenderer NullTileRenderer::inst;
NullTileRenderer &NullTileRenderer::instance() {
    return inst;
}

void NullTileRenderer::render(tcsize x, tcsize y, tcsize z, const Tile &tile, StaticRenderContext &context) {
    // No-op
}

}