#include "engine/subsystem/terrain.hpp"

#include "techcraft/rendering/dimensionrenderer.hpp"
#include "techcraft/dimension/chunk.hpp"
#include "techcraft/rendering/tile/base.hpp"
#include "techcraft/rendering/fluid/base.hpp"
#include "techcraft/rendering/renderstate.hpp"
#include "techcraft/common/debug.hpp"
#include "utilities/profiler.hpp"
#include <string>
#include <iostream>

namespace Techcraft {

// DimensionRenderer
DimensionRenderer::DimensionRenderer(
    Dimension *dimension,
    Engine::RenderEngine &engine,
    Utilities::WorkerFleet &workers,
    uint16_t viewDistance
) : dimension(dimension),
    engine(engine),
    workers(workers),
    renderActive(false),
    lastRenderActive(false),
    lastChunkViewDistance(0),
    chunkViewDistance(viewDistance)
{
    chunkDirtyHandler = dimension->eventChunkDirty().connect(std::bind(&DimensionRenderer::onChunkDirty, this, std::placeholders::_1, std::placeholders::_2));
    chunkLoadHandler = dimension->getChunkLoadedEvent().connect(std::bind(&DimensionRenderer::onChunkLoad, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    chunkUnloadHandler = dimension->getChunkUnloadedEvent().connect(std::bind(&DimensionRenderer::onChunkUnload, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    tileAddHandler = dimension->getTileAddEvent().connect(std::bind(&DimensionRenderer::onTileEvent, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    tileRemoveHandler = dimension->getTileRemoveEvent().connect(std::bind(&DimensionRenderer::onTileEvent, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    tileUpdateHandler = dimension->getTileUpdateEvent().connect(std::bind(&DimensionRenderer::onTileEvent, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
}

DimensionRenderer::~DimensionRenderer() {

}

void DimensionRenderer::setViewpoint(const Position &point, const glm::vec3 &forward) {
    // TODO: Eventually we wont render chunks that arent visible using a frustrum cull.
    centerChunk = point;
    renderActive = true;
}

void DimensionRenderer::clearViewpoint() {
    renderActive = false;
}

void DimensionRenderer::setViewRange(uint16_t viewRange) {
    chunkViewDistance = viewRange;
    auto viewDiameter = viewRange * 2 + 1;
    toBeUpdated.reserve(viewDiameter * viewDiameter * viewDiameter);
}

uint16_t DimensionRenderer::getViewRange() const {
    return chunkViewDistance;
}

void DimensionRenderer::update(double deltaTime) {
    ProfilerSection profiler("dimren-update");
    updateActiveChunks();
    processPending();

    // Update dynamic region renderers so they can update animations
    // TODO: Dont update any that dont have animations

    for (auto &chunk : activeChunks) {
        chunk.renderState->dynamicRenderer.frameUpdate(deltaTime);
    }
}

void DimensionRenderer::updateActiveChunks() {
    ProfilerSection profiler("updateActiveChunks");
    if (centerChunk != lastCenterChunk || lastRenderActive != renderActive || lastChunkViewDistance != chunkViewDistance) {
        // New range of chunks need to be updated

        ChunkPosition temp { chunkViewDistance, chunkViewDistance, chunkViewDistance };

        auto min = centerChunk - temp;
        auto max = centerChunk + temp;

        auto lastMin = lastCenterChunk - temp;
        auto lastMax = lastCenterChunk + temp;

        // Make sure chunks within are requested
        if (renderActive) {
            ProfilerSection profiler2("new chunks");
            for (auto x = min.x; x <= max.x; ++x) {
                for (auto y = min.y; y <= max.y; ++y) {
                    for (auto z = min.z; z <= max.z; ++z) {
                        if (
                            x < lastMin.x || x > lastMax.x ||
                            y < lastMin.y || y > lastMax.y ||
                            z < lastMin.z || z > lastMax.z
                        ) {
                            const auto &chunk = dimension->getChunk(x, y, z);
                            if (chunk) {
                               addToRender(chunk, false);
                            }
                        }
                    }
                }
            }
        }

        // Remove chunks that are not within the range
        Profiler::enter("remove");
        std::list<Chunk*> toRemove;
        for (auto &chunk : activeChunks) {
            auto &existing = chunk.getPosition();
            if (
                existing.x < min.x || existing.x > max.x ||
                existing.y < min.y || existing.y > max.y ||
                existing.z < min.z || existing.z > max.z ||
                !renderActive
            ) {
               toRemove.push_back(&chunk);
            }
        }

        for (auto existing : toRemove) {
            removeFromRender(*existing);
        }

        Profiler::leave();

        lastCenterChunk = centerChunk;
        lastRenderActive = renderActive;
        lastChunkViewDistance = chunkViewDistance;
    }
}

void DimensionRenderer::processPending() {
    ProfilerSection profiler("processPending");
    std::unique_lock lock(uploadLock);
    for (const auto &chunkPtr : uploadQueue) {
        auto chunk = chunkPtr.lock();
        if (chunk && chunk->renderState) {
            if (chunk->renderState->removed) {
                removeFromRender(*chunk);
            } else {
                chunk->renderState->staticRenderer.finishGeneration();
            }
        }
    }

    uploadQueue.clear();

    // Queue up render for modified chunks
    for (auto &coord : toBeUpdated) {
        queueGenerate(coord);
    }

    toBeUpdated.clear();
}

void DimensionRenderer::queueGenerate(const ChunkPosition &chunkCoords) {
    auto chunk = dimension->getChunk(chunkCoords.x, chunkCoords.y, chunkCoords.z);
    if (!chunk || !chunk->renderState) {
        return;
    }

    auto &state = *chunk->renderState;

    state.dirty = true;

    bool wasRendering = state.rendering.exchange(true);
    if (wasRendering) {
        return;
    }

    workers.submit([this, &state, chunk]() {
        state.dirty = false;
        state.staticRenderer.generateMesh();
        state.dynamicRenderer.render();

        // Queue up the uploading of the mesh to GPU
        // NOTE: Eventually this should be asynchronous too
        {
            std::unique_lock lock(uploadLock);
            uploadQueue.push_back(chunk);
        }

        state.rendering = false;
        if (state.dirty) {
            toBeUpdated.emplace(chunk->getPosition());
        }
    });
}

void DimensionRenderer::addToRender(const std::shared_ptr<Chunk> &chunk, bool withBackLink) {
    if (chunk->activeRenderHook.is_linked()) {
        assert(chunk->renderState);
        chunk->renderState->removed = false;
        return;
    }
    const auto &coord = chunk->getPosition();

    assert(!chunk->renderState);
    auto &state = chunk->renderState.emplace(chunk, engine, static_cast<TilePosition>(coord));
    activeChunks.push_back(*chunk);

    // Link neighbouring chunks
    for (auto dir : AllExtendedDirections) {
        const auto &offset = TileDirectionOffset::from(dir);
        ChunkPosition offsetChunk = {coord.x+offset.x, coord.y+offset.y, coord.z+offset.z};
        const auto &neighborChunk = dimension->getChunk(offsetChunk.x, offsetChunk.y, offsetChunk.z);

        // Link us to neighbor
        state.staticRenderer.link(neighborChunk, dir);
        state.dynamicRenderer.link(neighborChunk, dir);

        if (withBackLink) {
            // Link neighbor to us
            if (neighborChunk && neighborChunk->renderState) {
                auto &neighborState = neighborChunk->renderState;
                neighborState->staticRenderer.link(chunk, opposite(dir));
                neighborState->dynamicRenderer.link(chunk, opposite(dir));
                toBeUpdated.insert(offsetChunk);
            }
        }
    }

    toBeUpdated.insert(coord);

    // if (chunk) {
    //     // TODO: Eventually asyncronous
    //     auto pair = chunkDynRenderer.emplace(coord, std::make_shared<DynamicRegionRenderer>(
    //         chunk,
    //         engine.getSubsystem(Engine::Subsystem::ObjectSubsystem::ID),
    //         tileRegistry.air(),
    //         TilePos{coord.x << 4, coord.y << 4, coord.z << 4}
    //     ));

    //     auto renderer = pair.first->second;
    //     for (auto dir : AllExtendedDirections) {
    //         const auto &offset = TileDirectionOffset::from(dir);
    //         renderer->link(dimension->getChunk(coord.x+offset.x, coord.y+offset.y, coord.z+offset.z), dir);
    //     }

    //     std::cout << chunkDynRenderer[coord].get() << std::endl;

    //     // TODO: Need to scan the chunk and find the dynamic renderered tiles. This really needs to be async
    // }
}

void DimensionRenderer::removeFromRender(Chunk &chunk) {
    chunk.renderState->removed = true;

    if (chunk.renderState->rendering) {
        // This will be retried after render is finished
        return;
    }

    activeChunks.remove(chunk);
    chunk.renderState.reset();
}

// Event listeners
void DimensionRenderer::onChunkDirty(DimensionEvent event, const ChunkPosition &coord) {
    toBeUpdated.insert(coord);

    // TODO: Need to update this so it only does this when a tile on the edge between a chunk is changed.
    toBeUpdated.insert({coord.x-1, coord.y, coord.z});
    toBeUpdated.insert({coord.x, coord.y-1, coord.z});
    toBeUpdated.insert({coord.x, coord.y, coord.z-1});
    toBeUpdated.insert({coord.x+1, coord.y, coord.z});
    toBeUpdated.insert({coord.x, coord.y+1, coord.z});
    toBeUpdated.insert({coord.x, coord.y, coord.z+1});
}

void DimensionRenderer::onChunkLoad(DimensionEvent event, const ChunkPosition &coord, const std::shared_ptr<Chunk> &chunk) {
    ChunkPosition temp { chunkViewDistance, chunkViewDistance, chunkViewDistance };

    auto min = centerChunk - temp;
    auto max = centerChunk + temp;

    if (
        coord.x >= min.x && coord.x <= max.x &&
        coord.y >= min.y && coord.y <= max.y &&
        coord.z >= min.z && coord.z <= max.z
    ) {
        addToRender(chunk, true);
    }
}

void DimensionRenderer::onChunkUnload(DimensionEvent event, const ChunkPosition &coord, const std::shared_ptr<Chunk> &chunk) {
     // Unlink neighbouring chunks
    for (auto dir : AllExtendedDirections) {
        const auto &offset = TileDirectionOffset::from(dir);
        ChunkPosition offsetChunk = coord + ChunkPosition::direct(offset);
        auto neighborChunk = dimension->getChunk(offsetChunk.x, offsetChunk.y, offsetChunk.z);
        if (neighborChunk && neighborChunk->renderState) {
            auto &state = neighborChunk->renderState;
            state->staticRenderer.unlink(opposite(dir));
            state->dynamicRenderer.unlink(opposite(dir));
        }
    }
    removeFromRender(*chunk);
}

void DimensionRenderer::onTileEvent(DimensionEvent event, const TilePosition &position, const Tile &tile) {
    if (tile.type->getRenderer().isStatic()) {
        // Not interested in static rendered tiles
        return;
    }

    // Chunk must be loaded
    const auto &chunk = dimension->getChunkAt(position);
    if (!chunk || !chunk->renderState) {
        return;
    }

    auto &renderer = chunk->renderState->dynamicRenderer;

    // TODO: also needs to be asynchronous
    // if (event == DimensionEvent::TileAdd) {
    //     renderer->addTile(position, tile);
    // } else if (event == DimensionEvent::TileRemove) {
    //     renderer->removeTile(position, tile);
    // } else if (event == DimensionEvent::TileUpdate) {
    //     renderer->updateTile(position, tile);
    // }
}

ChunkRenderState::ChunkRenderState(
    const std::shared_ptr<Chunk> &chunk,
    Engine::RenderEngine &engine,
    const TilePosition &position
)
  : staticRenderer(chunk, engine.getSubsystem(Engine::Subsystem::TerrainSubsystem::ID), position),
    dynamicRenderer(chunk, engine.getSubsystem(Engine::Subsystem::ObjectSubsystem::ID), position)
{

}

}