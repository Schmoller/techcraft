#include "techcraft/rendering/item/microblock.hpp"
#include "techcraft/rendering/item/util.hpp"
#include "techcraft/rendering/utils.hpp"
#include "techcraft/items/subtile-type.hpp"

#include "engine/gui/drawer.hpp"

#include <vector>

namespace Techcraft::Rendering {

MicroblockItemRenderer &MicroblockItemRenderer::instance() {
    return inst;
}
MicroblockItemRenderer MicroblockItemRenderer::inst;

void MicroblockItemRenderer::init(Engine::RenderEngine &engine) {
    microblockIcon = engine.createTexture("gui.icons.microblock")
        .fromFile("assets/textures/gui/icons/microblock.png")
        .build();
}

bool MicroblockItemRenderer::renderForGUI(const ItemStack &item, const ItemType &type, Engine::Gui::Drawer &drawer) {
    auto &tileItem = static_cast<const SubtileItemType&>(type);

    auto &subtileType = tileItem.getSubtileType();

    auto bounds = subtileType.getBounds(*subtileType.getSlots()[0]);

    if (!bounds) {
        bounds = {1,1,1, false};
    }

    // Normalize bounds
    float xMin = bounds->xMin - 0.5f;
    float yMin = bounds->yMin - 0.5f;
    float zMin = bounds->zMin - 0.5f;

    float xMax = bounds->xMax - 0.5f;
    float yMax = bounds->yMax - 0.5f;
    float zMax = bounds->zMax - 0.5f;

    
    std::vector<Engine::Vertex> vertices(4);
    std::vector<Engine::Gui::GuiBufferInt> indices(6);

    // Always the same
    indices[0] = 0;
    indices[1] = 1;
    indices[2] = 2;
    indices[3] = 2;
    indices[4] = 3;
    indices[5] = 0;

    auto &matrix = gui3dTransform();

    // FIXME: We currently dont adjust the texture coords properly. We are stretching the texture across the entire face
    // instead of clipping to the relevant area

    // Only display 3 faces as no others are visible

    TextureInfo srcCoords;
    auto topTexture = subtileType.textureOf(TileDirection::Up)->asTexture({0,0,0}, &srcCoords);

    if (topTexture) {
        auto shade = DirectionalOcclusionShade[static_cast<int>(TileDirection::Up)];
        vertices[0] = {
            transform({xMin, yMax, zMax}, matrix),
            {srcCoords.u1, srcCoords.v1, topTexture->arraySlot},
            {shade,shade,shade,1}
        };
        vertices[1] = {
            transform({xMax, yMax, zMax}, matrix),
            {srcCoords.u2, srcCoords.v1, topTexture->arraySlot},
            {shade,shade,shade,1}
        };
        vertices[2] = {
            transform({xMax, yMin, zMax}, matrix),
            {srcCoords.u2, srcCoords.v2, topTexture->arraySlot},
            {shade,shade,shade,1}
        };
        vertices[3] = {
            transform({xMin, yMin, zMax}, matrix),
            {srcCoords.u1, srcCoords.v2, topTexture->arraySlot},
            {shade,shade,shade,1}
        };
        drawer.draw(vertices, indices, *topTexture);
    }

    auto eastTexture = subtileType.textureOf(TileDirection::East)->asTexture({0,0,0}, &srcCoords);
    if (eastTexture) {
        auto shade = DirectionalOcclusionShade[static_cast<int>(TileDirection::East)];
        vertices[0] = {
            transform({xMax, yMin, zMax}, matrix),
            {srcCoords.u1, srcCoords.v1, eastTexture->arraySlot},
            {shade,shade,shade,1}
        };
        vertices[1] = {
            transform({xMax, yMax, zMax}, matrix),
            {srcCoords.u2, srcCoords.v1, eastTexture->arraySlot},
            {shade,shade,shade,1}
        };
        vertices[2] = {
            transform({xMax, yMax, zMin}, matrix),
            {srcCoords.u2, srcCoords.v2, eastTexture->arraySlot},
            {shade,shade,shade,1}
        };
        vertices[3] = {
            transform({xMax, yMin, zMin}, matrix),
            {srcCoords.u1, srcCoords.v2, eastTexture->arraySlot},
            {shade,shade,shade,1}
        };
        drawer.draw(vertices, indices, *eastTexture);
    }

    auto southTexture = subtileType.textureOf(TileDirection::South)->asTexture({0,0,0}, &srcCoords);
    if (southTexture) {
        auto shade = DirectionalOcclusionShade[static_cast<int>(TileDirection::South)];
        vertices[0] = {
            transform({xMin, yMin, zMax}, matrix),
            {srcCoords.u1, srcCoords.v1, southTexture->arraySlot},
            {shade,shade,shade,1}
        };
        vertices[1] = {
            transform({xMax, yMin, zMax}, matrix),
            {srcCoords.u2, srcCoords.v1, southTexture->arraySlot},
            {shade,shade,shade,1}
        };
        vertices[2] = {
            transform({xMax, yMin, zMin}, matrix),
            {srcCoords.u2, srcCoords.v2, southTexture->arraySlot},
            {shade,shade,shade,1}
        };
        vertices[3] = {
            transform({xMin, yMin, zMin}, matrix),
            {srcCoords.u1, srcCoords.v2, southTexture->arraySlot},
            {shade,shade,shade,1}
        };
        drawer.draw(vertices, indices, *southTexture);
    }

    // Draw icon to indicate that its a microblock
    drawer.drawRect(
        {
            {-0.5, 0},
            {0, 0.5}
        },
        *microblockIcon
    );

    return true;
}

}