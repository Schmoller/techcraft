#include "techcraft/rendering/item/helper.hpp"
#include "techcraft/inventory/itemstack.hpp"

#include "engine/gui/drawer.hpp"

#include <sstream>

namespace Techcraft::Rendering {

const float StandardItemDisplaySize = 32;

ItemRenderHelper::ItemRenderHelper(Engine::RenderEngine &engine)
    : engine(engine)
{}

void ItemRenderHelper::update() {

}

void ItemRenderHelper::drawItemGUI(Engine::Gui::Drawer &drawer, const ItemStack &item, float x, float y) {
    drawItemGUI(drawer, item, x, y, StandardItemDisplaySize, StandardItemDisplaySize);
}

void ItemRenderHelper::drawItemGUI(Engine::Gui::Drawer &drawer, const ItemStack &item, float x, float y, float width, float height) {
    auto &renderer = item.getType().getRenderer();

    switch (renderer.type()) {
        case ItemRendererType::Mesh:
            // TODO: Work out how to handle these
            assert(false);
            break;
        case ItemRendererType::TESR: {
            auto &tesr = static_cast<TESRItemRenderer&>(renderer);

            // Prepare transform
            drawer.pushTransform();
            drawer.translate(x + width/2, y + height/2);
            drawer.scale(width, height);

            if (tesr.renderForGUI(item, item.getType(), drawer)) {
                // Do we do anything here?
            }

            drawer.popTransform();
            break;
        }
    }
}

}