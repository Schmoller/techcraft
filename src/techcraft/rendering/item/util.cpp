#include "techcraft/rendering/item/tile.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <cmath>

namespace Techcraft::Rendering {

glm::mat4 transform;
bool hasComputedTransform { false };

const glm::mat4 &gui3dTransform() {
    if (!hasComputedTransform) {
        const float offset = 0.5;
        const float scale = 2.1 / std::sqrt(2);
        
        auto view = glm::lookAtRH(glm::vec3{+offset, -offset, -offset * 2/3}, glm::vec3{0, 0, 0}, glm::vec3{0, 0, 1});
        auto proj = glm::orthoRH(-scale, scale, -scale, scale, -scale, scale);

        transform = view * proj;
    }

    return transform;
}

}