#include "techcraft/rendering/item/flat.hpp"

#include "engine/gui/drawer.hpp"

#include <vector>
#include <iostream>

namespace Techcraft::Rendering {

FlatItemRenderer &FlatItemRenderer::instance() {
    return inst;
}
FlatItemRenderer FlatItemRenderer::inst;

bool FlatItemRenderer::renderForGUI(const ItemStack &item, const ItemType &type, Engine::Gui::Drawer &drawer) {
    std::cout << "renderForGUI" << std::endl;
    if (!type.texture()) {
        std::cout << "no texture" << std::endl;
        return false;
    }

    TextureInfo srcCoords;
    auto texture = type.texture()->asTexture({0,0,0}, &srcCoords);

    // A simple quad that prints a texture
    std::vector<Engine::Vertex> vertices ( 4 );
    std::vector<uint16_t> indices ( 6 );

    vertices[0] = {
        {-0.5, -0.5, 0},
        {srcCoords.u1, srcCoords.v1, texture->arraySlot},
        {1, 1, 1, 1},
    };
    vertices[1] = {
        {0.5, -0.5, 0},
        {srcCoords.u2, srcCoords.v1, texture->arraySlot},
        {1, 1, 1, 1}
    };
    vertices[2] = {
        {0.5, 0.5, 0},
        {srcCoords.u2, srcCoords.v2, texture->arraySlot},
        {1, 1, 1, 1}
    };
    vertices[3] = {
        {-0.5, 0.5, 0},
        {srcCoords.u1, srcCoords.v2, texture->arraySlot},
        {1, 1, 1, 1}
    };

    indices[0] = 0;
    indices[1] = 1;
    indices[2] = 2;

    indices[3] = 2;
    indices[4] = 3;
    indices[5] = 0;

    std::cout << "Draw " << vertices.size() << " vertices. " << indices.size() << " indices. Tex: " << texture->arraySlot << std::endl;

    drawer.draw(vertices, indices, *texture);

    return true;
}
}