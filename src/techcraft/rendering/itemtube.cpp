#include "techcraft/rendering/tile/itemtube.hpp"
#include "techcraft/tileentities/itemtube.hpp"

#include <glm/gtc/matrix_transform.hpp>

#include <iostream>

namespace Techcraft::Rendering {

#define PART_TUBE_BODY 0
#define PART_TUBE_CONNECTOR 1

#define ROT_90 glm::radians(90.0f)
#define ROT_180 glm::radians(180.0f)
#define ROT_270 glm::radians(270.0f)

#define UP glm::vec3{0, 0, 1}
#define NORTH glm::vec3{0, 1, 0}
#define EAST glm::vec3{1, 0, 0}

#define FACE_NORTH *transform
#define FACE_SOUTH glm::rotate(glm::translate(*transform, glm::vec3{1, 1, 0}), ROT_180, UP)
#define FACE_EAST glm::rotate(glm::translate(*transform, glm::vec3{0, 1, 0}), ROT_270, UP)
#define FACE_WEST glm::rotate(glm::translate(*transform, glm::vec3{1, 0, 0}), ROT_90, UP)
#define FACE_UP glm::rotate(glm::translate(*transform, glm::vec3{0, 1, 0}), ROT_90, EAST)
#define FACE_DOWN glm::rotate(glm::translate(*transform, glm::vec3{0, 0, 1}), ROT_270, EAST)
#define ROLL_RIGHT glm::rotate(glm::translate(*transform, glm::vec3{0, 0, 1}), ROT_90, NORTH)
#define ROLL_LEFT glm::rotate(glm::translate(*transform, glm::vec3{1, 0, 0}), ROT_270, NORTH)
#define ROLL_OVER glm::rotate(glm::translate(*transform, glm::vec3{1, 0, 1}), ROT_180, NORTH)


struct {
    const char *Straight = "Pipe_Straight_2WS";
    const char *Corner = "Pipe_Corner_2WC";
    const char *Tee = "Pipe_3-Way_3W";
    const char *TeeCorner = "Pipe_3-Way_Corner_3WC";
    const char *TeeCross = "Pipe_4-Way_Perp_4WP";
    const char *CrossUp = "Pipe_5-Way_5W";
    const char *Cross6Way = "Pipe_6-Way_6W";
    const char *Cross4Way = "Pipe_4-Way_Flat_4WF";
    const char *None = "Pipe_None_0W";
    const char *Cap = "Pipe_Cap_1W";
    const char *Connector = "Pipe_Connector_Con";
} TubeModelNames;

#define ON_DIR(dir) case (1 << static_cast<uint32_t>(TileDirection::dir))
#define ON_DIR2(dir1, dir2) case TileDirection::dir1 | TileDirection::dir2
#define ON_DIR3(dir1, dir2, dir3) case TileDirection::dir1 | TileDirection::dir2 | TileDirection::dir3

#define IS_CONNECTED(dir) (connectivity[static_cast<int>(TileDirection::dir)])

constexpr uint32_t operator|(TileDirection a, TileDirection b) {
    return (1 << static_cast<uint32_t>(a)) | (1 << static_cast<uint32_t>(b));
}

constexpr uint32_t operator|(uint32_t a, TileDirection b) {
    return a | (1 << static_cast<uint32_t>(b));
}


// ItemTubeRenderer
ItemTubeRenderer &ItemTubeRenderer::instance() {
    return inst;   
}
ItemTubeRenderer ItemTubeRenderer::inst;


void ItemTubeRenderer::init(Engine::RenderEngine &engine) {
    Engine::Model model("assets/models/tube.obj");

    engine.createTexture("tube.basic")
        .fromFile("assets/textures/models/tubes.png")
        .build();
    
    materialBasic = engine.createMaterial({
        "tube.basic",
        "tube.basic",
        "",
        "",
        false,
        vk::Filter::eNearest,
        vk::Filter::eNearest
    });

    meshes.straight = engine.createStaticMesh<Engine::Vertex>("tube.item.straight")
        .fromModel(model, TubeModelNames.Straight)
        .build();
    meshes.corner = engine.createStaticMesh<Engine::Vertex>("tube.item.corner")
        .fromModel(model, TubeModelNames.Corner)
        .build();
    meshes.tee = engine.createStaticMesh<Engine::Vertex>("tube.item.tee")
        .fromModel(model, TubeModelNames.Tee)
        .build();
    meshes.teeCorner = engine.createStaticMesh<Engine::Vertex>("tube.item.teeCorner")
        .fromModel(model, TubeModelNames.TeeCorner)
        .build();
    meshes.teeCross = engine.createStaticMesh<Engine::Vertex>("tube.item.teeCross")
        .fromModel(model, TubeModelNames.TeeCross)
        .build();
    meshes.crossUp = engine.createStaticMesh<Engine::Vertex>("tube.item.crossUp")
        .fromModel(model, TubeModelNames.CrossUp)
        .build();
    meshes.cross6Way = engine.createStaticMesh<Engine::Vertex>("tube.item.cross6Way")
        .fromModel(model, TubeModelNames.Cross6Way)
        .build();
    meshes.cross4Way = engine.createStaticMesh<Engine::Vertex>("tube.item.cross4Way")
        .fromModel(model, TubeModelNames.Cross4Way)
        .build();
    meshes.none = engine.createStaticMesh<Engine::Vertex>("tube.item.none")
        .fromModel(model, TubeModelNames.None)
        .build();
    meshes.cap = engine.createStaticMesh<Engine::Vertex>("tube.item.cap")
        .fromModel(model, TubeModelNames.Cap)
        .build();
    meshes.connector = engine.createStaticMesh<Engine::Vertex>("tube.item.connector")
        .fromModel(model, TubeModelNames.Connector)
        .build();
}

void ItemTubeRenderer::initTile(tcsize x, tcsize y, tcsize z, const Tile &tile, DynamicRenderContext<TileAnimationController> &context) {
    ItemTubeTileEntity *entity = static_cast<ItemTubeTileEntity*>(tile.getTileEntity().get());

    const Engine::StaticMesh *mesh;
    glm::mat4 transform {glm::translate(glm::mat4{1.0f}, glm::vec3{x, y, z})};
    glm::mat4 connectorTransform {glm::translate(glm::mat4{1.0f}, glm::vec3{x, y, z})};
    bool allowConnector;

    getMeshAndTransform(
        entity->getConnectivity(),
        entity->getAttachSide(),
        &mesh,
        &transform,
        &connectorTransform,
        &allowConnector
    );

    auto part = context.renderPart(PART_TUBE_BODY, mesh, materialBasic, {x, y, z});

    part->setTransform(transform);

    if (allowConnector) {
        auto connectorPart = context.renderPart(PART_TUBE_CONNECTOR, meshes.connector, materialBasic, {x, y, z});
        connectorPart->setTransform(connectorTransform);
    }
}

void ItemTubeRenderer::update(tcsize x, tcsize y, tcsize z, const Tile &tile, DynamicRenderContext<TileAnimationController> &context) {
    ItemTubeTileEntity *entity = static_cast<ItemTubeTileEntity*>(tile.getTileEntity().get());

    const Engine::StaticMesh *mesh;
    auto part = context.getPart(PART_TUBE_BODY);

    glm::mat4 transform {glm::translate(glm::mat4{1.0f}, part->getPosition())};
    glm::mat4 connectorTransform {glm::translate(glm::mat4{1.0f}, glm::vec3{x, y, z})};
    bool allowConnector;

    getMeshAndTransform(
        entity->getConnectivity(),
        entity->getAttachSide(),
        &mesh,
        &transform,
        &connectorTransform,
        &allowConnector
    );

    part->setMesh(mesh);
    part->setTransform(transform);

    if (allowConnector) {
        auto connectorPart = context.renderPart(PART_TUBE_CONNECTOR, meshes.connector, materialBasic, {x, y, z});
        connectorPart->setTransform(connectorTransform);
    } else {
        context.unrender(PART_TUBE_CONNECTOR);
    }
}

void ItemTubeRenderer::getMeshAndTransform(
    const bool connectivity[6],
    TileDirection attachSide,
    const Engine::StaticMesh **mesh,
    glm::mat4 *transform,
    glm::mat4 *connectorTransform,
    bool *allowConnector
) const
{
    uint32_t connected = 0;
    bool hasX = false;
    bool hasY = false;
    bool hasZ = false;
    uint32_t connectionMask = 0;

    TileDirection first = TileDirection::Self;

    for (auto dir : AllDirections) {
        if (connectivity[static_cast<int>(dir)]) {
            connectionMask |= (1 << (static_cast<int>(dir)));
            ++connected;

            if (connected == 1) {
                first = dir;
            }

            switch (dir) {
                case TileDirection::Up:
                case TileDirection::Down:
                    hasZ = true;
                    break;
                case TileDirection::North:
                case TileDirection::South:
                    hasY = true;
                    break;
                case TileDirection::East:
                case TileDirection::West:
                    hasX = true;
                    break;
                default:
                    assert(false);
            }
        }
    }

    uint32_t axisCount = 0;
    if (hasX) {
        ++axisCount;
    }
    if (hasY) {
        ++axisCount;
    }
    if (hasZ) {
        ++axisCount;
    }

    switch (connected) {
        default:
        case 0:
            *mesh = meshes.none;
            break;
        case 1:
            *mesh = meshes.cap;
            switch (first) {
                case TileDirection::Up:
                    *transform = FACE_UP;
                    break;
                case TileDirection::Down:
                    *transform = FACE_DOWN;
                    break;
                case TileDirection::North:
                    *transform = FACE_NORTH;
                    break;
                case TileDirection::South:
                    *transform = FACE_SOUTH;
                    break;
                case TileDirection::East:
                    *transform = FACE_EAST;
                    break;
                case TileDirection::West:
                    *transform = FACE_WEST;
                    break;
                default:
                    assert(false);
            }
            break;
        case 2:
            if (axisCount == 1) {
                if (hasX) {
                    *transform = FACE_EAST;
                } else if (hasZ) {
                    *transform = FACE_UP;
                }
                *mesh = meshes.straight;
                break;
            } else {
                *mesh = meshes.corner;
                switch (connectionMask) {
                    case TileDirection::North | TileDirection::East:
                        *transform = FACE_NORTH;
                        break;
                    case TileDirection::North | TileDirection::West:
                        *transform = FACE_WEST;
                        break;
                    case TileDirection::North | TileDirection::Up:
                        *transform = FACE_WEST;
                        *transform = FACE_UP;
                        break;
                    case TileDirection::North | TileDirection::Down:
                        *transform = FACE_WEST;
                        *transform = FACE_DOWN;
                        break;
                    case TileDirection::South | TileDirection::East:
                        *transform = FACE_EAST;
                        break;
                    case TileDirection::South | TileDirection::West:
                        *transform = FACE_SOUTH;
                        break;
                    case TileDirection::South | TileDirection::Up:
                        *transform = FACE_EAST;
                        *transform = FACE_UP;
                        break;
                    case TileDirection::South | TileDirection::Down:
                        *transform = FACE_EAST;
                        *transform = FACE_DOWN;
                        break;
                    case TileDirection::Up | TileDirection::East:
                        *transform = FACE_UP;
                        break;
                    case TileDirection::Up | TileDirection::West:
                        *transform = FACE_UP;
                        *transform = FACE_WEST;
                        break;
                    case TileDirection::Down | TileDirection::East:
                        *transform = FACE_DOWN;
                        break;
                    case TileDirection::Down | TileDirection::West:
                        *transform = FACE_SOUTH;
                        *transform = FACE_DOWN;
                        break;
                    default:
                        assert(false);
                }
                break;
            }
        case 3:
            if (axisCount == 2) {
                *mesh = meshes.tee;
                switch (connectionMask) {
                    ON_DIR3(Up, Down, North):
                        *transform = FACE_EAST;
                        *transform = FACE_UP;
                        break;
                    ON_DIR3(Up, Down, South):
                        *transform = FACE_WEST;
                        *transform = FACE_UP;
                        break;
                    ON_DIR3(Up, North, South):
                        *transform = ROLL_RIGHT;
                        break;
                    ON_DIR3(Down, North, South):
                        *transform = ROLL_LEFT;
                        break;
                    ON_DIR3(Up, Down, East):
                        *transform = ROLL_OVER;
                        *transform = FACE_UP;
                        break;
                    ON_DIR3(North, South, East):
                        *transform = FACE_SOUTH;
                        break;
                    ON_DIR3(Up, Down, West):
                        *transform = FACE_UP;
                        break;
                    ON_DIR3(North, South, West):
                        *transform = FACE_NORTH;
                        break;
                    ON_DIR3(Up, East, West):
                        *transform = FACE_EAST;
                        *transform = ROLL_RIGHT;
                        break;
                    ON_DIR3(Down, East, West):
                        *transform = FACE_EAST;
                        *transform = ROLL_LEFT;
                        break;
                    ON_DIR3(North, East, West):
                        *transform = FACE_EAST;
                        break;
                    ON_DIR3(South, East, West):
                        *transform = FACE_WEST;
                        break;
                }
                break;
            } else {
                *mesh = meshes.teeCorner;
                switch (connectionMask) {
                    ON_DIR3(Up, North, East):
                        *transform = FACE_NORTH;
                        break;
                    ON_DIR3(Down, North, East):
                        *transform = ROLL_RIGHT;
                        break;
                    ON_DIR3(Up, South, East):
                        *transform = FACE_EAST;
                        break;
                    ON_DIR3(Down, South, East):
                        *transform = FACE_EAST;
                        *transform = ROLL_RIGHT;
                        break;
                    ON_DIR3(Up, North, West):
                        *transform = FACE_WEST;
                        break;
                    ON_DIR3(Down, North, West):
                        *transform = ROLL_OVER;
                        break;
                    ON_DIR3(Up, South, West):
                        *transform = FACE_SOUTH;
                        break;
                    ON_DIR3(Down, South, West):
                        *transform = FACE_WEST;
                        *transform = ROLL_OVER;
                        break;
                }
                break;
            }
        case 4:
            if (axisCount == 2) {
                *mesh = meshes.cross4Way;
                if (!hasX) {
                    *transform = ROLL_RIGHT;
                } else if (!hasY) {
                    *transform = FACE_UP;
                }
                break;
            } else {
                *mesh = meshes.teeCross;
                // Testing which ones are not set
                switch (~connectionMask & 63) {
                    ON_DIR2(South, West):
                        *transform = FACE_DOWN;
                        *transform = ROLL_RIGHT;
                        break;
                    ON_DIR2(North, West):
                        *transform = FACE_UP;
                        *transform = ROLL_RIGHT;
                        break;
                    ON_DIR2(Down, West):
                        *transform = FACE_SOUTH;
                        break;
                    ON_DIR2(Up, West):
                        *transform = FACE_SOUTH;
                        *transform = ROLL_LEFT;
                        break;
                    ON_DIR2(South, East):
                        *transform = FACE_DOWN;
                        break;
                    ON_DIR2(North, East):
                        *transform = FACE_UP;
                        break;
                    ON_DIR2(Down, East):
                        *transform = FACE_NORTH;
                        break;
                    ON_DIR2(Up, East):
                        *transform = ROLL_LEFT;
                        break;
                    ON_DIR2(Down, South):
                        *transform = FACE_EAST;
                        break;
                    ON_DIR2(Up, South):
                        *transform = FACE_EAST;
                        *transform = ROLL_LEFT;
                        break;
                    ON_DIR2(Down, North):
                        *transform = FACE_WEST;
                        break;
                    ON_DIR2(Up, North):
                        *transform = FACE_WEST;
                        *transform = ROLL_LEFT;
                        break;
                }
                break;
            }
        case 5:
            *mesh = meshes.crossUp;
            switch (~connectionMask & 63) {
                ON_DIR(Up):
                    *transform = ROLL_LEFT;
                    break;
                ON_DIR(Down):
                    *transform = ROLL_RIGHT;
                    break;
                ON_DIR(North):
                    *transform = FACE_WEST;
                    break;
                ON_DIR(South):
                    *transform = FACE_EAST;
                    break;
                ON_DIR(East):
                    *transform = FACE_NORTH;
                    break;
                ON_DIR(West):
                    *transform = FACE_SOUTH;
                    break;
            }
            break;
        case 6:
            *mesh = meshes.cross6Way;
            break;
    }
    if (attachSide != TileDirection::Self) {
        if (connected == 6) {
            *allowConnector = false;
        } else {
            *allowConnector = true; // TODO: This needs to check whether there is a compatible tube layout here.
        }

        if (connectivity[static_cast<int>(opposite(attachSide))]) {
            // Cannot be positioned
            *allowConnector = false;
        }

        // NOTE: The macros all need the transform name
        transform = connectorTransform;
        switch (attachSide) {
            case TileDirection::Up:
                *connectorTransform = ROLL_OVER;
                if (IS_CONNECTED(East) || IS_CONNECTED(West)) {
                    if (IS_CONNECTED(North) || IS_CONNECTED(South)) {
                        // Blocked on all sides
                        *allowConnector = false;
                    } else {
                        *connectorTransform = FACE_EAST;
                    }
                }
                break;
            case TileDirection::Down:
                // No base transform
                if (IS_CONNECTED(East) || IS_CONNECTED(West)) {
                    if (IS_CONNECTED(North) || IS_CONNECTED(South)) {
                        // Blocked on all sides
                        *allowConnector = false;
                    } else {
                        *connectorTransform = FACE_EAST;
                    }
                }
                break;
            case TileDirection::North:
                *connectorTransform = FACE_UP;
                if (IS_CONNECTED(East) || IS_CONNECTED(West)) {
                    if (IS_CONNECTED(Up) || IS_CONNECTED(Down)) {
                        // Blocked on all sides
                        *allowConnector = false;
                    } else {
                        *connectorTransform = FACE_EAST;
                    }
                }
                break;
            case TileDirection::South:
                *connectorTransform = FACE_DOWN;
                if (IS_CONNECTED(East) || IS_CONNECTED(West)) {
                    if (IS_CONNECTED(Up) || IS_CONNECTED(Down)) {
                        // Blocked on all sides
                        *allowConnector = false;
                    } else {
                        *connectorTransform = FACE_EAST;
                    }
                }
                break;
            case TileDirection::East:
                *connectorTransform = FACE_UP;
                *connectorTransform = ROLL_LEFT;
                if (IS_CONNECTED(North) || IS_CONNECTED(South)) {
                    if (IS_CONNECTED(Up) || IS_CONNECTED(Down)) {
                        // Blocked on all sides
                        *allowConnector = false;
                    } else {
                        *connectorTransform = FACE_EAST;
                    }
                }
                break;
            case TileDirection::West:
                *connectorTransform = FACE_UP;
                *connectorTransform = ROLL_RIGHT;
                if (IS_CONNECTED(North) || IS_CONNECTED(South)) {
                    if (IS_CONNECTED(Up) || IS_CONNECTED(Down)) {
                        // Blocked on all sides
                        *allowConnector = false;
                    } else {
                        *connectorTransform = FACE_EAST;
                    }
                }
                break;
            default:
                assert(false);
        }
    } else {
        *allowConnector = false;
    }
}

}