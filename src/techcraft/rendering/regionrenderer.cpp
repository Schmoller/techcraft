#include "techcraft/rendering/regionrenderer.hpp"
#include "techcraft/rendering/tile/base.hpp"
#include "techcraft/rendering/subtile/base.hpp"
#include "techcraft/rendering/fluid/base.hpp"
#include "techcraft/tiles/base.hpp"
#include "techcraft/subtiles/base.hpp"
#include "techcraft/lighting.hpp"
#include "techcraft/tiles/registry.hpp"

// #include "utilities/profiler.hpp"

#include <iostream>
#include <algorithm>

#define TILE_UNIT 1.0

const uint8_t NO_LIGHT[TILE_LIGHT_PALLET_COUNT] = {0};


namespace Techcraft {
    
const TileDirection DIRECTIONS[6] = {
    TileDirection::East,
    TileDirection::West,
    TileDirection::North,
    TileDirection::South,
    TileDirection::Up,
    TileDirection::Down
};


glm::vec3 mergeTile(const TileLighting &self, const TileLighting &tl1, const Tile &t1, const TileLighting &tl2, const Tile &t2, const TileLighting &tl3, const Tile &t3);
glm::vec3 mergeSky(const TileLighting &self, const TileLighting &tl1, const Tile &t1, const TileLighting &tl2, const Tile &t2, const TileLighting &tl3, const Tile &t3);
glm::vec3 occlude(const Tile &t1, const Tile &t2, const Tile &t3, const glm::vec3 &tint);

RegionRenderer::RegionRenderer(
    std::shared_ptr<Region> region,
    RegionRenderer::TerrainSubsystem *terrainSystem,
    const TilePosition &offset
) : region(std::move(region)),
    terrainSystem(terrainSystem),
    offset(offset)
{
    emptyTile = {TileTypes::Air};
    emptyTileLight = {};
    emptyFluid = {};
    emptySubtile = {};
}

RegionRenderer::~RegionRenderer() {
    for (auto &segmentPair : opaqueSegemnts) {
        terrainSystem->removeSegment(segmentPair.second.id);
    }
    for (auto &segmentPair : transparentSegments) {
        terrainSystem->removeSegment(segmentPair.second.id);
    }
}

void RegionRenderer::generateMesh() {
    // ProfilerSection profiler("RR::generateMesh");
    // This is a naive approach. Probably going to be very expensive and not scalable at all.

    lockInLinkedRegionChanges();
    beginMesh();
    // For each tile in the region
    for (tcsize x = 0; x < region->size().width; ++x) {
        for (tcsize y = 0; y < region->size().depth; ++y) {
            for (tcsize z = 0; z < region->size().height; ++z) {
                current = {x, y, z};
                // Add a face on each exposed tile
                auto &self = region->getTile({x, y, z});

                if (!self.isEmpty() && self.type->getRenderer().isStatic()) {
                    currentMode = CurrentMode::Tile;
                    currentTile = &self;
                    static_cast<Rendering::StaticTileRenderer&>(self.type->getRenderer()).render(x, y, z, self, *this);
                }

                auto subtiles = self.getSubtileContainer();
                if (subtiles) {
                    subtiles->forEach([this, x, y, z](const SubtileSlot &slot, const Subtile &subtile) {
                        if (subtile.type && !subtile.type->isEmpty() && subtile.type->getRenderer().isStatic()) {
                            currentMode = CurrentMode::Subtile;
                            currentSubtile = &subtile;
                            currentSubtileSlot = &slot;
                            static_cast<Rendering::StaticSubtileRenderer&>(subtile.type->getRenderer()).render(x, y, z, slot, subtile, *this);
                        }
                    });
                }

                auto &selfFluid = region->getFluid({x, y, z});
                if (!selfFluid.isEmpty()) {
                    currentMode = CurrentMode::Fluid;
                    currentFluid = &selfFluid;
                    selfFluid.getType()->getRenderer().render(x, y, z, selfFluid, *this);
                }
            }
        }
    }
}

void RegionRenderer::finishGeneration() {
    endMesh();
}

void RegionRenderer::link(std::shared_ptr<Region> region, TileDirection direction) {
    linkedRegionsForWriting[static_cast<int>(direction)] = std::move(region);
    linkedRegionsIsModified = true;
}

void RegionRenderer::unlink(TileDirection direction) {
    linkedRegionsForWriting[static_cast<int>(direction)].reset();
    linkedRegionsIsModified = true;
}

void RegionRenderer::link(std::shared_ptr<Region> region, TileDirectionExtended direction) {
    linkedRegionsForWriting[static_cast<int>(direction)] = std::move(region);
    linkedRegionsIsModified = true;
}

void RegionRenderer::unlink(TileDirectionExtended direction) {
    linkedRegionsForWriting[static_cast<int>(direction)].reset();
    linkedRegionsIsModified = true;
}

void RegionRenderer::beginMesh() {
    for (auto &segmentPair : opaqueSegemnts) {
        segmentPair.second.modified = false;
    }
    for (auto &segmentPair : transparentSegments) {
        segmentPair.second.modified = false;
    }
}

void RegionRenderer::endMesh() {
    glm::vec3 minCorner {
        offset.x,
        offset.y,
        offset.z,
    };

    glm::vec3 maxCorner {
        offset.x + region->size().width,
        offset.y + region->size().depth,
        offset.z + region->size().height,
    };

    for (auto &segmentPair : opaqueSegemnts) {
        auto &segment = segmentPair.second;

        if (segment.modified) {
            if (segment.id == NO_SEGMENT) {
                segment.id = terrainSystem->addSegment(
                    segment.vertices,
                    segment.indices,
                    segment.textureArrayId,
                    minCorner,
                    maxCorner
                );
            } else {
                segment.id = terrainSystem->updateSegment(
                    segment.id,
                    segment.vertices,
                    segment.indices,
                    segment.textureArrayId,
                    minCorner,
                    maxCorner
                );
            }
        } else {
            terrainSystem->removeSegment(segment.id);
            segment.id = NO_SEGMENT;
        }
    }

    for (auto &segmentPair : transparentSegments) {
        auto &segment = segmentPair.second;

        if (segment.modified) {
            if (segment.id == NO_SEGMENT) {
                segment.id = terrainSystem->addSegment(
                    segment.vertices,
                    segment.indices,
                    segment.textureArrayId,
                    minCorner,
                    maxCorner,
                    true
                );
            } else {
                segment.id = terrainSystem->updateSegment(
                    segment.id,
                    segment.vertices,
                    segment.indices,
                    segment.textureArrayId,
                    minCorner,
                    maxCorner,
                    true
                );
            }
        } else {
            terrainSystem->removeSegment(segment.id);
            segment.id = NO_SEGMENT;
        }
    }
}

void RegionRenderer::outputSolidFace(
    TileDirection face,
    const TileTexture *texture,
    TileVertex tlVertex,
    TileVertex trVertex,
    TileVertex brVertex,
    TileVertex blVertex,
    bool autoTextureCoords
)
{
    outputFace(opaqueSegemnts, face, texture, tlVertex, trVertex, brVertex, blVertex, autoTextureCoords);
}

void RegionRenderer::outputTransparentFace(
    TileDirection dir,
    const TileTexture *texture,
    TileVertex tlVertex,
    TileVertex trVertex,
    TileVertex brVertex,
    TileVertex blVertex,
    bool autoTextureCoords
)
{
    outputFace(transparentSegments, dir, texture, tlVertex, trVertex, brVertex, blVertex, autoTextureCoords);
}

void RegionRenderer::outputFace(
    std::unordered_map<uint32_t, Segment> &segmentMap,
    TileDirection face,
    const TileTexture *texture,
    TileVertex tlVertex,
    TileVertex trVertex,
    TileVertex brVertex,
    TileVertex blVertex,
    bool autoTextureCoords
) {
    TextureInfo info;
    const Engine::Texture *engineTexture;
    if (texture->isConnectedTexture()) {
        bool topLeft;
        bool topRight;
        bool bottomLeft;
        bool bottomRight;
        bool left;
        bool right;
        bool top;
        bool bottom;

        // TODO: Allow connected textures on other subtile sides. The logic does become way more complex however
        // We probably need to have something in the subtiles themselves which state how connected textures work
        if (
            currentMode == CurrentMode::Tile || (
                currentMode == CurrentMode::Subtile &&
                isFaceSlot(currentSubtileSlot) &&
                &faceSlotFrom(face) == currentSubtileSlot
            )
        ) {
            switch (face) {
                case TileDirection::Up: {
                    left = textureCanConnect(current + TilePosition{-1, 0, 0}, face, texture);
                    right = textureCanConnect(current + TilePosition{1, 0, 0}, face, texture);
                    top = textureCanConnect(current + TilePosition{0, 1, 0}, face, texture);
                    bottom = textureCanConnect(current + TilePosition{0, -1, 0}, face, texture);
                    
                    if (top && left)
                        topLeft = textureCanConnect(current + TilePosition{-1, 1, 0}, face, texture);
                    if (top && right)
                        topRight = textureCanConnect(current + TilePosition{1, 1, 0}, face, texture);
                    if (bottom && left)
                        bottomLeft = textureCanConnect(current + TilePosition{-1, -1, 0}, face, texture);
                    if (bottom && right)
                        bottomRight = textureCanConnect(current + TilePosition{1, -1, 0}, face, texture);
                    break;
                }
                case TileDirection::Down: {
                    left = textureCanConnect(current + TilePosition{-1, 0, 0}, face, texture);
                    right = textureCanConnect(current + TilePosition{1, 0, 0}, face, texture);
                    top = textureCanConnect(current + TilePosition{0, -1, 0}, face, texture);
                    bottom = textureCanConnect(current + TilePosition{0, 1, 0}, face, texture);
                    
                    if (top && left)
                        topLeft = textureCanConnect(current + TilePosition{-1, -1, 0}, face, texture);
                    if (top && right)
                        topRight = textureCanConnect(current + TilePosition{1, -1, 0}, face, texture);
                    if (bottom && left)
                        bottomLeft = textureCanConnect(current + TilePosition{-1, 1, 0}, face, texture);
                    if (bottom && right)
                        bottomRight = textureCanConnect(current + TilePosition{1, 1, 0}, face, texture);
                    break;
                }
                case TileDirection::North: {
                    left = textureCanConnect(current + TilePosition{1, 0, 0}, face, texture);
                    right = textureCanConnect(current + TilePosition{-1, 0, 0}, face, texture);
                    top = textureCanConnect(current + TilePosition{0, 0, 1}, face, texture);
                    bottom = textureCanConnect(current + TilePosition{0, 0, -1}, face, texture);
                    
                    if (top && left)
                        topLeft = textureCanConnect(current + TilePosition{1, 0, 1}, face, texture);
                    if (top && right)
                        topRight = textureCanConnect(current + TilePosition{-1, 0, 1}, face, texture);
                    if (bottom && left)
                        bottomLeft = textureCanConnect(current + TilePosition{1, 0, -1}, face, texture);
                    if (bottom && right)
                        bottomRight = textureCanConnect(current + TilePosition{-1, 0, -1}, face, texture);
                    break;
                }
                case TileDirection::South: {
                    left = textureCanConnect(current + TilePosition{-1, 0, 0}, face, texture);
                    right = textureCanConnect(current + TilePosition{1, 0, 0}, face, texture);
                    top = textureCanConnect(current + TilePosition{0, 0, 1}, face, texture);
                    bottom = textureCanConnect(current + TilePosition{0, 0, -1}, face, texture);
                    
                    if (top && left)
                        topLeft = textureCanConnect(current + TilePosition{-1, 0, 1}, face, texture);
                    if (top && right)
                        topRight = textureCanConnect(current + TilePosition{1, 0, 1}, face, texture);
                    if (bottom && left)
                        bottomLeft = textureCanConnect(current + TilePosition{-1, 0, -1}, face, texture);
                    if (bottom && right)
                        bottomRight = textureCanConnect(current + TilePosition{1, 0, -1}, face, texture);
                    break;
                }
                case TileDirection::East: {
                    left = textureCanConnect(current + TilePosition{0, -1, 0}, face, texture);
                    right = textureCanConnect(current + TilePosition{0, 1, 0}, face, texture);
                    top = textureCanConnect(current + TilePosition{0, 0, 1}, face, texture);
                    bottom = textureCanConnect(current + TilePosition{0, 0, -1}, face, texture);
                    
                    if (top && left)
                        topLeft = textureCanConnect(current + TilePosition{0, -1, 1}, face, texture);
                    if (top && right)
                        topRight = textureCanConnect(current + TilePosition{0, 1, 1}, face, texture);
                    if (bottom && left)
                        bottomLeft = textureCanConnect(current + TilePosition{0, -1, -1}, face, texture);
                    if (bottom && right)
                        bottomRight = textureCanConnect(current + TilePosition{0, 1, -1}, face, texture);
                    break;
                }
                case TileDirection::West: {
                    left = textureCanConnect(current + TilePosition{0, 1, 0}, face, texture);
                    right = textureCanConnect(current + TilePosition{0, -1, 0}, face, texture);
                    top = textureCanConnect(current + TilePosition{0, 0, 1}, face, texture);
                    bottom = textureCanConnect(current + TilePosition{0, 0, -1}, face, texture);
                    
                    if (top && left)
                        topLeft = textureCanConnect(current + TilePosition{0, 1, 1}, face, texture);
                    if (top && right)
                        topRight = textureCanConnect(current + TilePosition{0, -1, 1}, face, texture);
                    if (bottom && left)
                        bottomLeft = textureCanConnect(current + TilePosition{0, 1, -1}, face, texture);
                    if (bottom && right)
                        bottomRight = textureCanConnect(current + TilePosition{0, -1, -1}, face, texture);
                    break;
                }
            }
        } else {
            // TODO: fluid maybe if we feel like it
            topLeft = false;
            topRight = false;
            bottomLeft = false;
            bottomRight = false;
            left = false;
            right = false;
            top = false;
            bottom = false;
        }

        // TODO: Logic of when to split etc. for optimisations

        // For now we just output 4 sub faces
        TileVertex tVertex {
            (tlVertex.pos + trVertex.pos) / 2.0f,
            (tlVertex.tileLight + trVertex.tileLight) / 2.0f,
            (tlVertex.skyLight + trVertex.skyLight) / 2.0f,
            (tlVertex.occlusion + trVertex.occlusion) / 2.0f,
            (tlVertex.normal + trVertex.normal) / 2.0f,
            (tlVertex.texCoord + trVertex.texCoord) / 2.0f,
        };
        TileVertex rVertex {
            (trVertex.pos + brVertex.pos) / 2.0f,
            (trVertex.tileLight + brVertex.tileLight) / 2.0f,
            (trVertex.skyLight + brVertex.skyLight) / 2.0f,
            (trVertex.occlusion + brVertex.occlusion) / 2.0f,
            (trVertex.normal + brVertex.normal) / 2.0f,
            (trVertex.texCoord + brVertex.texCoord) / 2.0f,
        };
        TileVertex bVertex {
            (blVertex.pos + brVertex.pos) / 2.0f,
            (blVertex.tileLight + brVertex.tileLight) / 2.0f,
            (blVertex.skyLight + brVertex.skyLight) / 2.0f,
            (blVertex.occlusion + brVertex.occlusion) / 2.0f,
            (blVertex.normal + brVertex.normal) / 2.0f,
            (blVertex.texCoord + brVertex.texCoord) / 2.0f,
        };
        TileVertex lVertex {
            (tlVertex.pos + blVertex.pos) / 2.0f,
            (tlVertex.tileLight + blVertex.tileLight) / 2.0f,
            (tlVertex.skyLight + blVertex.skyLight) / 2.0f,
            (tlVertex.occlusion + blVertex.occlusion) / 2.0f,
            (tlVertex.normal + blVertex.normal) / 2.0f,
            (tlVertex.texCoord + blVertex.texCoord) / 2.0f,
        };
        TileVertex cVertex {
            (tlVertex.pos + brVertex.pos) / 2.0f,
            (tlVertex.tileLight + brVertex.tileLight) / 2.0f,
            (tlVertex.skyLight + brVertex.skyLight) / 2.0f,
            (tlVertex.occlusion + brVertex.occlusion) / 2.0f,
            (tlVertex.normal + brVertex.normal) / 2.0f,
            (tlVertex.texCoord + brVertex.texCoord) / 2.0f,
        };

        if (!top && !bottom) {
            // vertical flow?
            if (left == right) {
                // Use full tile
                engineTexture = texture->asTexture(current, {TileTexture::SubFace::TopLeft, top, left, topLeft, true, true}, &info);
                outputFinalFace(segmentMap, face, engineTexture, info, tlVertex, trVertex, brVertex, blVertex, autoTextureCoords);
            } else {
                // 2 halves
                engineTexture = texture->asTexture(current, {TileTexture::SubFace::TopLeft, top, left, topLeft, true, false}, &info);
                outputFinalFace(segmentMap, face, engineTexture, info, tlVertex, tVertex, bVertex, blVertex, autoTextureCoords);
                engineTexture = texture->asTexture(current, {TileTexture::SubFace::TopRight, top, right, topRight, true, false}, &info);
                outputFinalFace(segmentMap, face, engineTexture, info, tVertex, trVertex, brVertex, bVertex, autoTextureCoords);
            }
        } else if (!left && !right) {
            // Horizontal flow
            if (top == bottom) {
                // Use full tile
                engineTexture = texture->asTexture(current, {TileTexture::SubFace::TopLeft, top, left, topLeft, true, true}, &info);
                outputFinalFace(segmentMap, face, engineTexture, info, tlVertex, trVertex, brVertex, blVertex, autoTextureCoords);
            } else {
                // 2 halves
                engineTexture = texture->asTexture(current, {TileTexture::SubFace::TopLeft, top, left, topLeft, false, true}, &info);
                outputFinalFace(segmentMap, face, engineTexture, info, tlVertex, trVertex, rVertex, lVertex, autoTextureCoords);
                engineTexture = texture->asTexture(current, {TileTexture::SubFace::BottomLeft, bottom, left, bottomLeft, false, true}, &info);
                outputFinalFace(segmentMap, face, engineTexture, info, lVertex, rVertex, brVertex, blVertex, autoTextureCoords);
            }
        } else {
            if (topLeft == topRight && bottomLeft == bottomRight && topLeft == bottomLeft && left == right && top == bottom && left == bottom) {
                // Full tile
                engineTexture = texture->asTexture(current, {TileTexture::SubFace::TopLeft, top, left, topLeft, true, true}, &info);
                outputFinalFace(segmentMap, face, engineTexture, info, tlVertex, trVertex, brVertex, blVertex, autoTextureCoords);
            } else {
                // 4 quads
                engineTexture = texture->asTexture(current, {TileTexture::SubFace::TopLeft, top, left, topLeft}, &info);
                outputFinalFace(segmentMap, face, engineTexture, info, tlVertex, tVertex, cVertex, lVertex, autoTextureCoords);
                engineTexture = texture->asTexture(current, {TileTexture::SubFace::TopRight, top, right, topRight}, &info);
                outputFinalFace(segmentMap, face, engineTexture, info, tVertex, trVertex, rVertex, cVertex, autoTextureCoords);
                engineTexture = texture->asTexture(current, {TileTexture::SubFace::BottomRight, bottom, right, bottomRight}, &info);
                outputFinalFace(segmentMap, face, engineTexture, info, cVertex, rVertex, brVertex, bVertex, autoTextureCoords);
                engineTexture = texture->asTexture(current, {TileTexture::SubFace::BottomLeft, bottom, left, bottomLeft}, &info);
                outputFinalFace(segmentMap, face, engineTexture, info, lVertex, cVertex, bVertex, blVertex, autoTextureCoords);
            }
        }
    } else {
        engineTexture = texture->asTexture(current, &info);
        outputFinalFace(segmentMap, face, engineTexture, info, tlVertex, trVertex, brVertex, blVertex, autoTextureCoords);
    }

}

void RegionRenderer::outputFinalFace(
    std::unordered_map<uint32_t, Segment> &segmentMap,
    TileDirection face,
    const Engine::Texture *texture,
    const TextureInfo &info,
    TileVertex tlVertex,
    TileVertex trVertex,
    TileVertex brVertex,
    TileVertex blVertex,
    bool autoTextureCoords
) {
    if (autoTextureCoords) {
        tlVertex.texCoord.x = info.u1;
        tlVertex.texCoord.y = info.v1;

        trVertex.texCoord.x = info.u2;
        trVertex.texCoord.y = info.v1;

        blVertex.texCoord.x = info.u1;
        blVertex.texCoord.y = info.v2;

        brVertex.texCoord.x = info.u2;
        brVertex.texCoord.y = info.v2;
    } else {
        double texW = info.u2 - info.u1;
        double texH = info.v2 - info.v1;

        tlVertex.texCoord.x = info.u1 + texW * tlVertex.texCoord.x;
        tlVertex.texCoord.y = info.v1 + texH * tlVertex.texCoord.y;

        trVertex.texCoord.x = info.u1 + texW * trVertex.texCoord.x;
        trVertex.texCoord.y = info.v1 + texH * trVertex.texCoord.y;

        blVertex.texCoord.x = info.u1 + texW * blVertex.texCoord.x;
        blVertex.texCoord.y = info.v1 + texH * blVertex.texCoord.y;

        brVertex.texCoord.x = info.u1 + texW * brVertex.texCoord.x;
        brVertex.texCoord.y = info.v1 + texH * brVertex.texCoord.y;
    }

    auto textureId = texture->arraySlot;

    tlVertex.texCoord.z = textureId;
    trVertex.texCoord.z = textureId;
    brVertex.texCoord.z = textureId;
    blVertex.texCoord.z = textureId;

    auto it = segmentMap.find(texture->arrayId);
    Segment *segment;
    if (it == segmentMap.end()) {
        auto pair = segmentMap.emplace(texture->arrayId, Segment{texture->arrayId});
        
        segment = &pair.first->second;
    } else {
        segment = &it->second;
    }

    if (!segment->modified) {
        segment->indices.clear();
        segment->vertices.clear();
        segment->modified = true;
    }

    // Adjust position
    tlVertex.pos.x = (tlVertex.pos.x + this->offset.x) * TILE_UNIT;
    tlVertex.pos.y = (tlVertex.pos.y + this->offset.y) * TILE_UNIT;
    tlVertex.pos.z = (tlVertex.pos.z + this->offset.z) * TILE_UNIT;
    trVertex.pos.x = (trVertex.pos.x + this->offset.x) * TILE_UNIT;
    trVertex.pos.y = (trVertex.pos.y + this->offset.y) * TILE_UNIT;
    trVertex.pos.z = (trVertex.pos.z + this->offset.z) * TILE_UNIT;
    brVertex.pos.x = (brVertex.pos.x + this->offset.x) * TILE_UNIT;
    brVertex.pos.y = (brVertex.pos.y + this->offset.y) * TILE_UNIT;
    brVertex.pos.z = (brVertex.pos.z + this->offset.z) * TILE_UNIT;
    blVertex.pos.x = (blVertex.pos.x + this->offset.x) * TILE_UNIT;
    blVertex.pos.y = (blVertex.pos.y + this->offset.y) * TILE_UNIT;
    blVertex.pos.z = (blVertex.pos.z + this->offset.z) * TILE_UNIT;

    // Normals
    const auto &offset = TileDirectionOffset::from(face);
    tlVertex.normal = {offset.x, offset.y, offset.z};
    trVertex.normal = {offset.x, offset.y, offset.z};
    brVertex.normal = {offset.x, offset.y, offset.z};
    blVertex.normal = {offset.x, offset.y, offset.z};

    auto indexOffset = segment->vertices.size();

    segment->vertices.push_back(tlVertex);
    segment->vertices.push_back(trVertex);
    segment->vertices.push_back(brVertex);
    segment->vertices.push_back(blVertex);

    // Triangles (CW) 0-2-3 0-1-2
    segment->indices.push_back(indexOffset + 0);
    segment->indices.push_back(indexOffset + 3);
    segment->indices.push_back(indexOffset + 2);
    segment->indices.push_back(indexOffset + 0);
    segment->indices.push_back(indexOffset + 2);
    segment->indices.push_back(indexOffset + 1);
}

const Tile &RegionRenderer::getTile(const TilePosition &position) const {
    int chunkX = 0;
    int chunkY = 0;
    int chunkZ = 0;
    if (position.x < 0) {
        // West
        chunkX = -1;
    } else if (position.x >= region->size().width) {
        // East
        chunkX = 1;
    }
    if (position.y < 0) {
        // South
        chunkY = -1;
    } else if (position.y >= region->size().depth) {
        // North
        chunkY = 1;
    }
    if (position.z < 0) {
        // Down
        chunkZ = -1;
    } else if (position.z >= region->size().height) {
        // Up
        chunkZ = 1;
    }

    TileDirectionExtended dir = toDirection(chunkX, chunkY, chunkZ);
    std::shared_ptr<Region> target;
    if (dir == TileDirectionExtended::Self) {
        target = region;
    } else {
        target = linkedRegionsForReading[static_cast<int32_t>(dir)];
    }

    if (target) {
        TilePosition offset;
        if (position.x < 0) {
            // West
            offset.x = target->size().width;
        } else if (position.x >= region->size().width) {
            // East
            offset.x = -region->size().width;
        }
        if (position.y < 0) {
            // South
            offset.y = target->size().depth;
        } else if (position.y >= region->size().depth) {
            // North
            offset.y = -region->size().depth;
        }
        if (position.z < 0) {
            // Down
            offset.z = target->size().height;
        } else if (position.z >= region->size().height) {
            // Up
            offset.z = -region->size().height;
        }
        return target->getTile(position + offset);
    }

    return emptyTile;
}

const FluidTile &RegionRenderer::getFluid(const TilePosition &position) const {
    int chunkX = 0;
    int chunkY = 0;
    int chunkZ = 0;
    if (position.x < 0) {
        // West
        chunkX = -1;
    } else if (position.x >= region->size().width) {
        // East
        chunkX = 1;
    }
    if (position.y < 0) {
        // South
        chunkY = -1;
    } else if (position.y >= region->size().depth) {
        // North
        chunkY = 1;
    }
    if (position.z < 0) {
        // Down
        chunkZ = -1;
    } else if (position.z >= region->size().height) {
        // Up
        chunkZ = 1;
    }

    TileDirectionExtended dir = toDirection(chunkX, chunkY, chunkZ);
    std::shared_ptr<Region> target;
    if (dir == TileDirectionExtended::Self) {
        target = region;
    } else {
        target = linkedRegionsForReading[static_cast<int32_t>(dir)];
    }

    if (target) {
        TilePosition offset;
        if (position.x < 0) {
            // West
            offset.x = target->size().width;
        } else if (position.x >= region->size().width) {
            // East
            offset.x = -region->size().width;
        }
        if (position.y < 0) {
            // South
            offset.y = target->size().depth;
        } else if (position.y >= region->size().depth) {
            // North
            offset.y = -region->size().depth;
        }
        if (position.z < 0) {
            // Down
            offset.z = target->size().height;
        } else if (position.z >= region->size().height) {
            // Up
            offset.z = -region->size().height;
        }
        return target->getFluid(position + offset);
    }

    return emptyFluid;
}

const TileLighting &RegionRenderer::getTileLight(const TilePosition &position) const {
    int chunkX = 0;
    int chunkY = 0;
    int chunkZ = 0;
    if (position.x < 0) {
        // West
        chunkX = -1;
    } else if (position.x >= region->size().width) {
        // East
        chunkX = 1;
    }
    if (position.y < 0) {
        // South
        chunkY = -1;
    } else if (position.y >= region->size().depth) {
        // North
        chunkY = 1;
    }
    if (position.z < 0) {
        // Down
        chunkZ = -1;
    } else if (position.z >= region->size().height) {
        // Up
        chunkZ = 1;
    }

    TileDirectionExtended dir = toDirection(chunkX, chunkY, chunkZ);
    std::shared_ptr<Region> target;
    if (dir == TileDirectionExtended::Self) {
        target = region;
    } else {
        target = linkedRegionsForReading[static_cast<int32_t>(dir)];
    }

    if (target) {
        TilePosition offset;
        if (position.x < 0) {
            // West
            offset.x = target->size().width;
        } else if (position.x >= region->size().width) {
            // East
            offset.x = -region->size().width;
        }
        if (position.y < 0) {
            // South
            offset.y = target->size().depth;
        } else if (position.y >= region->size().depth) {
            // North
            offset.y = -region->size().depth;
        }
        if (position.z < 0) {
            // Down
            offset.z = target->size().height;
        } else if (position.z >= region->size().height) {
            // Up
            offset.z = -region->size().height;
        }
        return target->getTileLight(position + offset);
    }

    return emptyTileLight;
}


const Subtile &RegionRenderer::getSubtile(const TilePosition &position, const SubtileSlot &slot) const {
    int chunkX = 0;
    int chunkY = 0;
    int chunkZ = 0;
    if (position.x < 0) {
        // West
        chunkX = -1;
    } else if (position.x >= region->size().width) {
        // East
        chunkX = 1;
    }
    if (position.y < 0) {
        // South
        chunkY = -1;
    } else if (position.y >= region->size().depth) {
        // North
        chunkY = 1;
    }
    if (position.z < 0) {
        // Down
        chunkZ = -1;
    } else if (position.z >= region->size().height) {
        // Up
        chunkZ = 1;
    }

    TileDirectionExtended dir = toDirection(chunkX, chunkY, chunkZ);
    std::shared_ptr<Region> target;
    if (dir == TileDirectionExtended::Self) {
        target = region;
    } else {
        target = linkedRegionsForReading[static_cast<int32_t>(dir)];
    }

    if (target) {
        TilePosition offset;
        if (position.x < 0) {
            // West
            offset.x = target->size().width;
        } else if (position.x >= region->size().width) {
            // East
            offset.x = -region->size().width;
        }
        if (position.y < 0) {
            // South
            offset.y = target->size().depth;
        } else if (position.y >= region->size().depth) {
            // North
            offset.y = -region->size().depth;
        }
        if (position.z < 0) {
            // Down
            offset.z = target->size().height;
        } else if (position.z >= region->size().height) {
            // Up
            offset.z = -region->size().height;
        }
        return target->getSubtile(position + offset, slot);
    }

    return emptySubtile;
}

bool RegionRenderer::textureCanConnect(const TilePosition &position, TileDirection face, const TileTexture *match) const {
    auto &next = getTile(position);
    if (next.type->textureOf(face) == match) {
        return true;
    }

    auto container = next.getSubtileContainer();

    if (container) {
        const SubtileSlot &slot = faceSlotFrom(face);

        if (container->hasSlot(slot)) {
            auto &subtile = container->getSubtile(slot);
            if (subtile.type && subtile.type->textureOf(face) == match) {
                return true;
            }
        }
    }

    return false;
}

void RegionRenderer::lockInLinkedRegionChanges() {
    if (!linkedRegionsIsModified) {
        return;
    }

    std::copy(linkedRegionsForWriting.begin(), linkedRegionsForWriting.end(), linkedRegionsForReading.begin());
    linkedRegionsIsModified = false;
}

}