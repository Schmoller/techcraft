#include "techcraft/rendering/animation.hpp"

namespace Techcraft::Rendering {

AnimationController::AnimationController(const TilePosition &position)
    : position(position)
{}

TileAnimationController::TileAnimationController(const TilePosition &position, const Tile &tile)
    : AnimationController(position), tile(&tile)
{}

SubtileAnimationController::SubtileAnimationController(const TilePosition &position, const SubtileSlot &slot, const Subtile &subtile)
    : AnimationController(position), slot(&slot), subtile(&subtile)
{}

}