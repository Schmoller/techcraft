#include "techcraft/rendering/fluid/basicfluid.hpp"
#include "techcraft/rendering/utils.hpp"
#include "techcraft/lighting.hpp"
#include "techcraft/tiles/base.hpp"
#include "engine/subsystem/terrain.hpp"

namespace Techcraft::Rendering {

float heightOf(const FluidTile &fluid) {
    return static_cast<float>(fluid.getAmount()) / static_cast<float>(fluid.getType()->getSpread());
}

bool anyFluid(const FluidTile &t1, const FluidTile &t2, const FluidTile &t3) {
    if (!t1.isEmpty()) {
        return true;
    }
    if (!t2.isEmpty()) {
        return true;
    }
    if (!t3.isEmpty()) {
        return true;
    }

    return false;
}

float averageHeight(const FluidTile &t1, const FluidTile &t2, const FluidTile &t3, const FluidTile &t4) {
    int count = 0;
    float height = 0;
    if (!t1.isEmpty()) {
        height += heightOf(t1);
        ++count;
    }
    if (!t2.isEmpty()) {
        height += heightOf(t2);
        ++count;
    }
    if (!t3.isEmpty()) {
        height += heightOf(t3);
        ++count;
    }
    if (!t4.isEmpty()) {
        height += heightOf(t4);
        ++count;
    }

    if (count > 0) {
        return height / count;
    } else {
        return 0;
    }
}

// BasicFluidRenderer
BasicFluidRenderer BasicFluidRenderer::inst;
BasicFluidRenderer &BasicFluidRenderer::instance() {
    return inst;
}

void BasicFluidRenderer::render(tcsize x, tcsize y, tcsize z, const FluidTile &fluid, StaticRenderContext &renderer) {
    /** 
     * TODO: More sophisticated rendering:
     * - Lighting should obey similar rules to tiles
     */

    auto fluidEast = renderer.getFluid({x + 1, y, z});
    auto fluidWest = renderer.getFluid({x - 1, y, z});
    auto fluidNorth = renderer.getFluid({x, y + 1, z});
    auto fluidSouth = renderer.getFluid({x, y - 1, z});
    auto fluidUp = renderer.getFluid({x, y, z + 1});

    auto fluidNorthEast = renderer.getFluid({x + 1, y + 1, z});
    auto fluidNorthWest = renderer.getFluid({x - 1, y + 1, z});
    auto fluidSouthEast = renderer.getFluid({x + 1, y - 1, z});
    auto fluidSouthWest = renderer.getFluid({x - 1, y - 1, z});

    auto fluidUpEast = renderer.getFluid({x + 1, y, z + 1});
    auto fluidUpWest = renderer.getFluid({x - 1, y, z + 1});
    auto fluidUpNorth = renderer.getFluid({x, y + 1, z + 1});
    auto fluidUpSouth = renderer.getFluid({x, y - 1, z + 1});

    auto fluidUpNorthEast = renderer.getFluid({x + 1, y + 1, z + 1});
    auto fluidUpNorthWest = renderer.getFluid({x - 1, y + 1, z + 1});
    auto fluidUpSouthEast = renderer.getFluid({x + 1, y - 1, z + 1});
    auto fluidUpSouthWest = renderer.getFluid({x - 1, y - 1, z + 1});

    float heightNE;
    float heightNW;
    float heightSE;
    float heightSW;

    if (!fluidUp.isEmpty()) {
        // Always full height if there is fluid above
        heightNE = 1;
        heightNW = 1;
        heightSE = 1;
        heightSW = 1;
    } else {
        if (anyFluid(fluidUpEast, fluidUpNorth, fluidUpNorthEast)) {
            heightNE = 1;
        } else {
            heightNE = averageHeight(fluidEast, fluidNorth, fluidNorthEast, fluid);
        }
        if (anyFluid(fluidUpWest, fluidUpNorth, fluidUpNorthWest)) {
            heightNW = 1;
        } else {
            heightNW = averageHeight(fluidWest, fluidNorth, fluidNorthWest, fluid);
        }
        if (anyFluid(fluidUpEast, fluidUpSouth, fluidUpSouthEast)) {
            heightSE = 1;
        } else {
            heightSE = averageHeight(fluidEast, fluidSouth, fluidSouthEast, fluid);
        }
        if (anyFluid(fluidUpWest, fluidUpSouth, fluidUpSouthWest)) {
            heightSW = 1;
        } else {
            heightSW = averageHeight(fluidWest, fluidSouth, fluidSouthWest, fluid);
        }
    }

    for (auto face : AllDirections) {
        const auto &faceOffset = TileDirectionOffset::from(face);
        auto &adjacentTile = renderer.getTile({x + faceOffset.x, y + faceOffset.y, z + faceOffset.z});
        auto &adjacentFluid = renderer.getFluid({x + faceOffset.x, y + faceOffset.y, z + faceOffset.z});

        bool render = false;
        if (adjacentFluid.isEmpty() && adjacentTile.type->isTransparent()) {
            render = true;
        } else if (!adjacentFluid.contains(fluid.getType())) {
            render = true;
        }

        if (!adjacentTile.type->isTransparent()) {
            render = false;
        }

        if (render) {
            auto &light = renderer.getTileLight({x + faceOffset.x, y + faceOffset.y, z + faceOffset.z});

            glm::vec3 tileLightColor = blendTileLighting(light.tileIntensity);
            glm::vec3 skyLightTint = blendSkyLighting(light.skyIntensity);

            // glm::vec3 sideTint = glm::vec3(DirectionalOcclusionShade[static_cast<int>(face)]);

            Engine::Subsystem::TileVertex tlVertex, trVertex, blVertex, brVertex;
            
            switch (face) {
                case TileDirection::North:
                    tlVertex = {
                        {
                            (x + 1), 
                            (y + 1), 
                            (z + heightNE), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {0, 1 - heightNE, 0}
                    };
                    trVertex = {
                        {
                            (x + 0), 
                            (y + 1), 
                            (z + heightNW), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {1, 1 - heightNW, 0}
                    };
                    brVertex = {
                        {
                            (x + 0), 
                            (y + 1), 
                            (z + 0), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {1, 1, 0}
                    };
                    blVertex = {
                        {
                            (x + 1), 
                            (y + 1), 
                            (z + 0), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {0, 1, 0}
                    };
                    break;
                case TileDirection::South:
                    tlVertex = {
                        {
                            (x + 0), 
                            (y + 0), 
                            (z + heightSW), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {0, 1 - heightSW, 0}
                    };
                    trVertex = {
                        {
                            (x + 1), 
                            (y + 0), 
                            (z + heightSE), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {1, 1 - heightSE, 0}
                    };
                    brVertex = {
                        {
                            (x + 1), 
                            (y + 0), 
                            (z + 0), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {1, 1, 0}
                    };
                    blVertex = {
                        {
                            (x + 0), 
                            (y + 0), 
                            (z + 0), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {0, 1, 0}
                    };
                    break;
                case TileDirection::Up:
                    tlVertex = {
                        {
                            (x + 0), 
                            (y + 1), 
                            (z + heightNW), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {0, 0, 0}
                    };
                    trVertex = {
                        {
                            (x + 1), 
                            (y + 1), 
                            (z + heightNE), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {1, 0, 0}
                    };
                    brVertex = {
                        {
                            (x + 1), 
                            (y + 0), 
                            (z + heightSE), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {1, 1, 0}
                    };
                    blVertex = {
                        {
                            (x + 0), 
                            (y + 0), 
                            (z + heightSW), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {0, 1, 0}
                    };
                    break;
                case TileDirection::Down:
                    tlVertex = {
                        {
                            (x + 0), 
                            (y + 0), 
                            (z + 0), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {0, 0, 0}
                    };
                    trVertex = {
                        {
                            (x + 1), 
                            (y + 0), 
                            (z + 0), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {1, 0, 0}
                    };
                    brVertex = {
                        {
                            (x + 1), 
                            (y + 1), 
                            (z + 0), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {1, 1, 0}
                    };
                    blVertex = {
                        {
                            (x + 0), 
                            (y + 1), 
                            (z + 0), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {0, 1, 0}
                    };
                    break;
                case TileDirection::East:
                    tlVertex = {
                        {
                            (x + 1), 
                            (y + 0), 
                            (z + heightSE), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {0, 1 - heightSE, 0}
                    };
                    trVertex = {
                        {
                            (x + 1), 
                            (y + 1), 
                            (z + heightNE), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {1, 1 - heightNE, 0}
                    };
                    brVertex = {
                        {
                            (x + 1), 
                            (y + 1), 
                            (z + 0), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {1, 1, 0}
                    };
                    blVertex = {
                        {
                            (x + 1), 
                            (y + 0), 
                            (z + 0), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {0, 1, 0}
                    };
                    break;
                case TileDirection::West:
                    tlVertex = {
                        {
                            (x + 0), 
                            (y + 1), 
                            (z + heightNW), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {0, 1 - heightNW, 0}
                    };
                    trVertex = {
                        {
                            (x + 0), 
                            (y + 0), 
                            (z + heightSW), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {1, 1 - heightSW, 0}
                    };
                    brVertex = {
                        {
                            (x + 0), 
                            (y + 0), 
                            (z + 0), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {1, 1, 0}
                    };
                    blVertex = {
                        {
                            (x + 0), 
                            (y + 1), 
                            (z + 0), 
                        },
                        tileLightColor,
                        skyLightTint,
                        {1,1,1},
                        {},
                        {0, 1, 0}
                    };
                    break;
                default:
                    assert(false);
            }

            renderer.outputTransparentFace(face, fluid.getType()->textureOf(face), tlVertex, trVertex, brVertex, blVertex, false);
        }
    }
}

}