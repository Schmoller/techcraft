#include "techcraft/rendering/subtile/basic.hpp"
#include "techcraft/rendering/utils.hpp"
#include "techcraft/lighting.hpp"
#include "engine/subsystem/terrain.hpp"

namespace Techcraft::Rendering {

BasicSubtileRenderer BasicSubtileRenderer::inst;
BasicSubtileRenderer &BasicSubtileRenderer::instance() {
    return inst;
}

void BasicSubtileRenderer::render(tcsize x, tcsize y, tcsize z, const SubtileSlot &slot, const Subtile &subtile, StaticRenderContext &renderer) {
    // TODO: This does not account for lighting at all

    auto bounds = subtile.type->getBounds(slot);
    if (!bounds) {
        return;
    }

    for (auto face : AllDirections) {
        const auto &faceOffset = TileDirectionOffset::from(face);

        bool render = true;
        // TODO: Render rules.

        if (render) {
            auto &light = renderer.getTileLight({x + faceOffset.x, y + faceOffset.y, z + faceOffset.z});

            glm::vec3 sideTint = glm::vec3(DirectionalOcclusionShade[static_cast<int>(face)]);

            Engine::Subsystem::TileVertex tlVertex, trVertex, blVertex, brVertex;
            
            switch (face) {
                case TileDirection::North:
                    tlVertex = {
                        {
                            x + bounds->xMax, 
                            y + bounds->yMax, 
                            z + bounds->zMax, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    trVertex = {
                        {
                            x + bounds->xMin, 
                            y + bounds->yMax, 
                            z + bounds->zMax, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    brVertex = {
                        {
                            x + bounds->xMin, 
                            y + bounds->yMax, 
                            z + bounds->zMin, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    blVertex = {
                        {
                            x + bounds->xMax, 
                            y + bounds->yMax, 
                            z + bounds->zMin, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    break;
                case TileDirection::South:
                    tlVertex = {
                        {
                            x + bounds->xMin, 
                            y + bounds->yMin, 
                            z + bounds->zMax, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    trVertex = {
                        {
                            x + bounds->xMax, 
                            y + bounds->yMin, 
                            z + bounds->zMax, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    brVertex = {
                        {
                            x + bounds->xMax, 
                            y + bounds->yMin, 
                            z + bounds->zMin, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    blVertex = {
                        {
                            x + bounds->xMin, 
                            y + bounds->yMin, 
                            z + bounds->zMin, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    break;
                case TileDirection::Up:
                    tlVertex = {
                        {
                            x + bounds->xMin, 
                            y + bounds->yMax, 
                            z + bounds->zMax, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    trVertex = {
                        {
                            x + bounds->xMax, 
                            y + bounds->yMax, 
                            z + bounds->zMax, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    brVertex = {
                        {
                            x + bounds->xMax, 
                            y + bounds->yMin, 
                            z + bounds->zMax, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    blVertex = {
                        {
                            x + bounds->xMin, 
                            y + bounds->yMin, 
                            z + bounds->zMax, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    break;
                case TileDirection::Down:
                    tlVertex = {
                        {
                            x + bounds->xMin, 
                            y + bounds->yMin, 
                            z + bounds->zMin, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    trVertex = {
                        {
                            x + bounds->xMax, 
                            y + bounds->yMin, 
                            z + bounds->zMin, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    brVertex = {
                        {
                            x + bounds->xMax, 
                            y + bounds->yMax, 
                            z + bounds->zMin, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    blVertex = {
                        {
                            x + bounds->xMin, 
                            y + bounds->yMax, 
                            z + bounds->zMin, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    break;
                case TileDirection::East:
                    tlVertex = {
                        {
                            x + bounds->xMax, 
                            y + bounds->yMin, 
                            z + bounds->zMax, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    trVertex = {
                        {
                            x + bounds->xMax, 
                            y + bounds->yMax, 
                            z + bounds->zMax, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    brVertex = {
                        {
                            x + bounds->xMax, 
                            y + bounds->yMax, 
                            z + bounds->zMin, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    blVertex = {
                        {
                            x + bounds->xMax, 
                            y + bounds->yMin, 
                            z + bounds->zMin, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    break;
                case TileDirection::West:
                    tlVertex = {
                        {
                            x + bounds->xMin, 
                            y + bounds->yMax, 
                            z + bounds->zMax, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    trVertex = {
                        {
                            x + bounds->xMin, 
                            y + bounds->yMin, 
                            z + bounds->zMax, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    brVertex = {
                        {
                            x + bounds->xMin, 
                            y + bounds->yMin, 
                            z + bounds->zMin, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    blVertex = {
                        {
                            x + bounds->xMin, 
                            y + bounds->yMax, 
                            z + bounds->zMin, 
                        },
                        blendTileLighting(light.tileIntensity), // Tile light
                        blendSkyLighting(light.skyIntensity), // Sky tint
                        sideTint, // Occlusion
                    };
                    break;
                default:
                    assert(false);
            }

            // TODO: We dont yet set the actual coords based on the size of anything. Once we do, we need to turn auto tex coords off

            renderer.outputSolidFace(face, subtile.type->textureOf(face), tlVertex, trVertex, brVertex, blVertex, true);
        }
    }
}

}