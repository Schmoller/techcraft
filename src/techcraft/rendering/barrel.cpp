#include "techcraft/rendering/tile/barrel.hpp"
#include "techcraft/tileentities/barrel.hpp"

#include <iostream>

namespace Techcraft::Rendering {

BarrelTileRenderer BarrelTileRenderer::inst{};
BarrelTileRenderer &BarrelTileRenderer::instance() {
    return inst;
}

void BarrelTileRenderer::init(Engine::RenderEngine &engine) {
    mesh = engine.createStaticMesh<Engine::Vertex>("barrel")
        .fromModel("assets/models/barrel.obj")
        .build();
    
    engine.createTexture("barrel")
        .fromFile("assets/textures/models/barrel.png")
        .build();
    
    material = engine.createMaterial({
        "barrel",
        "barrel",
        "",
        "*unused*",
        false,
        vk::Filter::eNearest,
        vk::Filter::eNearest,
    });
}

void BarrelTileRenderer::initTile(tcsize x, tcsize y, tcsize z, const Tile &tile, DynamicRenderContext<TileAnimationController> &context) {
    BarrelTileEntity *barrel = static_cast<BarrelTileEntity*>(tile.getTileEntity().get());
    std::cout << "Render barrel " << barrel << std::endl;
    // context.renderPart(0, mesh, material, Position{x, y, z});
    context.renderAnimation(0, mesh, material, Position{x, y, z}, std::make_shared<BarrelAnimator>(Position{x, y, z}, tile));
}

void BarrelTileRenderer::update(tcsize x, tcsize y, tcsize z, const Tile &tile, DynamicRenderContext<TileAnimationController> &context) {

}

// BarrelAnimator
BarrelAnimator::BarrelAnimator(const TilePosition &position, const Tile &tile)
    : TileAnimationController(position, tile)
{}

void BarrelAnimator::updateFrame(double deltaSeconds, double clientTime) {
    auto &barrel = getObject();

    barrel.setScale(sin(clientTime / 1) * 0.05 + 1);
}

}