#include "techcraft/rendering/dynamicrenderer.hpp"
#include "techcraft/rendering/utils.hpp"
#include "techcraft/rendering/tile/base.hpp"
#include "techcraft/lighting.hpp"
#include "techcraft/tiles/registry.hpp"
#include "techcraft/common/debug.hpp"

#include <thread>
#include <iostream>
#include <unordered_set>

#define OCCLUSION_VALUE 0.8f

namespace Techcraft {

// Forward declaration
void mergeVertex(glm::vec3 &outTileLight, glm::vec3 &outSkyTint, glm::vec3 &outOcclusion, const std::array<Tile, 7> tiles, const std::array<TileLighting, 7> lighting);

DynamicRegionRenderer::DynamicRegionRenderer(
    std::shared_ptr<Region> region,
    ObjectSubsystem *objectSubsystem,
    const TilePosition &offset
) : region(std::move(region)),
    objectSubsystem(objectSubsystem),
    offset(offset),
    emptyTile({TileTypes::Air}),
    emptyTileLight({}),
    animationTime(0)
{
}

DynamicRegionRenderer::~DynamicRegionRenderer() {
    // Cleanup all objects
    for (auto &statePair : tileState) {
        auto &state = statePair.second;
        for (auto &partPair : state.parts) {
            objectSubsystem->removeObject(partPair.second->getObjectId());
        }
    }
}

void DynamicRegionRenderer::addTile(const TilePosition &pos, const Tile &tile) {
    std::cout << "Add tile " << pos << " thread: " << std::this_thread::get_id() << std::endl;
    setTileForRendering(pos);

    Rendering::DynamicTileRenderer &renderer = static_cast<Rendering::DynamicTileRenderer &>(tile.type->getRenderer());

    renderer.initTile(pos.x + offset.x, pos.y + offset.y, pos.z + offset.z, tile, *this);
}

void DynamicRegionRenderer::removeTile(const TilePosition &pos, const Tile &tile) {
    std::cout << "Remove tile " << pos << " thread: " << std::this_thread::get_id() << std::endl;

    auto it = tileState.find(pos);
    if (it != tileState.end()) {
        auto &state = it->second;
        // Unrender everything
        for (auto &pair : state.parts) {
            // Remove animators first
            for (auto &aniPair : state.animationControllers) {
                auto controller = aniPair.second;

                auto contrIt = allAnimationControllers.begin();
                for (; contrIt != allAnimationControllers.end(); ++contrIt) {
                    if (*contrIt == controller.get()) {
                        allAnimationControllers.erase(contrIt);
                        break;
                    }
                }
            }

            objectSubsystem->removeObject(pair.second->getObjectId());
        }
        tileState.erase(it);
    }
}

void DynamicRegionRenderer::updateTile(const TilePosition &pos, const Tile &tile) {
    std::cout << "Update tile " << pos << " thread: " << std::this_thread::get_id() << std::endl;
    setTileForRendering(pos);

    Rendering::DynamicTileRenderer &renderer = static_cast<Rendering::DynamicTileRenderer &>(tile.type->getRenderer());

    renderer.update(pos.x + offset.x, pos.y + offset.y, pos.z + offset.z, tile, *this);

    for (auto part : currentTileState->parts) {
        part.second->setTileLight(tileLight);
        part.second->setSkyTint(skyTint);
        part.second->setOcclusion(occlusion);
    }
}

void DynamicRegionRenderer::render() {
    std::unordered_set<TilePosition> removed;

    for (auto &pair : tileState) {
        removed.insert(pair.first);
    }

    auto size = region->size();
    for (tcsize x = 0; x < size.width; ++x) {
        for (tcsize y = 0; y < size.depth; ++y) {
            for (tcsize z = 0; z < size.height; ++z) {
                TilePosition pos = {x,y,z};
                auto &tile = region->getTile(pos);

                if (!tile.type->getRenderer().isStatic()) {
                    removed.erase(pos);
                    auto it = tileState.find(pos);
                    if (it == tileState.end()) {
                        addTile(pos, tile);
                    } else {
                        updateTile(pos, tile);
                    }
                }
            }
        }
    }

    for (auto &pos : removed) {
        removeTile(pos, region->getTile(pos));
    }
}

void DynamicRegionRenderer::frameUpdate(double deltaTime) {
    // TODO: We dont really need to do this for every single dynamic region renderer, only ones with animations
    animationTime += deltaTime;

    for (auto &controller : allAnimationControllers) {
        controller->updateFrame(deltaTime, animationTime);
    }
}

void DynamicRegionRenderer::setTileForRendering(const TilePosition &pos) {
    currentTile = pos;
    auto it = tileState.find(pos);
    if (it == tileState.end()) {
        auto pair = tileState.emplace(pos, DynamicState{});
        currentTileState = &pair.first->second;
    } else {
        currentTileState = &it->second;
    }

    genLightCubes(currentTile.x, currentTile.y, currentTile.z, tileLight, skyTint, occlusion);
}

std::shared_ptr<Engine::Object> DynamicRegionRenderer::renderPart(uint32_t partID, const Engine::Mesh *mesh, const Engine::Material *material, const Position &position) {
    auto it = currentTileState->parts.find(partID);
    if (it != currentTileState->parts.end()) {
        // Just update instead
        auto &object = it->second;
        object->setMesh(mesh);
        object->setMaterial(material);
        object->setPosition(position);

        return object;
    }
    
    auto object = objectSubsystem->createObject()
        .withMesh(mesh)
        .withMaterial(material)
        .withPosition(position)
        .withTileLight(tileLight)
        .withSkyTint(skyTint)
        .withOcclusion(occlusion)
        .build();

    currentTileState->parts[partID] = object;
    return object;
}

void DynamicRegionRenderer::renderAnimation(uint32_t partID, const Engine::Mesh *mesh, const Engine::Material *material, const Position &position, std::shared_ptr<Rendering::TileAnimationController> animation) {
    // TODO: Upsert functionality. Animations make this much more complicated
    auto object = objectSubsystem->createObject()
        .withMesh(mesh)
        .withMaterial(material)
        .withPosition(position)
        .withTileLight(tileLight)
        .withSkyTint(skyTint)
        .withOcclusion(occlusion)
        .build();

    currentTileState->parts[partID] = object;

    if (!animation->isValid()) {
        allAnimationControllers.push_back(animation.get());
    }

    animation->acceptPart(partID, object);
    currentTileState->animationControllers[partID] = animation;
}

std::shared_ptr<Engine::Object> DynamicRegionRenderer::getPart(uint32_t partID) {
    auto it = currentTileState->parts.find(partID);
    if (it != currentTileState->parts.end()) {
        return it->second;
    } else {
        return nullptr;
    }
}

void DynamicRegionRenderer::unrender(uint32_t partID) {
    auto it = currentTileState->parts.find(partID);
    if (it != currentTileState->parts.end()) {
        // Clear animations if present
        auto aniIt = currentTileState->animationControllers.find(partID);
        if (aniIt != currentTileState->animationControllers.end()) {
            auto &controller = aniIt->second;
            controller->removePart(partID);

            if (!controller->isValid()) {
                // Also remove from the update list
                auto contrIt = allAnimationControllers.begin();
                for (; contrIt != allAnimationControllers.end(); ++contrIt) {
                    if (*contrIt == controller.get()) {
                        allAnimationControllers.erase(contrIt);
                        break;
                    }
                }
            }

            currentTileState->animationControllers.erase(aniIt);
        }

        objectSubsystem->removeObject(it->second->getObjectId());
        currentTileState->parts.erase(it);
    }
}

void DynamicRegionRenderer::link(std::shared_ptr<Region> region, TileDirection direction) {
    nextRegion[static_cast<int>(direction)] = std::move(region);
}

void DynamicRegionRenderer::unlink(TileDirection direction) {
    nextRegion[static_cast<int>(direction)] = nullptr;
}

void DynamicRegionRenderer::link(std::shared_ptr<Region>region, TileDirectionExtended direction) {
    nextRegion[static_cast<int>(direction)] = std::move(region);
}

void DynamicRegionRenderer::unlink(TileDirectionExtended direction) {
    nextRegion[static_cast<int>(direction)] = nullptr;
}


const Tile &DynamicRegionRenderer::getTile(int32_t x, int32_t y, int32_t z) const {
    int chunkX = 0;
    int chunkY = 0;
    int chunkZ = 0;
    if (x < 0) {
        // West
        chunkX = -1;
    } else if (x >= region->size().width) {
        // East
        chunkX = 1;
    }
    if (y < 0) {
        // South
        chunkY = -1;
    } else if (y >= region->size().depth) {
        // North
        chunkY = 1;
    }
    if (z < 0) {
        // Down
        chunkZ = -1;
    } else if (z >= region->size().height) {
        // Up
        chunkZ = 1;
    }

    TileDirectionExtended dir = toDirection(chunkX, chunkY, chunkZ);
    std::shared_ptr<Region> target;
    if (dir == TileDirectionExtended::Self) {
        target = region;
    } else {
        target = nextRegion[static_cast<int32_t>(dir)];
    }

    if (target) {
        if (x < 0) {
            // West
            x += target->size().width;
        } else if (x >= region->size().width) {
            // East
            x -= region->size().width;
        }
        if (y < 0) {
            // South
            y += target->size().depth;
        } else if (y >= region->size().depth) {
            // North
            y -= region->size().depth;
        }
        if (z < 0) {
            // Down
            z += target->size().height;
        } else if (z >= region->size().height) {
            // Up
            z -= region->size().height;
        }
        return target->getTile({x, y, z});
    }

    return emptyTile;
}

const TileLighting &DynamicRegionRenderer::getTileLight(int32_t x, int32_t y, int32_t z) const {
    int chunkX = 0;
    int chunkY = 0;
    int chunkZ = 0;
    if (x < 0) {
        // West
        chunkX = -1;
    } else if (x >= region->size().width) {
        // East
        chunkX = 1;
    }
    if (y < 0) {
        // South
        chunkY = -1;
    } else if (y >= region->size().depth) {
        // North
        chunkY = 1;
    }
    if (z < 0) {
        // Down
        chunkZ = -1;
    } else if (z >= region->size().height) {
        // Up
        chunkZ = 1;
    }

    TileDirectionExtended dir = toDirection(chunkX, chunkY, chunkZ);
    std::shared_ptr<Region> target;
    if (dir == TileDirectionExtended::Self) {
        target = region;
    } else {
        target = nextRegion[static_cast<int32_t>(dir)];
    }

    if (target) {
        if (x < 0) {
            // West
            x += target->size().width;
        } else if (x >= region->size().width) {
            // East
            x -= region->size().width;
        }
        if (y < 0) {
            // South
            y += target->size().depth;
        } else if (y >= region->size().depth) {
            // North
            y -= region->size().depth;
        }
        if (z < 0) {
            // Down
            z += target->size().height;
        } else if (z >= region->size().height) {
            // Up
            z -= region->size().height;
        }
        return target->getTileLight({x, y, z});
    }

    return emptyTileLight;
}

void DynamicRegionRenderer::genLightCubes(int32_t x, int32_t y, int32_t z, Engine::LightCube &tileLight, Engine::LightCube &skyTint, Engine::LightCube &occlusion) {
    // Tiles

    // Cardinal directions
    auto &tilepXYZ = getTile(x+1, y, z);
    auto &tileXpYZ = getTile(x, y+1, z);
    auto &tileXYpZ = getTile(x, y, z+1);
    auto &tilenXYZ = getTile(x-1, y, z);
    auto &tileXnYZ = getTile(x, y-1, z);
    auto &tileXYnZ = getTile(x, y, z-1);

    // Corners
    auto &tilepXpYpZ = getTile(x+1, y+1, z+1);
    auto &tilepXpYnZ = getTile(x+1, y+1, z-1);
    auto &tilepXnYpZ = getTile(x+1, y-1, z+1);
    auto &tilepXnYnZ = getTile(x+1, y-1, z-1);
    auto &tilenXpYpZ = getTile(x-1, y+1, z+1);
    auto &tilenXpYnZ = getTile(x-1, y+1, z-1);
    auto &tilenXnYpZ = getTile(x-1, y-1, z+1);
    auto &tilenXnYnZ = getTile(x-1, y-1, z-1);

    // Edges
    auto &tileXpYpZ = getTile(x, y+1, z+1);
    auto &tileXpYnZ = getTile(x, y+1, z-1);
    auto &tileXnYpZ = getTile(x, y-1, z+1);
    auto &tileXnYnZ = getTile(x, y-1, z-1);

    auto &tilepXYpZ = getTile(x+1, y, z+1);
    auto &tilepXYnZ = getTile(x+1, y, z-1);
    auto &tilenXYpZ = getTile(x-1, y, z+1);
    auto &tilenXYnZ = getTile(x-1, y, z-1);

    auto &tilepXpYZ = getTile(x+1, y+1, z);
    auto &tilepXnYZ = getTile(x+1, y-1, z);
    auto &tilenXpYZ = getTile(x-1, y+1, z);
    auto &tilenXnYZ = getTile(x-1, y-1, z);

    // Lighting

    // Cardinal directions
    auto &lightpXYZ = getTileLight(x+1, y, z);
    auto &lightXpYZ = getTileLight(x, y+1, z);
    auto &lightXYpZ = getTileLight(x, y, z+1);
    auto &lightnXYZ = getTileLight(x-1, y, z);
    auto &lightXnYZ = getTileLight(x, y-1, z);
    auto &lightXYnZ = getTileLight(x, y, z-1);

    // Corners
    auto &lightpXpYpZ = getTileLight(x+1, y+1, z+1);
    auto &lightpXpYnZ = getTileLight(x+1, y+1, z-1);
    auto &lightpXnYpZ = getTileLight(x+1, y-1, z+1);
    auto &lightpXnYnZ = getTileLight(x+1, y-1, z-1);
    auto &lightnXpYpZ = getTileLight(x-1, y+1, z+1);
    auto &lightnXpYnZ = getTileLight(x-1, y+1, z-1);
    auto &lightnXnYpZ = getTileLight(x-1, y-1, z+1);
    auto &lightnXnYnZ = getTileLight(x-1, y-1, z-1);

    // Edges
    auto &lightXpYpZ = getTileLight(x, y+1, z+1);
    auto &lightXpYnZ = getTileLight(x, y+1, z-1);
    auto &lightXnYpZ = getTileLight(x, y-1, z+1);
    auto &lightXnYnZ = getTileLight(x, y-1, z-1);

    auto &lightpXYpZ = getTileLight(x+1, y, z+1);
    auto &lightpXYnZ = getTileLight(x+1, y, z-1);
    auto &lightnXYpZ = getTileLight(x-1, y, z+1);
    auto &lightnXYnZ = getTileLight(x-1, y, z-1);

    auto &lightpXpYZ = getTileLight(x+1, y+1, z);
    auto &lightpXnYZ = getTileLight(x+1, y-1, z);
    auto &lightnXpYZ = getTileLight(x-1, y+1, z);
    auto &lightnXnYZ = getTileLight(x-1, y-1, z);

    // Build the cubes

    mergeVertex(
        tileLight.westSouthDown, skyTint.westSouthDown, occlusion.westSouthDown,
        {
            tilenXYZ,
            tileXnYZ,
            tileXYnZ,
            tilenXnYZ,
            tilenXYnZ,
            tileXnYnZ,
            tilenXnYnZ
        },
        {
            lightnXYZ,
            lightXnYZ,
            lightXYnZ,
            lightnXnYZ,
            lightnXYnZ,
            lightXnYnZ,
            lightnXnYnZ
        }
    );

    mergeVertex(
        tileLight.westSouthUp, skyTint.westSouthUp, occlusion.westSouthUp,
        {
            tilenXYZ,
            tileXnYZ,
            tileXYpZ,
            tilenXnYZ,
            tilenXYpZ,
            tileXnYpZ,
            tilenXnYpZ
        },
        {
            lightnXYZ,
            lightXnYZ,
            lightXYpZ,
            lightnXnYZ,
            lightnXYpZ,
            lightXnYpZ,
            lightnXnYpZ
        }
    );

    mergeVertex(
        tileLight.westNorthDown, skyTint.westNorthDown, occlusion.westNorthDown,
        {
            tilenXYZ,
            tileXpYZ,
            tileXYnZ,
            tilenXpYZ,
            tilenXYnZ,
            tileXpYnZ,
            tilenXpYnZ
        },
        {
            lightnXYZ,
            lightXpYZ,
            lightXYnZ,
            lightnXpYZ,
            lightnXYnZ,
            lightXpYnZ,
            lightnXpYnZ
        }
    );

    mergeVertex(
        tileLight.westNorthUp, skyTint.westNorthUp, occlusion.westNorthUp,
        {
            tilenXYZ,
            tileXpYZ,
            tileXYpZ,
            tilenXpYZ,
            tilenXYpZ,
            tileXpYpZ,
            tilenXpYpZ
        },
        {
            lightnXYZ,
            lightXpYZ,
            lightXYpZ,
            lightnXpYZ,
            lightnXYpZ,
            lightXpYpZ,
            lightnXpYpZ
        }
    );

    mergeVertex(
        tileLight.eastSouthDown, skyTint.eastSouthDown, occlusion.eastSouthDown,
        {
            tilepXYZ,
            tileXnYZ,
            tileXYnZ,
            tilepXnYZ,
            tilepXYnZ,
            tileXnYnZ,
            tilepXnYnZ
        },
        {
            lightpXYZ,
            lightXnYZ,
            lightXYnZ,
            lightpXnYZ,
            lightpXYnZ,
            lightXnYnZ,
            lightpXnYnZ
        }
    );

    mergeVertex(
        tileLight.eastSouthUp, skyTint.eastSouthUp, occlusion.eastSouthUp,
        {
            tilepXYZ,
            tileXnYZ,
            tileXYpZ,
            tilepXnYZ,
            tilepXYpZ,
            tileXnYpZ,
            tilepXnYpZ
        },
        {
            lightpXYZ,
            lightXnYZ,
            lightXYpZ,
            lightpXnYZ,
            lightpXYpZ,
            lightXnYpZ,
            lightpXnYpZ
        }
    );

    mergeVertex(
        tileLight.eastNorthDown, skyTint.eastNorthDown, occlusion.eastNorthDown,
        {
            tilepXYZ,
            tileXpYZ,
            tileXYnZ,
            tilepXpYZ,
            tilepXYnZ,
            tileXpYnZ,
            tilepXpYnZ
        },
        {
            lightpXYZ,
            lightXpYZ,
            lightXYnZ,
            lightpXpYZ,
            lightpXYnZ,
            lightXpYnZ,
            lightpXpYnZ
        }
    );

    mergeVertex(
        tileLight.eastNorthUp, skyTint.eastNorthUp, occlusion.eastNorthUp,
        {
            tilepXYZ,
            tileXpYZ,
            tileXYpZ,
            tilepXpYZ,
            tilepXYpZ,
            tileXpYpZ,
            tilepXpYpZ
        },
        {
            lightpXYZ,
            lightXpYZ,
            lightXYpZ,
            lightpXpYZ,
            lightpXYpZ,
            lightXpYpZ,
            lightpXpYpZ
        }
    );
}

void mergeVertex(glm::vec3 &outTileLight, glm::vec3 &outSkyTint, glm::vec3 &outOcclusion, const std::array<Tile, 7> tiles, const std::array<TileLighting, 7> lighting) {
    int count = 0;
    outTileLight = {0,0,0};
    outSkyTint = {0,0,0};
    outOcclusion = {1,1,1};

    bool occluded = false;

    for (auto i = 0; i < 7; ++i) {
        auto &tile = tiles[i];
        auto &light = lighting[i];

        if (tile.type->isTransparent() || tile.type->hasLight()) {
            outTileLight += blendTileLighting(light.tileIntensity);
            outSkyTint += blendSkyLighting(light.skyIntensity);

            ++count;
        }

        if (!tile.type->isTransparent()) {
            occluded = true;
        }
    }

    // FIXME: Occlusion isnt that good in this form. Some faces are dimmed even if they really shouldnt.
    if (occluded) {
        outOcclusion *= OCCLUSION_VALUE;
    }

    if (count > 0) {
        outTileLight /= static_cast<float>(count);
        outSkyTint /= static_cast<float>(count);
    }
}

}