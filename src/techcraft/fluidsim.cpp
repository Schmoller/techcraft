#include "techcraft/fluidsim.hpp"
#include "techcraft/dimension/dimension.hpp"
#include "utilities/profiler.hpp"
#include "techcraft/common/debug.hpp"

#include <iostream>
#include <unordered_map>
#include "techcraft/dimension/chunk.hpp"

namespace Techcraft {

const TileDirection FLUID_SPREAD_DIRECTIONS[4] = {
    TileDirection::East,
    TileDirection::West,
    TileDirection::North,
    TileDirection::South
};

const TileDirection FLUID_SOURCE_DIRECTIONS[5] = {
    TileDirection::Up,
    TileDirection::East,
    TileDirection::West,
    TileDirection::North,
    TileDirection::South
};

FluidSimulation::FluidSimulation(Dimension &dimension)
: dimension(dimension)
{}

void FluidSimulation::fluidStep() {
    ProfilerSection profiler("fluidStep");

    std::unordered_map<TilePosition, FluidTile> pendingUpdates;

    uint32_t active = 0;
    dimension.forEachLoadedChunk([&](const ChunkPosition &coord, const std::shared_ptr<Chunk> &chunk) {
        chunk->forEachActiveFluid(true, [&](const FluidTile &fluid, uint8_t x, uint8_t y, uint8_t z) {
            ++active;
            TilePosition pos = {
                coord.x << ChunkBits | x,
                coord.y << ChunkBits | y,
                coord.z << ChunkBits | z
            };

            if (!fluid.isEmpty()) {
                bool canTick = tickCount % fluid.getType()->getViscosity() == 0;
                if (!canTick) {
                    pendingUpdates[pos] = fluid;
                    return;
                }

                uint16_t supportedAmount = fluid.getAmount();
                if (fluid.isFlowing() && needsUpdate(pos, fluid, &supportedAmount)) {
                    if (supportedAmount == 0) {
                        pendingUpdates[pos] = FluidTile{};
                        return;
                    } else {
                        pendingUpdates[pos] = FluidTile{
                            fluid.getType(),
                            supportedAmount,
                            true
                        };
                    }
                }

                // Dont spread out if there is a space below
                auto downPos = pos + TileDirectionOffset::Down;

                const Tile &downTile = dimension.getTile(downPos);
                const FluidTile &downFluid = dimension.getFluid(downPos);

                bool handled = false;
                if (downTile.type->isFluidPassable() || downTile.type->isFluidReplaceable()) {
                    if (downFluid.isEmpty() || (downFluid.contains(fluid.getType()) && downFluid.isFlowing())) {
                        if (downFluid.getAmount() < fluid.getType()->getSpread()) {
                            // Spread down instead
                            pendingUpdates[downPos] = FluidTile{
                                fluid.getType(),
                                fluid.getType()->getSpread(),
                                true
                            };
                        }
                        handled = true;
                    }
                }

                if (!handled && supportedAmount > 1) {
                    // Attempt spread
                    for (auto dir : FLUID_SPREAD_DIRECTIONS) {
                        auto adjacentPos = pos + TileDirectionOffset::from(dir);

                        const Tile &adjacentTile = dimension.getTile(adjacentPos);
                        const FluidTile &adjacentFluid = dimension.getFluid(adjacentPos);

                        if (adjacentTile.type->isFluidPassable() || adjacentTile.type->isFluidReplaceable()) {
                            if (adjacentFluid.isEmpty()) {
                                pendingUpdates[adjacentPos] = FluidTile{
                                    fluid.getType(),
                                    supportedAmount - 1,
                                    true
                                };
                            } else if (adjacentFluid.contains(fluid.getType())) {
                                if (adjacentFluid.getAmount() < supportedAmount - 1) {
                                    if (adjacentFluid.isFlowing()) {
                                        pendingUpdates[adjacentPos] = FluidTile{
                                            fluid.getType(),
                                            supportedAmount - 1,
                                            true
                                        };
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (fluid.isEmpty()) {
                // It was probably removed
                auto downPos = pos + TileDirectionOffset::Down;

                const Tile &downTile = dimension.getTile(downPos);
                const FluidTile &downFluid = dimension.getFluid(downPos);

                bool handled = false;
                if (downTile.type->isFluidPassable() || downTile.type->isFluidReplaceable()) {
                    if (downFluid.isFlowing()) {
                        uint16_t supportedAmount;

                        if (needsUpdate(downPos, downFluid, &supportedAmount)) {
                            if (supportedAmount == 0) {
                                // Dry up
                                pendingUpdates[downPos] = FluidTile{};
                            } else {
                                pendingUpdates[downPos] = FluidTile{
                                    downFluid.getType(),
                                    supportedAmount,
                                    true
                                };
                            }
                        }

                        handled = true;
                    }
                }

                if (!handled) {
                    // Try to dry up others
                    for (auto dir : FLUID_SPREAD_DIRECTIONS) {
                        auto adjacentPos = pos + TileDirectionOffset::from(dir);

                        const Tile &adjacentTile = dimension.getTile(adjacentPos);
                        const FluidTile &adjacentFluid = dimension.getFluid(adjacentPos);

                        if (adjacentTile.type->isFluidPassable() || adjacentTile.type->isFluidReplaceable()) {
                            if (adjacentFluid.isFlowing()) {
                                uint16_t supportedAmount;

                                if (needsUpdate(adjacentPos, adjacentFluid, &supportedAmount)) {
                                    if (supportedAmount == 0) {
                                        // Dry up
                                        pendingUpdates[adjacentPos] = FluidTile{};
                                    } else {
                                        pendingUpdates[adjacentPos] = FluidTile{
                                            adjacentFluid.getType(),
                                            supportedAmount,
                                            true
                                        };
                                    }
                                }
                            } else {
                                // TODO: We probably want to allow the other type (if not empty) to spread if needed
                            }
                        }
                    }
                }
            }
        });
    });

    // Now apply the fluid
    for (auto &pair : pendingUpdates) {
        dimension.setFluid(pair.first, pair.second);
    }

    ++tickCount;
}

bool FluidSimulation::needsUpdate(const TilePosition &position, const FluidTile &fluid, uint16_t *newAmount) {
    bool supported = false;
    int supportedAmount = 0;

    for (auto dir : FLUID_SOURCE_DIRECTIONS) {
        auto adjacentPos = position + TileDirectionOffset::from(dir);

        const FluidTile &adjacentFluid = dimension.getFluid(adjacentPos);

        if (adjacentFluid.contains(fluid.getType())) {
            supported = true;
            if (dir == TileDirection::Up) {
                supportedAmount = fluid.getType()->getSpread();
                break;
            } else {
                supportedAmount = std::max(supportedAmount, adjacentFluid.getAmount() - 1);
            }
        }
    }

    if (supported) {
        if (supportedAmount != fluid.getAmount()) {
            *newAmount = supportedAmount;
            return true;
        }
    } else {
        *newAmount = 0;
        return true;
    }

    return false;
}

}