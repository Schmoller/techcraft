#include "techcraft/common/debug.hpp"

namespace Techcraft {

std::ostream &operator<<(std::ostream &os, const BoundingBox &box) {
    return os << "{min:{" << box.xMin << "," << box.yMin << "," << box.zMin << ", "
        << "max:{" << box.xMax << "," << box.yMax << "," << box.zMax << "}}";
}


std::string toString(TileDirection direction) {
    switch (direction) {
        case TileDirection::Up:
            return "Up";
        case TileDirection::Down:
            return "Down";
        case TileDirection::North:
            return "North";
        case TileDirection::South:
            return "South";
        case TileDirection::East:
            return "East";
        case TileDirection::West:
            return "West";
        case TileDirection::Self:
            return "Self";
    }
    return "unknown";
}
std::ostream &operator<<(std::ostream &os, const TileDirection &direction) {
    return os << toString(direction);
}

std::string toString(TileDirectionExtended direction) {
    switch (direction) {
        case TileDirectionExtended::Up:
            return "Up";
        case TileDirectionExtended::Down:
            return "Down";
        case TileDirectionExtended::North:
            return "North";
        case TileDirectionExtended::South:
            return "South";
        case TileDirectionExtended::East:
            return "East";
        case TileDirectionExtended::West:
            return "West";
        case TileDirectionExtended::NorthEast:
            return "NorthEast";
        case TileDirectionExtended::NorthWest:
            return "NorthWest";
        case TileDirectionExtended::SouthEast:
            return "SouthEast";
        case TileDirectionExtended::SouthWest:
            return "SouthWest";
        case TileDirectionExtended::UpEast:
            return "UpEast";
        case TileDirectionExtended::UpWest:
            return "UpWest";
        case TileDirectionExtended::UpNorth:
            return "UpNorth";
        case TileDirectionExtended::UpSouth:
            return "UpSouth";
        case TileDirectionExtended::UpNorthEast:
            return "UpNorthEast";
        case TileDirectionExtended::UpNorthWest:
            return "UpNorthWest";
        case TileDirectionExtended::UpSouthEast:
            return "UpSouthEast";
        case TileDirectionExtended::UpSouthWest:
            return "UpSouthWest";
        case TileDirectionExtended::DownEast:
            return "DownEast";
        case TileDirectionExtended::DownWest:
            return "DownWest";
        case TileDirectionExtended::DownNorth:
            return "DownNorth";
        case TileDirectionExtended::DownSouth:
            return "DownSouth";
        case TileDirectionExtended::DownNorthEast:
            return "DownNorthEast";
        case TileDirectionExtended::DownNorthWest:
            return "DownNorthWest";
        case TileDirectionExtended::DownSouthEast:
            return "DownSouthEast";
        case TileDirectionExtended::DownSouthWest:
            return "DownSouthWest";
        case TileDirectionExtended::Self:
            return "Self";
    }
    return "unknown";
}

std::ostream &operator<<(std::ostream &os, const TileDirectionExtended &direction) {
    return os << toString(direction);
}

std::ostream &operator<<(std::ostream &os, const TilePosition &pos) {
    return os << "{" << pos.x << "," << pos.y << "," << pos.z << "}";
}

std::ostream &operator<<(std::ostream &os, const Position &pos) {
    return os << "{" << pos.x << "," << pos.y << "," << pos.z << "}";
}

std::ostream &operator<<(std::ostream &os, const ChunkPosition &pos) {
    return os << "{" << pos.x << "," << pos.y << "," << pos.z << "}";
}

}