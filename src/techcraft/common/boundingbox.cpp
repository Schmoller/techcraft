#include "techcraft/common/boundingbox.hpp"

namespace Techcraft {


// BoundingBox
BoundingBox::BoundingBox(const Position &a, const Position &b) {
    xMin = std::min(a.x, b.x);
    yMin = std::min(a.y, b.y);
    zMin = std::min(a.z, b.z);
    xMax = std::max(a.x, b.x);
    yMax = std::max(a.y, b.y);
    zMax = std::max(a.z, b.z);
}

BoundingBox::BoundingBox(double xMin, double yMin, double zMin, double xMax, double yMax, double zMax)
: xMin(xMin), yMin(yMin), zMin(zMin), xMax(xMax), yMax(yMax), zMax(zMax)
{}

BoundingBox::BoundingBox(double width, double depth, double height, bool centered) {
    if (centered) {
        xMin = -width/2;
        yMin = -depth/2;
        zMin = -height/2;
        xMax = width/2;
        yMax = depth/2;
        zMax = height/2;
    } else {
        xMin = 0;
        yMin = 0;
        zMin = 0;
        xMax = width;
        yMax = depth;
        zMax = height;
    }
}

bool BoundingBox::operator==(const BoundingBox &other) const {
    return (
        xMin == other.xMin &&
        yMin == other.yMin &&
        zMin == other.zMin &&
        xMax == other.xMax &&
        yMax == other.yMax &&
        zMax == other.zMax
    );
}

BoundingBox BoundingBox::operator+(const Position &position) const {
    return {
        xMin + position.x,
        yMin + position.y,
        zMin + position.z,
        xMax + position.x,
        yMax + position.y,
        zMax + position.z
    };
}

BoundingBox BoundingBox::operator-(const Position &position) const {
    return {
        xMin - position.x,
        yMin - position.y,
        zMin - position.z,
        xMax - position.x,
        yMax - position.y,
        zMax - position.z
    };
}

BoundingBox &BoundingBox::operator+=(const Position &position) {
    xMin += position.x;
    yMin += position.y;
    zMin += position.z;
    xMax += position.x;
    yMax += position.y;
    zMax += position.z;

    return *this;
}

BoundingBox &BoundingBox::operator-=(const Position &position) {
    xMin -= position.x;
    yMin -= position.y;
    zMin -= position.z;
    xMax -= position.x;
    yMax -= position.y;
    zMax -= position.z;

    return *this;
}

BoundingBox &BoundingBox::offsetX(double x) {
    xMin += x;
    xMax += x;
    return *this;
}
BoundingBox &BoundingBox::offsetY(double y) {
    yMin += y;
    yMax += y;
    return *this;
}
BoundingBox &BoundingBox::offsetZ(double z) {
    zMin += z;
    zMax += z;
    return *this;
}

BoundingBox BoundingBox::include(const Position &position) const {
    return {
        std::min(xMin, static_cast<double>(position.x)),
        std::min(yMin, static_cast<double>(position.y)),
        std::min(zMin, static_cast<double>(position.z)),
        std::max(xMax, static_cast<double>(position.x)),
        std::max(yMax, static_cast<double>(position.y)),
        std::max(zMax, static_cast<double>(position.z))
    };
}

BoundingBox &BoundingBox::includeSelf(const Position &position) {
    xMin = std::min(xMin, static_cast<double>(position.x));
    yMin = std::min(yMin, static_cast<double>(position.y));
    zMin = std::min(zMin, static_cast<double>(position.z));
    xMax = std::max(xMax, static_cast<double>(position.x));
    yMax = std::max(yMax, static_cast<double>(position.y));
    zMax = std::max(zMax, static_cast<double>(position.z));
    return *this;
}

BoundingBox BoundingBox::expand(double x, double y, double z) const {
    return {
        xMin - x,
        yMin - y,
        zMin - z,
        xMax + x,
        yMax + y,
        zMax + z
    };
}

BoundingBox BoundingBox::expand(double all) const {
    return expand(all, all, all);
}

BoundingBox BoundingBox::expandSkew(double x, double y, double z) const {
    return {
        (x < 0 ? xMin + x : xMin),
        (y < 0 ? yMin + y : yMin),
        (z < 0 ? zMin + z : zMin),
        (x > 0 ? xMax + x : xMax),
        (y > 0 ? yMax + y : yMax),
        (z > 0 ? zMax + z : zMax)
    };
}
BoundingBox &BoundingBox::expandSkewSelf(double x, double y, double z) {
    if (x > 0) {
        xMax += x;
    } else {
        xMin += x;
    }
    if (y > 0) {
        yMax += y;
    } else {
        yMin += y;
    }
    if (z > 0) {
        zMax += z;
    } else {
        zMin += z;
    }

    return *this;
}
BoundingBox BoundingBox::shrinkSkew(double x, double y, double z) const {
    return {
        (x > 0 ? xMin + x : xMin),
        (y > 0 ? yMin + y : yMin),
        (z > 0 ? zMin + z : zMin),
        (x < 0 ? xMax + x : xMax),
        (y < 0 ? yMax + y : yMax),
        (z < 0 ? zMax + z : zMax)
    };
}
BoundingBox &BoundingBox::shrinkSkewSelf(double x, double y, double z) {
    if (x < 0) {
        xMax += x;
    } else {
        xMin += x;
    }
    if (y < 0) {
        yMax += y;
    } else {
        yMin += y;
    }
    if (z < 0) {
        zMax += z;
    } else {
        zMin += z;
    }

    return *this;
}

bool BoundingBox::contains(const BoundingBox &other) const {
    return (
        xMin <= other.xMin && other.xMax <= xMax &&
        yMin <= other.yMin && other.yMax <= yMax &&
        zMin <= other.zMin && other.zMax <= zMax
    );
}
bool BoundingBox::contains(const Position &point) const {
    return (
        xMin <= point.x && point.x <= xMax &&
        yMin <= point.y && point.y <= yMax &&
        zMin <= point.z && point.z <= zMax
    );
}

bool BoundingBox::intersects(const BoundingBox &other) const {
    return (
        xMin < other.xMax && xMax > other.xMin &&
        yMin < other.yMax && yMax > other.yMin &&
        zMin < other.zMax && zMax > other.zMin
    );
}

bool BoundingBox::intersectedBy(const Position &rayOrigin, const Position &rayDirection, TileDirection &outHitFace, double *outDistance) {
    glm::vec3 fractionalDir = 1.0f / static_cast<glm::vec3>(rayDirection);
    
    double tXMin = (xMin - rayOrigin.x) * fractionalDir.x;
    double tXMax = (xMax - rayOrigin.x) * fractionalDir.x;
    double tYMin = (yMin - rayOrigin.y) * fractionalDir.y;
    double tYMax = (yMax - rayOrigin.y) * fractionalDir.y;
    double tZMin = (zMin - rayOrigin.z) * fractionalDir.z;
    double tZMax = (zMax - rayOrigin.z) * fractionalDir.z;

    double tX, tY, tZ;
    tX = std::min(tXMin, tXMax);
    tY = std::min(tYMin, tYMax);
    tZ = std::min(tZMin, tZMax);

    double tClosePlane = std::max(std::max(tX, tY), tZ);
    double tFarPlane = std::min(std::min(std::max(tXMin, tXMax), std::max(tYMin, tYMax)), std::max(tZMin, tZMax));

    // Hit exists behind origin
    if (tFarPlane < 0) {
        return false;
    }

    // Hit not present within the AABB
    if (tClosePlane > tFarPlane) {
        return false;
    }

    // Determine hit dir
    if (tX == tClosePlane) {
        if (rayDirection.x > 0) {
            outHitFace = TileDirection::West;
        } else {
            outHitFace = TileDirection::East;
        }
    } else if (tY == tClosePlane) {
        if (rayDirection.y > 0) {
            outHitFace = TileDirection::South;
        } else {
            outHitFace = TileDirection::North;
        }
    } else if (tZ == tClosePlane) {
        if (rayDirection.z > 0) {
            outHitFace = TileDirection::Down;
        } else {
            outHitFace = TileDirection::Up;
        }
    }

    if (outDistance) {
        *outDistance = tClosePlane;
    }

    return true;
}

Position BoundingBox::center() const {
    return {
        (xMin + xMax) / 2.0f,
        (yMin + yMax) / 2.0f,
        (zMin + zMax) / 2.0f
    };
}

double BoundingBox::width() const {
    return (xMax - xMin);
}
double BoundingBox::depth() const {
    return (yMax - yMin);
}
double BoundingBox::height() const {
    return (zMax - zMin);
}

}