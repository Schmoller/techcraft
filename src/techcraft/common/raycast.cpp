#include "techcraft/common/raycast.hpp"

#include "techcraft/dimension/dimension.hpp"
#include "techcraft/tiles/registry.hpp"

namespace Techcraft {

// RaycastResult
RaycastResult::RaycastResult(
    const TilePosition &tilePosition,
    const Position &position,
    const Tile *tile,
    const FluidTile *fluid,
    const Subtile *subtile,
    HitType type,
    TileDirection direction,
    const SubtileSlot *slot
) : hitTilePosition(tilePosition),
    hitPosition(position),
    hitResult(type),
    hitTile(tile),
    hitFluid(fluid),
    hitSubtile(subtile),
    hitFace(direction),
    hitSlot(slot)
{
}

Position RaycastResult::relativePosition() const {
    std::optional<BoundingBox> bounds = this->bounds();
    
    if (bounds) {
        return hitPosition - Position{bounds->xMin, bounds->yMin, bounds->zMin} - hitTilePosition;
    } else {
        return hitPosition - hitTilePosition;
    }
}

std::optional<BoundingBox> RaycastResult::bounds() const {
    switch (hitResult) {
        case HitType::Tile:
            return hitTile->getBounds();
            break;
        case HitType::Subtile:
            return hitSubtile->type->getBounds(*hitSlot);
            break;
        case HitType::Fluid:
            return hitFluid->getBounds();
            break;
        case HitType::Entity:
            // TODO
            assert(false);
            break;
        default:
            // Unhandled hit type
            assert(false);
            break;
    }

    return {};
}

// Raycast
Raycast::Raycast(const Position &start, const Position &end)
    : start(start), end(end)
{
    direction = glm::normalize(glm::vec3(end - start));
}

Raycast::Raycast(const Position &start, const Position &direction, float maxRange)
    : start(start), direction(direction)
{
    end = start + direction * maxRange;
}

Raycast &Raycast::onlyTiles() {
    allowTiles = true;
    allowFluid = false;
    allowEntities = false;
    allowSubtiles = false;

    return *this;
}

Raycast &Raycast::onlyFluid() {
    allowTiles = false;
    allowFluid = true;
    allowEntities = false;
    allowSubtiles = false;

    return *this;
}

Raycast &Raycast::onlySubtiles() {
    allowTiles = false;
    allowFluid = false;
    allowEntities = false;
    allowSubtiles = true;

    return *this;
}

Raycast &Raycast::onlyEntities() {
    allowTiles = false;
    allowFluid = false;
    allowEntities = true;
    allowSubtiles = false;

    return *this;
}

Raycast &Raycast::ignoreTiles() {
    allowTiles = false;
    
    return *this;
}

Raycast &Raycast::ignoreFluid() {
    allowFluid = false;

    return *this;
}

Raycast &Raycast::ignoreSubtiles() {
    allowSubtiles = false;

    return *this;
}

Raycast &Raycast::ignoreEntities() {
    allowEntities = false;

    return *this;
}

Raycast &Raycast::ignore(const TileType *type) {
    ignoredTileTypes.insert(type);

    return *this;
}

Raycast &Raycast::ignore(const FluidType *type) {
    ignoredFluidTypes.insert(type);

    return *this;
}

Raycast &Raycast::ignore(const SubtileType *type) {
    ignoredSubtileTypes.insert(type);

    return *this;
}

RaycastResult Raycast::cast(Dimension &dimension) {
    // This will perform the cast with the Bresenhams line drawing algorithm
    TilePosition endTile = end;
    
    TilePosition current = start;

    auto stepX = endTile.x > current.x ? 1 : endTile.x < current.x ? -1 : 0;
    auto stepY = endTile.y > current.y ? 1 : endTile.y < current.y ? -1 : 0;
    auto stepZ = endTile.z > current.z ? 1 : endTile.z < current.z ? -1 : 0;

    //Planes for each axis that we will next cross
    auto gxp = current.x + (endTile.x > current.x ? 1 : 0);
    auto gyp = current.y + (endTile.y > current.y ? 1 : 0);
    auto gzp = current.z + (endTile.z > current.z ? 1 : 0);

    //Only used for multiplying up the error margins
    auto deltaX = end.x == start.x ? 1 : end.x - start.x;
    auto deltaY = end.y == start.y ? 1 : end.y - start.y;
    auto deltaZ = end.z == start.z ? 1 : end.z - start.z;

    auto deltaXY = deltaX * deltaY;
    auto deltaXZ = deltaX * deltaZ;
    auto deltaYZ = deltaY * deltaZ;

    auto errX = (gxp - start.x) * deltaYZ;
    auto errY = (gyp - start.y) * deltaXZ;
    auto errZ = (gzp - start.z) * deltaXY;

    auto derrx = stepX * deltaYZ;
    auto derry = stepY * deltaXZ;
    auto derrz = stepZ * deltaXY;

    TileDirection face = TileDirection::Self;

    do {
        if (allowTiles || allowSubtiles) {
            auto hit = testTile(dimension, current, face);
            if (hit) {
                return *hit;
            }
        }
        if (allowFluid) {
            auto fluid = dimension.getFluid(current);

            if (fluid.getType() && ignoredFluidTypes.count(fluid.getType()) == 0) {
                auto bounds = fluid.getBounds();

                if (bounds) {
                    auto adjBounds = *bounds + current;
                    double dist;
                
                    if (adjBounds.intersectedBy(start, direction, face, &dist)) {
                        // We have a hit. Yay
                        return {
                            current,
                            start + direction * dist,
                            &dimension.getTile(current),
                            &fluid,
                            nullptr, 
                            HitType::Fluid,
                            face
                        };
                    }
                }
            }
        }

        if (current == endTile) {
            break;
        }

        //Which plane do we cross first?
        auto xr = abs(errX);
        auto yr = abs(errY);
        auto zr = abs(errZ);

        if (stepX != 0 && (stepY == 0 || xr < yr) && (stepZ == 0 || xr < zr)) {
            current.x += stepX;
            errX += derrx;

            if (stepX == 1) {
                face = TileDirection::West;
            } else {
                face = TileDirection::East;
            }
        }
        else if (stepY != 0 && (stepZ == 0 || yr < zr)) {
            current.y += stepY;
            errY += derry;

            if (stepY == 1) {
                face = TileDirection::South;
            } else {
                face = TileDirection::North;
            }
        }
        else if (stepZ != 0) {
            current.z += stepZ;
            errZ += derrz;

            if (stepZ == 1) {
                face = TileDirection::Down;
            } else {
                face = TileDirection::Up;
            }
        }

    } while (true);

    // No hit
    return {
        current,
        current,
        &dimension.getTile(current),
        &dimension.getFluid(current),
        nullptr, 
        HitType::Nothing
    };
}

std::optional<RaycastResult> Raycast::testTile(Dimension &dimension, const TilePosition &current, TileDirection face) {
    auto tile = dimension.getTile(current);
    TileDirection outFace;

    if (tile.type != TileTypes::Air && ignoredTileTypes.count(tile.type) == 0) {
        double bestDistance;
        const SubtileSlot *bestSlot = nullptr;
        const Subtile *best = nullptr;

        if (allowSubtiles) {
            auto container = tile.getSubtileContainer();
            if (container) {
                container->forEach([&](auto &slot, auto &subtile) {
                    if (!subtile.type) {
                        return;
                    }

                    auto bounds = subtile.type->getBounds(slot);
                    if (bounds) {
                        auto adjBounds = *bounds + current;
                        double outDist;
                    
                        if (adjBounds.intersectedBy(start, direction, outFace, &outDist)) {
                            // We have a hit. Yay
                            if (!best || outDist < bestDistance) {
                                best = &subtile;
                                bestSlot = &slot;
                                bestDistance = outDist;
                            }
                        }
                    }
                });
            }
        }

        if (allowTiles) {
            auto bounds = tile.getBounds();
            
            if (bounds) {
                auto adjBounds = *bounds + current;
                double outDist;
            
                if (adjBounds.intersectedBy(start, direction, outFace, &outDist)) {
                    // If there is a subtile hit and its better, then use that instead
                    if (best && outDist > bestDistance) {
                        return RaycastResult{
                            current,
                            start + direction * bestDistance,
                            &dimension.getTile(current),
                            &dimension.getFluid(current),
                            best, 
                            HitType::Subtile,
                            outFace,
                            bestSlot
                        };
                    } else {
                        // Tile hit wins
                        return RaycastResult{
                            current,
                            start + direction * outDist,
                            &dimension.getTile(current),
                            &dimension.getFluid(current),
                            nullptr, 
                            HitType::Tile,
                            outFace
                        };
                    }
                }
            }
        }

        // Tile hit didnt win. If there was a subtile hit, use that
        if (best) {
            return RaycastResult{
                current,
                start + direction * bestDistance,
                &dimension.getTile(current),
                &dimension.getFluid(current),
                best, 
                HitType::Subtile,
                outFace,
                bestSlot
            };
        }
    }

    return {};
}

}