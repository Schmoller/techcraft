#include "techcraft/common/direction.hpp"

namespace Techcraft {

TileDirection opposite(TileDirection direction) {
    switch (direction) {
        case TileDirection::Up:
            return TileDirection::Down;
        case TileDirection::Down:
            return TileDirection::Up;
        case TileDirection::North:
            return TileDirection::South;
        case TileDirection::South:
            return TileDirection::North;
        case TileDirection::East:
            return TileDirection::West;
        case TileDirection::West:
            return TileDirection::East;
        case TileDirection::Self:
        default:
            return TileDirection::Self;
    }
}

TileDirectionExtended opposite(TileDirectionExtended direction) {
    switch (direction) {
        case TileDirectionExtended::Up:
            return TileDirectionExtended::Down;
        case TileDirectionExtended::Down:
            return TileDirectionExtended::Up;
        case TileDirectionExtended::North:
            return TileDirectionExtended::South;
        case TileDirectionExtended::South:
            return TileDirectionExtended::North;
        case TileDirectionExtended::East:
            return TileDirectionExtended::West;
        case TileDirectionExtended::West:
            return TileDirectionExtended::East;
        case TileDirectionExtended::NorthEast:
            return TileDirectionExtended::SouthWest;
        case TileDirectionExtended::NorthWest:
            return TileDirectionExtended::SouthEast;
        case TileDirectionExtended::SouthEast:
            return TileDirectionExtended::NorthWest;
        case TileDirectionExtended::SouthWest:
            return TileDirectionExtended::NorthEast;
        case TileDirectionExtended::UpEast:
            return TileDirectionExtended::DownWest;
        case TileDirectionExtended::UpWest:
            return TileDirectionExtended::DownEast;
        case TileDirectionExtended::UpNorth:
            return TileDirectionExtended::DownSouth;
        case TileDirectionExtended::UpSouth:
            return TileDirectionExtended::DownNorth;
        case TileDirectionExtended::UpNorthEast:
            return TileDirectionExtended::DownSouthWest;
        case TileDirectionExtended::UpNorthWest:
            return TileDirectionExtended::DownSouthEast;
        case TileDirectionExtended::UpSouthEast:
            return TileDirectionExtended::DownNorthWest;
        case TileDirectionExtended::UpSouthWest:
            return TileDirectionExtended::DownNorthEast;
        case TileDirectionExtended::DownEast:
            return TileDirectionExtended::UpWest;
        case TileDirectionExtended::DownWest:
            return TileDirectionExtended::UpEast;
        case TileDirectionExtended::DownNorth:
            return TileDirectionExtended::UpSouth;
        case TileDirectionExtended::DownSouth:
            return TileDirectionExtended::UpNorth;
        case TileDirectionExtended::DownNorthEast:
            return TileDirectionExtended::UpSouthWest;
        case TileDirectionExtended::DownNorthWest:
            return TileDirectionExtended::UpSouthEast;
        case TileDirectionExtended::DownSouthEast:
            return TileDirectionExtended::UpNorthWest;
        case TileDirectionExtended::DownSouthWest:
            return TileDirectionExtended::UpNorthEast;
        case TileDirectionExtended::Self:
        default:
            return TileDirectionExtended::Self;
    }
}

// Packed as z + y * 3 + x * 9
const TileDirectionExtended OFFSET_TO_DIR[27] = {
    TileDirectionExtended::DownSouthWest,
    TileDirectionExtended::SouthWest,
    TileDirectionExtended::UpSouthWest,
    TileDirectionExtended::DownWest,
    TileDirectionExtended::West,
    TileDirectionExtended::UpWest,
    TileDirectionExtended::DownNorthWest,
    TileDirectionExtended::NorthWest,
    TileDirectionExtended::UpNorthWest,
    TileDirectionExtended::DownSouth,
    TileDirectionExtended::South,
    TileDirectionExtended::UpSouth,
    TileDirectionExtended::Down,
    TileDirectionExtended::Self,
    TileDirectionExtended::Up,
    TileDirectionExtended::DownNorth,
    TileDirectionExtended::North,
    TileDirectionExtended::UpNorth,
    TileDirectionExtended::DownSouthEast,
    TileDirectionExtended::SouthEast,
    TileDirectionExtended::UpSouthEast,
    TileDirectionExtended::DownEast,
    TileDirectionExtended::East,
    TileDirectionExtended::UpEast,
    TileDirectionExtended::DownNorthEast,
    TileDirectionExtended::NorthEast,
    TileDirectionExtended::UpNorthEast
};

template <typename T>
int sign(T val) {
    return (T(0) < val) - (val < T(0));
}

TileDirectionExtended toDirection(int x, int y, int z) {
    return OFFSET_TO_DIR[
        (sign(z) + 1) + 
        (sign(y) + 1) * 3 + 
        (sign(x) + 1) * 9
    ];
}

}