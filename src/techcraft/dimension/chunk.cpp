#include "techcraft/dimension/chunk.hpp"
#include "techcraft/dimension/column.hpp"
#include "techcraft/dimension/dimension.hpp"

#include <strings.h>

namespace Techcraft {

// Utilities
inline size_t packCoord(uint8_t x, uint8_t y, uint8_t z) {
    return (
        (x & ChunkMask) + (y & ChunkMask) * ChunkSize + (z & ChunkMask) * (ChunkArea)
    );
}

inline size_t packCoord(uint8_t x, uint8_t y) {
    return (
        (x & ChunkMask) + (y & ChunkMask) * ChunkSize
    );
}

// Chunk
Chunk::Chunk(const ChunkPosition &coord, const Tile &initialFill)
    : coord(coord), column(nullptr)
{
    std::fill_n(tilesByXYZ, ChunkVolume, initialFill);
    std::fill_n(fluidByXYZ, ChunkVolume, FluidTile{});
    std::fill_n(lightingByXYZ, ChunkVolume, TileLighting{});
    std::fill_n(highestByXY, ChunkArea, NOT_PRESENT);
}

void Chunk::onAcceptIntoDimension(std::shared_ptr<Column> column, Dimension *dimension) {
    this->column = std::move(column);
    this->dimension = dimension;

    // Find any tile entities and register them
    for (auto &tileEntity : allTileEntities) {
        tileEntity->registerInto(dimension);
    }
}

void Chunk::setTile(uint8_t x, uint8_t y, uint8_t z, const Tile &tile) {
    auto &previous = tilesByXYZ[packCoord(x, y, z)];

    auto existingEntity = previous.getTileEntity();

    if (existingEntity) {
        std::remove(allTileEntities.begin(), allTileEntities.end(), existingEntity);
    }

    tilesByXYZ[packCoord(x, y, z)] = tile;

    auto entity = tile.getTileEntity();
    if (!entity) {
        entity = tile.type->createTileEntity(
            {
                coord.x << ChunkBits | x,
                coord.y << ChunkBits | y,
                coord.z << ChunkBits | z
            }
        );
    }

    if (entity) {
        std::shared_ptr<Tickable> asTickable = std::dynamic_pointer_cast<Tickable>(entity);
        if (asTickable) {
            tickableTileEntities.push_back(asTickable);
        }
        if (dimension) {
            entity->registerInto(dimension);
        }
        tilesByXYZ[packCoord(x, y, z)].data = entity;
        allTileEntities.push_back(entity);
    }

    int8_t top = getTop(x, y);
    if (top == z) {
        if (tile.isEmpty()) {
            bool columnHighest = column && column->getTop(x, y) == (static_cast<tcsize>(coord.z << ChunkBits) | top);

            // Find a new top lower down
            top = static_cast<int8_t>(z) - 1_i8;
            for (; top >= 0; --top) {
                if (!tilesByXYZ[packCoord(x, y, top)].isEmpty()) {
                    break;
                }
            }
            highestByXY[packCoord(x, y)] = top;

            if (columnHighest) {
                if (top < 0) {
                    column->updateHighestFail(x, y, coord.z);
                } else {
                    column->updateHighest(x, y, static_cast<tcsize>(coord.z << ChunkBits) | top);
                }
            }
        }
    } else if (top < z) {
        if (!tile.isEmpty()) {
            bool columnHighest = column && column->getTop(x, y) < (static_cast<tcsize>(coord.z << ChunkBits) | z);
            // This is the new top
            highestByXY[packCoord(x, y)] = z;
            if (columnHighest) {
                column->updateHighest(x, y, static_cast<tcsize>(coord.z << ChunkBits) | z);
            }
        }
    }
    markDirty();
}

const Tile &Chunk::getTile(uint8_t x, uint8_t y, uint8_t z) const {
    return tilesByXYZ[packCoord(x, y, z)];
}

void Chunk::setTile(const TilePosition& location, const Tile &tile) {
    setTile(
        static_cast<uint8_t>(location.x),
        static_cast<uint8_t>(location.y),
        static_cast<uint8_t>(location.z),
        tile
    );
}
const Tile &Chunk::getTile(const TilePosition& location) const {
    return getTile(
        static_cast<uint8_t>(location.x),
        static_cast<uint8_t>(location.y),
        static_cast<uint8_t>(location.z)
    );
}

void Chunk::setFluid(uint8_t x, uint8_t y, uint8_t z, const FluidTile &fluid) {
    size_t index = packCoord(x, y, z);
    fluidByXYZ[index] = fluid;

    activeFluidTiles.set(index);

    // TODO: Top calculation
}
const FluidTile &Chunk::getFluid(uint8_t x, uint8_t y, uint8_t z) const {
    return fluidByXYZ[packCoord(x, y, z)];
}

void Chunk::setFluid(const TilePosition& location, const FluidTile &fluid) {
    setFluid(
        static_cast<uint8_t>(location.x),
        static_cast<uint8_t>(location.y),
        static_cast<uint8_t>(location.z),
        fluid
    );
}

const FluidTile &Chunk::getFluid(const TilePosition& location) const {
    return getFluid(
        static_cast<uint8_t>(location.x),
        static_cast<uint8_t>(location.y),
        static_cast<uint8_t>(location.z)
    );
}

void Chunk::markFluidActive(uint8_t x, uint8_t y, uint8_t z) {
    size_t index = packCoord(x, y, z);
    if (!fluidByXYZ[index].isEmpty()) {
        activeFluidTiles.set(index);
    }
}

void Chunk::setTileLight(uint8_t x, uint8_t y, uint8_t z, const TileLighting &light) {
    lightingByXYZ[packCoord(x, y, z)] = light;
}
const TileLighting &Chunk::getTileLight(uint8_t x, uint8_t y, uint8_t z) const {
    return lightingByXYZ[packCoord(x, y, z)];
}

void Chunk::setTileLight(const TilePosition &location, const TileLighting &light) {
    setTileLight(
        static_cast<uint8_t>(location.x),
        static_cast<uint8_t>(location.y),
        static_cast<uint8_t>(location.z),
        light
    );
}
const TileLighting &Chunk::getTileLight(const TilePosition &location) const {
    return getTileLight(
        static_cast<uint8_t>(location.x),
        static_cast<uint8_t>(location.y),
        static_cast<uint8_t>(location.z)
    );
}

void Chunk::setSubtile(const TilePosition& location, const SubtileSlot &slot, const Subtile &subtile) {
    // TODO: This is never going to be implemented. See below for why
}

const Subtile &Chunk::getSubtile(const TilePosition& location, const SubtileSlot &slot) const {
    // TODO: This is duplicating the logic from dimension. This shouldnt be here, but
    // this is required because the renderers only render based on regions
    // eventually I will remove the regions which will make it much quicker due to removal
    // of virtual calls
    auto &tile = tilesByXYZ[packCoord(
        static_cast<uint8_t>(location.x),
        static_cast<uint8_t>(location.y),
        static_cast<uint8_t>(location.z)
    )];

    auto container = tile.getSubtileContainer();
    if (!container) {
        return {};
    }

    return container->getSubtile(slot);
}

void Chunk::markDirty() {
    dirty = true;
    if (dirtyNotifier) {
        dirtyNotifier();
    }
}
void Chunk::markClean() {
    dirty = false;
}

void Chunk::updateDirtyNotifier(DirtyCallback callback) {
    dirtyNotifier = callback;
}

int8_t Chunk::getTop(uint8_t x, uint8_t y) {
    return highestByXY[packCoord(x, y)];
}

void Chunk::doTick() {
    auto it = tickableTileEntities.begin();
    while (it != tickableTileEntities.end()) {
        auto &ptr = *it;
        auto tickable = ptr.lock();

        if (!tickable) {
            it = tickableTileEntities.erase(it);
        } else {
            tickable->doTick();

            ++it;
        }
    }
}

void Chunk::forEachActiveFluid(bool clear, const std::function<void(const FluidTile &, uint8_t x, uint8_t y, uint8_t z)> &callback) {
    activeFluidTiles.forEachSet([&] (size_t index) {
        uint8_t x = (index >> 0) & ChunkMask;
        uint8_t y = (index >> 4) & ChunkMask;
        uint8_t z = (index >> 8) & ChunkMask;

        callback(fluidByXYZ[index], x, y, z);
    });

    if (clear) {
        activeFluidTiles.clear();
    }
}

bool Chunk::operator==(const Chunk &other) const {
    return coord == other.coord;
}

}