#include "techcraft/dimension/column.hpp"
#include "techcraft/dimension/chunk.hpp"
#include "techcraft/dimension/dimension.hpp"

#include <limits>
#include <strings.h>

namespace Techcraft {

// Utilities

inline size_t packCoord(uint8_t x, uint8_t y) {
    return (
        (x & ChunkMask) + (y & ChunkMask) * ChunkSize
    );
}

// Column
Column::Column(ChunkCoordRes x, ChunkCoordRes y, LightingUpdater &lighting)
    : columnX(x), columnY(y), lighting(lighting)
{
    std::fill_n(highestByXY, ChunkArea, std::numeric_limits<tcsize>::min());
}

tcsize Column::getTop(uint8_t x, uint8_t y) const {
    return highestByXY[packCoord(x, y)];
}

void Column::updateHighestFail(uint8_t x, uint8_t y, ChunkCoordRes chunkZ) {
    auto startZ = std::min(firstZ + static_cast<ChunkCoordRes>(verticalChunks.size()), chunkZ - 1);
    for (auto z = startZ; z >= firstZ; --z) {
        auto chunk = verticalChunks[z - firstZ];
        if (chunk) {
            auto top = chunk->getTop(x, y);
            if (top != NOT_PRESENT) {
                tcsize oldTop = highestByXY[packCoord(x, y)];
                tcsize newTop = z << ChunkBits | top;
                highestByXY[packCoord(x, y)] = newTop;

                if (oldTop != std::numeric_limits<tcsize>::min()) {
                    if (oldTop > newTop) {
                        lighting.recalculateSkyForColumn(columnX << ChunkBits | x, columnY << ChunkBits | y, newTop, oldTop, false);
                    } else if (oldTop < newTop) {
                        lighting.recalculateSkyForColumn(columnX << ChunkBits | x, columnY << ChunkBits | y, oldTop, newTop, true);
                    }
                }
                return;
            }
        }
    }

    highestByXY[packCoord(x, y)] = std::numeric_limits<tcsize>::min();
}

void Column::updateHighest(uint8_t x, uint8_t y, tcsize newTop) {
    tcsize oldTop = highestByXY[packCoord(x, y)];
    highestByXY[packCoord(x, y)] = newTop;

    if (oldTop != std::numeric_limits<tcsize>::min()) {
        if (oldTop > newTop) {
            lighting.recalculateSkyForColumn(columnX << ChunkBits | x, columnY << ChunkBits | y, newTop, oldTop, false);
        } else if (oldTop < newTop) {
            lighting.recalculateSkyForColumn(columnX << ChunkBits | x, columnY << ChunkBits | y, oldTop, newTop, true);
        }
    }
}

void Column::addChunk(const std::shared_ptr<Chunk> &chunk) {
    auto newZ = chunk->getPosition().z;
    if (verticalChunks.empty()) {
        firstZ = newZ;
        verticalChunks.push_back(chunk);
        return;
    }

    ChunkCoordRes lastZ = firstZ + static_cast<ChunkCoordRes>(verticalChunks.size()) - 1_tcc; // inclusive
    if (newZ < firstZ) {
        auto padding = firstZ - newZ - 1; // Ignore the chunk at firstZ and the new chunk
        for (; padding > 0; --padding) {
            verticalChunks.push_front(nullptr);
        }
        verticalChunks.push_front(chunk);
        firstZ = newZ;

    } else if (newZ > lastZ) {
        auto padding = newZ - lastZ - 1; // Ignore the chunk at lastZ and the new chunk
        for (; padding > 0; --padding) {
            verticalChunks.push_back(nullptr);
        }
        verticalChunks.push_back(chunk);

    } else {
        verticalChunks[newZ - firstZ] = chunk;
    }

    // Check the highests
    tcsize offset = newZ << ChunkBits;
    for (int i = 0; i < ChunkArea; ++i) {
        if (chunk->highestByXY[i] != NOT_PRESENT) {
            tcsize highest = offset | chunk->highestByXY[i];
            if (highest > highestByXY[i]) {
                highestByXY[i] = highest;
            }
        }
    }

}

void Column::removeChunk(const std::shared_ptr<Chunk> &chunk) {
    auto oldZ = chunk->getPosition().z;

    if (verticalChunks.empty()) {
        return;
    }

    ChunkCoordRes lastZ = firstZ + static_cast<ChunkCoordRes>(verticalChunks.size());
    if (oldZ == firstZ) {
        do {
            verticalChunks.pop_front();
            ++firstZ;
        } while (!verticalChunks.empty() && !verticalChunks.front());

    } else if (oldZ == lastZ) {
        do {
            verticalChunks.pop_back();
        } while (!verticalChunks.empty() && !verticalChunks.back());

    } else {
        verticalChunks[oldZ - firstZ] = nullptr;
    }
}

bool Column::isEmpty() const {
    return verticalChunks.empty();
}

}