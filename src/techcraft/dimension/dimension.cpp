#include "techcraft/dimension/dimension.hpp"
#include "techcraft/tiles/registry.hpp"
#include "techcraft/dimension/chunk.hpp"
#include "techcraft/dimension/column.hpp"
#include <iostream>

namespace Techcraft {

// Utilities
inline uint32_t packChunkCoord(ChunkCoordRes x, ChunkCoordRes y) {
    return (
        ((static_cast<uint32_t>(y) & 0xFFFF) << 16) |
        ((static_cast<uint32_t>(x) & 0xFFFF) << 0)
    );
}

inline int64_t rescale(int64_t coord) {
    constexpr int64_t shift = 1 << (OctreeMaxDepth - 1);
    return coord + shift;
}

// Dimension
Dimension::Dimension(
    uint32_t id,
    std::shared_ptr<Generation::ChunkGenerator> generator,
    Utilities::WorkerFleet &workers,
    uint16_t heightChunks,
    uint16_t chunkDistanceLimit
) : id(id),
    maxHeightChunks(heightChunks),
    maxChunkCoord(chunkDistanceLimit),
    voidTile({
        TileTypes::Air
    }),
    voidFluid{},
    voidSubtile({}),
    chunkLoader(*this, voidTile, generator, workers),
    lighting(*this),
    physicsSim(*this),
    fluidSim(*this),
    playerChunkLoadRange(5)
{}

Dimension::~Dimension() {
    // Deallocate all columns
    for (auto pair : chunkCoordToColumn) {
        pair.second.reset();
    }

    chunkCoordToColumn.clear();
}

uint32_t Dimension::getId() const {
    return id;
}

ChunkLoader &Dimension::getChunkLoader() {
    return chunkLoader;
}
LightingUpdater &Dimension::getLightingUpdater() {
    return lighting;
}
PhysicsSimulation &Dimension::getPhysicsSimulation() {
    return physicsSim;
}

// Events
Dimension::EventHandlerDirty &Dimension::eventChunkDirty() {
    return dirtyEvent;
}
Dimension::ChunkLoadedEvent &Dimension::getChunkLoadedEvent() {
    return chunkLoadedEvent;
}
Dimension::ChunkUnloadedEvent &Dimension::getChunkUnloadedEvent() {
    return chunkUnloadedEvent;
}
Dimension::EntityAddEvent &Dimension::getEntityAddEvent() {
    return entityAddEvent;
}
Dimension::EntityRemoveEvent &Dimension::getEntityRemoveEvent() {
    return entityRemoveEvent;
}
Dimension::TileAddEvent &Dimension::getTileAddEvent() {
    return tileAddEvent;
}
Dimension::TileRemoveEvent &Dimension::getTileRemoveEvent() {
    return tileRemoveEvent;
}
Dimension::TileUpdateEvent &Dimension::getTileUpdateEvent() {
    return tileUpdateEvent;
}

std::shared_ptr<Chunk> Dimension::getChunkAt(const TilePosition &pos) const {
    auto result = chunks.find(rescale(pos.x >> ChunkBits), rescale(pos.y >> ChunkBits), rescale(pos.z >> ChunkBits));
    if (result) {
        return *result;
    } else {
        return {};
    }
}

std::shared_ptr<Chunk> Dimension::getChunkAt(tcsize x, tcsize y, tcsize z) const {
    auto result = chunks.find(rescale(x >> ChunkBits), rescale(y >> ChunkBits), rescale(z >> ChunkBits));
    if (result) {
        return *result;
    } else {
        return {};
    }
}

std::shared_ptr<Chunk> Dimension::getChunk(ChunkCoordRes x, ChunkCoordRes y, ChunkCoordRes z) const {
    auto result = chunks.find(rescale(x), rescale(y), rescale(z));
    if (result) {
        return *result;
    } else {
        return {};
    }
}

const Tile &Dimension::getTile(const TilePosition &location) const {
    auto chunk = getChunkAt(location);

    if (chunk) {
        return chunk->getTile(
            static_cast<uint8_t>(location.x & ChunkMask),
            static_cast<uint8_t>(location.y & ChunkMask),
            static_cast<uint8_t>(location.z & ChunkMask)
        );
    }

    return voidTile;
}

const Tile &Dimension::getTile(tcsize x, tcsize y, tcsize z) const {
    auto chunk = getChunkAt(x, y, z);

    if (chunk) {
        return chunk->getTile(
            static_cast<uint8_t>(x & ChunkMask),
            static_cast<uint8_t>(y & ChunkMask),
            static_cast<uint8_t>(z & ChunkMask)
        );
    }

    return voidTile;
}

bool Dimension::setTile(const TilePosition &location, const Tile &tile) {
    auto chunk = getChunkAt(location);

    if (chunk) {
        auto previous = chunk->getTile(
            static_cast<uint8_t>(location.x & ChunkMask),
            static_cast<uint8_t>(location.y & ChunkMask),
            static_cast<uint8_t>(location.z & ChunkMask)
        );

        if (previous.type != tile.type) {
            if (previous.getTileEntity()) {
                previous.getTileEntity()->onRemove();
            }
            tileRemoveEvent.send(location, previous);
        }

        auto highest = getTop(location);

        chunk->setTile(
            static_cast<uint8_t>(location.x & ChunkMask),
            static_cast<uint8_t>(location.y & ChunkMask),
            static_cast<uint8_t>(location.z & ChunkMask),
            tile
        );

        if (!tile.type->isFluidPassable() && !tile.type->isFluidReplaceable()) {
            auto &fluid = getFluid(location);
            if (!fluid.isEmpty()) {
                setFluid(location, {});
            }
        } else if (!previous.type->isFluidPassable() && !previous.type->isFluidReplaceable()) {
            // Give neighbour fluids an update so it can do what it needs to do
            for (auto dir : AllDirections) {
                auto adjPos = location + TileDirectionOffset::from(dir);
                auto adjChunk = getChunkAt(adjPos);
                if (adjChunk) {
                    adjChunk->markFluidActive(
                        static_cast<uint8_t>(adjPos.x & ChunkMask),
                        static_cast<uint8_t>(adjPos.y & ChunkMask),
                        static_cast<uint8_t>(adjPos.z & ChunkMask)
                    );
                }
            }
        }

        if (previous.type != tile.type) {
            tileAddEvent.send(location, tile);
            auto &newTile = chunk->getTile(location);
            if (newTile.getTileEntity()) {
                newTile.getTileEntity()->onPlace();
            }
        } else {
            tileUpdateEvent.send(location, tile);
        }

        notifyNeighboursOfUpdate(location);
        lighting.recalculateLightingFromTile(location, highest);

        return true;
    }

    return false;
}

const FluidTile &Dimension::getFluid(const TilePosition &location) const {
    auto chunk = getChunkAt(location);

    if (chunk) {
        return chunk->getFluid(
            static_cast<uint8_t>(location.x & ChunkMask),
            static_cast<uint8_t>(location.y & ChunkMask),
            static_cast<uint8_t>(location.z & ChunkMask)
        );
    }

    return voidFluid;
}

const FluidTile &Dimension::getFluid(tcsize x, tcsize y, tcsize z) const {
    auto chunk = getChunkAt(x, y, z);

    if (chunk) {
        return chunk->getFluid(
            static_cast<uint8_t>(x & ChunkMask),
            static_cast<uint8_t>(y & ChunkMask),
            static_cast<uint8_t>(z & ChunkMask)
        );
    }

    return voidFluid;
}

bool Dimension::setFluid(const TilePosition &location, const FluidTile &fluid) {
    auto chunk = getChunkAt(location);

    if (chunk) {
        auto previous = chunk->getFluid(
            static_cast<uint8_t>(location.x & ChunkMask),
            static_cast<uint8_t>(location.y & ChunkMask),
            static_cast<uint8_t>(location.z & ChunkMask)
        );

        if (!previous.contains(fluid.getType())) {
            // tileRemoveEvent.send(location, previous);
        }

        auto highest = getTop(location);

        chunk->setFluid(
            static_cast<uint8_t>(location.x & ChunkMask),
            static_cast<uint8_t>(location.y & ChunkMask),
            static_cast<uint8_t>(location.z & ChunkMask),
            fluid
        );

        // Give neighbours an update so it can do what it needs to do
        for (auto dir : AllDirections) {
            auto adjPos = location + TileDirectionOffset::from(dir);
            auto adjChunk = getChunkAt(adjPos);
            if (adjChunk) {
                adjChunk->markFluidActive(
                    static_cast<uint8_t>(adjPos.x & ChunkMask),
                    static_cast<uint8_t>(adjPos.y & ChunkMask),
                    static_cast<uint8_t>(adjPos.z & ChunkMask)
                );
            }
        }

        if (!previous.contains(fluid.getType())) {
            // tileAddEvent.send(location, fluid);
        } else {
            // tileUpdateEvent.send(location, fluid);
        }

        lighting.recalculateLightingFromTile(location, highest);

        return true;
    }

    return false;
}

/**
 * Retrieves a subtile within the given tile location and slot.
 * If there is no container at that location or the slot doesnt exist,
 * the return value will be a blank subtile
 */
const Subtile &Dimension::getSubtile(const TilePosition &location, const SubtileSlot &slot) const {
    auto &tile = getTile(location);

    auto container = tile.getSubtileContainer();
    if (!container) {
        return voidSubtile;
    }

    return container->getSubtile(slot);
}

/**
 * Sets a subtile within the given tile location and slot.
 * If the location does not have a container or no container can be made, then SubtileResponse::NoContainer will be returned.
 * If the slot is not valid for the given container, then SubtileResponse::NoSlot will be returned.
 * If there is already a subtile in the given slot and location, then SubtileResponse::Blocked will be returned.
 */
SubtileResponse Dimension::setSubtile(const TilePosition &location, const SubtileSlot &slot, const Subtile &subtile) {
    auto &tile = getTile(location);

    auto container = tile.getSubtileContainer();
    bool coerced = false;
    if (!container) {
        container = tile.type->coerseIntoSubtileContainer(location, tile);

        if (!container) {
            return SubtileResponse::NoContainer;
        }

        coerced = true;
    }

    if (!container->hasSlot(slot)) {
        return SubtileResponse::NoSlot;
    }

    auto existing = container->getSubtile(slot);

    // TODO: Tile entities etc.

    container->setSubtile(slot, subtile);

    if (coerced) {
        setTile(location, {
            TileTypes::InternalSubtileContainer,
            container
        });
    } else {
        // Forces updates of lighting, etc.
        setTile(location, tile);
    }

    return SubtileResponse::Success;
}

std::optional<TilePosition> Dimension::getTop(const TilePosition &location) {
    return getTop(location.x, location.y);
}

std::optional<TilePosition> Dimension::getTop(tcsize x, tcsize y) {
    auto it = chunkCoordToColumn.find(packChunkCoord(x >> ChunkBits, y >> ChunkBits));
    if (it != chunkCoordToColumn.end()) {
        auto column = it->second;
        auto height = column->getTop(x & ChunkMask, y & ChunkMask);
        if (height == std::numeric_limits<tcsize>::min()) {
            return {};
        }

        return TilePosition{x, y, height};
    }

    return {};
}

void Dimension::acceptChunk(const ChunkPosition &coord, const std::shared_ptr<Chunk> &chunk) {
    chunks.put(rescale(coord.x), rescale(coord.y), rescale(coord.z), chunk);
    // Chunk is now owned by Dimension

    auto column = getOrMakeColumn(coord);
    column->addChunk(chunk);
    chunk->onAcceptIntoDimension(column, this);

    // install callbacks
    chunk->updateDirtyNotifier(
        [this, coord]() {
            dirtyEvent.send(coord);
        }
    );

    // Let all listeners know of the new chunk
    chunkLoadedEvent.send(coord, chunk);

    #ifdef DEBUG_CHUNKLOAD
    std::cout << "Accepted chunk " << coord.x << "," << coord.y << "," << coord.z << std::endl;
    #endif

    // Check if any entities were falling within this chunk space. If so, push them to the next air block up

    // TODO: It would help if there was a flag which indicated that its falling in unloaded space. This way it handles more neatly when chunks load, and handles world borders.
    // FIXME: Change this later
    for (auto &pair : entities) {
        auto &entity = pair.second;
        ChunkPosition entityChunk = entity->position;
        if (entityChunk.x == coord.x && entityChunk.y == coord.y && entityChunk.z == coord.z) {
            auto &pos = entity->getPosition();
            auto top = getTop(pos);
            // std::cout << "Entity " << entity->getID() << " was in space" << std::endl;

            if (top) {
                // std::cout << "Top is at " << top->x << "," << top->y << "," << top->z << std::endl;
                for (tcsize z = pos.z; z <= top->z + 1; ++z) {
                    if (getTile(pos.x, pos.y, z).isEmpty() && getTile(pos.x, pos.y, z + 1).isEmpty()) {
                        // std::cout << "Air space at " << pos.x << "," << pos.y << "," << z << std::endl;
                        entity->setPosition(pos.x, pos.y, z + 0.01);
                        break;
                    }
                }
            }
        }
    }
}

std::shared_ptr<Chunk> Dimension::disownChunk(const ChunkPosition &coord) {
    auto optionalChunk = chunks.find(rescale(coord.x), rescale(coord.y), rescale(coord.z));
    if (!optionalChunk) {
        return {};
    }

    auto chunk = *optionalChunk;

    auto column = getOrMakeColumn(coord);
    column->removeChunk(chunk);

    chunkUnloadedEvent.send(coord, chunk);

    chunk->updateDirtyNotifier({});
    chunks.remove(rescale(coord.x), rescale(coord.y), rescale(coord.z));

    if (column->isEmpty()) {
        column.reset();
        chunkCoordToColumn.erase(packChunkCoord(coord.x, coord.y));
    }

    // Chunk is now disowned from Dimension
    #ifdef DEBUG_CHUNKLOAD
    std::cout << "Disowned chunk " << coord.x << "," << coord.y << "," << coord.z << std::endl;
    #endif
    return chunk;
}

std::shared_ptr<Column> Dimension::getOrMakeColumn(const ChunkPosition &coord) {
    auto columnCoord = packChunkCoord(coord.x, coord.y);

    std::shared_ptr<Column> column;

    auto it = chunkCoordToColumn.find(columnCoord);
    if (it == chunkCoordToColumn.end()) {
        column = std::make_shared<Column>(coord.x, coord.y, lighting);
        chunkCoordToColumn[columnCoord] = column;
    } else {
        column = it->second;
    }

    return column;
}

EntityID Dimension::addEntity(std::unique_ptr<Entity> &&entity, const Position &position) {
    entity->takeOwnership(this, nextEntityId++);

    Entity *entityPtr = entity.get();
    entity->setPosition(position);

    entities[entity->getID()] = std::move(entity);

    entityAddEvent.send(entityPtr);
    physicsSim.onEntityAdd(entityPtr);

    // Special behaviours
    auto chunkTicketingBehaviour = dynamic_cast<ChunkTicketing*>(entityPtr);
    if (chunkTicketingBehaviour) {
        entityChunkTicketers[entityPtr->getID()] = std::make_unique<AreaTicketer>(
            chunkLoader,
            chunkTicketingBehaviour->getTicketAgentId(),
            chunkTicketingBehaviour->getTicketReason(),
            playerChunkLoadRange // TODO: Other types might want their own ranges at some point
        );
    }

    return entityPtr->getID();
}

Entity *Dimension::getEntity(EntityID id) {
    auto it = entities.find(id);
    if (it != entities.end()) {
        return it->second.get();
    }

    return nullptr;
}

void Dimension::removeEntity(EntityID id) {
    auto it = entities.find(id);
    if (it != entities.end()) {
        auto *entity = it->second.get();

        // Special behaviours
        auto chunkTicketingBehaviour = dynamic_cast<ChunkTicketing*>(entity);
        if (chunkTicketingBehaviour) {
            entityChunkTicketers.erase(entity->getID());
            chunkLoader.removeAllFromAgent(chunkTicketingBehaviour->getTicketAgentId(), chunkTicketingBehaviour->getTicketReason());
        }

        entityRemoveEvent.send(entity);
        physicsSim.onEntityRemove(entity);

        entities.erase(it);
    }
}

void Dimension::transferEntity(EntityID id, Dimension &owner, const Position &target) {
    // TODO: Entitiy transfer between dimensions
}

void Dimension::doTick() {
    chunkLoader.update();
    lighting.update();
    fluidSim.fluidStep();

    // TODO: Entity updates
    // Chunk ticking

    chunks.forEach([](const std::shared_ptr<Chunk> &chunk) {
        chunk->doTick();
    });

    // Update ticketing
    for (auto &pair : entityChunkTicketers) {
        auto *entity = getEntity(pair.first);
        if (entity) {
            pair.second->updatePosition(entity->getPosition());
        }
    }
}

void Dimension::doUpdate() {
    physicsSim.physicsStep();
}

void Dimension::notifyNeighboursOfUpdate(const TilePosition &pos) {
    for (auto dir : AllDirections) {
        auto newPos = pos + TileDirectionOffset::from(dir);
        auto &tile = getTile(newPos);

        if (tile.getTileEntity()) {
            tile.getTileEntity()->onNeighbourUpdate(pos, tile);
        }
    }
}

void Dimension::forEachLoadedChunk(const std::function<void(const ChunkPosition &, const std::shared_ptr<Chunk> &)> &callback) {
    chunks.forEach([&callback](const std::shared_ptr<Chunk> &chunk) {
        callback(chunk->getPosition(), chunk);
    });
}

void Dimension::setChunkLoadDistance(ChunkCoordRes distance) {
    playerChunkLoadRange = distance;

    for (auto &ticketer : entityChunkTicketers) {
        ticketer.second->setViewRange(distance);
    }
}
ChunkCoordRes Dimension::getChunkLoadDistance() const {
    return playerChunkLoadRange;
}

}