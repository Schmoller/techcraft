#include "techcraft/dimension/chunkloader.hpp"
#include "techcraft/dimension/dimension.hpp"
#include "techcraft/common/debug.hpp"

#include "utilities/profiler.hpp"

#include <iostream>
#include "techcraft/dimension/chunk.hpp"

namespace Techcraft {

// ChunkLoader
ChunkLoader::ChunkLoader(
    Dimension &dimension,
    const Tile &voidTile,
    std::shared_ptr<Generation::ChunkGenerator> chunkGenerator,
    Utilities::WorkerFleet &workers
) : dimension(dimension),
    voidTile(voidTile),
    chunkGenerator(std::move(chunkGenerator)),
    workers(workers)
{
    // Dont query actual value, because it is not set at this point
    auto dist = 12;
    auto viewVolume = dist * dist * dist;
    chunkTickets.reserve(viewVolume * 10);
    activeTickets.reserve(viewVolume * 10);
}

ChunkLoader::~ChunkLoader() {
}

/**
 * Adds a request to have a chunk loaded.
 * 
 * @param coord The coordinates of the chunk to load
 * @param cause The cause of the request
 */
void ChunkLoader::addRequest(const ChunkPosition &coord, const ChunkLoadCause &cause) {
    ProfilerSection profiler("CL addRequest1");

    auto ticket = LoadTicket{
        coord,
        cause,
        {}
    };

    auto result = activeTickets.insert(ticket);

    if (!result.second) {
        // Duplicate ticket
        return;
    }

    auto it = chunkTickets.find(coord);
    if (it == chunkTickets.end()) {
        auto iteratorAndResult = chunkTickets.emplace(coord, std::allocate_shared<ChunkLoadState>(chunkStateAllocator));
        if (!iteratorAndResult.second) {
            return;
        }

        it = iteratorAndResult.first;
        it->second->coord = coord;
        queueChunkLoad(coord);
    }

    auto &state = it->second;
    state->tickets.push_back(ticket);
    state->unloadIfEmptyAt = {};
}

/**
 * Adds a request to have a chunk loaded.
 * 
 * @param coord The coordinates of the chunk to load
 * @param cause The cause of the request
 * @param agent The agent behind the request
 */
void ChunkLoader::addRequest(const ChunkPosition &coord, const ChunkLoadCause &cause, AgentType agent) {
    auto ticket = LoadTicket{
        coord,
        cause,
        {agent}
    };

    auto result = activeTickets.insert(ticket);

    if (!result.second) {
        // Duplicate ticket
        return;
    }

    auto it = chunkTickets.find(coord);
    if (it == chunkTickets.end()) {
        auto iteratorAndResult = chunkTickets.emplace(coord, std::allocate_shared<ChunkLoadState>(chunkStateAllocator));
        if (!iteratorAndResult.second) {
            return;
        }

        it = iteratorAndResult.first;
        it->second->coord = coord;
        queueChunkLoad(coord);
    }

    auto &state = it->second;
    state->tickets.push_back(ticket);
    state->unloadIfEmptyAt = {};
}

/**
 * Removes a load request.
 * If there are no other requests for this chunk, then it will be unloaded at some point.
 * 
 * @param coord The coordinates of the chunk to unload
 * @param cause The cause being rescinded.
 */
void ChunkLoader::removeRequest(const ChunkPosition &coord, const ChunkLoadCause &cause) {
    ProfilerSection profiler("CL removeRequest");

    auto chunkIterator = chunkTickets.find(coord);
    if (chunkIterator == chunkTickets.end()) {
        return;
    }

    auto &state = chunkIterator->second;
    for (auto ticket = state->tickets.begin(); ticket != state->tickets.end(); ++ticket) {
        if (ticket->cause != cause || ticket->agent) {
            continue;
        }

        activeTickets.erase(*ticket);
        state->tickets.erase(ticket);

        if (state->tickets.empty()) {
            state->unloadIfEmptyAt = std::chrono::system_clock::now() + unloadGracePeriod;
            chunksToCheck.push_back(state);
        }

        break;
    }
}

/**
 * Removes a load request.
 * If there are no other requests for this chunk, then it will be unloaded at some point.
 * 
 * @param coord The coordinates of the chunk to unload
 * @param cause The cause being rescinded.
 * @param agent The agent behind the request
 */
void ChunkLoader::removeRequest(const ChunkPosition &coord, const ChunkLoadCause &cause, AgentType agent) {
    auto chunkIterator = chunkTickets.find(coord);
    if (chunkIterator == chunkTickets.end()) {
        return;
    }

    auto &state = chunkIterator->second;
    for (auto ticket = state->tickets.begin(); ticket != state->tickets.end(); ++ticket) {
        if (ticket->cause != cause || !ticket->agent || ticket->agent.value() != agent) {
            continue;
        }

        activeTickets.erase(*ticket);
        state->tickets.erase(ticket);

        if (state->tickets.empty()) {
            state->unloadIfEmptyAt = std::chrono::system_clock::now() + unloadGracePeriod;
            chunksToCheck.push_back(state);
        }

        break;
    }
}

/**
 * Removes all requests by the given agent.
 * Chunks will only be unloaded if there are no other requests for that chunk.
 *
 * @param agent The agent behind the requests.
 */
void ChunkLoader::removeAllFromAgent(AgentType agent) {
    ProfilerSection profiler("CL removeAllFromAgent1");

    for (auto chunkState = chunkTickets.begin(); chunkState != chunkTickets.end(); ++chunkState) {
        auto &state = chunkState->second;
        for (auto ticket = state->tickets.begin(); ticket != state->tickets.end(); ++ticket) {
            if (!ticket->agent || ticket->agent.value() != agent) {
                continue;
            }

            activeTickets.erase(*ticket);
            state->tickets.erase(ticket);

            if (state->tickets.empty()) {
                state->unloadIfEmptyAt = std::chrono::system_clock::now() + unloadGracePeriod;
                chunksToCheck.push_back(state);
            }

            break;
        }
    }
}

/**
 * Removes all requests by the given agent which have the given cause.
 * 
 * @param agent The agent behind the requests.
 * @param cause The cause to match
 */
void ChunkLoader::removeAllFromAgent(AgentType agent, ChunkLoadCause cause) {
    ProfilerSection profiler("CL removeAllFromAgent2");

    for (auto chunkState = chunkTickets.begin(); chunkState != chunkTickets.end(); ++chunkState) {
        auto &state = chunkState->second;
        for (auto ticket = state->tickets.begin() ; ticket != state->tickets.end() ; ++ticket) {
            if (ticket->cause != cause || !ticket->agent || ticket->agent.value() != agent) {
                continue;
            }

            activeTickets.erase(*ticket);
            state->tickets.erase(ticket);

            if (state->tickets.empty()) {
                state->unloadIfEmptyAt = std::chrono::system_clock::now() + unloadGracePeriod;
                chunksToCheck.push_back(state);
            }

            break;
        }
    }
}

void ChunkLoader::update() {
    ProfilerSection profiler("CL update");
    processChunkQueue();

    // This exists so that adding chunks into the dimension does not occur across thread boundaries.

    auto loaded = loadedChunks.capture();
    for (auto pair : loaded) {
        if (chunkTickets.count(pair.first) > 0) {
            dimension.acceptChunk(pair.first, pair.second);
        } else {
            queueChunkUnload(pair.first, pair.second);
        }
    }

    processUnloadRequests();
}

void ChunkLoader::queueChunkLoad(const ChunkPosition &coord) {
    // This must be created in main thread. It has no synchronisation
    dimension.getOrMakeColumn(coord);

    chunksToLoad.push_back(coord);
}

void ChunkLoader::queueChunkUnload(const ChunkPosition &coord, const std::shared_ptr<Chunk> &chunk) {
    workers.submit([=, this]() {
        unloadChunk(coord, chunk);
    });
}

void ChunkLoader::processChunkQueue() {
    constexpr size_t maxBatchedChunks = 16;

    while (!chunksToLoad.empty()) {
        std::vector<ChunkPosition> batch;
        batch.reserve(maxBatchedChunks);

        auto batchSize = std::min(maxBatchedChunks, chunksToLoad.size());

        for (auto i = 0; i < batchSize; ++i) {
            batch.push_back(chunksToLoad.front());
            chunksToLoad.pop_front();
        }

        workers.submit(
            [this, batch = std::move(batch)]() {
                for (auto &coord : batch) {
                    loadOrGenerateChunk(coord);
                }
            }
        );
    }
}

void ChunkLoader::processUnloadRequests() {
    auto now = std::chrono::system_clock::now();

    while (!chunksToCheck.empty()) {
        auto &state = chunksToCheck.front();
        if (!state->tickets.empty()) {
            chunksToCheck.pop_front();
            state->unloadIfEmptyAt = {};
            continue;
        }

        assert (state->unloadIfEmptyAt);

        if (now < *state->unloadIfEmptyAt) {
            break;
        }

        auto it = chunkTickets.find(state->coord);
        if (it == chunkTickets.end()) {
            // Chunk that was going to unload, got ticketed again, then got marked for unload again
            chunksToCheck.pop_front();
            continue;
        }
        assert(it != chunkTickets.end());

        chunkTickets.erase(it);

        auto chunk = dimension.disownChunk(state->coord);
        if (chunk) {
            queueChunkUnload(state->coord, chunk);
        }

        chunksToCheck.pop_front();
    }
}

// Misc
std::shared_ptr<Chunk> ChunkLoader::allocateChunk(const ChunkPosition &coord) {
    return std::make_shared<Chunk>(coord, voidTile);
}

void ChunkLoader::loadOrGenerateChunk(const ChunkPosition &coord) {
    auto chunk = allocateChunk(coord);
    
    // TODO: Chunk loading
    chunkGenerator->populate(coord, chunk);

    chunk->setReady();

    loadedChunks.pushBack(std::pair(coord, chunk));
    #ifdef DEBUG_CHUNKLOAD
    std::cout << "Loaded chunk " << coord << std::endl;
    #endif
}

void ChunkLoader::unloadChunk(const ChunkPosition &coord, const std::shared_ptr<Chunk> &chunk) {
    // TODO: Might need to do some extra cleanup at some point, or maybe saving?

    #ifdef DEBUG_CHUNKLOAD
    std::cout << "Unloaded chunk " << coord << std::endl;
    #endif
}


// PlayerViewTicketer
AreaTicketer::AreaTicketer(
    ChunkLoader &chunkLoader,
    AgentType agentId,
    ChunkLoadCause cause,
    ChunkCoordRes viewRange
) : chunkLoader(chunkLoader),
    agentId(agentId),
    cause(cause),
    viewRange(viewRange),
    hasTicketed(false)
{}

void AreaTicketer::setViewRange(ChunkCoordRes viewRange) {
    updateTickets(lastChunk, viewRange);
    this->viewRange = viewRange;
}

const ChunkCoordRes &AreaTicketer::getViewRange() const {
    return viewRange;
}

void AreaTicketer::updatePosition(const Position &position) {
    updateTickets(position, viewRange);
    lastChunk = position;
}

void AreaTicketer::updateTickets(const ChunkPosition &newChunk, ChunkCoordRes newViewRange) {
    ProfilerSection profiler("updateTickets");

    if (newChunk != lastChunk || newViewRange != viewRange || !hasTicketed) {
        auto minX = newChunk.x - newViewRange;
        auto minY = newChunk.y - newViewRange;
        auto minZ = newChunk.z - newViewRange;
        
        auto maxX = newChunk.x + newViewRange;
        auto maxY = newChunk.y + newViewRange;
        auto maxZ = newChunk.z + newViewRange;

        auto lastMinX = lastChunk.x - viewRange;
        auto lastMinY = lastChunk.y - viewRange;
        auto lastMinZ = lastChunk.z - viewRange;
        
        auto lastMaxX = lastChunk.x + viewRange;
        auto lastMaxY = lastChunk.y + viewRange;
        auto lastMaxZ = lastChunk.z + viewRange;

        // Find new regions
        for (auto x = minX; x <= maxX; ++x) {
            for (auto y = minY; y <= maxY; ++y) {
                for (auto z = minZ; z <= maxZ; ++z) {
                    if (
                        !hasTicketed ||
                        x < lastMinX || x > lastMaxX ||
                        y < lastMinY || y > lastMaxY ||
                        z < lastMinZ || z > lastMaxZ
                    ) {
                        // Not previously ticketed
                        chunkLoader.addRequest({x, y, z}, cause, agentId);
                    }
                }
            }
        }

        if (hasTicketed) {
            // Find old regions
            for (auto x = lastMinX; x <= lastMaxX; ++x) {
                for (auto y = lastMinY; y <= lastMaxY; ++y) {
                    for (auto z = lastMinZ; z <= lastMaxZ; ++z) {
                        if (
                            x < minX || x > maxX ||
                            y < minY || y > maxY ||
                            z < minZ || z > maxZ
                        ) {
                            // Previously ticketed
                            chunkLoader.removeRequest({x, y, z}, cause, agentId);
                        }
                    }
                }
            }
        }

        hasTicketed = true;
    }
}

}