#include "techcraft/items/tile-type.hpp"
#include "techcraft/common/raycast.hpp"
#include "techcraft/rendering/item/tile.hpp"

// To fill out forward declarations
#include "techcraft/dimension/dimension.hpp"
#include "techcraft/inventory/itemstack.hpp"
#include "techcraft/entities/player.hpp"

namespace Techcraft {

// FIXME: The name should be "item.{tileType.id()}". We should install fmtlib https://github.com/fmtlib/fmt

TileItemType::TileItemType(const TileType &tileType)
    : tileType(tileType), ItemType(tileType.id())
{}

const TileTexture *TileItemType::texture() const {
    return tileType.textureOf(TileDirection::Up);
}

bool TileItemType::doPlace(ItemStack &item, LocalPlayer &player, Dimension &dimension) const {
    Raycast raycast(player.getEyePosition(), player.getForward(), player.getReach());
    
    auto result = raycast
        .ignoreFluid()
        .ignoreEntities()
        .cast(*player.getDimension());

    if (result.result() != HitType::Tile && result.result() != HitType::Subtile) {
        return false;
    }

    auto position = result.tilePosition() + Techcraft::TileDirectionOffset::from(result.face());

    // Ensure that tiles cannot be placed where the player is located
    auto tileBounds = tileType.getBoundsForPlacement(result, dimension);
    if (tileBounds) {
        BoundingBox testBounds = *tileBounds + position;
        if (testBounds.intersects(player.getBounds() + player.getPosition())) {
            return false;
        }
    }

    return tileType.doPlaceBlock(position, result, dimension);
}

Rendering::ItemRenderer &TileItemType::getRenderer() const {
    return Rendering::TileItemRenderer::instance();
}

}