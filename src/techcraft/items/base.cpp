#include "techcraft/items/base.hpp"
#include "techcraft/rendering/item/flat.hpp"

// To fill out forward declarations
#include "techcraft/dimension/dimension.hpp"
#include "techcraft/inventory/itemstack.hpp"
#include "techcraft/entities/player.hpp"

namespace Techcraft {

ItemType::ItemType(const std::string_view &name)
    : name(name)
{}

bool ItemType::doPlace(ItemStack &item, LocalPlayer &player, Dimension &dimension) const {
    return false;
}

bool ItemType::operator==(const ItemType &other) const {
    return other.name == name;
}

void ItemType::renderPlacementHighlight(ItemStack &item, LocalPlayer &player, Dimension &dimension) const {
    // No-op
}

Rendering::ItemRenderer &ItemType::getRenderer() const {
    return Rendering::FlatItemRenderer::instance();
}

}