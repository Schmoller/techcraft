#include "techcraft/items/registry.hpp"

namespace Techcraft {
// ItemRegistryBuilder
ItemRegistryBuilder::ItemRegistryBuilder(ResourceManager &manager)
    : manager(manager)
{}

std::unique_ptr<ItemRegistry> ItemRegistryBuilder::build() {
    return std::unique_ptr<ItemRegistry>(new ItemRegistry(byName, slots));
}

void ItemRegistryBuilder::initializeItem(ItemType *item, size_t slotId) {
    item->initialiseItem(manager);
}

// ItemRegistry
ItemRegistry::ItemRegistry(std::unordered_map<std::string, std::shared_ptr<ItemType>> byName, std::vector<std::shared_ptr<ItemType>> slots)
    : Registry(byName, slots)
{}

}