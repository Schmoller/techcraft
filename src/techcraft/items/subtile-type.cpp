#include "techcraft/items/subtile-type.hpp"
#include "techcraft/common/raycast.hpp"
#include "techcraft/subtiles/placement.hpp"
#include "techcraft/subtiles/debug.hpp"
#include "techcraft/rendering/item/microblock.hpp"
#include "engine/subsystem/debug.hpp"

// To fill out forward declarations
#include "techcraft/dimension/dimension.hpp"
#include "techcraft/inventory/itemstack.hpp"
#include "techcraft/entities/player.hpp"

namespace Techcraft {

const float squarePercent = 0.25;
const float insetPercent = 0.2;

// TODO: Name should be prefixed otherwise the name could conflict. use fmtlib
SubtileItemType::SubtileItemType(const SubtileType &type)
    : subtileType(type), ItemType(type.id())
{
    for (auto *slot : type.getSlots()) {
        if (isCenterSlot(slot)) {
            hasCenterSlot = true;
        } else if (isFaceSlot(slot)) {
            hasFaceSlot = true;
        } else if (isEdgeSlot(slot)) {
            hasEdgeSlot = true;
        } else if (isCornerSlot(slot)) {
            hasCornerSlot = true;
        }
    }
}

bool SubtileItemType::doPlace(ItemStack &item, LocalPlayer &player, Dimension &dimension) const {
    Raycast raycast(player.getEyePosition(), player.getForward(), player.getReach());
    
    auto result = raycast
        .ignoreFluid()
        .ignoreEntities()
        .cast(*player.getDimension());

    if (result.result() != HitType::Tile && result.result() != HitType::Subtile) {
        return false;
    }

    auto position = result.tilePosition() + Techcraft::TileDirectionOffset::from(result.face());
    auto relPosition = result.relativePosition();
    auto bounds = result.bounds();

    // TODO: Placement rules should be different for placing on subtiles that dont cover a full face.

    if (bounds) {
        // Determine the face relative hit location
        double hitX, hitY;
        double width, height;
        switch (result.face()) {
            case TileDirection::Up:
            case TileDirection::Down:
                hitX = relPosition.x;
                hitY = relPosition.y;
                width = bounds->width();
                height = bounds->depth();
                break;
            case TileDirection::North:
            case TileDirection::South:
                hitX = relPosition.x;
                hitY = relPosition.z;
                width = bounds->width();
                height = bounds->height();
                break;
            case TileDirection::East:
            case TileDirection::West:
                hitX = relPosition.y;
                hitY = relPosition.z;
                width = bounds->depth();
                height = bounds->height();
                break;
            default:
                hitX = 0.5;
                hitY = 0.5;
                width = 1;
                height = 1;
                break;
        }

        hitX /= width;
        hitY /= height;

        // Determine hit marker
        std::cout << "HitX: " << hitX << " HitY: " << hitY << std::endl;
        auto marker = computeHitLocation(
            hitX, hitY,
            hasCenterSlot,
            hasCornerSlot || hasEdgeSlot,
            hasEdgeSlot || hasFaceSlot
        );

        if (marker != SubtilePlaceHit::None) {
            auto slot = computeSlotFromHit(
                marker, result.face(), player.isCrouching(),
                hasCenterSlot,
                hasEdgeSlot,
                hasCornerSlot,
                hasFaceSlot
            );

            if (slot) {
                std::cout << "Placing in slot " << *slot << std::endl;

                auto existing = dimension.getSubtile(position, *slot);
                if (existing.type) {
                    std::cout << "Slot full" << std::endl;
                    return false;
                }

                auto response = dimension.setSubtile(position, *slot, {&subtileType});
                if (response == SubtileResponse::Success) {
                    return true;
                }
            }
        }
    }

    return false;
}

void SubtileItemType::renderPlacementHighlight(ItemStack &item, LocalPlayer &player, Dimension &dimension) const {
    auto result = player.getLookAtTile();
    if (!result.didHitAny()) {
        return;
    }

    auto debug = Engine::Subsystem::DebugSubsystem::instance();
    // DEBUG: This is just a test. This will be much more interesting

    auto position = result.tilePosition() + Techcraft::TileDirectionOffset::from(result.face());
    auto relPosition = result.relativePosition();
    auto bounds = result.bounds();

    // TODO: Placement rules should be different for placing on subtiles that dont cover a full face.

    if (bounds) {
        // Face hit location
        double hitX, hitY;
        double width, height;
        switch (result.face()) {
            case TileDirection::Up:
            case TileDirection::Down:
                hitX = relPosition.x;
                hitY = relPosition.y;
                width = bounds->width();
                height = bounds->depth();
                break;
            case TileDirection::North:
            case TileDirection::South:
                hitX = relPosition.x;
                hitY = relPosition.z;
                width = bounds->width();
                height = bounds->height();
                break;
            case TileDirection::East:
            case TileDirection::West:
                hitX = relPosition.y;
                hitY = relPosition.z;
                width = bounds->depth();
                height = bounds->height();
                break;
            default:
                hitX = 0.5;
                hitY = 0.5;
                width = 1;
                height = 1;
                break;
        }

        hitX /= width;
        hitY /= height;

        drawPlacementHint(
            result.face(),
            *bounds + result.tilePosition(),
            hasCenterSlot,
            hasEdgeSlot || hasFaceSlot,
            hasCornerSlot || hasEdgeSlot
        );

        // Render where the subtile will be placed
        auto marker = computeHitLocation(
            hitX, hitY,
            hasCenterSlot,
            hasCornerSlot || hasEdgeSlot,
            hasEdgeSlot || hasFaceSlot
        );

        if (marker != SubtilePlaceHit::None) {
            auto slot = computeSlotFromHit(
                marker, result.face(), player.isCrouching(),
                hasCenterSlot,
                hasEdgeSlot,
                hasCornerSlot,
                hasFaceSlot
            );


            if (slot) {
                auto bounds = subtileType.getBounds(*slot);
                if (bounds) {
                    debug->debugDrawBox(
                        {bounds->xMin + position.x, bounds->yMin + position.y, bounds->zMin + position.z},
                        {bounds->xMax + position.x, bounds->yMax + position.y, bounds->zMax + position.z}
                    );
                }
            }
        }
    }
}

Rendering::ItemRenderer &SubtileItemType::getRenderer() const {
    return Rendering::MicroblockItemRenderer::instance();
}

}