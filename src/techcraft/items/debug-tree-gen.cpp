#include "techcraft/items/debug-tree-gen.hpp"
#include "techcraft/common/raycast.hpp"
#include "techcraft/entities/player.hpp"
// #include "techcraft/generation/structure/tree.hpp"
#include "techcraft/generation/structure/trees/bushy.hpp"
#include "techcraft/rand.hpp"

namespace Techcraft {

DebugTreeGenItem::DebugTreeGenItem()
    : ItemType("debug.tree-generator")
{}

bool DebugTreeGenItem::doPlace(ItemStack &item, LocalPlayer &player, Dimension &dimension) const {

    Raycast raycast(player.getEyePosition(), player.getForward(), player.getReach() * 5);
    
    auto result = raycast
        .ignoreFluid()
        .ignoreEntities()
        .cast(*player.getDimension());

    if (result.result() != HitType::Tile && result.result() != HitType::Subtile) {
        return false;
    }

    auto position = result.tilePosition() + Techcraft::TileDirectionOffset::from(result.face());
    // Generation::TreeGenerator generator;

    Generation::BushyTreeGenerator generator;

    auto seed = std::hash<TilePosition>()(position);
    
    Random rand(seed);

    generator.generateAt(*player.getDimension(), position, rand);

    return true;
}

void DebugTreeGenItem::initialiseItem(ResourceManager &manager) {
    icon = manager.asTileTexture({"items.debug-wand"});
}

}