#include "techcraft/game.hpp"
#include "techcraft/debug.hpp"

#include "utilities/profiler.hpp"

#include "engine/font.hpp"

#include "engine/subsystem/objects.hpp"
#include "engine/subsystem/debug.hpp"
#include "engine/subsystem/terrain.hpp"
#include "engine/subsystem/sky.hpp"
#include "engine/subsystem/light.hpp"

#include "techcraft/tiles/registry.hpp"
#include "techcraft/subtiles/registry.hpp"
#include "techcraft/fluid/registry.hpp"

#include "techcraft/items/tile-type.hpp"
#include "techcraft/items/subtile-type.hpp"
#include "techcraft/items/debug-tree-gen.hpp"

#include "techcraft/inventory/itemstack.hpp"

#include "techcraft/entities/player.hpp"

#include "techcraft/gui/debug.hpp"
#include "techcraft/gui/hud.hpp"

#include "techcraft/commands/test.hpp"
#include "techcraft/commands/fly.hpp"

#include "techcraft/default.hpp"

#include "techcraft/resources/manager.hpp"

#include <iostream>

#ifdef WITHGPERFTOOLS
#include <gperftools/profiler.h>
#endif

using namespace Engine;

namespace Techcraft {

Game *Game::instance { nullptr };

Game::Game()
    : fleet(false)
{
    instance = this;
}

void Game::reloadSettings() {
    // TODO: This will be loaded from a settings file
    if (universe) {
        universe->setChunkLoadDistance(5);
        universe->setChunkViewDistance(12);
    }
}

void Game::loadNewUniverse(const std::string &name, size_t seed) {
    // TODO: Add argument for seed
    universe = std::make_unique<ClientUniverse>(
        bindingManager.get(),
        fleet,
        engine,
        *commandExecutor
    );

    // Load up scene
    Techcraft::LocalPlayer *player = universe->getDimension(0)->addEntity<LocalPlayer>(
        Techcraft::Position{0, 0, 30},
        *bindingManager,
        engine.getInputManager()
    );

    universe->setLocalPlayer(player);

    // Give the player a test inventory
    auto &inventory = player->getInventory();
    inventory[0] = ItemStack(*itemRegistry->lookup("debug.tree-generator"), 1);
    inventory[1] = ItemStack(*itemRegistry->lookup("debug.connected"), 1);
    inventory[2] = ItemStack(*itemRegistry->lookup("cover.debug.connected.1"), 2);
    inventory[3] = ItemStack(*itemRegistry->lookup("leaves1"), 2);
    inventory[4] = ItemStack(*itemRegistry->lookup("grass"), 2);
    inventory[5] = ItemStack(*itemRegistry->lookup("dirt.slab"), 1);
    inventory[6] = ItemStack(*itemRegistry->lookup("corner.stone.1"), 1);
    inventory[7] = ItemStack(*itemRegistry->lookup("strip.dirt.1"), 1);
    inventory[8] = ItemStack(*itemRegistry->lookup("tube.item"), 1);
    inventory[9] = ItemStack(*itemRegistry->lookup("cover.stone.1"), 1);
    inventory[10] = ItemStack(*itemRegistry->lookup("dirt"), 2);
}

bool Game::launch() {
    if (!initEngine()) {
        return false;
    }

    initMisc();

    // Phase 1 Loading
    loadResources();

    // Phase 2 Loading
    initBindings();
    initCommands();
    TileTypes::initialize(*resourceManager);
    SubtileTypes::initialize(*resourceManager);
    FluidTypes::initialize(*resourceManager);
    initItems();

    // Phase 3 - Resources
    resourceManager->finaliseResources();

    // DEBUG: Jump right to a universe.
    // In the future, we would go to a menu where the user could create a new universe or load an existing one.
    loadNewUniverse("debug universe", 12345);
    reloadSettings();

    // DEBUG: Load in the debug UI
    engine.getGuiManager().addComponent(std::make_shared<GuiDebug>(this, engine.getScreenBounds()));

    // DEBUG: Hud. This needs to move somewhere else
    auto hud = std::make_shared<Techcraft::GuiHUD>(engine.getScreenBounds(), universe->getLocalPlayer(), engine.getTextureManager(), engine.getFontManager().getFont("monospace", FontStyle::Regular));
    hud->setAnchor(Engine::Gui::AnchorAll);

    engine.getGuiManager().addComponent(hud);

    auto crosshairTexture = engine.createTexture("hud.crosshair")
        .fromFile("assets/textures/gui/crosshair.png")
        .build();

    auto crosshair = std::make_shared<Engine::Gui::Image>(
        crosshairTexture,
        Engine::Gui::Rect{
            engine.getScreenBounds().center() - glm::vec2{25.0f, 25.0f},
            engine.getScreenBounds().center() + glm::vec2{25.0f, 25.0f}
        }
    );
    crosshair->setAnchor(Engine::Gui::AnchorFlag::Center);

    engine.getGuiManager().addComponent(crosshair);

    // Here runs the game itself
    #ifdef WITHGPERFTOOLS
    ProfilerStart("profile.log");
    #endif
    eventLoop();
    #ifdef WITHGPERFTOOLS
    ProfilerStop();
    #endif

    // Cleanup
    fleet.shutdown();
    engine.getInputManager().releaseMouse();

    return true;
}

bool Game::initEngine() {
    // Prepare any required subsystems
    engine.addSubsystem(Subsystem::LightSubsystem::ID);
    engine.addSubsystem(Subsystem::SkySubsystem::ID);
    engine.addSubsystem(Subsystem::ObjectSubsystem::ID);
    engine.addSubsystem(Subsystem::DebugSubsystem::ID);
    engine.addSubsystem(Subsystem::TerrainSubsystem::ID);

    // Bootstrap the render engine
    try {
        engine.initialize("TechCraft");
    } catch (const std::exception& e) {
        std::cerr << "Failed to initialise the render engine." << std::endl;
        std::cerr << e.what() << std::endl;
        return false;
    }

    return true;
}

void Game::initMisc() {
    bindingManager = std::make_unique<BindingManager>(engine.getInputManager());
    itemRenderHelper = std::make_unique<Rendering::ItemRenderHelper>(engine);
    Debug::initialize(this);

    engine.getInputManager().addCallback(
        std::bind(&Game::onInputUpdate, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)
    );

    engine.getInputManager().addTextCallback(
        std::bind(&Game::onCharInputUpdate, this, std::placeholders::_1)
    );

    engine.getInputManager().addMouseCallback(
        std::bind(&Game::onMouseInputUpdate, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5)
    );

    engine.getInputManager().addMouseMoveCallback(
        std::bind(&Game::onMouseMove, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4)
    );

    engine.getInputManager().addScrollCallback(
        std::bind(&Game::onMouseScroll, this, std::placeholders::_1, std::placeholders::_2)
    );
}

void Game::loadResources() {
    resourceManager = std::make_unique<ResourceManager>(engine.getTextureManager());

    // Fonts
    auto &fontManager = engine.getFontManager();
    fontManager.addFont("assets/fonts/SourceCodePro-Regular.ttf", "monospace", FontStyle::Regular);
    fontManager.addFont("assets/fonts/SourceCodePro-Italic.ttf", "monospace", FontStyle::Italic);
    fontManager.addFont("assets/fonts/SourceCodePro-SemiBold.ttf", "monospace", FontStyle::Bold);
    fontManager.addFont("assets/fonts/SourceCodePro-SemiBoldItalic.ttf", "monospace", FontStyle::BoldItalic);

    // TODO: I dont want to manually add the resources here for each tile and whatever. I want that to be discovered somehow
}

void Game::initItems() {
    ItemRegistryBuilder builder(*resourceManager);

    builder.add(std::make_shared<DebugTreeGenItem>());

    // Register all valid tile types as items
    for (auto tileType : TileTypes::registry()->all()) {
        // TODO: We also need a hasItem() or something method so that special tiles cant be in item form
        if (!tileType->isEmpty()) {
            builder.add(std::make_shared<TileItemType>(*tileType));
        }
    }

    // Register all valid subtile types as items
    for (auto subtileType : SubtileTypes::registry()->all()) {
        // TODO: We also need a hasItem() or something method so that special tiles cant be in item form
        if (!subtileType->isEmpty()) {
            builder.add(std::make_shared<SubtileItemType>(*subtileType));
        }
    }

    std::unordered_set<const Rendering::ItemRenderer*> initialised;

    itemRegistry = builder.build();

    // Initialise renderers
    for (auto type : itemRegistry->all()) {
        auto &renderer = type->getRenderer();
        if (initialised.insert(&renderer).second) {
            renderer.init(engine);
        }
    }
}

void Game::initBindings() {
    bindingManager->createBinding(Default::KeyBindings::MOVE_FORWARD)
        .withKey(Engine::Key::eW)
        .build();
    bindingManager->createBinding(Default::KeyBindings::MOVE_BACKWARD)
        .withKey(Engine::Key::eS)
        .build();
    bindingManager->createBinding(Default::KeyBindings::STRAFE_LEFT)
        .withKey(Engine::Key::eA)
        .build();
    bindingManager->createBinding(Default::KeyBindings::STRAFE_RIGHT)
        .withKey(Engine::Key::eD)
        .build();

    bindingManager->createBinding(Default::KeyBindings::CROUCH)
        .withKey(Engine::Key::eLeftShift)
        .build();
    bindingManager->createBinding(Default::KeyBindings::JUMP)
        .withKey(Engine::Key::eSpace)
        .build();

    bindingManager->createBinding(Default::KeyBindings::SWITCH_SLOT_0)
        .withKey(Engine::Key::e0)
        .withContextMask(KeybindContextFlag::Inventory)
        .build();
    bindingManager->createBinding(Default::KeyBindings::SWITCH_SLOT_1)
        .withKey(Engine::Key::e1)
        .withContextMask(KeybindContextFlag::Inventory)
        .build();
    bindingManager->createBinding(Default::KeyBindings::SWITCH_SLOT_2)
        .withKey(Engine::Key::e2)
        .withContextMask(KeybindContextFlag::Inventory)
        .build();
    bindingManager->createBinding(Default::KeyBindings::SWITCH_SLOT_3)
        .withKey(Engine::Key::e3)
        .withContextMask(KeybindContextFlag::Inventory)
        .build();
    bindingManager->createBinding(Default::KeyBindings::SWITCH_SLOT_4)
        .withKey(Engine::Key::e4)
        .withContextMask(KeybindContextFlag::Inventory)
        .build();
    bindingManager->createBinding(Default::KeyBindings::SWITCH_SLOT_5)
        .withKey(Engine::Key::e5)
        .withContextMask(KeybindContextFlag::Inventory)
        .build();
    bindingManager->createBinding(Default::KeyBindings::SWITCH_SLOT_6)
        .withKey(Engine::Key::e6)
        .withContextMask(KeybindContextFlag::Inventory)
        .build();
    bindingManager->createBinding(Default::KeyBindings::SWITCH_SLOT_7)
        .withKey(Engine::Key::e7)
        .withContextMask(KeybindContextFlag::Inventory)
        .build();
    bindingManager->createBinding(Default::KeyBindings::SWITCH_SLOT_8)
        .withKey(Engine::Key::e8)
        .withContextMask(KeybindContextFlag::Inventory)
        .build();
    bindingManager->createBinding(Default::KeyBindings::SWITCH_SLOT_9)
        .withKey(Engine::Key::e9)
        .withContextMask(KeybindContextFlag::Inventory)
        .build();
    bindingManager->createBinding(Default::KeyBindings::OPEN_BACKPACK)
        .withKey(Engine::Key::eE)
        .withContextMask(KeybindContextFlag::Inventory)
        .build();

    bindingManager->createBinding(Default::KeyBindings::BREAK_TILE)
        .withKey(Engine::Key::eMouse1)
        .build();
    bindingManager->createBinding(Default::KeyBindings::PLACE_TILE)
        .withKey(Engine::Key::eMouse2)
        .build();

    bindingManager->createBinding(Default::KeyBindings::OPEN_CONSOLE)
        .withKey(Engine::Key::eF7)
        .withContextMask(KeybindContextFlag::Console)
        .build();
}

void Game::initCommands() {
    commandExecutor = std::make_unique<Commands::Executor>();

    // Register commands
    commandExecutor->addCommand(std::make_shared<Commands::TestCommand>());
    commandExecutor->addCommand(std::make_shared<Commands::FlyCommand>());
}

void Game::eventLoop() {
    auto &input = engine.getInputManager();

    // DEBUG: These will go away from here
    auto *sky = engine.getSubsystem(Engine::Subsystem::SkySubsystem::ID);
    auto *globalLight = engine.getSubsystem(Engine::Subsystem::LightSubsystem::ID);
    float timeOfDay = 100;
    const float dayLength = 300;
    sky->setTimeOfDay(timeOfDay / dayLength);
    globalLight->setTimeOfDay(timeOfDay / dayLength);

    bool hasMouse = true;
    input.captureMouse();
    while (engine.beginFrame()) {
        // Frame timing
        auto currentTime = std::chrono::high_resolution_clock::now();
        double timeDelta = static_cast<std::chrono::duration<double>>(currentTime - lastFrameTime).count();
        instantFPS = 1.0 / timeDelta;
        rollingFPS = rollingFPS * fpsRollingWeight + instantFPS * (1-fpsRollingWeight);
        lastFrameTime = currentTime;
        // FIXME: When the game hitches, the instant FPS becomes really high. It makes no sense.

        // DEBUG: Temporary method to quit
        if (input.wasPressed(Key::eEscape)) {
            break;
        }
        // DEBUG: Pause shouldnt be here
        if (input.wasPressed(Key::eM)) {
            if (hasMouse) {
                universe->setPause(true);
                input.releaseMouse();
            } else {
                input.captureMouse();
                universe->setPause(false);
            }

            hasMouse = !hasMouse;
        }

        if (input.isPressed(Key::eY)) {
            timeOfDay = fmod(timeOfDay + 1, dayLength);

            sky->setTimeOfDay(timeOfDay / dayLength);
            globalLight->setTimeOfDay(timeOfDay / dayLength);
        }
        if (input.isPressed(Key::eU)) {
            timeOfDay = fmod(timeOfDay + 0.1, dayLength);

            sky->setTimeOfDay(timeOfDay / dayLength);
            globalLight->setTimeOfDay(timeOfDay / dayLength);
        }

        if (input.wasPressed(Key::eF1)) {
            debugPrintProfilerTimings();
        }

        if (input.wasPressed(Key::eKpAdd)) {
            universe->setChunkLoadDistance(universe->getChunkLoadDistance() + 1);
        }

        if (input.wasPressed(Key::eKpSubtract)) {
            if (universe->getChunkLoadDistance() > 1) {
                universe->setChunkLoadDistance(universe->getChunkLoadDistance() - 1);
            }
        }

        if (universe) {
            ProfilerSection profiler("update");
            universe->update(timeDelta);
        }

        itemRenderHelper->update();

        Debug::getInstance()->doUpdate();

        {
            ProfilerSection profiler("render");
            engine.render();
        }

    }
}

std::shared_ptr<Window> Game::getCurrentGUI() const {
    return currentWindow;
}

void Game::setCurrentGUI(std::shared_ptr<Window> window) {
    if (currentWindow) {
        currentWindow->onHide();
        engine.getGuiManager().removeComponent(currentWindowId);
    }

    if (window) {
        currentWindow = window;
        currentWindowId = engine.getGuiManager().addComponent(window);

        if (currentWindow->doesCaptureInput()) {
            engine.getInputManager().releaseMouse();
            if (currentWindow->allowKeybinds()) {
                bindingManager->setEnabled(true);
            } else {
                bindingManager->setEnabled(false);
            }
            bindingManager->setContext(currentWindow->keybindContext());
        } else {
            engine.getInputManager().captureMouse();
            bindingManager->setEnabled(true);
            bindingManager->setContext(KeybindContextFlag::None);
        }
        
        window->onShow(window.get());
    } else {
        currentWindow.reset();
        currentWindowId = -1;
        engine.getInputManager().captureMouse();
        bindingManager->setEnabled(true);
        bindingManager->setContext(KeybindContextFlag::None);
    }
}

void Game::onInputUpdate(Engine::Key key, Engine::Action action, const Engine::Modifier &modifers) {
    if (currentWindow) {
        switch (action) {
            case Engine::Action::Repeat:
            case Engine::Action::Press:
                currentWindow->onKeyPress(key, modifers);
                break;
            case Engine::Action::Release:
                currentWindow->onKeyRelease(key, modifers);
                break;
        }
    }
}

void Game::onCharInputUpdate(wchar_t ch) {
    if (currentWindow) {
        currentWindow->onKeyTyped(ch);
    }
}

void Game::onMouseInputUpdate(double x, double y, Engine::Action action, Engine::MouseButton button, Engine::Modifier modifiers) {
    if (currentWindow) {
        auto &bounds = currentWindow->getBounds();
        x = x - bounds.left();
        y = y - bounds.top();

        if (action == Engine::Action::Press) {
            currentWindow->onMousePress(button, x, y, modifiers);
        } else if (action == Engine::Action::Release) {
            currentWindow->onMouseRelease(button, x, y, modifiers);
        }
    }
}

void Game::onMouseMove(double x, double y, Engine::MouseButtons buttons, Engine::Modifier modifiers) {
    if (currentWindow) {
        auto &bounds = currentWindow->getBounds();
        x = x - bounds.left();
        y = y - bounds.top();

        currentWindow->onMouseMove(buttons, x, y, modifiers);
    }
}

void Game::onMouseScroll(double scrollX, double scrollY) {
    if (currentWindow) {
        currentWindow->onMouseScroll(scrollX, scrollY);
    }
}

}