#include "techcraft/tiles/subtile-only.hpp"
#include "techcraft/rendering/tile/null.hpp"
#include "techcraft/dimension/dimension.hpp"
#include "techcraft/common/raycast.hpp"

namespace Techcraft {

SubtileOnlyTileType::SubtileOnlyTileType(const std::string &id)
    : TileType(id)
{
    setTransparent();
}

bool SubtileOnlyTileType::doPlaceBlock(const TilePosition &pos, RaycastResult hit, Dimension &dimension) const {
    return false;
}

Rendering::TileRenderer &SubtileOnlyTileType::getRenderer() const {
    return Rendering::NullTileRenderer::instance();
}

void SubtileOnlyTileType::initialize(ResourceManager &manager) {
    // Noop
}

bool SubtileOnlyTileType::isEmpty(const Tile &tile) const {
    auto container = tile.getSubtileContainer();
    bool hasAny = false;
    container->forEach([&](auto &slot, auto &subtile) {
        if (subtile.type) {
            hasAny = true;
        }
    });
    
    return !hasAny;
}


}