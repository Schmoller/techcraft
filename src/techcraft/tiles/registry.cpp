#include "techcraft/tiles/registry.hpp"

#include "techcraft/tiles/slab.hpp"
#include "techcraft/tiles/barrel.hpp"
#include "techcraft/tiles/itemtube.hpp"
#include "techcraft/tiles/subtile-only.hpp"
#include "techcraft/rendering/tile/basic-transparent.hpp"
#include "techcraft/rendering/tile/basictile.hpp"

namespace Techcraft {

// Basic tiles
const TileType *TileTypes::Dirt;
const TileType *TileTypes::Stone;
const TileType *TileTypes::Grass;
const TileType *TileTypes::Log1;
const TileType *TileTypes::Leaves1;
const TileType *TileTypes::Log2;
const TileType *TileTypes::Leaves2;

// Tier 1
const TileType *TileTypes::Tube;
const TileType *TileTypes::Barrel;

// Slabs
const TileType *TileTypes::StoneSlab;
const TileType *TileTypes::DirtSlab;

// Internal blocks. Not player usable
const TileType *TileTypes::InternalSubtileContainer;
const TileType *TileTypes::Air;

// Debug blocks
const TileType *TileTypes::Reference;
const TileType *TileTypes::Reference2;
const TileType *TileTypes::ConnectedTest;

std::unique_ptr<TileRegistry> TileTypes::tileRegistry;


void TileTypes::initialize(ResourceManager &manager) {
    TileRegistryBuilder builder(manager);

    // Basic types
    TileTypeDefinition dirt("dirt");
    dirt.setTextureAll({"tiles.dirt"});
    Dirt = builder.add(dirt);

    TileTypeDefinition stone("stone");
    stone.setTextureAll({"tiles.stone"});
    Stone = builder.add(stone);

    TileTypeDefinition grass("grass");
    grass.setTextureAll({"tiles.grass", "Side"});
    grass.setTexture({"tiles.grass", "Top"}, TileDirection::Up);
    grass.setTexture({"tiles.grass", "Bottom"}, TileDirection::Down);
    Grass = builder.add(grass);

    TileTypeDefinition log1("log1");
    log1.setTextureAll({"tiles.log1"});
    Log1 = builder.add(log1);
    TileTypeDefinition log2("log2");
    log2.setTextureAll({"tiles.log2"});
    Log2 = builder.add(log2);
    TileTypeDefinition leaves1("leaves1");
    leaves1.setTextureAll({"tiles.leaves1"});
    // leaves1.setRenderer(Rendering::BasicTransparentTileRenderer::instance());
    leaves1.setTransparent();
    Leaves1 = builder.add(leaves1);
    TileTypeDefinition leaves2("leaves2");
    leaves2.setTextureAll({"tiles.leaves2"});
    // leaves2.setRenderer(Rendering::BasicTransparentTileRenderer::instance());
    leaves2.setTransparent();
    Leaves2 = builder.add(leaves2);

    // Tier 1
    Barrel = builder.add(std::make_shared<BarrelTileType>());
    Tube = builder.add(std::make_shared<ItemTubeTileType>());

    // Slabs
    StoneSlab = builder.add(std::make_shared<SlabTileType>("stone.slab", Stone));
    DirtSlab = builder.add(std::make_shared<SlabTileType>("dirt.slab", Dirt));

    // Internal
    InternalSubtileContainer = builder.add(std::make_shared<SubtileOnlyTileType>("internal.subtile-container"));

    // Debug
    TileTypeDefinition ref("reference");
    ref.setTextureAll({"tiles.reference"});
    ref.setLightEmission(TileLightPallet::White, 15);
    Reference = builder.add(ref);

    TileTypeDefinition ref2("reference2");
    ref2.setTextureAll({"tiles.reference"});
    ref2.setLightEmission(TileLightPallet::Blue, 31);
    Reference2 = builder.add(ref2);

    TileTypeDefinition connectedTest("debug.connected");
    connectedTest.setTextureAll({"tiles.connected-test"});
    ConnectedTest = builder.add(connectedTest);
    
    tileRegistry = builder.build();

    Air = tileRegistry->air();
}


// TileTypeDefinition
TileTypeDefinition::TileTypeDefinition(const std::string &name)
  : name(name),
    transparent(false),
    tileLight {}
{}

void TileTypeDefinition::setTextureAll(const Resource &resource) {
    for (size_t i = 0; i < 6; ++i) {
        faceTextures[i] = resource;
    }
}

void TileTypeDefinition::setTexture(const Resource &resource, TileDirection direction) {
    faceTextures[static_cast<int>(direction)] = resource;
}

void TileTypeDefinition::setLightEmission(TileLightPallet pallet, uint8_t intensity) {
    tileLight[static_cast<int>(pallet)] = intensity;
}

// BasicTileType
BasicTileType::BasicTileType(
    const std::string &id,
    const TileTexture *faceTextures[6],
    const uint8_t tileLight[TILE_LIGHT_PALLET_COUNT],
    bool transparent,
    Rendering::TileRenderer &renderer,
    bool empty
) : TileType(id), empty(empty), renderer(renderer)
{
    if (transparent) {
        setTransparent();
    }

    for (auto pallet : AllPallets) {
        setLightEmission(pallet, tileLight[static_cast<int>(pallet)]);
    }

    for (auto dir : AllDirections) {
        setTexture(faceTextures[static_cast<int>(dir)], dir);
    }

    if (!transparent) {
        bounds = {1,1,1,false};
    }
}

std::optional<BoundingBox> BasicTileType::getBounds() const {
    return bounds;
}

bool BasicTileType::isEmpty() const {
    return empty;
}

void BasicTileType::initialize(ResourceManager &manager) {
    // No-op
}

// TileRegistryBuilder
TileRegistryBuilder::TileRegistryBuilder(ResourceManager &manager)
    : manager(manager) 
{
    // Built in type
    auto air = TileTypeDefinition("air");
    air.setTransparent();
    air.setEmpty();
    add(air);
}

const TileType *TileRegistryBuilder::add(const TileTypeDefinition &definition) {
    const TileTexture *textures[6];

    for (size_t i = 0; i < 6; ++i) {
        textures[i] = manager.asTileTexture(definition.faceTextures[i]);
    }

    auto basicType = std::make_shared<BasicTileType>(
        definition.name,
        textures,
        definition.tileLight,
        definition.transparent,
        definition.renderer ? *definition.renderer : Rendering::BasicTileRenderer::instance(),
        definition.empty
    );

    return add(basicType);
}

std::unique_ptr<TileRegistry> TileRegistryBuilder::build() {
    return std::unique_ptr<TileRegistry>(new TileRegistry(
        byName,
        slots
    ));
}

void TileRegistryBuilder::initializeItem(TileType *item, size_t slotId) {
    item->initialize(manager);
    item->slotId = slotId;

    // Ensure no unset textures
    for (int i = 0; i < 6; ++i) {
        if (!item->faceTextures[i]) {
            item->faceTextures[i] = manager.asTileTexture({});
        }
    }
}

// TileRegisty
TileRegistry::TileRegistry(
    std::unordered_map<std::string, std::shared_ptr<TileType>> byName,
    std::vector<std::shared_ptr<TileType>> slots
) : Registry(byName, slots)
{}

const TileType *TileRegistry::air() const {
    return get(0);
}

}