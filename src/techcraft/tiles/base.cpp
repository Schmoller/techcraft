#include "techcraft/tiles/base.hpp"

#include "techcraft/dimension/dimension.hpp"
#include "techcraft/common/raycast.hpp"
#include "techcraft/rendering/tile/basictile.hpp"

namespace Techcraft {

// TileType
TileType::TileType(const std::string &id)
    : name(id), emitsLight(false), transparent(false), faceTextures {}
{}

void TileType::setTextureAll(const TileTexture *texture) {
    for (size_t i = 0; i < 6; ++i) {
        faceTextures[i] = texture;
    }
}

void TileType::setTexture(const TileTexture *texture, TileDirection direction) {
    faceTextures[static_cast<int>(direction)] = texture;
}

void TileType::setLightEmission(TileLightPallet pallet, uint8_t intensity) {
    tileLight[static_cast<int>(pallet)] = intensity;
    if (intensity > 0) {
        emitsLight = true;
    }
}

const std::string &TileType::id() const {
    return name;
}

bool TileType::operator==(const TileType &other) const {
    return slotId == other.slotId;
}

bool TileType::operator!=(const TileType &other) const {
    return slotId != other.slotId;
}

std::optional<BoundingBox> TileType::getBounds() const {
    return {}; // No bounds
}

std::optional<BoundingBox> TileType::getBoundsForPlacement(RaycastResult hit, const Dimension &dimension) const {
    return getBounds();
}

bool TileType::doPlaceBlock(const TilePosition &pos, RaycastResult hit, Dimension &dimension) const {
    const Tile &existingAtPlace = dimension.getTile(pos);
    if (!existingAtPlace.isEmpty()) {
        return false;
    }
    
    dimension.setTile(pos, {this});
    return true;
}

std::shared_ptr<TileEntity> TileType::createTileEntity(const TilePosition &pos) const {
    return {};
}

Rendering::TileRenderer &TileType::getRenderer() const {
    return Rendering::BasicTileRenderer::instance();
}

bool TileType::isEmpty() const {
    return false;
}

bool TileType::isFluidReplaceable() const {
    return false;
}

bool TileType::isFluidPassable() const {
    return isEmpty();
}

void TileType::doFluidReplace(const TilePosition &pos, Dimension &dimension, const Fluid &fluid) const {
    
}

std::shared_ptr<SubtileContainer> TileType::coerseIntoSubtileContainer(const TilePosition &pos, const Tile &tile) const {
    if (isEmpty()) {
        return std::make_shared<StandardSubtileContainer>();
    } else {
        return {};
    }
}


// Tile
std::shared_ptr<TileEntity> Tile::getTileEntity() const {
    auto entity = std::get_if<std::shared_ptr<TileEntity>>(&data);

    if (entity) {
        return *entity;
    } else {
        return {};
    }
}

std::shared_ptr<SubtileContainer> Tile::getSubtileContainer() const {
    auto subtileContainer = std::get_if<std::shared_ptr<SubtileContainer>>(&data);
    if (subtileContainer && *subtileContainer) {
        return *subtileContainer;
    } else {
        // Tile entities are allowed to be a subtile container too
        auto entity = std::get_if<std::shared_ptr<TileEntity>>(&data);
        if (entity && *entity) {
            return std::dynamic_pointer_cast<SubtileContainer>(*entity);
        }

        return {};
    }
}

std::optional<BoundingBox> Tile::getBounds() const {
    std::optional<BoundingBox> bounds {};
    auto entity = getTileEntity();
    if (entity) {
        bounds = entity->getBounds();
    }

    if (!bounds) {
        bounds = type->getBounds();
    }

    return bounds;
}

bool Tile::isSolidOnSide(TileDirection face) const {
    auto entity = getTileEntity();
    if (entity) {
        return entity->isSolidOn(face);
    }
    auto subtile = getSubtileContainer();
    if (subtile) {
        return subtile->isSolidOn(face);
    }

    return !type->isTransparent();
}

bool Tile::isEmpty() const {
    return type->isEmpty(*this);
}


}