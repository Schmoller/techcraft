#include "techcraft/tiles/barrel.hpp"
#include "techcraft/dimension/dimension.hpp"
#include "techcraft/rendering/tile/barrel.hpp"
#include "techcraft/tileentities/barrel.hpp"
#include "techcraft/common/raycast.hpp"

namespace Techcraft {

BarrelTileType::BarrelTileType()
    : TileType("barrel")
{

}

std::optional<BoundingBox> BarrelTileType::getBounds() const {
    return BoundingBox{
        1, 1, 1, false
    };
}

bool BarrelTileType::doPlaceBlock(const TilePosition &pos, RaycastResult hit, Dimension &dimension) const {
    // TODO: Custom behaviour
    return TileType::doPlaceBlock(pos, hit, dimension);
}

std::shared_ptr<TileEntity> BarrelTileType::createTileEntity(const TilePosition &pos) const {
    return std::make_shared<BarrelTileEntity>(pos);
}

Rendering::TileRenderer &BarrelTileType::getRenderer() const {
    return Rendering::BarrelTileRenderer::instance();
}

void BarrelTileType::initialize(ResourceManager &manager) {
    setTransparent();
}

}