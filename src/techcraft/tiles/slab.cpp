#include "techcraft/tiles/slab.hpp"
#include "techcraft/dimension/dimension.hpp"
#include "techcraft/common/raycast.hpp"

namespace Techcraft {

SlabTileType::SlabTileType(const std::string &id, const TileType *fullTile)
    : TileType(id), fullTile(fullTile)
{}

std::optional<BoundingBox> SlabTileType::getBounds() const {
    return BoundingBox{1.0f, 1.0f, 0.5f, false};
}

std::optional<BoundingBox> SlabTileType::getBoundsForPlacement(RaycastResult hit, const Dimension &dimension) const {
    TileDirection attachmentDir = opposite(hit.face());

    const Tile &existing = *hit.tile();

    if (existing.type == this) {
        return BoundingBox{1.0f, 1.0f, 1.0f, false} + TileDirectionOffset::from(opposite(hit.face()));
    }

    switch (attachmentDir) {
        case TileDirection::Up:
            return BoundingBox{0.0f, 0.0f, 0.5f,  1.0f, 1.0f, 1.0f};
        case TileDirection::Down:
            return BoundingBox{0.0f, 0.0f, 0.0f,  1.0f, 1.0f, 0.5f};
        case TileDirection::North:
            return BoundingBox{0.0f, 0.5f, 0.0f,  1.0f, 1.0f, 1.0f};
        case TileDirection::South:
            return BoundingBox{0.0f, 0.0f, 0.0f,  1.0f, 0.5f, 1.0f};
        case TileDirection::East:
            return BoundingBox{0.5f, 0.0f, 0.0f,  1.0f, 1.0f, 1.0f};
        case TileDirection::West:
            return BoundingBox{0.0f, 0.0f, 0.0f,  0.5f, 1.0f, 1.0f};
        default:
            return BoundingBox{1.0f, 1.0f, 1.0f, false};
    }
}

bool SlabTileType::doPlaceBlock(const TilePosition &pos, RaycastResult hit, Dimension &dimension) const {
    TileDirection attachmentDir = opposite(hit.face());

    const Tile &existing = *hit.tile();

    if (existing.type == this) {
        // Merge if its the same attachment direction
        // NOTE: This is assumed to always be the case. It really should never not be
        SlabTileEntity *other = static_cast<SlabTileEntity*>(existing.getTileEntity().get());

        if (other->getAttached() == attachmentDir) {
            // Make a full tile
            dimension.setTile(hit.tilePosition(), {fullTile});
            return true;
        }
    }

    const Tile &existingAtPlace = dimension.getTile(pos);
    if (!existingAtPlace.isEmpty()) {
        // Dont place into non-empty space
        return false;
    }
    
    // Was empty, put in a slab attached to the clicked face
    auto entity = std::make_shared<SlabTileEntity>(pos, attachmentDir);
    dimension.setTile(pos, {this, entity});

    return true;
}

std::shared_ptr<TileEntity> SlabTileType::createTileEntity(const TilePosition &pos) const {
    return std::make_shared<SlabTileEntity>(pos);
}

Rendering::TileRenderer &SlabTileType::getRenderer() const {
    return Rendering::SlabTileRenderer::instance();
}

void SlabTileType::initialize(ResourceManager &manager) {
    // Copy attributes of the full tile
    for (auto dir : AllDirections) {
        setTexture(fullTile->textureOf(dir), dir);
    }

    for (auto pallet : AllPallets) {
        setLightEmission(pallet, fullTile->getLight(pallet));
    }

    setTransparent();
}

}