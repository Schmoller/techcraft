#include "techcraft/tiles/itemtube.hpp"
#include "techcraft/dimension/dimension.hpp"
#include "techcraft/tileentities/itemtube.hpp"
#include "techcraft/rendering/tile/itemtube.hpp"
#include "techcraft/common/raycast.hpp"

namespace Techcraft {

ItemTubeTileType::ItemTubeTileType()
    : TileType("tube.item")
{}

std::optional<BoundingBox> ItemTubeTileType::getBounds() const {
    return BoundingBox{
        0.28f, 0.28f, 0.28f, 0.72f, 0.72f, 0.72f
    };
}

std::optional<BoundingBox> ItemTubeTileType::getBoundsForPlacement(RaycastResult hit, const Dimension &dimension) const {
    return getBounds(); // TODO: This
}

bool ItemTubeTileType::doPlaceBlock(const TilePosition &pos, RaycastResult hit, Dimension &dimension) const {
    // TODO: Custom behaviour
    return TileType::doPlaceBlock(pos, hit, dimension);
}

std::shared_ptr<TileEntity> ItemTubeTileType::createTileEntity(const TilePosition &pos) const {
    return std::make_shared<ItemTubeTileEntity>(pos);
}

Rendering::TileRenderer &ItemTubeTileType::getRenderer() const {
    return Rendering::ItemTubeRenderer::instance();
}

void ItemTubeTileType::initialize(ResourceManager &manager) {
    setTransparent();
}

bool ItemTubeTileType::isFluidPassable() const {
    return true;
}

}