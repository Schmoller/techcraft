#include "techcraft/debug.hpp"
#include "techcraft/common.hpp"

#include "engine/subsystem/debug.hpp"

namespace Techcraft {
std::unique_ptr<Debug> Debug::instance;

void Debug::initialize(Game *game) {
    instance = std::unique_ptr<Debug>(new Debug(game));
}

Debug::Debug(Game *game)
    : game(game)
{}

void Debug::doUpdate() {
    auto debug = Engine::Subsystem::DebugSubsystem::instance();

    if (showChunkBorders) {
        auto universe = game->getUniverse();
        if (universe) {
            auto player = universe->getLocalPlayer();
            if (player) {
                ChunkPosition chunkPos = player->getPosition();

                debug->debugDrawBox(
                    {chunkPos.x * ChunkSize, chunkPos.y * ChunkSize, chunkPos.z * ChunkSize},
                    {(chunkPos.x+1) * ChunkSize, (chunkPos.y+1) * ChunkSize, (chunkPos.z+1) * ChunkSize},
                    0xFFFFFF
                );

            }
        }
    }
}
}