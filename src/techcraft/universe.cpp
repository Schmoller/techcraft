#include "techcraft/universe.hpp"
#include "techcraft/game.hpp"
#include "techcraft/rendering/tile/base.hpp"
#include "techcraft/generation/overworld.hpp"
#include "utilities/profiler.hpp"
#include "engine/subsystem/debug.hpp"
#include "techcraft/common/raycast.hpp"

#include "techcraft/default.hpp"
#include "techcraft/tiles/registry.hpp"

#include <iostream>

namespace Techcraft {

// Universe
Universe::Universe(Utilities::WorkerFleet &workers)
 : workers(workers)
{
    dimensions[0] = std::make_unique<Dimension>(0, std::make_shared<Generation::OverworldGenerator>(12345), workers, 50, 50);
}

Dimension *Universe::getDimension(uint32_t id) {
    auto it = dimensions.find(id);
    if (it != dimensions.end()) {
        return it->second.get();
    }

    return nullptr;
}

void Universe::update(double deltaTime) {
    ProfilerSection profiler("base update");
    auto now = std::chrono::high_resolution_clock::now();

    if (!pauseState) {
        if (now > lastTickStart + TICK_DURATION) {
            lastTickStart = now;
            doTick();
        }
        doUpdate();
    }
}

void Universe::doTick() {
    ProfilerSection profiler("tick");

    for (auto &dimension : dimensions) {
        dimension.second->doTick();
    }
}
void Universe::doUpdate() {
    ProfilerSection profiler("frame-update");
    for (auto &dimension : dimensions) {
        dimension.second->doUpdate();
    }
}

void Universe::setPause(bool paused) {
    pauseState = paused;
    if (!paused) {
        lastTickStart = std::chrono::high_resolution_clock::now() - TICK_DURATION;
    }
}
bool Universe::isPaused() {
    return pauseState;
}

Utilities::WorkerFleet &Universe::getWorkers() {
    return workers;
}

void Universe::setChunkLoadDistance(ChunkCoordRes distance) {
    chunkLoadRange = distance;
    for (auto &dimension : dimensions) {
        dimension.second->setChunkLoadDistance(distance);
    }
}
ChunkCoordRes Universe::getChunkLoadDistance() const {
    return chunkLoadRange;
}

// ClientUniverse
ClientUniverse::ClientUniverse(
    BindingManager *bindingManager,
    Utilities::WorkerFleet &workers,
    Engine::RenderEngine &renderEngine,
    Commands::Executor &executor,
    uint16_t viewDistance
) : Universe(workers),
    renderEngine(renderEngine),
    inputManager(renderEngine.getInputManager()),
    bindingManager(bindingManager),
    camera(90.f, {}),
    localPlayer(nullptr),
    lastPlayerDimension(nullptr),
    chunkViewDistance(viewDistance),
    console(executor)
{
    renderEngine.setCamera(camera);

    consoleKey = bindingManager->getBinding(Default::KeyBindings::OPEN_CONSOLE);

    dimensionRenderers[0] = std::make_unique<DimensionRenderer>(getDimension(0), renderEngine, workers, chunkViewDistance);

    // Initialise the dynamic renderers
    for (auto type : TileTypes::registry()->all()) {
        if (!type->getRenderer().isStatic()) {
            auto &renderer = static_cast<Rendering::DynamicTileRenderer&>(type->getRenderer());

            renderer.init(renderEngine);
        }
    }
}

void ClientUniverse::setLocalPlayer(LocalPlayer *player) {
    localPlayer = player;
}

void ClientUniverse::setChunkViewDistance(uint16_t viewDistance) {
    chunkViewDistance = viewDistance;

    for (auto &dimRenderer : dimensionRenderers) {
        dimRenderer.second->setViewRange(viewDistance);
    }
}

uint16_t ClientUniverse::getChunkViewDistance() const {
    return chunkViewDistance;
}

void ClientUniverse::showConsole() {
    auto bounds = renderEngine.getScreenBounds();
    
    consoleGui = std::make_shared<GuiConsole>(
        Engine::Gui::Rect{{0, 0}, {bounds.bottomRight.x, bounds.bottomRight.y / 2}},
        console,
        renderEngine.getFontManager().getFont("monospace", Engine::FontStyle::Regular)
    );
    Game::getInstance().setCurrentGUI(consoleGui);
}

void ClientUniverse::hideConsole() {
    // This could result in nothing happening for 1 press if something stole the GUI. whatever
    if (Game::getInstance().getCurrentGUI() == consoleGui) {
        Game::getInstance().setCurrentGUI({});
    }
    consoleGui.reset();
}

void ClientUniverse::update(double deltaTime) {
    Universe::update(deltaTime);

    if (consoleKey->wasPressed()) {
        if (consoleGui) {
            hideConsole();
        } else {
            // Dont steal the GUI
            if (!Game::getInstance().getCurrentGUI()) {
                showConsole();
            }
        }
    }

    // Update local player
    if (localPlayer) {
        ProfilerSection profiler("client-player-update");
        if (!isPaused()) {
            localPlayer->processInputs();
        }

        camera.setPosition(localPlayer->getEyePosition());
        camera.setForward(localPlayer->getForward());

        // Update viewable chunks
        if (lastPlayerDimension != localPlayer->getDimension() && lastPlayerDimension) {
            auto *oldRenderer = dimensionRenderers[lastPlayerDimension->getId()].get();
            oldRenderer->clearViewpoint();
        }

        auto dimensionId = localPlayer->getDimension()->getId();
        auto *renderer = dimensionRenderers[dimensionId].get();
        renderer->setViewpoint(localPlayer->getPosition(), localPlayer->getForward());

        lastPlayerDimension = localPlayer->getDimension();

        drawLookTile();
    }

    Profiler::enter("update renderers");
    for (auto &renderer : dimensionRenderers) {
        renderer.second->update(deltaTime);
    }
    Profiler::leave();

    bindingManager->doTick();
}

void ClientUniverse::drawLookTile() {
    auto result = localPlayer->getLookAtTile();
    if (result.didHitAny()) {
        std::optional<BoundingBox> bounds;
        if (result.result() == HitType::Tile) {
            bounds = result.tile()->getBounds();
        } else {
            bounds = result.subTile()->type->getBounds(result.slot());
        }
        
        if (bounds) {
            auto debugRenderer = Engine::Subsystem::DebugSubsystem::instance();
            auto renderBounds = bounds->expand(0.01f) + result.tilePosition();

            debugRenderer->debugDrawBox(
                {renderBounds.xMin, renderBounds.yMin, renderBounds.zMin},
                {renderBounds.xMax, renderBounds.yMax, renderBounds.zMax},
                0x20202020
            );
        }
    }

    auto item = localPlayer->getInventory().getSelectedItem();
    if (item) {
        item->getType().renderPlacementHighlight(*item, *localPlayer, *localPlayer->getDimension());
    }
}

}