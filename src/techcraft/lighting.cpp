#include "techcraft/lighting.hpp"
#include "techcraft/dimension/dimension.hpp"

#include <deque>
#include <unordered_set>
#include "techcraft/dimension/chunk.hpp"

namespace Techcraft {

#define DEBUG_FALLOFF 0.9f
#define MIN_INTENSITY 0.015

const TileDirection DIRECTIONS[6] = {
    TileDirection::East,
    TileDirection::West,
    TileDirection::North,
    TileDirection::South,
    TileDirection::Up,
    TileDirection::Down
};

LightingUpdater::LightingUpdater(Dimension &dimension)
: dimension(dimension)
{}

void LightingUpdater::recalculateLighting(const ChunkPosition &chunkCoord) {
    auto chunk = dimension.getChunk(chunkCoord.x, chunkCoord.y, chunkCoord.z);

    if (!chunk) {
        return;
    }

    std::deque<FloodState> fill;

    // Scan chunk for light emitting blocks
    for (uint8_t x = 0; x < ChunkSize; ++x) {
        for (uint8_t y = 0; y < ChunkSize; ++y) {
            for (uint8_t z = 0; z < ChunkSize; ++z) {
                auto &tile = chunk->getTile(x, y, z);

                if (tile.type->hasLight()) {
                    FloodState state = {
                        chunkCoord.x << ChunkBits | x,
                        chunkCoord.y << ChunkBits | y,
                        chunkCoord.z << ChunkBits | z
                    };
                    for (int i = 0; i < TILE_LIGHT_PALLET_COUNT; ++i) {
                        auto intensity = tile.type->getLightAll()[i];
                        state.tileLight[i] = toLightBaked(intensity, TileDirection::Self);
                        state.process[i] = true;
                    }

                    state.seed = true;
                    
                    fill.push_back(state);
                }
            }
        }
    }

    // Scan adjacent chunks for lighted tiles on the edges
    for (auto direction : DIRECTIONS) {
        const auto &offset = TileDirectionOffset::from(direction);
        auto other = dimension.getChunk(chunkCoord.x + offset.x, chunkCoord.y + offset.y, chunkCoord.z + offset.z);
        if (!other) {
            continue;
        }

        for (uint8_t i = 0; i < ChunkSize; ++i) {
            for (uint8_t j = 0; j < ChunkSize; ++j) {
                int8_t x, y, z;
                switch (direction) {
                    case TileDirection::East:
                        x = 0;
                        y = i;
                        z = j;
                        break;
                    case TileDirection::West:
                        x = ChunkSize - 1;
                        y = i;
                        z = j;
                        break;
                    case TileDirection::North:
                        x = i;
                        y = 0;
                        z = j;
                        break;
                    case TileDirection::South:
                        x = i;
                        y = ChunkSize - 1;
                        z = j;
                        break;
                    case TileDirection::Up:
                        x = i;
                        y = j;
                        z = 0;
                        break;
                    case TileDirection::Down:
                        x = i;
                        y = j;
                        z = ChunkSize - 1;
                        break;
                    default:
                        assert(false);
                }

                auto light = other->getTileLight(x, y, z);
                bool spill = false;

                FloodState state = {
                    ((chunkCoord.x + offset.x) << ChunkBits) + x,
                    ((chunkCoord.y + offset.y) << ChunkBits) + y,
                    ((chunkCoord.z + offset.z) << ChunkBits) + z,
                };
                for (int i = 0; i < TILE_LIGHT_PALLET_COUNT; ++i) {
                    uint8_t intensity = getIntensity(light.tileIntensity[i]);
                    TileDirection from = getDirection(light.tileIntensity[i]);
                    if (from == opposite(direction)) {
                        // If the light comes from this chunk, dont spill obviously
                        state.tileLight[i] = toLightBaked(0, TileDirection::Self);
                        continue;
                    }

                    if (intensity > 1) {
                        state.tileLight[i] = light.tileIntensity[i];
                        state.process[i] = true;

                        spill = true;
                    }
                }

                uint8_t skyIntensity = getIntensity(light.skyIntensity);
                TileDirection skyFrom = getDirection(light.skyIntensity);
                if (skyFrom == opposite(direction)) {
                    // If the light comes from this chunk, dont spill obviously
                    state.skyLight = toLightBaked(0, TileDirection::Self);
                    continue;
                }

                if (skyIntensity > 1) {
                    state.skyLight = light.skyIntensity;
                    state.processSky = true;

                    spill = true;
                }

                if (spill) {
                    fill.push_back(state);
                }
            }
        }
    }

    // Reset lighting to black
    std::fill_n(chunk->lightingByXYZ, ChunkVolume, TileLighting{});
    floodLight(fill, {}, true, chunkCoord);
}

void LightingUpdater::floodLight(const FloodState &state, bool positive) {
    std::deque<FloodState> positiveFill;
    std::deque<FloodState> negativeFill;

    if (positive) {
        positiveFill.push_back(state);
    } else {
        negativeFill.push_back(state);
    }

    floodLight(positiveFill, negativeFill);
}

void LightingUpdater::floodLight(const std::deque<FloodState> &positive, const std::deque<FloodState> &negative, bool singleChunkOnly, const ChunkPosition &limit) {
    std::deque<FloodState> positiveFill = positive;
    std::deque<FloodState> negativeFill = negative;
    std::unordered_set<ChunkPosition> visitedChunks;

    std::shared_ptr<Chunk> sourceChunk;
    if (singleChunkOnly) {
        sourceChunk = dimension.getChunk(limit.x, limit.y, limit.z);
    }
    
    while (!negativeFill.empty() || !positiveFill.empty()) {
        // Negative propagation has priority.
        if (!negativeFill.empty()) {
            auto next = negativeFill.front();
            negativeFill.pop_front();

            ChunkPosition chunkCoord = {next.x >> ChunkBits, next.y >> ChunkBits, next.z >> ChunkBits};

            auto chunk = dimension.getChunk(chunkCoord.x, chunkCoord.y, chunkCoord.z);
            if (!chunk) {
                // Dont proceed with this fill
                continue;
            }

            if (singleChunkOnly && chunk != sourceChunk) {
                // Dont allow passage outside scope
                continue;
            }

            visitedChunks.insert(chunkCoord);

            bool anyLeft = next.processSky;
            for (int i = 0; i < TILE_LIGHT_PALLET_COUNT; ++i) {
                if (next.process[i]) {
                    anyLeft = true;
                }
            }

            // Nothing left to do in this fill
            if (!anyLeft) {
                continue;
            }

            // When in negative mode, set the cell to 0 and then propogate to any neighbours
            // that source their light from this one.
            // For any neighbours that have a light value > 1 that dont source from this one,
            // add a positive propagation with check on them to fill light back in

            FloodState possibleNextStates[6];
            bool positivePropagate[6] = {false};

            for (TileDirection dir : DIRECTIONS) {
                auto offset = TileDirectionOffset::from(dir);
                auto x = next.x + offset.x;
                auto y = next.y + offset.y;
                auto z = next.z + offset.z;

                auto adjChunk = dimension.getChunkAt(x, y, z);
                if (!adjChunk) {
                    // Dont proceed with this fill
                    continue;
                }

                if (singleChunkOnly && adjChunk != sourceChunk) {
                    // Dont allow passage outside scope
                    continue;
                }

                auto light = adjChunk->getTileLight(x & ChunkMask, y & ChunkMask, z & ChunkMask);
                
                bool propagate = false;
                bool process[TILE_LIGHT_PALLET_COUNT] = {false};
                bool processSky = false;

                for (int i = 0; i < TILE_LIGHT_PALLET_COUNT; ++i) {
                    if (!next.process[i]) {
                        continue;
                    }

                    uint8_t existingIntensity = getIntensity(light.tileIntensity[i]);
                    TileDirection existingDirection = getDirection(light.tileIntensity[i]);
                    
                    if (existingDirection == opposite(dir) && existingIntensity > 0) {
                        // Propagate in negative mode
                        propagate = true;
                        process[i] = true;
                    } else if (existingIntensity > 1) {
                        // Note: we might want to make this only the brighest ones. All if there are multiple

                        int dirInt = static_cast<int>(dir);
                        if (!positivePropagate[dirInt]) {
                            positivePropagate[dirInt] = true;

                            possibleNextStates[dirInt] = {
                                x,
                                y,
                                z
                            };
                            possibleNextStates[dirInt].check = true;
                        }

                        possibleNextStates[dirInt].process[i] = true;
                        // No need to set light level because check is set.
                    }
                }

                if (next.processSky) {
                    uint8_t existingIntensity = getIntensity(light.skyIntensity);
                    TileDirection existingDirection = getDirection(light.skyIntensity);
                    
                    if (existingDirection == opposite(dir) && existingIntensity > 0) {
                        // Propagate in negative mode
                        propagate = true;
                        processSky = true;
                    } else if (existingIntensity > 1) {
                        // Note: we might want to make this only the brighest ones. All if there are multiple

                        int dirInt = static_cast<int>(dir);
                        if (!positivePropagate[dirInt]) {
                            positivePropagate[dirInt] = true;

                            possibleNextStates[dirInt] = {
                                x,
                                y,
                                z
                            };
                            possibleNextStates[dirInt].check = true;
                        }

                        possibleNextStates[dirInt].processSky = true;
                        // No need to set light level because check is set.
                    }
                }

                if (propagate) {
                    FloodState adjState = {x, y, z};
                    memcpy(adjState.process, process, sizeof(process));
                    adjState.processSky = processSky;
                    negativeFill.push_back(adjState);
                }
            }

            // Adjust self to be 0
            auto light = chunk->getTileLight(next.x & ChunkMask, next.y & ChunkMask, next.z & ChunkMask);
            for (int i = 0; i < TILE_LIGHT_PALLET_COUNT; ++i) {
                if (!next.process[i]) {
                    continue;
                }

                light.tileIntensity[i] = toLightBaked(0, TileDirection::Self);
            }
            if (next.processSky) {
                light.skyIntensity = toLightBaked(0, TileDirection::Self);
            }

            chunk->setTileLight(next.x & ChunkMask, next.y & ChunkMask, next.z & ChunkMask, light);

            // Do positive propagation
            for (auto dir : DIRECTIONS) {
                auto dirInt = static_cast<int>(dir);
                if (positivePropagate[dirInt]) {
                    positiveFill.push_back(possibleNextStates[dirInt]);
                }
            }

        // Positive propagation
        } else if (!positiveFill.empty()) {
            auto next = positiveFill.front();
            positiveFill.pop_front();

            ChunkPosition chunkCoord = {next.x >> ChunkBits, next.y >> ChunkBits, next.z >> ChunkBits};

            auto chunk = dimension.getChunk(chunkCoord.x, chunkCoord.y, chunkCoord.z);
            if (!chunk) {
                // Dont proceed with this fill
                continue;
            }

            if (singleChunkOnly && chunk != sourceChunk) {
                // Dont allow passage outside scope
                continue;
            }

            visitedChunks.insert(chunkCoord);

            // Seeds need to apply lighting to themselves first
            if (next.seed) {
                auto light = chunk->getTileLight(next.x & ChunkMask, next.y & ChunkMask, next.z & ChunkMask);
                bool brighter = false;
                for (int i = 0; i < TILE_LIGHT_PALLET_COUNT; ++i) {
                    if (next.process[i]) {
                        uint8_t existingIntensity = getIntensity(light.tileIntensity[i]);
                        uint8_t newIntensity = getIntensity(next.tileLight[i]);
                        if (newIntensity > existingIntensity) {
                            light.tileIntensity[i] = toLightBaked(newIntensity, TileDirection::Self);
                            brighter = true;
                        }
                    }
                }

                if (next.processSky) {
                    uint8_t existingIntensity = getIntensity(light.skyIntensity);
                    uint8_t newIntensity = getIntensity(next.skyLight);
                    if (newIntensity > existingIntensity) {
                        light.skyIntensity = toLightBaked(newIntensity, TileDirection::Self);
                        brighter = true;
                    }
                }

                if (brighter) {
                    chunk->setTileLight(next.x & ChunkMask, next.y & ChunkMask, next.z & ChunkMask, light);
                }
            }

            bool anyLeft = false;
            // Check pulls in the brightness from the current value
            if (next.check) {
                auto light = chunk->getTileLight(next.x & ChunkMask, next.y & ChunkMask, next.z & ChunkMask);
                for (int i = 0; i < TILE_LIGHT_PALLET_COUNT; ++i) {
                    if (next.process[i]) {
                        next.tileLight[i] = light.tileIntensity[i];
                    }
                }
                if (next.processSky) {
                    next.skyLight = light.skyIntensity;
                }
            }

            // Reduce next intensity for positive
            for (int i = 0; i < TILE_LIGHT_PALLET_COUNT; ++i) {
                uint8_t newIntensity = getIntensity(next.tileLight[i]);
                if (next.process[i]) {
                    // Decrease next intensity
                    if (newIntensity == 1) {
                        newIntensity = 0;
                    } else if (newIntensity > 1) {
                        --newIntensity;
                        anyLeft = true;
                    }

                    next.tileLight[i] = toLightBaked(newIntensity, getDirectionInt(next.tileLight[i]));
                }
            }
            if (next.processSky) {
                uint8_t newIntensity = getIntensity(next.skyLight);
                // Decrease next intensity
                if (newIntensity == 1) {
                    newIntensity = 0;
                } else if (newIntensity > 1) {
                    --newIntensity;
                    anyLeft = true;
                }

                next.skyLight = toLightBaked(newIntensity, getDirectionInt(next.skyLight));
            }

            // Nothing left to do in this fill
            if (!anyLeft) {
                continue;
            }

            // Once there is no more light to propogate, dont continue
            // Propagate to adjacent tiles
            for (TileDirection dir : DIRECTIONS) {
                auto offset = TileDirectionOffset::from(dir);
                auto x = next.x + offset.x;
                auto y = next.y + offset.y;
                auto z = next.z + offset.z;

                auto adjChunk = dimension.getChunk(x >> ChunkBits, y >> ChunkBits, z >> ChunkBits);
                if (!adjChunk) {
                    // Dont proceed with this fill
                    continue;
                }

                if (singleChunkOnly && adjChunk != sourceChunk) {
                    // Dont allow passage outside scope
                    continue;
                }

                auto &tile = dimension.getTile({x, y, z});
                    
                if (tile.type->isTransparent()) {
                    auto light = adjChunk->getTileLight(x & ChunkMask, y & ChunkMask, z & ChunkMask);
                    bool brighter = false;

                    uint8_t nextTileLight[TILE_LIGHT_PALLET_COUNT];
                    uint8_t nextSkyLight;
                    for (int i = 0; i < TILE_LIGHT_PALLET_COUNT; ++i) {
                        if (!next.process[i]) {
                            nextTileLight[i] = light.tileIntensity[i];
                            continue;
                        }

                        uint8_t existingIntensity = getIntensity(light.tileIntensity[i]);
                        uint8_t newIntensity = getIntensity(next.tileLight[i]);
                        if (newIntensity > existingIntensity) {
                            nextTileLight[i] = toLightBaked(newIntensity, opposite(dir));
                            light.tileIntensity[i] = nextTileLight[i];
                            brighter = true;
                        } else {
                            nextTileLight[i] = light.tileIntensity[i];
                        }
                    }
                    if (next.processSky) {
                        uint8_t existingIntensity = getIntensity(light.skyIntensity);
                        uint8_t newIntensity = getIntensity(next.skyLight);
                        if (newIntensity > existingIntensity) {
                            nextSkyLight = toLightBaked(newIntensity, opposite(dir));
                            light.skyIntensity = nextSkyLight;
                            brighter = true;
                        } else {
                            nextSkyLight = light.skyIntensity;
                        }
                    }

                    if (brighter) {
                        adjChunk->setTileLight(x & ChunkMask, y & ChunkMask, z & ChunkMask, light);
                        FloodState adjState = {x, y, z};
                        memcpy(adjState.tileLight, nextTileLight, sizeof(nextTileLight));
                        memcpy(adjState.process, next.process, sizeof(adjState.process));
                        adjState.skyLight = nextSkyLight;
                        adjState.processSky = next.processSky;
                        positiveFill.push_back(adjState);
                    }
                }
            }
        }
    }

    // Ensure all touched chunks are updated
    for (auto chunkCoord : visitedChunks) {
        auto chunk = dimension.getChunk(chunkCoord.x, chunkCoord.y, chunkCoord.z);
        if (chunk) {
            chunk->markDirty();
        }
    }
}

void LightingUpdater::recalculateLightingFromTile(const TilePosition &position, std::optional<TilePosition> highest) {
    // When a light is added, we need to flood fill (even across chunks), to work out the visual extent
    // When a light is removed, we also do a flood fill, but we are undoing the brightness
    // - Find brightness that didnt come from ourselves, then fill that back
    // When a non light block is added, check adjacent to see if that block was the light source
    //   then fill onwards

    auto &current = dimension.getTile(position);

    if (current.type->hasLight()) {
        // light source logic

        FloodState state = {position.x, position.y, position.z};
        for (int i = 0; i < TILE_LIGHT_PALLET_COUNT; ++i) {
            state.tileLight[i] = toLightBaked(current.type->getLightAll()[i], TileDirection::Self);
        }

        state.seed = true;
        std::fill_n(state.process, TILE_LIGHT_PALLET_COUNT, true);

        floodLight(state, true);
    } else if (current.type->isTransparent()) {
        // Block removed maybe, find brightest neighbour, then propogate from there
        // If any of the neibours used the current block as the source, then we need to enter below logic

        FloodState state = {position.x, position.y, position.z};
        std::fill_n(state.process, TILE_LIGHT_PALLET_COUNT, true);
        if (highest && !(position == *highest)) {
            state.processSky = true;
        }

        floodLight(state, false);
    } else {
        // Block added. Find neighbours that got light from this block.
        // Those ones that did, find new light source and adjust brightness.
        // If reduced, repeat on its neighbours.

        FloodState state = {position.x, position.y, position.z};
        std::fill_n(state.process, TILE_LIGHT_PALLET_COUNT, true);
        state.processSky = true;

        floodLight(state, false);
    }
}

void LightingUpdater::recalculateSkyForColumn(tcsize x, tcsize y, tcsize zMin, tcsize zMax, bool raised) {
    std::deque<FloodState> fill;

    if (raised) {
        // Everything from the last top to, but not including, the new top will do negative prop
        for (tcsize z = zMin; z < zMax; ++z) {
            FloodState state = {
                x,
                y,
                z
            };

            state.processSky = true;
            fill.push_back(state);
        }

        floodLight({}, fill);
    } else {
        // Everything from just above the new top to the old top (inclusive) will be filled with light
        for (tcsize z = zMin + 1; z <= zMax; ++z) {
            FloodState state = {
                x,
                y,
                z
            };

            state.processSky = true;
            state.seed = true;
            state.skyLight = toLightBaked(SKY_LIGHT_INTENSITY, TileDirection::Self);
            fill.push_back(state);
        }

        floodLight(fill, {});
    }
}

void LightingUpdater::update() {

}


glm::vec3 blendTileLighting(const uint8_t *tileLight) {
    glm::vec3 output = {0,0,0};
    for (auto i = 0; i < TILE_LIGHT_PALLET_COUNT; ++i) {
        uint8_t intensity = (tileLight[i] & 0b11111000) >> 3;
        output += TILE_LIGHTING_PALLETS[i] * (intensity / TILE_LIGHT_MAX_INTENSITY);
    }

    return output;
}
glm::vec3 blendSkyLighting(uint8_t skyLight) {
    uint8_t intensity = (skyLight & 0b11111000) >> 3;
    return (intensity / SKY_LIGHT_MAX_INTENSITY) * glm::vec3(1, 1, 1);
}

}