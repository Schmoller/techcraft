#include "techcraft/keybinding.hpp"

#include <functional>

namespace Techcraft {

//KeyBindingBuilder
KeyBindingBuilder::KeyBindingBuilder(const std::string_view &name, BindingManager &bindingManager)
    : name(name), bindingManager(bindingManager)
{}

KeyBindingBuilder &KeyBindingBuilder::withKey(Engine::Key key, Engine::Modifier modifiers) {
    binding = KeyBinding::Definition{key, modifiers};
    return *this;
}

KeyBindingBuilder &KeyBindingBuilder::withAltKey(Engine::Key key, Engine::Modifier modifiers) {
    alternate = KeyBinding::Definition{key, modifiers};
    return *this;
}

KeyBindingBuilder &KeyBindingBuilder::withContextMask(KeybindContext mask) {
    this->mask = mask;
    return *this;
}

std::shared_ptr<KeyBinding> KeyBindingBuilder::build() {
    auto keybind = std::make_shared<KeyBinding>(name, mask, binding, alternate);
    bindingManager.addBinding(keybind);

    return keybind;
}

// KeyBinding::Definition
KeyBinding::Definition::Definition(const Definition &other)
    : key(other.key), modifiers(other.modifiers)
{}

KeyBinding::Definition::Definition(Engine::Key key, Engine::Modifier modifiers)
    : key(key), modifiers(modifiers)
{}

bool KeyBinding::Definition::isActive(Engine::InputManager &inputManager, Engine::Modifier modifiers) const {
    return (inputManager.isPressedImmediate(key) && (modifiers & this->modifiers) == this->modifiers);
}

bool KeyBinding::Definition::isContained(const Definition &other) const {
    Engine::Modifier allModifiers = modifiers;
    bool modKey = false;
    switch (key) {
        case Engine::Key::eLeftControl:
        case Engine::Key::eRightControl:
            allModifiers |= Engine::ModifierFlag::Control;
            modKey = true;
            break;
        case Engine::Key::eLeftShift:
        case Engine::Key::eRightShift:
            allModifiers |= Engine::ModifierFlag::Shift;
            modKey = true;
            break;
        case Engine::Key::eLeftAlt:
        case Engine::Key::eRightAlt:
            allModifiers |= Engine::ModifierFlag::Alt;
            modKey = true;
            break;
        case Engine::Key::eLeftSuper:
        case Engine::Key::eRightSuper:
            allModifiers |= Engine::ModifierFlag::Super;
            modKey = true;
            break;
    }

    if ((other.modifiers & allModifiers) == allModifiers) {
        // All of our modifiers are contained within the other one
        return modKey || key == other.key;
    }

    return false;
}

// KeyBinding
KeyBinding::KeyBinding(const std::string &name, KeybindContext mask, std::optional<Definition> binding, std::optional<Definition> alternateBinding)
    : name(name), mask(mask), binding(binding), alternateBinding(alternateBinding)
{}

void KeyBinding::setBinding(const Definition &bind) {
    binding = bind;
}

void KeyBinding::unsetBinding() {
    binding = {};
}

void KeyBinding::setAlternateBinding(const Definition &bind) {
    alternateBinding = bind;
}

void KeyBinding::unsetAlternateBinding() {
    alternateBinding = {};
}

bool KeyBinding::wasPressed() {
    return !lastPressed && pressed;
}

bool KeyBinding::wasReleased() {
    return lastPressed && !pressed;
}

bool KeyBinding::isPressed() {
    return pressed;
}

void KeyBinding::update(bool pressed) {
    this->pressed = pressed;
}

void KeyBinding::tick() {
    lastPressed = pressed;
}

bool KeyBinding::isEnabledIn(KeybindContext context) const {
    return (context == KeybindContextFlag::None || mask & context);
}

// BindingManager
BindingManager::BindingManager(Engine::InputManager &inputManager)
    : inputManager(inputManager)
{
    inputManager.addCallback(
        std::bind(&BindingManager::onInputUpdate, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)
    );
}

void BindingManager::setEnabled(bool enabled) {
    this->enabled = enabled;
}

void BindingManager::setContext(KeybindContextFlag context) {
    currentContext = context;
}

void BindingManager::doTick() {
    for (auto &keybinding : bindings) {
        keybinding->tick();
    }
}

bool BindingManager::forceCheckBindingActive(const KeyBinding &binding) {
    Engine::Modifier modifiers;
    if (inputManager.isPressedImmediate(Engine::Key::eLeftControl)) {
        modifiers |= Engine::ModifierFlag::Control;
    } else if (inputManager.isPressedImmediate(Engine::Key::eRightControl)) {
        modifiers |= Engine::ModifierFlag::Control;
    }

    if (inputManager.isPressedImmediate(Engine::Key::eLeftShift)) {
        modifiers |= Engine::ModifierFlag::Shift;
    } else if (inputManager.isPressedImmediate(Engine::Key::eRightShift)) {
        modifiers |= Engine::ModifierFlag::Shift;
    }

    if (inputManager.isPressedImmediate(Engine::Key::eLeftAlt)) {
        modifiers |= Engine::ModifierFlag::Alt;
    } else if (inputManager.isPressedImmediate(Engine::Key::eRightAlt)) {
        modifiers |= Engine::ModifierFlag::Alt;
    }

    if (inputManager.isPressedImmediate(Engine::Key::eLeftSuper)) {
        modifiers |= Engine::ModifierFlag::Super;
    } else if (inputManager.isPressedImmediate(Engine::Key::eRightSuper)) {
        modifiers |= Engine::ModifierFlag::Super;
    }

    auto primary = binding.getBinding();
    auto secondary = binding.getAlternateBinding();

    bool primaryActive = (primary && primary->isActive(inputManager, modifiers));
    bool secondaryActive = (secondary && secondary->isActive(inputManager, modifiers));

    return primaryActive || secondaryActive;
}

bool BindingManager::forceCheckBindingActive(const std::string_view &name) {
    auto binding = getBinding(name);
    if (binding) {
        return forceCheckBindingActive(*binding);
    } else {
        return false;
    }
}


void BindingManager::onInputUpdate(Engine::Key key, Engine::Action action, const Engine::Modifier &inModifiers) {
    if (!enabled) {
        return;
    }

    if (action == Engine::Action::Repeat) {
        return;
    }

    // If a modifier key is pressed / released, the incoming modifiers does not reflect the change
    Engine::Modifier modifiers = inModifiers;
    if (action == Engine::Action::Press) {
        switch (key) {
            case Engine::Key::eLeftControl:
            case Engine::Key::eRightControl:
                modifiers |= Engine::ModifierFlag::Control;
                break;
            case Engine::Key::eLeftShift:
            case Engine::Key::eRightShift:
                modifiers |= Engine::ModifierFlag::Shift;
                break;
            case Engine::Key::eLeftAlt:
            case Engine::Key::eRightAlt:
                modifiers |= Engine::ModifierFlag::Alt;
                break;
            case Engine::Key::eLeftSuper:
            case Engine::Key::eRightSuper:
                modifiers |= Engine::ModifierFlag::Super;
                break;
            default:
                break;
        }
    } else {
        switch (key) {
            case Engine::Key::eLeftControl:
                if (!inputManager.isPressed(Engine::Key::eRightControl)) {
                    modifiers &= ~modifiers & Engine::ModifierFlag::Control;
                }
                break;
            case Engine::Key::eRightControl:
                if (!inputManager.isPressed(Engine::Key::eLeftControl)) {
                    modifiers &= ~modifiers & Engine::ModifierFlag::Control;
                }
                break;
            case Engine::Key::eLeftShift:
                if (!inputManager.isPressed(Engine::Key::eRightShift)) {
                    modifiers &= ~modifiers & Engine::ModifierFlag::Shift;
                }
                break;
            case Engine::Key::eRightShift:
                if (!inputManager.isPressed(Engine::Key::eLeftShift)) {
                    modifiers &= ~modifiers & Engine::ModifierFlag::Shift;
                }
                break;
            case Engine::Key::eLeftAlt:
                if (!inputManager.isPressed(Engine::Key::eRightAlt)) {
                    modifiers &= ~modifiers & Engine::ModifierFlag::Alt;
                }
                break;
            case Engine::Key::eRightAlt:
                if (!inputManager.isPressed(Engine::Key::eLeftAlt)) {
                    modifiers &= ~modifiers & Engine::ModifierFlag::Alt;
                }
                break;
            case Engine::Key::eLeftSuper:
                if (!inputManager.isPressed(Engine::Key::eRightSuper)) {
                    modifiers &= ~modifiers & Engine::ModifierFlag::Super;
                }
                break;
            case Engine::Key::eRightSuper:
                if (!inputManager.isPressed(Engine::Key::eLeftSuper)) {
                    modifiers &= ~modifiers & Engine::ModifierFlag::Super;
                }
                break;
            default:
                break;
        }

    }

    std::vector<std::shared_ptr<KeyBinding>> updatedBindings;

    // Update state of all keybindings
    for (auto &keybinding : bindings) {
        if (keybinding->isEnabledIn(currentContext)) {
            auto primary = keybinding->getBinding();
            auto secondary = keybinding->getAlternateBinding();

            bool primaryActive = (primary && primary->isActive(inputManager, modifiers));
            bool secondaryActive = (secondary && secondary->isActive(inputManager, modifiers));

            keybinding->update(primaryActive || secondaryActive);
            updatedBindings.push_back(keybinding);
        } else if (keybinding->isPressed()) {
            // If we are masking, we dont allow the keybind to be active
            keybinding->update(false);
            updatedBindings.push_back(keybinding);
        }
    }

    // check any binding that may be shadowed by others
    // we only need to check active bindings, we dont need to raise bindings here
    for (auto &keybinding : bindings) {
        auto primary = keybinding->getBinding();
        auto secondary = keybinding->getAlternateBinding();

        if (!keybinding->isPressed()) {
            continue;
        }

        bool cancelled = false;

        for (auto &other : bindings) {
            if (other.get() == keybinding.get()) {
                continue;
            }

            // An unpressed keybinding isnt shadowing us
            if (!other->isPressed()) {
                continue;
            }

            auto otherPrimary = other->getBinding();
            auto otherSecondary = other->getAlternateBinding();

            if (primary) {
                if (otherPrimary && primary->isContained(*otherPrimary)) {
                    // Shadowed
                    cancelled = true;
                }
                if (otherSecondary && primary->isContained(*otherSecondary)) {
                    // Shadowed
                    cancelled = true;
                }
            }
            if (secondary) {
                if (otherPrimary && secondary->isContained(*otherPrimary)) {
                    // Shadowed
                    cancelled = true;
                }
                if (otherSecondary && secondary->isContained(*otherSecondary)) {
                    // Shadowed
                    cancelled = true;
                }
            }

            if (cancelled) {
                break;
            }
        }

        if (cancelled) {
            keybinding->update(false);
            updatedBindings.push_back(keybinding);
        }
    }
}

KeyBindingBuilder BindingManager::createBinding(const std::string_view &name) {
    return KeyBindingBuilder(name, *this);
}

std::shared_ptr<KeyBinding> BindingManager::getBinding(const std::string_view &name) const {
    for (auto &binding : bindings) {
        if (binding->getName() == name) {
            return binding;
        }
    }

    return {};
}

void BindingManager::addBinding(const std::shared_ptr<KeyBinding> &binding) {
    bindings.push_back(binding);
}

}