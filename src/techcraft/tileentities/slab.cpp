#include "techcraft/tileentities/slab.hpp"

namespace Techcraft {

// SlabTileEntity
SlabTileEntity::SlabTileEntity(const TilePosition &pos)
    : TileEntity(pos), attached(TileDirection::Down)
{}

SlabTileEntity::SlabTileEntity(const TilePosition &pos, TileDirection attachment)
    : TileEntity(pos), attached(attachment)
{}

TileDirection SlabTileEntity::getAttached() const {
    return attached;
}

void SlabTileEntity::setAttached(TileDirection face) {
    if (face != attached) {
        markDirty();
    }
    attached = face;
}

bool SlabTileEntity::isSolidOn(TileDirection face) const {
    return face == attached;
}

std::optional<BoundingBox> SlabTileEntity::getBounds() const {
    switch (attached) {
        case TileDirection::Up:
            return BoundingBox{0.0f, 0.0f, 0.5f,  1.0f, 1.0f, 1.0f};
        case TileDirection::Down:
            return BoundingBox{0.0f, 0.0f, 0.0f,  1.0f, 1.0f, 0.5f};
        case TileDirection::North:
            return BoundingBox{0.0f, 0.5f, 0.0f,  1.0f, 1.0f, 1.0f};
        case TileDirection::South:
            return BoundingBox{0.0f, 0.0f, 0.0f,  1.0f, 0.5f, 1.0f};
        case TileDirection::East:
            return BoundingBox{0.5f, 0.0f, 0.0f,  1.0f, 1.0f, 1.0f};
        case TileDirection::West:
            return BoundingBox{0.0f, 0.0f, 0.0f,  0.5f, 1.0f, 1.0f};
        default:
            return BoundingBox{1.0f, 1.0f, 1.0f, false};
    }
}


}