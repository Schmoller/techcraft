#include "techcraft/tileentities/itemtube.hpp"
#include "techcraft/dimension/dimension.hpp"

#include <iostream>

namespace Techcraft {

const TileDirection AttachmentSidePreferences[] = {
    TileDirection::Down,
    TileDirection::North,
    TileDirection::South,
    TileDirection::East,
    TileDirection::West,
    TileDirection::Up
};

ItemTubeTileEntity::ItemTubeTileEntity(const TilePosition &pos)
    : TileEntity(pos)
{}

std::optional<BoundingBox> ItemTubeTileEntity::getBounds() const {
    return {}; // TODO: This
}

bool ItemTubeTileEntity::isSolidOn(TileDirection face) const {
    return false;
}

void ItemTubeTileEntity::onPlace() {
    // Look at surrounding tiles and find ones that are item tubes, then connect ourself and them
    for (auto dir : AllDirections) {
        auto pos = getPosition() + TileDirectionOffset::from(dir);
        auto &tile = getDimension().getTile(pos);

        if (tile.getTileEntity()) {
            ItemTubeTileEntity *otherTube = dynamic_cast<ItemTubeTileEntity*>(tile.getTileEntity().get());
            if (otherTube) {
                // TODO: There might be reasons we dont want to connect the tube. This is where we would check those
                otherTube->setConnectedOn(opposite(dir), true);
                setConnectedOn(dir, true);
            }
        }
    }

    updateAttachment();
}

void ItemTubeTileEntity::onNeighbourUpdate(const TilePosition &neighbourPos, const Tile &neighbour) {
    for (auto dir : AllDirections) {
        auto pos = getPosition() + TileDirectionOffset::from(dir);
        auto &tile = getDimension().getTile(pos);

        if (tile.getTileEntity()) {
            ItemTubeTileEntity *otherTube = dynamic_cast<ItemTubeTileEntity*>(tile.getTileEntity().get());
            if (otherTube) {
                setConnectedOn(dir, true);
            }
        } else {
            setConnectedOn(dir, false);
        }
    }

    updateAttachment();
}

void ItemTubeTileEntity::updateAttachment() {
    if (attachSide != TileDirection::Self) {
        // Keep attachment if still viable
        auto pos = getPosition() + TileDirectionOffset::from(attachSide);
        auto &tile = getDimension().getTile(pos);

        if (tile.isSolidOnSide(opposite(attachSide))) {
            // Still good
            return;
        }
        // Not good
        attachSide = TileDirection::Self;
    }

    // Find a new attachment side
    for (auto dir : AttachmentSidePreferences) {
        auto pos = getPosition() + TileDirectionOffset::from(dir);
        auto &tile = getDimension().getTile(pos);

        if (tile.isSolidOnSide(opposite(dir))) {
            // Good to attach to
            attachSide = dir;
            break;
        }
    }
}

}