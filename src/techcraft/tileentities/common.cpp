#include "techcraft/tileentities/common.hpp"

namespace Techcraft {

TileEntity::TileEntity(const TilePosition &pos)
    : position(pos), dimension(nullptr)
{}

const TilePosition &TileEntity::getPosition() const {
    return position;
}

const Dimension &TileEntity::getDimension() const {
    return *dimension;
}

void TileEntity::registerInto(Dimension *dimension) {
    this->dimension = dimension;
}

std::optional<BoundingBox> TileEntity::getBounds() const {
    return {};
}

bool TileEntity::isSolidOn(TileDirection face) const {
    return true;
}

void TileEntity::markDirty() {
    dirty = true;
    // TODO: Notify?
}

void TileEntity::markClean() {
    dirty = false;
}

}