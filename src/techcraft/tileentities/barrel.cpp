#include "techcraft/tileentities/barrel.hpp"

#include <iostream>

namespace Techcraft {

BarrelTileEntity::BarrelTileEntity(const TilePosition &pos)
    : TileEntity(pos)
{
}

bool BarrelTileEntity::isSolidOn(TileDirection face) const {
    return false;
}

}