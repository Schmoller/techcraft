#include "techcraft/rand.hpp"

namespace Techcraft {

Random::Random(uint64_t seed)
    : internalRand(seed)
{}

uint64_t Random::randRange(uint64_t min, uint64_t max) {
    auto value = internalRand();

    return min + (value % (max - min));
}

float Random::randFloat() {
    auto intValue = internalRand();
    return static_cast<float>(intValue - internalRand.min()) / static_cast<float>(internalRand.max() - internalRand.min());
}

}