#include "techcraft/physics.hpp"
#include "techcraft/dimension/dimension.hpp"
#include "engine/subsystem/debug.hpp"

#include <functional>
#include <iostream>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/projection.hpp>

#define StepUpStep 0.05

// Used to deal with floating-point precision issues when it comes to collision prevention
// If we do not use this, then entities are able to phase through tiles in certain specific locations
#define PRECISION_OFFSET 0.00001

namespace Techcraft {

PhysicsSimulation::PhysicsSimulation(Dimension &dimension)
    : dimension(dimension), lastStepTime(std::chrono::high_resolution_clock::now())
{
}

void PhysicsSimulation::physicsStep() {
    // auto debug = Engine::Subsystem::DebugSubsystem::instance();
    auto currentTime = std::chrono::high_resolution_clock::now();
    double delta = static_cast<std::chrono::duration<double>>(currentTime - lastStepTime).count();
    // double delta = 0.016;
    
    for (auto entity : physicsEntities) {
        // TODO: It would be nice if I didnt need to do this cast to get access to both
        // position and velocity
        PhysicsObject *phys = dynamic_cast<PhysicsObject*>(entity);

        if (!phys->isPhysicsEnabled()) {
            continue;
        }

        glm::highp_dvec3 positionInitial = {
            entity->getPosition().x,
            entity->getPosition().y,
            entity->getPosition().z
        };

        glm::highp_dvec3 velocity = phys->getVelocity();

        TilePosition tpos = entity->getPosition();

        // X(t) = X0 + V0 * t
        glm::highp_dvec3 endPosition = positionInitial + velocity * delta;

        BoundingBox collisionBox = entity->getBounds() + entity->getPosition();
        if (phys->isCollidable()) {
            // Expand collision box to encompas the entire path between where it was and will be.
            BoundingBox expanded = collisionBox.expandSkew(velocity.x * delta, velocity.y * delta, velocity.z * delta);

            // debug->debugDrawBox(
            //     {collisionBox.xMin, collisionBox.yMin, collisionBox.zMin},
            //     {collisionBox.xMax, collisionBox.yMax, collisionBox.zMax},
            //     0x00FF00
            // );
            // debug->debugDrawBox(
            //     {expanded.xMin, expanded.yMin, expanded.zMin},
            //     {expanded.xMax, expanded.yMax, expanded.zMax},
            //     0x00FFFF
            // );

            // Find a collision
            glm::highp_dvec3 moveDelta = endPosition - positionInitial;

            // Will be recalculated
            endPosition = positionInitial;
            
            // TODO: Entity-Entity collisions

            // Tile collisions
            bool hitX = false;
            bool hitY = false;
            bool hitZ = false;

            bool lockX = false;
            bool lockY = false;

            if (moveDelta.z != 0) {
                testBoundingBoxes(expanded, [&](BoundingBox tileBounds) {
                    
                    if (
                        collisionBox.xMax > tileBounds.xMin && collisionBox.xMin < tileBounds.xMax &&
                        collisionBox.yMax > tileBounds.yMin && collisionBox.yMin < tileBounds.yMax
                    ) {
                        if (moveDelta.z > 0 && collisionBox.zMax <= tileBounds.zMin) {
                            double dist = tileBounds.zMin - collisionBox.zMax;
                            if (dist < moveDelta.z) {
                                moveDelta.z = dist - PRECISION_OFFSET;
                                hitZ = true;
                            }
                        } else if (moveDelta.z < 0 && collisionBox.zMin >= tileBounds.zMax) {
                            double dist = tileBounds.zMax - collisionBox.zMin;
                            if (dist > moveDelta.z) {
                                moveDelta.z = dist + PRECISION_OFFSET;
                                hitZ = true;
                            }
                        }
                    }

                    return true;
                });
            }

            collisionBox.offsetZ(moveDelta.z);
            endPosition.z += moveDelta.z;

            if (phys->getPreventDropoff() && (hitZ && velocity.z < 0)) {
                // Prevent the entity from dropping off an edge by
                // limiting the amount we intend to move to be no further
                // than that which would take us off the edge.

                if (moveDelta.x != 0) {
                    double moveX = moveDelta.x;
                    
                    bool didAnything = false;

                    do {
                        BoundingBox offsetBox = collisionBox + Position{static_cast<tcfloat>(moveX), static_cast<tcfloat>(0), static_cast<tcfloat>(-StepUpStep)};

                        bool inAir = true;
                        double dist = 0;
                        testBoundingBoxes(expanded, [&](BoundingBox tileBounds) {
                            if (tileBounds.intersects(offsetBox)) {
                                inAir = false;

                                if (moveDelta.x > 0) {
                                    dist = std::max(dist, tileBounds.xMax - collisionBox.xMin - PRECISION_OFFSET);
                                } else if (moveDelta.x < 0) {
                                    dist = std::min(dist, tileBounds.xMin - collisionBox.xMax + PRECISION_OFFSET);
                                }

                            }
                            return true;
                        });

                        if (!inAir) {
                            if (didAnything) {
                                if (moveDelta.x < 0) {
                                    moveDelta.x = std::min(moveX, dist);
                                } else {
                                    moveDelta.x = std::max(moveX, dist);
                                }
                                lockX = true;
                            }
                            break;
                        }

                        didAnything = true;

                        if (moveX == 0) {
                            break;
                        }

                        if (moveX < -StepUpStep) {
                            moveX += StepUpStep;
                        } else if (moveX > StepUpStep) {
                            moveX -= StepUpStep;
                        } else {
                            moveX = 0;
                        }
                    } while (true);
                }

                if (moveDelta.y != 0) {
                    double moveY = moveDelta.y;
                    
                    bool didAnything = false;

                    do {
                        BoundingBox offsetBox = collisionBox + Position{static_cast<tcfloat>(moveDelta.x), static_cast<tcfloat>(moveY), static_cast<tcfloat>(-StepUpStep)};

                        bool inAir = true;
                        double dist = 0;
                        testBoundingBoxes(expanded, [&](BoundingBox tileBounds) {
                            if (tileBounds.intersects(offsetBox)) {
                                inAir = false;

                                if (moveDelta.y > 0) {
                                    dist = std::max(dist, tileBounds.yMax - collisionBox.yMin - PRECISION_OFFSET);
                                } else if (moveDelta.y < 0) {
                                    dist = std::min(dist, tileBounds.yMin - collisionBox.yMax + PRECISION_OFFSET);
                                }

                            }
                            return true;
                        });

                        if (!inAir) {
                            if (didAnything) {
                                if (moveDelta.y < 0) {
                                    moveDelta.y = std::min(moveY, dist);
                                } else {
                                    moveDelta.y = std::max(moveY, dist);
                                }

                                lockY = true;
                            }
                            break;
                        }

                        didAnything = true;

                        if (moveY == 0) {
                            break;
                        }

                        if (moveY < -StepUpStep) {
                            moveY += StepUpStep;
                        } else if (moveY > StepUpStep) {
                            moveY -= StepUpStep;
                        } else {
                            moveY = 0;
                        }
                    } while (true);
                }
            }

            if (moveDelta.x != 0) {
                double originalX = moveDelta.x;
                testBoundingBoxes(expanded, [&](BoundingBox tileBounds) {
                    if (
                        collisionBox.yMax > tileBounds.yMin && collisionBox.yMin < tileBounds.yMax &&
                        collisionBox.zMax > tileBounds.zMin && collisionBox.zMin < tileBounds.zMax
                    ) {
                        if (moveDelta.x > 0 && collisionBox.xMax <= tileBounds.xMin) {
                            double dist = tileBounds.xMin - collisionBox.xMax;
                            if (dist < moveDelta.x) {
                                moveDelta.x = dist - PRECISION_OFFSET;
                                hitX = true;
                            }
                        } else if (moveDelta.x < 0 && collisionBox.xMin >= tileBounds.xMax) {
                            double dist = tileBounds.xMax - collisionBox.xMin;
                            if (dist > moveDelta.x) {
                                moveDelta.x = dist + PRECISION_OFFSET;
                                hitX = true;
                            }
                        }
                    }

                    return true;
                });

                if (hitX) {
                    // See if there is an option to step-up
                    if (phys->getStepUp() > 0 && hitZ && velocity.z < 0) {
                        BoundingBox expandedStepUp = expanded.expandSkew(0, 0, phys->getStepUp());
                        for (double step = StepUpStep; step < phys->getStepUp() + StepUpStep; step += StepUpStep) {
                            BoundingBox offsetBox = collisionBox + Position{static_cast<tcfloat>(originalX), static_cast<tcfloat>(0), static_cast<tcfloat>(step)};

                            bool isOk = true;
                            testBoundingBoxes(expandedStepUp, [&offsetBox, &isOk](BoundingBox tileBounds) {
                                if (tileBounds.intersects(offsetBox)) {
                                    isOk = false;
                                    return false;
                                }
                                return true;
                            });

                            if (isOk) {
                                collisionBox.offsetZ(step);
                                endPosition.z += step;
                                moveDelta.x = originalX;
                                // NOTE: Should we keep the hit flag?
                                hitX = false;
                                break;
                            }
                        }
                    }
                }
            }

            collisionBox.offsetX(moveDelta.x);
            endPosition.x += moveDelta.x;

            if (moveDelta.y != 0) {
                double originalY = moveDelta.y;
                testBoundingBoxes(expanded, [&hitY, &moveDelta, &collisionBox](BoundingBox tileBounds) {
                    if (
                        collisionBox.xMax > tileBounds.xMin && collisionBox.xMin < tileBounds.xMax &&
                        collisionBox.zMax > tileBounds.zMin && collisionBox.zMin < tileBounds.zMax
                    ) {
                        if (moveDelta.y > 0 && collisionBox.yMax <= tileBounds.yMin) {
                            double dist = tileBounds.yMin - collisionBox.yMax;
                            if (dist < moveDelta.y) {
                                moveDelta.y = dist - PRECISION_OFFSET;
                                hitY = true;
                            }
                        } else if (moveDelta.y < 0 && collisionBox.yMin >= tileBounds.yMax) {
                            double dist = tileBounds.yMax - collisionBox.yMin;
                            if (dist > moveDelta.y) {
                                moveDelta.y = dist + PRECISION_OFFSET;
                                hitY = true;
                            }
                        }
                    }

                    return true;
                });

                if (hitY) {
                    // See if there is an option to step-up
                    if (phys->getStepUp() > 0 && hitZ && velocity.z < 0) {
                        BoundingBox expandedStepUp = expanded.expandSkew(0, 0, phys->getStepUp());
                        for (double step = StepUpStep; step < phys->getStepUp() + StepUpStep; step += StepUpStep) {
                            BoundingBox offsetBox = collisionBox + Position{static_cast<tcfloat>(0), static_cast<tcfloat>(originalY), static_cast<tcfloat>(step)};

                            bool isOk = true;
                            testBoundingBoxes(expandedStepUp, [&offsetBox, &isOk](BoundingBox tileBounds) {
                                if (tileBounds.intersects(offsetBox)) {
                                    isOk = false;
                                    return false;
                                }
                                return true;
                            });

                            if (isOk) {
                                collisionBox.offsetZ(step);
                                endPosition.z += step;
                                moveDelta.y = originalY;
                                // NOTE: Should we keep the hit flag?
                                hitY = false;
                                break;
                            }
                        }
                    }
                }
            }

            collisionBox.offsetY(moveDelta.y);
            endPosition.y += moveDelta.y;

            phys->standing = hitZ && velocity.z < 0;

            if (hitX || lockX) {
                velocity.x = 0;
            }
            if (hitY || lockY) {
                velocity.y = 0;
            }
            if (hitZ) {
                velocity.z = 0;
            }
        }

        entity->setPosition(endPosition);

        // Check for fluid
        bool inFluid = false;

        testFluidBoundingBoxes(collisionBox, [&](BoundingBox fluidBounds) {
            if (collisionBox.intersects(fluidBounds)) {
                inFluid = true;
                return false;
            }

            return true;
        });

        phys->inFluid = inFluid;

        // Update velocity
        glm::highp_dvec3 accelleration = {};
        if (phys->isGravityEnabled()) {
            accelleration += gravity * phys->getGravityInfuence();
        }
        
        // V(t) = V0 + A0 * t
        velocity = velocity * phys->getDrag() + accelleration * delta;

        phys->setVelocity(velocity);
    }

    lastStepTime = currentTime;
}

void PhysicsSimulation::onEntityAdd(Entity *entity) {
    auto *physicsEntity = dynamic_cast<PhysicsObject*>(entity);
    if (physicsEntity) {
        physicsEntities.push_back(entity);
    }
}

void PhysicsSimulation::onEntityRemove(Entity *entity) {
    auto *physicsEntity = dynamic_cast<PhysicsObject*>(entity);
    if (!physicsEntity) {
        return;
    }

    auto it = physicsEntities.begin();
    for (; it != physicsEntities.end(); ++it) {
        if (*it == entity) {
            physicsEntities.erase(it);
            return;
        }
    }
}

void PhysicsSimulation::testBoundingBoxes(const BoundingBox &testArea, std::function<bool(BoundingBox)> testFunction) {
    tcsize xMin = static_cast<tcsize>(std::floor(testArea.xMin));
    tcsize yMin = static_cast<tcsize>(std::floor(testArea.yMin));
    tcsize zMin = static_cast<tcsize>(std::floor(testArea.zMin));
    tcsize xMax = static_cast<tcsize>(std::ceil(testArea.xMax));
    tcsize yMax = static_cast<tcsize>(std::ceil(testArea.yMax));
    tcsize zMax = static_cast<tcsize>(std::ceil(testArea.zMax));

    for (auto x = xMin; x < xMax; ++x) {
        for (auto y = yMin; y < yMax; ++y) {
            for (auto z = zMin; z < zMax; ++z) {
                auto &tile = dimension.getTile(x, y, z);
                std::optional<BoundingBox> tileBounds = tile.getBounds();
                if (tileBounds) {
                    auto testBounds = *tileBounds + Position{x, y, z};
                    if (!testFunction(testBounds)) {
                        return;
                    }
                }

                auto container = tile.getSubtileContainer();
                if (container) {
                    bool done = false;
                    container->forEach([&](auto &slot, const Subtile &subtile) {
                        if (subtile.type) {
                            auto bounds = subtile.type->getBounds(slot);
                            if (bounds) {
                                auto testBounds = *bounds + Position{x, y, z};
                                if (!testFunction(testBounds)) {
                                    done = true;
                                }
                            }
                        }
                    });
                    if (done) {
                        return;
                    }
                }
            }
        }
    }
}

void PhysicsSimulation::testFluidBoundingBoxes(const BoundingBox &testArea, std::function<bool(BoundingBox)> testFunction) {
    tcsize xMin = static_cast<tcsize>(std::floor(testArea.xMin));
    tcsize yMin = static_cast<tcsize>(std::floor(testArea.yMin));
    tcsize zMin = static_cast<tcsize>(std::floor(testArea.zMin));
    tcsize xMax = static_cast<tcsize>(std::ceil(testArea.xMax));
    tcsize yMax = static_cast<tcsize>(std::ceil(testArea.yMax));
    tcsize zMax = static_cast<tcsize>(std::ceil(testArea.zMax));

    for (auto x = xMin; x < xMax; ++x) {
        for (auto y = yMin; y < yMax; ++y) {
            for (auto z = zMin; z < zMax; ++z) {
                auto &tile = dimension.getFluid(x, y, z);
                std::optional<BoundingBox> tileBounds = tile.getBounds();
                if (tileBounds) {
                    auto testBounds = *tileBounds + Position{x, y, z};
                    if (!testFunction(testBounds)) {
                        return;
                    }
                }
            }
        }
    }
}
}