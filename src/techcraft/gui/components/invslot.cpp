#include "techcraft/gui/components/invslot.hpp"
#include "engine/gui/drawer.hpp"

#include "techcraft/gui/utils.hpp"

#include <iostream>

namespace Techcraft {

GuiInventorySlot::GuiInventorySlot(Inventory &inventory, uint32_t slot, const glm::vec2 &coords, const glm::vec2 &size)
    : inventory(inventory), slot(slot), BaseComponent({coords, coords + size})
{
}

void GuiInventorySlot::setHover(bool hover) {
    this->hover = hover;
    markDirty();
}

void GuiInventorySlot::setItem(const OptionalItemStack &item) {
    inventory[slot] = item;
    markDirty();
}

void GuiInventorySlot::onPaint(_G::Drawer &drawer) {
    static auto font = drawer.getFontManager().getFont("monospace", Engine::FontStyle::Regular);

    auto stack = inventory[slot];

    drawer.drawRect(bounds, 0xFCF5F51A);

    if (stack) {
        drawItemStack(drawer, *stack, font, bounds.left(), bounds.top(), bounds.width(), bounds.height());
    }

    if (hover) {
        drawer.drawRectOutline(
            {
                {bounds.left() - 2, bounds.top() - 2},
                {bounds.right() + 2, bounds.bottom() + 2}
            },
            1,
            Engine::Gui::StrokePosition::Inside,
            0xFCF5F5FF
        );
    }
}

void GuiInventorySlot::onFrameUpdate() {

}

}