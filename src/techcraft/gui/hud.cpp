#include "engine/gui/drawer.hpp"

#include "techcraft/gui/hud.hpp"
#include "techcraft/gui/utils.hpp"

#define GUI_SCALE 2
#define IMAGE_HUD_SLOT_WIDTH 17

const Engine::Gui::Rect IMAGE_HUD_INVENTORY = {
    {0, 235},
    {177, 256}
};
const Engine::Gui::Rect IMAGE_HUD_SELECTOR = {
    {192, 238},
    {210, 256}
};

const char *slotNames[10] = {
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "0"
};

namespace Techcraft {

GuiHUD::GuiHUD(const _G::Rect &bounds, LocalPlayer *player, Engine::TextureManager &textureManager, Engine::Font *itemFont)
    : BaseComponent(bounds), player(player), itemFont(itemFont), lastSelectedSlot(0), lastItems({})
{
    hudTexture = textureManager.createTexture("gui.hud")
        .fromFile("assets/textures/gui/hud.png")
        .build();
    
    whiteTexture = textureManager.getTexture("internal.white");
}

void GuiHUD::onFrameUpdate() {
    auto &inventory = player->getInventory();
    auto slot = inventory.getSelected();

    bool changed = false;
    if (slot != lastSelectedSlot) {
        changed = true;
        lastSelectedSlot = slot;
    }

    for (int i = 0; i < 10; ++i) {
        if (!(lastItems[i] == inventory[i])) {
            changed = true;
            lastItems[i] = inventory[i];
        }
    }

    if (changed) {
        markDirty();
    }
}

void GuiHUD::onPaint(_G::Drawer &drawer) {
    auto &inventory = player->getInventory();
    auto slot = inventory.getSelected();

    float screenWidth = (bounds.bottomRight.x - bounds.topLeft.x);

    // Toolbar
    const float toolbarSlotW = 71;
    float spacing = 28;
    float toolbarX = (screenWidth - ((toolbarSlotW * 10) + (spacing * 9))) / 2;

    for (int i = 0; i < 10; ++i) {
        float slotX = toolbarX + ((spacing + toolbarSlotW) * i);
        float slotY = bounds.bottomRight.y - 114;
        drawToolbarSlot(
            drawer,
            slotX,
            slotY,
            slot == i,
            slotNames[i]
        );

        auto item = inventory[i];
        if (item) {
            drawItemStack(drawer, *item, itemFont, slotX + 3, slotY + 3, 59, 59);
        }
    }
}

void GuiHUD::drawToolbarSlot(_G::Drawer &drawer, float x, float y, bool selected, const std::string &name) {
    // TODO: Deal with GUI scaling at some point
    
    // background
    drawer.drawRect({{x, y}, {x + 65, y + 65}}, 0xFCF5F51A);

    // Selected bounds
    if (selected) {
        drawer.drawRectOutline({{x-3, y-3}, {x+68, y+68}}, 1, _G::StrokePosition::Inside, 0xFCF5F5FF);
    }

    // Line thing
    drawer.drawLine({x-3, y+83}, {x+23, y+83}, 0xFCF5F5FF);
    drawer.drawLine({x-2, y+84}, {x+24, y+84}, 0x00000019);
    drawer.drawLine({x+42, y+83}, {x+68, y+83}, 0xFCF5F5FF);
    drawer.drawLine({x+43, y+84}, {x+69, y+84}, 0x00000019);

    // Slot
    drawer.drawText(name, x+33.5, y+82, itemFont, Engine::Alignment::Middle, Engine::Alignment::Middle, 0x00000019);
    drawer.drawText(name, x+32.5, y+81, itemFont, Engine::Alignment::Middle, Engine::Alignment::Middle, 0xFCF5F5FF);
}

}