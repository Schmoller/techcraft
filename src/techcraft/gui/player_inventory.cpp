#include "techcraft/gui/player_inventory.hpp"
#include "techcraft/gui/utils.hpp"

#include "engine/gui/drawer.hpp"

#include <iostream>

namespace Techcraft {

GuiPlayerInventory::GuiPlayerInventory(PlayerInventory &playerInventory)
    : Window({{0, 0}, {430, 455}}), playerInventory(playerInventory)
{
    setAnchor(_G::AnchorFlag::Center);
    float x = 0;
    float y = 25;

    slots.reserve(playerInventory.getSize());

    for (size_t i = 0; i < playerInventory.getSize(); ++i) {
        auto slot = std::make_shared<GuiInventorySlot>(playerInventory, i, glm::vec2{x, y});
        slots.push_back(slot);

        if (i % 7 == 6) {
            y += 52 + 11;
            x = 0;
        } else {
            x += 52 + 11;
        }

        addChild(slot);
    }
}

void GuiPlayerInventory::onKeyPress(Engine::Key key, Engine::Modifier modifiers) {

}

void GuiPlayerInventory::onKeyRelease(Engine::Key key, Engine::Modifier modifiers) {

}

void GuiPlayerInventory::onKeyTyped(wchar_t ch) {

}

void GuiPlayerInventory::onMousePress(Engine::MouseButton button, double x, double y, Engine::Modifier modifiers) {
    auto slot = getSlotAt(x, y);
    if (!slot) {
        if (heldItemstack) {
            if (isDropZone(x, y)) {
                std::cout << "Drop " << heldItemstack->getType().id() << std::endl;
            } else if (isDeleteZone(x, y)) {
                std::cout << "Delete " << heldItemstack->getType().id() << std::endl;
                heldItemstack.reset();
            }
        } else {
            // TODO: Buttons and search box
        }
        return;
    }

    if (modifiers & Engine::ModifierFlag::Shift) {
        // TODO: Transfer stack
    } else {
        auto item = slot->getItem();

        if (heldItemstack) {
            if (button == Engine::MouseButton::MouseLeft) {
                if (item) {
                    if (item->isSimilar(*heldItemstack)) {
                        // Merge
                        if (item->merge(*heldItemstack)) {
                            slot->setItem(item);

                            if (heldItemstack->isEmpty()) {
                                heldItemstack = {};
                            }
                            std::cout << "Merge slot" << std::endl;
                        } else {
                            // Swap instead
                            slot->setItem(heldItemstack);
                            heldItemstack = item;
                            std::cout << "Swap held with slot failed merge" << std::endl;
                        }
                    } else {
                        // Swap item
                        slot->setItem(heldItemstack);
                        heldItemstack = item;
                        std::cout << "Swap held with slot" << std::endl;
                    }
                } else {
                    // Place stack in slot
                    slot->setItem(heldItemstack);
                    heldItemstack = {};
                    std::cout << "Place in slot" << std::endl;
                }
            } else if (button == Engine::MouseButton::MouseRight) {
                if (item) {
                    if (item->isSimilar(*heldItemstack)) {
                        // Try to merge in 1
                        if (item->getQuantity() < item->getMaximumStackSize()) {
                            item->combine(heldItemstack->splitOff(1));
                            slot->setItem(item);

                            if (heldItemstack->isEmpty()) {
                                heldItemstack = {};
                            }
                        }
                    }
                    // Otherwise no action
                    return;
                } else {
                    // Place half in the slot
                    slot->setItem(heldItemstack->splitOff(1));
                    if (heldItemstack->isEmpty()) {
                        heldItemstack = {};
                    }
                    std::cout << "Place half in slot" << std::endl;
                }
            }
        } else {
            if (!item) {
                return;
            }

            if (button == Engine::MouseButton::MouseLeft) {
                // Pickup whole stack
                heldItemstack = item;
                slot->setItem({});
                std::cout << "Pickup " << item->getType().id() << std::endl;
                if (slot->getItem()) {
                    std::cout << "WTH. Item still in slot?" << std::endl;
                }
            } else if (button == Engine::MouseButton::MouseRight) {
                // Pickup half stack
                auto pickedUp = item->splitOff(item->getQuantity() / 2);
                if (!pickedUp.isEmpty()) {
                    heldItemstack = pickedUp;
                    if (item->isEmpty()) {
                        slot->setItem({});
                    } else {
                        slot->setItem(item);
                    }
                    std::cout << "Pickup half " << item->getType().id() << std::endl;
                }
            }
        }
    }
}

void GuiPlayerInventory::onMouseRelease(Engine::MouseButton button, double x, double y, Engine::Modifier modifiers) {

}

void GuiPlayerInventory::onMouseMove(Engine::MouseButtons buttons, double x, double y, Engine::Modifier modifiers) {
    auto hoverSlot = getSlotAt(x, y);

    cursorX = x;
    cursorY = y;

    if (hoverSlot == lastHoverSlot) {
        return;
    }

    if (lastHoverSlot) {
        lastHoverSlot->setHover(false);
    }

    if (hoverSlot) {
        hoverSlot->setHover(true);
    }

    lastHoverSlot = hoverSlot;
}

void GuiPlayerInventory::onMouseScroll(double scrollX, double scrollY) {

}

void GuiPlayerInventory::onPaint(_G::Drawer &drawer) {
    static auto font = drawer.getFontManager().getFont("monospace", Engine::FontStyle::Regular);

    // Draw backdrop
    // FIXME: Size is completely temporary HACK
    drawer.drawRect({{0,0}, {1920, 1080}}, 0x00000066);

    // Title
    // TODO: Fontsize is wrong
    drawer.drawTextWithFormatting(L"Backpack", bounds.left() + 30, bounds.top(), 0xFCF5F5FF);

    drawer.drawLine({bounds.left()+1, bounds.top() + 11}, {bounds.left() + 28, bounds.top() + 11}, 0x00000019);
    drawer.drawLine({bounds.left(), bounds.top() + 10}, {bounds.left() + 27, bounds.top() + 10}, 0xFCF5F5FF);

    // TODO: This position is wrong
    drawer.drawLine({bounds.left()+93, bounds.top() + 11}, {bounds.left() + 120, bounds.top() + 11}, 0x00000019);
    drawer.drawLine({bounds.left()+92, bounds.top() + 10}, {bounds.left() + 119, bounds.top() + 10}, 0xFCF5F5FF);

    // TODO: Need a way to draw on top of children...
    // Also need a better way to handle this
    if (heldItemstack) {
        drawItemStack(drawer, *heldItemstack, font, bounds.left() + cursorX, bounds.top() + cursorY);
    }
}

void GuiPlayerInventory::onFrameUpdate() {
    if (heldItemstack) {
        markDirty();
    }
}

GuiInventorySlot *GuiPlayerInventory::getSlotAt(float x, float y) const {
    for (auto &slot : slots) {
        if (slot->getBounds().contains(x, y)) {
            return slot.get();
        }
    }

    return nullptr;
}

bool GuiPlayerInventory::isDropZone(float x, float y) const {
    return false;
}
bool GuiPlayerInventory::isDeleteZone(float x, float y) const {
    return false;
}

}