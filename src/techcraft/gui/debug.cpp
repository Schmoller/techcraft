#include "techcraft/gui/debug.hpp"
#include "techcraft/game.hpp"

#include "techcraft/gui/utils.hpp"

namespace Techcraft {

GuiDebug::GuiDebug(Game *game, const _G::Rect &bounds)
    : game(game), _G::BaseComponent(bounds, _G::AnchorAll)
{}

void GuiDebug::onFrameUpdate() {
    // Want to update this every frame
    markDirty();
}

void GuiDebug::onPaint(_G::Drawer &drawer) {
    // FPS marker
    const size_t BUFFER_SIZE = 500;
    wchar_t buffer[BUFFER_SIZE];
    if (game->getFPSAverage() < 50) {
        std::swprintf(buffer, BUFFER_SIZE, L"FPS: \0336%.2f \033r%.2f", game->getFPSAverage(), game->getFPS());
    } else if (game->getFPSAverage() < 30) {
        std::swprintf(buffer, BUFFER_SIZE, L"FPS: \0335%.2f \033r%.2f", game->getFPSAverage(), game->getFPS());
    } else {
        std::swprintf(buffer, BUFFER_SIZE, L"FPS: %.2f \033r%.2f", game->getFPSAverage(), game->getFPS());
    }

    drawer.drawTextWithFormatting(buffer, 0, 0);

    // Position info
    auto universe = game->getUniverse();
    if (universe) {
        auto player = universe->getLocalPlayer();

        if (player) {
            auto position = player->getPosition();
            ChunkPosition chunkPos = position;
            TilePosition tilePos = position;

            std::swprintf(
                buffer, BUFFER_SIZE,
                L"Position: %.1f,%.1f,%.1f (%d,%d,%d) Yaw: %.1f Pitch: %.1f\n"
                L"Chunk: %d,%d,%d",
                position.x, position.y, position.z,
                tilePos.x, tilePos.y, tilePos.z,
                player->getYaw(),
                player->getPitch(),
                chunkPos.x, chunkPos.y, chunkPos.z
            );

            drawer.drawTextWithFormatting(buffer, 0, 20);
        }
    }
}

}