#include "techcraft/gui/console.hpp"

#include "engine/gui/drawer.hpp"
#include "techcraft/game.hpp"
#include "techcraft/default.hpp"

#include <iostream>

namespace Techcraft {

GuiConsole::GuiConsole(const _G::Rect &bounds, Console &console, Engine::Font *font)
    : Window(bounds), console(console), font(font)
{
    setAnchor(_G::AnchorFlag::Top | _G::AnchorFlag::Left | _G::AnchorFlag::Right);

    consoleKeybind = Game::getInstance().getBindingManager().getBinding(Default::KeyBindings::OPEN_CONSOLE);
}

void GuiConsole::onKeyTyped(wchar_t ch) {
    console.type(ch);
}

void GuiConsole::onKeyPress(Engine::Key key, Engine::Modifier modifiers) {
    switch (key) {
        case Engine::Key::eEnter:
        case Engine::Key::eKpEnter:
            console.submit(*Game::getInstance().getUniverse()->getLocalPlayer());
            break;
        case Engine::Key::eBackspace:
            console.backspace(modifiers & Engine::ModifierFlag::Control);
            break;
        case Engine::Key::eDelete:
            console.del(modifiers & Engine::ModifierFlag::Control);
            break;
        case Engine::Key::eLeft:
            console.cursorLeft();
            break;
        case Engine::Key::eRight:
            console.cursorRight();
            break;
        case Engine::Key::eUp:
            console.historyPrevious();
            break;
        case Engine::Key::eDown:
            console.historyNext();
            break;
        default:
            break;
    }
}

void GuiConsole::onPaint(_G::Drawer &drawer) {
    const float padding = 5;

    drawer.drawRect(bounds, 0x08080880);

    // Command line
    drawer.drawText(
        console.getCommandLine(),
        bounds.topLeft.x + padding,
        bounds.bottomRight.y - padding,
        font,
        Engine::Alignment::Begining,
        Engine::Alignment::End,
        0xFFF5F5FF
    );

    // Draw cursor
    auto preCursorStr = console.getCommandLine().substr(0, console.getCursorPos());
    float cursorOffsetX, ignore;
    font->computeSize(preCursorStr, cursorOffsetX, ignore);

    drawer.drawRect(
        {
            {bounds.topLeft.x + padding + cursorOffsetX, bounds.bottomRight.y - padding - font->getLineHeight()},
            {bounds.topLeft.x + padding + cursorOffsetX + 1, bounds.bottomRight.y - padding}
        },
        0xFFF5F5FF
    );

    // Scrollback
    auto &scrollback = console.getScrollback();
    if (!scrollback.empty()) {
        int32_t line = scrollback.size() - 1 - scrollOffset;
        float y = bounds.bottomRight.y - padding - font->getLineHeight();
        while (line >= 0 && y > 0) {
            drawer.drawText(
                scrollback[line],
                bounds.topLeft.x + padding,
                y,
                font,
                Engine::Alignment::Begining,
                Engine::Alignment::End,
                0xFFF5F5FF
            );
            --line;
            y -= font->getLineHeight();
        }
    }
}

void GuiConsole::onFrameUpdate() {
    if (console.wasModifiedSince()) {
        markDirty();
    }
}


}