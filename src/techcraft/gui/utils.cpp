#include "techcraft/gui/utils.hpp"

#include "techcraft/game.hpp"

namespace Techcraft {

void drawItemStack(Engine::Gui::Drawer &drawer, const ItemStack &item, Engine::Font *font, float x, float y, float width, float height) {
    auto &renderHelper = Game::getInstance().getItemRenderHelper();
    
    renderHelper.drawItemGUI(drawer, item, x, y, width, height);

    if (item.getQuantity() > 1 || true) {
        std::string quantityStr;
        if (item.getQuantity() < 1000) {
            quantityStr = std::to_string(item.getQuantity());
        } else {
            quantityStr = std::to_string(item.getQuantity() / 1000) + "K";
        }

        drawer.drawText(quantityStr, x + width - 1, y + height - 1, font, Engine::Alignment::End, Engine::Alignment::End, 0x000000FF);
        drawer.drawText(quantityStr, x + width - 2, y + height - 2, font, Engine::Alignment::End, Engine::Alignment::End, 0xFCF5F5FF);
    }
}

}