#include "techcraft/subtiles/basic.hpp"

namespace Techcraft {

// BasicSubtileType
BasicSubtileType::BasicSubtileType(
    const std::string &id,
    const std::vector<const SubtileSlot *> &slots,
    const TileTexture *faceTextures[6],
    bool transparent,
    std::optional<BoundingBox> bounds,
    bool empty
): SubtileType(id), empty(empty), bounds(bounds)
{
    setSlots(slots);
    
    if (transparent) {
        setTransparent();
    }

    for (auto dir : AllDirections) {
        setTexture(faceTextures[static_cast<int>(dir)], dir);
    }
}


}