#include "techcraft/subtiles/placement.hpp"
#include "engine/subsystem/debug.hpp"

namespace Techcraft {

const float MarkerCornerInset = 0.25;
const float MarkerEdgeInset = 0.2;
const float HintOutset = 0.01;

const uint32_t HintColour = 0x20202020;

SubtilePlaceHit computeHitLocation(
    double hitX,
    double hitY,
    bool centerMarker,
    bool cornerMarker,
    bool edgeMarker
) {
    if (cornerMarker) {
        if (hitX < MarkerCornerInset && hitY > (1-MarkerCornerInset)) {
            return SubtilePlaceHit::CornerTopLeft;
        } else if (hitX < MarkerCornerInset && hitY < MarkerCornerInset) {
            return SubtilePlaceHit::CornerBottomLeft;
        } else if (hitX > (1-MarkerCornerInset) && hitY > (1-MarkerCornerInset)) {
            return SubtilePlaceHit::CornerTopRight;
        } else if (hitX > (1-MarkerCornerInset) && hitY < MarkerCornerInset) {
            return SubtilePlaceHit::CornerBottomRight;
        }
    }

    if (centerMarker) {
        // Center marker
        if ((hitY > (1 - MarkerCornerInset) / 2 && hitY < (1 + MarkerCornerInset) / 2) &&
            (hitX > (1 - MarkerCornerInset) / 2 && hitX < (1 + MarkerCornerInset) / 2)) {

            return SubtilePlaceHit::Center;
        }
    }

    if (edgeMarker) {
        if (!cornerMarker) {
            // Corners merge
            if (hitX < MarkerEdgeInset) {
                if (hitY < MarkerEdgeInset && hitY < hitX) {
                    return SubtilePlaceHit::EdgeBottom;
                } else if (hitY > (1-MarkerEdgeInset) && (1-hitY) < hitX) {
                    return SubtilePlaceHit::EdgeTop;
                } else {
                    return SubtilePlaceHit::EdgeRight;
                }
            } else if (hitX > (1-MarkerEdgeInset)) {
                if (hitY < MarkerEdgeInset && (1-hitY) > hitX) {
                    return SubtilePlaceHit::EdgeBottom;
                } else if (hitY > (1-MarkerEdgeInset) && hitY > hitX) {
                    return SubtilePlaceHit::EdgeTop;
                } else {
                    return SubtilePlaceHit::EdgeLeft;
                }
            } else if (hitY < MarkerEdgeInset) {
                return SubtilePlaceHit::EdgeBottom;
            } else if (hitY > (1-MarkerEdgeInset)) {
                return SubtilePlaceHit::EdgeTop;
            } else {
                return SubtilePlaceHit::CenterFace;
            }

        } else {
            if (hitX < MarkerEdgeInset) {
                if (hitY > MarkerCornerInset && hitY < (1-MarkerCornerInset)) {
                    return SubtilePlaceHit::EdgeRight;
                }
            } else if (hitX > (1-MarkerEdgeInset)) {
                if (hitY > MarkerCornerInset && hitY < (1-MarkerCornerInset)) {
                    return SubtilePlaceHit::EdgeLeft;
                }
            } else if (hitY < MarkerEdgeInset) {
                if (hitX > MarkerCornerInset && hitX < (1-MarkerCornerInset)) {
                    return SubtilePlaceHit::EdgeBottom;
                }
            } else if (hitY > (1-MarkerEdgeInset)) {
                if (hitX > MarkerCornerInset && hitX < (1-MarkerCornerInset)) {
                    return SubtilePlaceHit::EdgeTop;
                }
            } else {
                if ((hitY > MarkerCornerInset && hitY < (1-MarkerCornerInset)) &&
                    (hitX > MarkerCornerInset && hitX < (1-MarkerCornerInset))) {
                    return SubtilePlaceHit::CenterFace;
                }
            }
        }
    }

    return SubtilePlaceHit::None;
}

const SubtileSlot *computeSlotFromHit(
    SubtilePlaceHit hit,
    TileDirection hitFace,
    bool adjacent,
    bool allowCenter,
    bool allowEdge,
    bool allowCorner,
    bool allowFace
) {
    if (allowFace) {
        auto slot = computeFaceSlotFromHit(hit, hitFace, adjacent);
        if (slot) {
            return slot;
        }
    }

    if (allowCorner) {
        auto slot = computeCornerSlotFromHit(hit, hitFace, adjacent);
        if (slot) {
            return slot;
        }
    }

    if (allowEdge) {
        auto slot = computeEdgeSlotFromHit(hit, hitFace, adjacent);
        if (slot) {
            return slot;
        }
    }

    if (allowCenter && hit == SubtilePlaceHit::Center) {
        return &CenterSlot::Center;
    }

    return nullptr;
}

const SubtileSlot *computeEdgeSlotFromHit(SubtilePlaceHit hit, TileDirection hitFace, bool adjacent) {
    switch (hitFace) {
        case TileDirection::Up:
            switch (hit) {
                case SubtilePlaceHit::EdgeTop:
                    if (adjacent) {
                        return &EdgeSlot::NorthUp;
                    } else {
                        return &EdgeSlot::NorthDown;
                    }
                case SubtilePlaceHit::EdgeBottom:
                    if (adjacent) {
                        return &EdgeSlot::SouthUp;
                    } else {
                        return &EdgeSlot::SouthDown;
                    }
                case SubtilePlaceHit::EdgeLeft:
                    if (adjacent) {
                        return &EdgeSlot::EastUp;
                    } else {
                        return &EdgeSlot::EastDown;
                    }
                case SubtilePlaceHit::EdgeRight:
                    if (adjacent) {
                        return &EdgeSlot::WestUp;
                    } else {
                        return &EdgeSlot::WestDown;
                    }
                case SubtilePlaceHit::CornerTopLeft:
                    return &EdgeSlot::NorthWest;
                case SubtilePlaceHit::CornerTopRight:
                    return &EdgeSlot::NorthEast;
                case SubtilePlaceHit::CornerBottomLeft:
                    return &EdgeSlot::SouthWest;
                case SubtilePlaceHit::CornerBottomRight:
                    return &EdgeSlot::SouthEast;
                default:
                    break;
            }
            break;
        case TileDirection::Down:
            switch (hit) {
                case SubtilePlaceHit::EdgeTop:
                    if (adjacent) {
                        return &EdgeSlot::NorthDown;
                    } else {
                        return &EdgeSlot::NorthUp;
                    }
                case SubtilePlaceHit::EdgeBottom:
                    if (adjacent) {
                        return &EdgeSlot::SouthDown;
                    } else {
                        return &EdgeSlot::SouthUp;
                    }
                case SubtilePlaceHit::EdgeLeft:
                    if (adjacent) {
                        return &EdgeSlot::EastDown;
                    } else {
                        return &EdgeSlot::EastUp;
                    }
                case SubtilePlaceHit::EdgeRight:
                    if (adjacent) {
                        return &EdgeSlot::WestDown;
                    } else {
                        return &EdgeSlot::WestUp;
                    }
                case SubtilePlaceHit::CornerTopLeft:
                    return &EdgeSlot::NorthWest;
                case SubtilePlaceHit::CornerTopRight:
                    return &EdgeSlot::NorthEast;
                case SubtilePlaceHit::CornerBottomLeft:
                    return &EdgeSlot::SouthWest;
                case SubtilePlaceHit::CornerBottomRight:
                    return &EdgeSlot::SouthEast;
                default:
                    break;
            }
            break;
        case TileDirection::North:
            switch (hit) {
                case SubtilePlaceHit::EdgeTop:
                    if (adjacent) {
                        return &EdgeSlot::NorthUp;
                    } else {
                        return &EdgeSlot::SouthUp;
                    }
                case SubtilePlaceHit::EdgeBottom:
                    if (adjacent) {
                        return &EdgeSlot::NorthDown;
                    } else {
                        return &EdgeSlot::SouthDown;
                    }
                case SubtilePlaceHit::EdgeLeft:
                    if (adjacent) {
                        return &EdgeSlot::NorthEast;
                    } else {
                        return &EdgeSlot::SouthEast;
                    }
                case SubtilePlaceHit::EdgeRight:
                    if (adjacent) {
                        return &EdgeSlot::NorthWest;
                    } else {
                        return &EdgeSlot::SouthWest;
                    }
                case SubtilePlaceHit::CornerTopLeft:
                    return &EdgeSlot::WestUp;
                case SubtilePlaceHit::CornerTopRight:
                    return &EdgeSlot::EastUp;
                case SubtilePlaceHit::CornerBottomLeft:
                    return &EdgeSlot::WestDown;
                case SubtilePlaceHit::CornerBottomRight:
                    return &EdgeSlot::EastDown;
                default:
                    break;
            }
            break;
        case TileDirection::South:
            switch (hit) {
                case SubtilePlaceHit::EdgeTop:
                    if (adjacent) {
                        return &EdgeSlot::SouthUp;
                    } else {
                        return &EdgeSlot::NorthUp;
                    }
                case SubtilePlaceHit::EdgeBottom:
                    if (adjacent) {
                        return &EdgeSlot::SouthDown;
                    } else {
                        return &EdgeSlot::NorthDown;
                    }
                case SubtilePlaceHit::EdgeLeft:
                    if (adjacent) {
                        return &EdgeSlot::SouthEast;
                    } else {
                        return &EdgeSlot::NorthEast;
                    }
                case SubtilePlaceHit::EdgeRight:
                    if (adjacent) {
                        return &EdgeSlot::SouthWest;
                    } else {
                        return &EdgeSlot::NorthWest;
                    }
                case SubtilePlaceHit::CornerTopLeft:
                    return &EdgeSlot::WestUp;
                case SubtilePlaceHit::CornerTopRight:
                    return &EdgeSlot::EastUp;
                case SubtilePlaceHit::CornerBottomLeft:
                    return &EdgeSlot::WestDown;
                case SubtilePlaceHit::CornerBottomRight:
                    return &EdgeSlot::EastDown;
                default:
                    break;
            }
            break;
        case TileDirection::East:
            switch (hit) {
                case SubtilePlaceHit::EdgeTop:
                    if (adjacent) {
                        return &EdgeSlot::EastUp;
                    } else {
                        return &EdgeSlot::WestUp;
                    }
                case SubtilePlaceHit::EdgeBottom:
                    if (adjacent) {
                        return &EdgeSlot::EastDown;
                    } else {
                        return &EdgeSlot::WestDown;
                    }
                case SubtilePlaceHit::EdgeLeft:
                    if (adjacent) {
                        return &EdgeSlot::NorthEast;
                    } else {
                        return &EdgeSlot::NorthWest;
                    }
                case SubtilePlaceHit::EdgeRight:
                    if (adjacent) {
                        return &EdgeSlot::SouthEast;
                    } else {
                        return &EdgeSlot::SouthWest;
                    }
                case SubtilePlaceHit::CornerTopLeft:
                    return &EdgeSlot::SouthUp;
                case SubtilePlaceHit::CornerTopRight:
                    return &EdgeSlot::NorthUp;
                case SubtilePlaceHit::CornerBottomLeft:
                    return &EdgeSlot::SouthDown;
                case SubtilePlaceHit::CornerBottomRight:
                    return &EdgeSlot::NorthDown;
                default:
                    break;
            }
            break;
        case TileDirection::West:
            switch (hit) {
                case SubtilePlaceHit::EdgeTop:
                    if (adjacent) {
                        return &EdgeSlot::WestUp;
                    } else {
                        return &EdgeSlot::EastUp;
                    }
                case SubtilePlaceHit::EdgeBottom:
                    if (adjacent) {
                        return &EdgeSlot::WestDown;
                    } else {
                        return &EdgeSlot::EastDown;
                    }
                case SubtilePlaceHit::EdgeLeft:
                    if (adjacent) {
                        return &EdgeSlot::NorthWest;
                    } else {
                        return &EdgeSlot::NorthEast;
                    }
                case SubtilePlaceHit::EdgeRight:
                    if (adjacent) {
                        return &EdgeSlot::SouthWest;
                    } else {
                        return &EdgeSlot::SouthEast;
                    }
                case SubtilePlaceHit::CornerTopLeft:
                    return &EdgeSlot::SouthUp;
                case SubtilePlaceHit::CornerTopRight:
                    return &EdgeSlot::NorthUp;
                case SubtilePlaceHit::CornerBottomLeft:
                    return &EdgeSlot::SouthDown;
                case SubtilePlaceHit::CornerBottomRight:
                    return &EdgeSlot::NorthDown;
                default:
                    break;
            }
            break;
        default:
            break;
    }
    return nullptr;
}

const SubtileSlot *computeCornerSlotFromHit(SubtilePlaceHit hit, TileDirection hitFace, bool adjacent) {
    switch (hitFace) {
        case TileDirection::Up:
            switch (hit) {
                case SubtilePlaceHit::CornerTopLeft:
                    if (adjacent) {
                        return &CornerSlot::NorthWestUp;
                    } else {
                        return &CornerSlot::NorthWestDown;
                    }
                case SubtilePlaceHit::CornerTopRight:
                    if (adjacent) {
                        return &CornerSlot::NorthEastUp;
                    } else {
                        return &CornerSlot::NorthEastDown;
                    }
                case SubtilePlaceHit::CornerBottomLeft:
                    if (adjacent) {
                        return &CornerSlot::SouthWestUp;
                    } else {
                        return &CornerSlot::SouthWestDown;
                    }
                case SubtilePlaceHit::CornerBottomRight:
                    if (adjacent) {
                        return &CornerSlot::SouthEastUp;
                    } else {
                        return &CornerSlot::SouthEastDown;
                    }
                default:
                    break;
            }
            break;
        case TileDirection::Down:
            switch (hit) {
                case SubtilePlaceHit::CornerTopLeft:
                    if (adjacent) {
                        return &CornerSlot::NorthWestDown;
                    } else {
                        return &CornerSlot::NorthWestUp;
                    }
                case SubtilePlaceHit::CornerTopRight:
                    if (adjacent) {
                        return &CornerSlot::NorthEastDown;
                    } else {
                        return &CornerSlot::NorthEastUp;
                    }
                case SubtilePlaceHit::CornerBottomLeft:
                    if (adjacent) {
                        return &CornerSlot::SouthWestDown;
                    } else {
                        return &CornerSlot::SouthWestUp;
                    }
                case SubtilePlaceHit::CornerBottomRight:
                    if (adjacent) {
                        return &CornerSlot::SouthEastDown;
                    } else {
                        return &CornerSlot::SouthEastUp;
                    }
                default:
                    break;
            }
            break;
        case TileDirection::North:
            switch (hit) {
                case SubtilePlaceHit::CornerTopLeft:
                    if (adjacent) {
                        return &CornerSlot::NorthWestUp;
                    } else {
                        return &CornerSlot::SouthWestUp;
                    }
                case SubtilePlaceHit::CornerTopRight:
                    if (adjacent) {
                        return &CornerSlot::NorthEastUp;
                    } else {
                        return &CornerSlot::SouthEastUp;
                    }
                case SubtilePlaceHit::CornerBottomLeft:
                    if (adjacent) {
                        return &CornerSlot::NorthWestDown;
                    } else {
                        return &CornerSlot::SouthWestDown;
                    }
                case SubtilePlaceHit::CornerBottomRight:
                    if (adjacent) {
                        return &CornerSlot::NorthEastDown;
                    } else {
                        return &CornerSlot::SouthEastDown;
                    }
                default:
                    break;
            }
            break;
        case TileDirection::South:
            switch (hit) {
                case SubtilePlaceHit::CornerTopLeft:
                    if (adjacent) {
                        return &CornerSlot::SouthWestUp;
                    } else {
                        return &CornerSlot::NorthWestUp;
                    }
                case SubtilePlaceHit::CornerTopRight:
                    if (adjacent) {
                        return &CornerSlot::SouthEastUp;
                    } else {
                        return &CornerSlot::NorthEastUp;
                    }
                case SubtilePlaceHit::CornerBottomLeft:
                    if (adjacent) {
                        return &CornerSlot::SouthWestDown;
                    } else {
                        return &CornerSlot::NorthWestDown;
                    }
                case SubtilePlaceHit::CornerBottomRight:
                    if (adjacent) {
                        return &CornerSlot::SouthEastDown;
                    } else {
                        return &CornerSlot::NorthEastDown;
                    }
                default:
                    break;
            }
            break;
        case TileDirection::East:
            switch (hit) {
                case SubtilePlaceHit::CornerTopLeft:
                    if (adjacent) {
                        return &CornerSlot::SouthEastUp;
                    } else {
                        return &CornerSlot::SouthWestUp;
                    }
                case SubtilePlaceHit::CornerTopRight:
                    if (adjacent) {
                        return &CornerSlot::NorthEastUp;
                    } else {
                        return &CornerSlot::NorthWestUp;
                    }
                case SubtilePlaceHit::CornerBottomLeft:
                    if (adjacent) {
                        return &CornerSlot::SouthEastDown;
                    } else {
                        return &CornerSlot::SouthWestDown;
                    }
                case SubtilePlaceHit::CornerBottomRight:
                    if (adjacent) {
                        return &CornerSlot::NorthEastDown;
                    } else {
                        return &CornerSlot::NorthWestDown;
                    }
                default:
                    break;
            }
            break;
        case TileDirection::West:
            switch (hit) {
                case SubtilePlaceHit::CornerTopLeft:
                    if (adjacent) {
                        return &CornerSlot::SouthWestUp;
                    } else {
                        return &CornerSlot::SouthEastUp;
                    }
                case SubtilePlaceHit::CornerTopRight:
                    if (adjacent) {
                        return &CornerSlot::NorthWestUp;
                    } else {
                        return &CornerSlot::NorthEastUp;
                    }
                case SubtilePlaceHit::CornerBottomLeft:
                    if (adjacent) {
                        return &CornerSlot::SouthWestDown;
                    } else {
                        return &CornerSlot::SouthEastDown;
                    }
                case SubtilePlaceHit::CornerBottomRight:
                    if (adjacent) {
                        return &CornerSlot::NorthWestDown;
                    } else {
                        return &CornerSlot::NorthEastDown;
                    }
                default:
                    break;
            }
            break;
        default:
            break;
    }

    return nullptr;
}

const SubtileSlot *computeFaceSlotFromHit(SubtilePlaceHit hit, TileDirection hitFace, bool adjacent) {
    switch (hitFace) {
        case TileDirection::Up:
            switch(hit) {
                case SubtilePlaceHit::CenterFace:
                    if (adjacent) {
                        return &FaceSlot::Up;
                    } else {
                        return &FaceSlot::Down;
                    }
                case SubtilePlaceHit::EdgeTop:
                    return &FaceSlot::North;
                case SubtilePlaceHit::EdgeBottom:
                    return &FaceSlot::South;
                case SubtilePlaceHit::EdgeLeft:
                    return &FaceSlot::East;
                case SubtilePlaceHit::EdgeRight:
                    return &FaceSlot::West;
            }
            break;
        case TileDirection::Down:
            switch(hit) {
                case SubtilePlaceHit::CenterFace:
                    if (adjacent) {
                        return &FaceSlot::Down;
                    } else {
                        return &FaceSlot::Up;
                    }
                case SubtilePlaceHit::EdgeTop:
                    return &FaceSlot::North;
                case SubtilePlaceHit::EdgeBottom:
                    return &FaceSlot::South;
                case SubtilePlaceHit::EdgeLeft:
                    return &FaceSlot::East;
                case SubtilePlaceHit::EdgeRight:
                    return &FaceSlot::West;
            }
            break;
        case TileDirection::North:
            switch(hit) {
                case SubtilePlaceHit::CenterFace:
                    if (adjacent) {
                        return &FaceSlot::North;
                    } else {
                        return &FaceSlot::South;
                    }
                case SubtilePlaceHit::EdgeTop:
                    return &FaceSlot::Up;
                case SubtilePlaceHit::EdgeBottom:
                    return &FaceSlot::Down;
                case SubtilePlaceHit::EdgeLeft:
                    return &FaceSlot::East;
                case SubtilePlaceHit::EdgeRight:
                    return &FaceSlot::West;
            }
            break;
        case TileDirection::South:
            switch(hit) {
                case SubtilePlaceHit::CenterFace:
                    if (adjacent) {
                        return &FaceSlot::South;
                    } else {
                        return &FaceSlot::North;
                    }
                case SubtilePlaceHit::EdgeTop:
                    return &FaceSlot::Up;
                case SubtilePlaceHit::EdgeBottom:
                    return &FaceSlot::Down;
                case SubtilePlaceHit::EdgeLeft:
                    return &FaceSlot::East;
                case SubtilePlaceHit::EdgeRight:
                    return &FaceSlot::West;
            }
            break;
        case TileDirection::East:
            switch(hit) {
                case SubtilePlaceHit::CenterFace:
                    if (adjacent) {
                        return &FaceSlot::East;
                    } else {
                        return &FaceSlot::West;
                    }
                case SubtilePlaceHit::EdgeTop:
                    return &FaceSlot::Up;
                case SubtilePlaceHit::EdgeBottom:
                    return &FaceSlot::Down;
                case SubtilePlaceHit::EdgeLeft:
                    return &FaceSlot::North;
                case SubtilePlaceHit::EdgeRight:
                    return &FaceSlot::South;
            }
            break;
        case TileDirection::West:
            switch(hit) {
                case SubtilePlaceHit::CenterFace:
                    if (adjacent) {
                        return &FaceSlot::West;
                    } else {
                        return &FaceSlot::East;
                    }
                case SubtilePlaceHit::EdgeTop:
                    return &FaceSlot::Up;
                case SubtilePlaceHit::EdgeBottom:
                    return &FaceSlot::Down;
                case SubtilePlaceHit::EdgeLeft:
                    return &FaceSlot::North;
                case SubtilePlaceHit::EdgeRight:
                    return &FaceSlot::South;
            }
            break;
        default:
            break;
    }

    return nullptr;
}


void drawSquare(float xMin, float yMin, float xMax, float yMax, TileDirection face, const BoundingBox &bounds, uint32_t colour) {
    static auto debug = Engine::Subsystem::DebugSubsystem::instance();

    switch (face) {
        case TileDirection::Up:
            debug->debugDrawLine(
                {bounds.xMin + xMin, bounds.yMin + yMin, bounds.zMax},
                {bounds.xMin + xMax, bounds.yMin + yMin, bounds.zMax},
                colour
            );
            debug->debugDrawLine(
                {bounds.xMin + xMin, bounds.yMin + yMin, bounds.zMax},
                {bounds.xMin + xMin, bounds.yMin + yMax, bounds.zMax},
                colour
            );
            debug->debugDrawLine(
                {bounds.xMin + xMax, bounds.yMin + yMin, bounds.zMax},
                {bounds.xMin + xMax, bounds.yMin + yMax, bounds.zMax},
                colour
            );
            debug->debugDrawLine(
                {bounds.xMin + xMin, bounds.yMin + yMax, bounds.zMax},
                {bounds.xMin + xMax, bounds.yMin + yMax, bounds.zMax},
                colour
            );
            break;
        case TileDirection::Down:
            debug->debugDrawLine(
                {bounds.xMin + xMin, bounds.yMin + yMin, bounds.zMin},
                {bounds.xMin + xMax, bounds.yMin + yMin, bounds.zMin},
                colour
            );
            debug->debugDrawLine(
                {bounds.xMin + xMin, bounds.yMin + yMin, bounds.zMin},
                {bounds.xMin + xMin, bounds.yMin + yMax, bounds.zMin},
                colour
            );
            debug->debugDrawLine(
                {bounds.xMin + xMax, bounds.yMin + yMin, bounds.zMin},
                {bounds.xMin + xMax, bounds.yMin + yMax, bounds.zMin},
                colour
            );
            debug->debugDrawLine(
                {bounds.xMin + xMin, bounds.yMin + yMax, bounds.zMin},
                {bounds.xMin + xMax, bounds.yMin + yMax, bounds.zMin},
                colour
            );
            break;
        case TileDirection::North:
            debug->debugDrawLine(
                {bounds.xMin + xMin, bounds.yMax, bounds.zMin + yMin},
                {bounds.xMin + xMax, bounds.yMax, bounds.zMin + yMin},
                colour
            );
            debug->debugDrawLine(
                {bounds.xMin + xMin, bounds.yMax, bounds.zMin + yMin},
                {bounds.xMin + xMin, bounds.yMax, bounds.zMin + yMax},
                colour
            );
            debug->debugDrawLine(
                {bounds.xMin + xMax, bounds.yMax, bounds.zMin + yMin},
                {bounds.xMin + xMax, bounds.yMax, bounds.zMin + yMax},
                colour
            );
            debug->debugDrawLine(
                {bounds.xMin + xMin, bounds.yMax, bounds.zMin + yMax},
                {bounds.xMin + xMax, bounds.yMax, bounds.zMin + yMax},
                colour
            );
            break;
        case TileDirection::South:
            debug->debugDrawLine(
                {bounds.xMin + xMin, bounds.yMin, bounds.zMin + yMin},
                {bounds.xMin + xMax, bounds.yMin, bounds.zMin + yMin},
                colour
            );
            debug->debugDrawLine(
                {bounds.xMin + xMin, bounds.yMin, bounds.zMin + yMin},
                {bounds.xMin + xMin, bounds.yMin, bounds.zMin + yMax},
                colour
            );
            debug->debugDrawLine(
                {bounds.xMin + xMax, bounds.yMin, bounds.zMin + yMin},
                {bounds.xMin + xMax, bounds.yMin, bounds.zMin + yMax},
                colour
            );
            debug->debugDrawLine(
                {bounds.xMin + xMin, bounds.yMin, bounds.zMin + yMax},
                {bounds.xMin + xMax, bounds.yMin, bounds.zMin + yMax},
                colour
            );
            break;
        case TileDirection::East:
            debug->debugDrawLine(
                {bounds.xMax, bounds.yMin + xMin, bounds.zMin + yMin},
                {bounds.xMax, bounds.yMin + xMax, bounds.zMin + yMin},
                colour
            );
            debug->debugDrawLine(
                {bounds.xMax, bounds.yMin + xMin, bounds.zMin + yMin},
                {bounds.xMax, bounds.yMin + xMin, bounds.zMin + yMax},
                colour
            );
            debug->debugDrawLine(
                {bounds.xMax, bounds.yMin + xMax, bounds.zMin + yMin},
                {bounds.xMax, bounds.yMin + xMax, bounds.zMin + yMax},
                colour
            );
            debug->debugDrawLine(
                {bounds.xMax, bounds.yMin + xMin, bounds.zMin + yMax},
                {bounds.xMax, bounds.yMin + xMax, bounds.zMin + yMax},
                colour
            );
            break;
        case TileDirection::West:
            debug->debugDrawLine(
                {bounds.xMin, bounds.yMin + xMin, bounds.zMin + yMin},
                {bounds.xMin, bounds.yMin + xMax, bounds.zMin + yMin},
                colour
            );
            debug->debugDrawLine(
                {bounds.xMin, bounds.yMin + xMin, bounds.zMin + yMin},
                {bounds.xMin, bounds.yMin + xMin, bounds.zMin + yMax},
                colour
            );
            debug->debugDrawLine(
                {bounds.xMin, bounds.yMin + xMax, bounds.zMin + yMin},
                {bounds.xMin, bounds.yMin + xMax, bounds.zMin + yMax},
                colour
            );
            debug->debugDrawLine(
                {bounds.xMin, bounds.yMin + xMin, bounds.zMin + yMax},
                {bounds.xMin, bounds.yMin + xMax, bounds.zMin + yMax},
                colour
            );
            break;
    }
}

void drawLine(float xMin, float yMin, float xMax, float yMax, TileDirection face, const BoundingBox &bounds, uint32_t colour) {
    static auto debug = Engine::Subsystem::DebugSubsystem::instance();

    switch (face) {
        case TileDirection::Up:
            debug->debugDrawLine(
                {bounds.xMin + xMin, bounds.yMin + yMin, bounds.zMax},
                {bounds.xMin + xMax, bounds.yMin + yMax, bounds.zMax},
                colour
            );
            break;
        case TileDirection::Down:
            debug->debugDrawLine(
                {bounds.xMin + xMin, bounds.yMin + yMin, bounds.zMin},
                {bounds.xMin + xMax, bounds.yMin + yMax, bounds.zMin},
                colour
            );
            break;
        case TileDirection::North:
            debug->debugDrawLine(
                {bounds.xMin + xMin, bounds.yMax, bounds.zMin + yMin},
                {bounds.xMin + xMax, bounds.yMax, bounds.zMin + yMax},
                colour
            );
            break;
        case TileDirection::South:
            debug->debugDrawLine(
                {bounds.xMin + xMin, bounds.yMin, bounds.zMin + yMin},
                {bounds.xMin + xMax, bounds.yMin, bounds.zMin + yMax},
                colour
            );
            break;
        case TileDirection::East:
            debug->debugDrawLine(
                {bounds.xMax, bounds.yMin + xMin, bounds.zMin + yMin},
                {bounds.xMax, bounds.yMin + xMax, bounds.zMin + yMax},
                colour
            );
            break;
        case TileDirection::West:
            debug->debugDrawLine(
                {bounds.xMin, bounds.yMin + xMin, bounds.zMin + yMin},
                {bounds.xMin, bounds.yMin + xMax, bounds.zMin + yMax},
                colour
            );
            break;
    }
}


void drawPlacementHint(
    TileDirection face,
    const BoundingBox &onBounds,
    bool allowCenter,
    bool allowEdge,
    bool allowCorner
) {
    Position offset;
    switch (face) {
        case TileDirection::Up:
        case TileDirection::Down:
            offset = {0, 0, 1};
            break;
        case TileDirection::North:
        case TileDirection::South:
            offset = {0, 1, 0};
            break;
        case TileDirection::East:
        case TileDirection::West:
            offset = {1, 0, 0};
            break;
        default:
            offset = {};
    }

    BoundingBox bounds = onBounds.expand(
        HintOutset * offset.x,
        HintOutset * offset.y,
        HintOutset * offset.z
    );

    // Corner markers
    if (allowCorner) {
        drawSquare(0, 0, MarkerCornerInset, MarkerCornerInset, face, bounds, HintColour);
        drawSquare(1-MarkerCornerInset, 0, 1, MarkerCornerInset, face, bounds, HintColour);
        drawSquare(0, 1-MarkerCornerInset, MarkerCornerInset, 1, face, bounds, HintColour);
        drawSquare(1-MarkerCornerInset, 1-MarkerCornerInset, 1, 1, face, bounds, HintColour);
    }

    if (allowEdge) {
        if (!allowCorner) {
            // Corners merge
            drawSquare(MarkerEdgeInset, MarkerEdgeInset, 1-MarkerEdgeInset, 1-MarkerEdgeInset, face, bounds, HintColour);
            drawLine(0, 0, MarkerEdgeInset, MarkerEdgeInset, face, bounds, HintColour);
            drawLine(1, 0, 1-MarkerEdgeInset, MarkerEdgeInset, face, bounds, HintColour);
            drawLine(0, 1, MarkerEdgeInset, 1-MarkerEdgeInset, face, bounds, HintColour);
            drawLine(1, 1, 1-MarkerEdgeInset, 1-MarkerEdgeInset, face, bounds, HintColour);
        } else {
            drawLine(MarkerCornerInset, MarkerEdgeInset, 1-MarkerCornerInset, MarkerEdgeInset, face, bounds, HintColour);
            drawLine(MarkerEdgeInset, MarkerCornerInset, MarkerEdgeInset, 1-MarkerCornerInset, face, bounds, HintColour);
            drawLine(MarkerCornerInset, 1-MarkerEdgeInset, 1-MarkerCornerInset, 1-MarkerEdgeInset, face, bounds, HintColour);
            drawLine(1-MarkerEdgeInset, MarkerCornerInset, 1-MarkerEdgeInset, 1-MarkerCornerInset, face, bounds, HintColour);
        }
    }

    if (allowCenter) {
        // Center marker
        drawSquare((1 - MarkerCornerInset) / 2, (1 - MarkerCornerInset) / 2, (1 + MarkerCornerInset) / 2, (1 + MarkerCornerInset) / 2, face, onBounds, HintColour);
    }
}

}