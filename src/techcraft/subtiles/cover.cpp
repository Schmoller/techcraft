#include "techcraft/subtiles/cover.hpp"
#include "techcraft/subtiles/slot.hpp"

namespace Techcraft {

// FIXME: The name needs to be in the form of "cover.<type>.<size>". We should use fmtlib to do it
CoverSubtileType::CoverSubtileType(const std::string_view &name, const TileType &tileType, CoverSize size)
    : tileType(tileType), size(size), SubtileType(name)
{
    setSlots(AllFaceSlots);
    for (auto face : AllDirections) {
        setTexture(tileType.textureOf(face), face);
    }
}

void CoverSubtileType::initialize(ResourceManager &manager) {
    // No-op
}

/**
 * Retreives the bounding box (non-located) for this subtile
 */
std::optional<BoundingBox> CoverSubtileType::getBounds(const SubtileSlot &slot) const {
    float thickness;
    switch (size) {
        case CoverSize::VeryThin:
            thickness = 0.08f;
            break;
        case CoverSize::Thin:
            thickness = 0.25f;
            break;
        case CoverSize::Slab:
            thickness = 0.5f;
            break;
        case CoverSize::Thick:
            thickness = 0.75f;
            break;
        case CoverSize::VeryThick:
            thickness = 0.92f;
            break;
        [[unlikely]] default:
            // Missed size
            assert(false);
    }
    
    if (slot == FaceSlot::Up) {
        return BoundingBox{
            0, 0, 1-thickness,
            1, 1, 1
        };
    } else if (slot == FaceSlot::Down) {
        return BoundingBox{
            0, 0, 0,
            1, 1, thickness
        };
    } else if (slot == FaceSlot::North) {
        return BoundingBox{
            0, 1-thickness, 0,
            1, 1, 1
        };
    } else if (slot == FaceSlot::South) {
        return BoundingBox{
            0, 0, 0,
            1, thickness, 1
        };
    } else if (slot == FaceSlot::East) {
        return BoundingBox{
            1-thickness, 0, 0,
            1, 1, 1
        };
    } else if (slot == FaceSlot::West) {
        return BoundingBox{
            0, 0, 0,
            thickness, 1, 1
        };
    } else [[unlikely]] {
        // Should never be reached
        assert(false);
        return BoundingBox{
            0,0,0,
            0,0,0
        };
    }
}

}