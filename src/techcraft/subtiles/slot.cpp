#include "techcraft/subtiles/slot.hpp"

namespace Techcraft {

CornerSlot CornerSlot::NorthEastUp("NorthEastUp");
CornerSlot CornerSlot::NorthEastDown("NorthEastDown");
CornerSlot CornerSlot::NorthWestUp("NorthWestUp");
CornerSlot CornerSlot::NorthWestDown("NorthWestDown");
CornerSlot CornerSlot::SouthEastUp("SouthEastUp");
CornerSlot CornerSlot::SouthEastDown("SouthEastDown");
CornerSlot CornerSlot::SouthWestUp("SouthWestUp");
CornerSlot CornerSlot::SouthWestDown("SouthWestDown");
EdgeSlot EdgeSlot::NorthDown("NorthDown");
EdgeSlot EdgeSlot::SouthDown("SouthDown");
EdgeSlot EdgeSlot::EastDown("EastDown");
EdgeSlot EdgeSlot::WestDown("WestDown");
EdgeSlot EdgeSlot::NorthUp("NorthUp");
EdgeSlot EdgeSlot::SouthUp("SouthUp");
EdgeSlot EdgeSlot::EastUp("EastUp");
EdgeSlot EdgeSlot::WestUp("WestUp");
EdgeSlot EdgeSlot::NorthEast("NorthEast");
EdgeSlot EdgeSlot::NorthWest("NorthWest");
EdgeSlot EdgeSlot::SouthEast("SouthEast");
EdgeSlot EdgeSlot::SouthWest("SouthWest");
FaceSlot FaceSlot::North("North");
FaceSlot FaceSlot::South("South");
FaceSlot FaceSlot::East("East");
FaceSlot FaceSlot::West("West");
FaceSlot FaceSlot::Up("Up");
FaceSlot FaceSlot::Down("Down");
CenterSlot CenterSlot::Center("Center");

bool isCornerSlot(const SubtileSlot *slot) {
    if (slot == &CornerSlot::NorthEastUp) {
        return true;
    }
    if (slot == &CornerSlot::NorthEastDown) {
        return true;
    }
    if (slot == &CornerSlot::NorthWestUp) {
        return true;
    }
    if (slot == &CornerSlot::NorthWestDown) {
        return true;
    }
    if (slot == &CornerSlot::SouthEastUp) {
        return true;
    }
    if (slot == &CornerSlot::SouthEastDown) {
        return true;
    }
    if (slot == &CornerSlot::SouthWestUp) {
        return true;
    }
    if (slot == &CornerSlot::SouthWestDown) {
        return true;
    }
    return false;
}
bool isEdgeSlot(const SubtileSlot *slot) {
    if (slot == &EdgeSlot::NorthDown) {
        return true;
    }
    if (slot == &EdgeSlot::SouthDown) {
        return true;
    }
    if (slot == &EdgeSlot::EastDown) {
        return true;
    }
    if (slot == &EdgeSlot::WestDown) {
        return true;
    }
    if (slot == &EdgeSlot::NorthUp) {
        return true;
    }
    if (slot == &EdgeSlot::SouthUp) {
        return true;
    }
    if (slot == &EdgeSlot::EastUp) {
        return true;
    }
    if (slot == &EdgeSlot::WestUp) {
        return true;
    }
    if (slot == &EdgeSlot::NorthEast) {
        return true;
    }
    if (slot == &EdgeSlot::NorthWest) {
        return true;
    }
    if (slot == &EdgeSlot::SouthEast) {
        return true;
    }
    if (slot == &EdgeSlot::SouthWest) {
        return true;
    }
    return false;
}
bool isFaceSlot(const SubtileSlot *slot) {
    if (slot == &FaceSlot::North) {
        return true;
    }
    if (slot == &FaceSlot::South) {
        return true;
    }
    if (slot == &FaceSlot::East) {
        return true;
    }
    if (slot == &FaceSlot::West) {
        return true;
    }
    if (slot == &FaceSlot::Up) {
        return true;
    }
    if (slot == &FaceSlot::Down) {
        return true;
    }
    return false;
}
bool isCenterSlot(const SubtileSlot *slot) {
    return slot == &CenterSlot::Center;
}

const SubtileSlot &faceSlotFrom(TileDirection dir) {
    switch (dir) {
        case TileDirection::Up:
            return FaceSlot::Up;
            break;
        case TileDirection::Down:
            return FaceSlot::Down;
            break;
        case TileDirection::North:
            return FaceSlot::North;
            break;
        case TileDirection::South:
            return FaceSlot::South;
            break;
        case TileDirection::East:
            return FaceSlot::East;
            break;
        case TileDirection::West:
            return FaceSlot::West;
            break;
    }
}

}