#include "techcraft/subtiles/registry.hpp"
#include "techcraft/subtiles/basic.hpp"

#include "techcraft/tiles/registry.hpp"
#include "techcraft/subtiles/cover.hpp"
#include "techcraft/subtiles/strip.hpp"
#include "techcraft/subtiles/corner.hpp"

namespace Techcraft {

std::unique_ptr<SubtileRegistry> SubtileTypes::subtileRegistry;

void SubtileTypes::initialize(ResourceManager &manager) {
    SubtileRegistryBuilder builder(manager);

    builder.add(std::make_shared<CoverSubtileType>("cover.stone.1", *TileTypes::Stone, CoverSize::Thin));
    builder.add(std::make_shared<StripSubtileType>("strip.dirt.1", *TileTypes::Dirt, StripSize::Thin));
    builder.add(std::make_shared<CornerSubtileType>("corner.stone.1", *TileTypes::Stone, CornerSize::Thin));
    builder.add(std::make_shared<CoverSubtileType>("cover.debug.connected.1", *TileTypes::ConnectedTest, CoverSize::Thin));

    subtileRegistry = builder.build();
}

// SubtileTypeDefinition
SubtileTypeDefinition::SubtileTypeDefinition(const std::string &name)
    : name(name)
{}

void SubtileTypeDefinition::setTextureAll(const Resource &resource) {
    std::fill_n(faceTextures, 6, resource);
}

void SubtileTypeDefinition::setTexture(const Resource &resource, TileDirection direction) {
    faceTextures[static_cast<int>(direction)] = resource;
}


// SubtileRegistry
SubtileRegistry::SubtileRegistry(
    std::unordered_map<std::string, std::shared_ptr<SubtileType>> byName,
    std::vector<std::shared_ptr<SubtileType>> slots
) : Registry(byName, slots)
{}


// SubtileRegistryBuilder
SubtileRegistryBuilder::SubtileRegistryBuilder(ResourceManager &manager)
    : manager(manager)
{}

const SubtileType *SubtileRegistryBuilder::add(const SubtileTypeDefinition &definition) {
    const TileTexture *textures[6];

    for (size_t i = 0; i < 6; ++i) {
        textures[i] = manager.asTileTexture(definition.faceTextures[i]);
    }

    auto basicType = std::make_shared<BasicSubtileType>(
        definition.name,
        definition.slots,
        textures,
        definition.transparent,
        definition.bounds,
        definition.empty
    );

    return add(basicType);
}

std::unique_ptr<SubtileRegistry> SubtileRegistryBuilder::build() {
    return std::unique_ptr<SubtileRegistry>(new SubtileRegistry(
        byName,
        slots
    ));
}

void SubtileRegistryBuilder::initializeItem(SubtileType *item, size_t slotId) {
    item->initialize(manager);

    // Ensure no unset textures
    for (int i = 0; i < 6; ++i) {
        if (!item->faceTextures[i]) {
            item->faceTextures[i] = manager.asTileTexture({});
        }
    }
}


}