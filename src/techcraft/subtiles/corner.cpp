#include "techcraft/subtiles/corner.hpp"
#include "techcraft/subtiles/slot.hpp"

namespace Techcraft {

// FIXME: The name needs to be in the form of "corner.<type>.<size>". We should use fmtlib to do it
CornerSubtileType::CornerSubtileType(const std::string_view &name, const TileType &tileType, CornerSize size)
    : tileType(tileType), size(size), SubtileType(name)
{
    setSlots(AllCornerSlots);
    for (auto face : AllDirections) {
        setTexture(tileType.textureOf(face), face);
    }
}

void CornerSubtileType::initialize(ResourceManager &manager) {
    // No-op
}

/**
 * Retreives the bounding box (non-located) for this subtile
 */
std::optional<BoundingBox> CornerSubtileType::getBounds(const SubtileSlot &slot) const {
    float thickness;
    switch (size) {
        case CornerSize::VeryThin:
            thickness = 0.08f;
            break;
        case CornerSize::Thin:
            thickness = 0.25f;
            break;
        case CornerSize::Slab:
            thickness = 0.5f;
            break;
        case CornerSize::Thick:
            thickness = 0.75f;
            break;
        case CornerSize::VeryThick:
            thickness = 0.92f;
            break;
        [[unlikely]] default:
            // Missed size
            assert(false);
    }

    if (slot == CornerSlot::NorthEastUp) {
        return BoundingBox{
            1-thickness, 1-thickness, 1-thickness,
            1, 1, 1
        };
    } else if (slot == CornerSlot::NorthEastDown) {
        return BoundingBox{
            1-thickness, 1-thickness, 0,
            1, 1, thickness
        };
    } else if (slot == CornerSlot::NorthWestUp) {
        return BoundingBox{
            0, 1-thickness, 1-thickness,
            thickness, 1, 1
        };
    } else if (slot == CornerSlot::NorthWestDown) {
        return BoundingBox{
            0, 1-thickness, 0,
            thickness, 1, thickness
        };
    } else if (slot == CornerSlot::SouthEastUp) {
        return BoundingBox{
            1-thickness, 0, 1-thickness,
            1, thickness, 1
        };
    } else if (slot == CornerSlot::SouthEastDown) {
        return BoundingBox{
            1-thickness, 0, 0,
            1, thickness, thickness
        };
    } else if (slot == CornerSlot::SouthWestUp) {
        return BoundingBox{
            0, 0, 1-thickness,
            thickness, thickness, 1
        };
    } else if (slot == CornerSlot::SouthWestDown) {
        return BoundingBox{
            0, 0, 0,
            thickness, thickness, thickness
        };
    } else [[unlikely]] {
        // Should never be reached
        assert(false);
        return BoundingBox{
            0,0,0,
            0,0,0
        };
    }
}

}