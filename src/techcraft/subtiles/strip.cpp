#include "techcraft/subtiles/strip.hpp"
#include "techcraft/subtiles/slot.hpp"

namespace Techcraft {

// FIXME: The name needs to be in the form of "strip.<type>.<size>". We should use fmtlib to do it
StripSubtileType::StripSubtileType(const std::string_view &name, const TileType &tileType, StripSize size)
    : tileType(tileType), size(size), SubtileType(name)
{
    setSlots(AllEdgeSlots);
    for (auto face : AllDirections) {
        setTexture(tileType.textureOf(face), face);
    }
}

void StripSubtileType::initialize(ResourceManager &manager) {
    // No-op
}

/**
 * Retreives the bounding box (non-located) for this subtile
 */
std::optional<BoundingBox> StripSubtileType::getBounds(const SubtileSlot &slot) const {
    float thickness;
    switch (size) {
        case StripSize::VeryThin:
            thickness = 0.08f;
            break;
        case StripSize::Thin:
            thickness = 0.25f;
            break;
        case StripSize::Slab:
            thickness = 0.5f;
            break;
        case StripSize::Thick:
            thickness = 0.75f;
            break;
        case StripSize::VeryThick:
            thickness = 0.92f;
            break;
        [[unlikely]] default:
            // Missed size
            assert(false);
    }

    if (slot == EdgeSlot::EastDown) {
        return BoundingBox{
            1-thickness, 0, 0,
            1, 1, thickness
        };
    } else if (slot == EdgeSlot::EastUp) {
        return BoundingBox{
            1-thickness, 0, 1-thickness,
            1, 1, 1
        };
    } else if (slot == EdgeSlot::NorthDown) {
        return BoundingBox{
            0, 1-thickness, 0,
            1, 1, thickness
        };
    } else if (slot == EdgeSlot::NorthUp) {
        return BoundingBox{
            0, 1-thickness, 1-thickness,
            1, 1, 1
        };
    } else if (slot == EdgeSlot::SouthDown) {
        return BoundingBox{
            0, 0, 0,
            1, thickness, thickness
        };
    } else if (slot == EdgeSlot::SouthUp) {
        return BoundingBox{
            0, 0, 1-thickness,
            1, thickness, 1
        };
    } else if (slot == EdgeSlot::WestUp) {
        return BoundingBox{
            0, 0, 1-thickness,
            thickness, 1, 1
        };
    } else if (slot == EdgeSlot::WestDown) {
        return BoundingBox{
            0, 0, 0,
            thickness, 1, thickness
        };
    } else if (slot == EdgeSlot::NorthEast) {
        return BoundingBox{
            1-thickness, 1-thickness, 0,
            1, 1, 1
        };
    } else if (slot == EdgeSlot::NorthWest) {
        return BoundingBox{
            0, 1-thickness, 0,
            thickness, 1, 1
        };
    } else if (slot == EdgeSlot::SouthEast) {
        return BoundingBox{
            1-thickness, 0, 0,
            1, thickness, 1
        };
    } else if (slot == EdgeSlot::SouthWest) {
        return BoundingBox{
            0, 0, 0,
            thickness, thickness, 1
        };
    } else [[unlikely]] {
        // Should never be reached
        assert(false);
        return BoundingBox{
            0,0,0,
            0,0,0
        };
    }
}

}