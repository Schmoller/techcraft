#include "techcraft/subtiles/base.hpp"

#include "techcraft/rendering/subtile/base.hpp"
#include "techcraft/rendering/subtile/basic.hpp"

namespace Techcraft {


// SubtileType
SubtileType::SubtileType(const std::string_view &id)
    : name(id)
{}

bool SubtileType::operator==(const SubtileType &other) const {
    return (
        other.name == name
    );
}

bool SubtileType::operator!=(const SubtileType &other) const {
    return (
        other.name != name
    );
}

void SubtileType::setTextureAll(const TileTexture *texture) {
    std::fill_n(faceTextures, 6, texture);
}

void SubtileType::setTexture(const TileTexture *texture, TileDirection direction) {
    faceTextures[static_cast<int>(direction)] = texture;
}

void SubtileType::setSlots(const std::vector<const SubtileSlot *> &slots) {
    this->slots = slots;
}

Rendering::SubtileRenderer &SubtileType::getRenderer() const {
    return Rendering::BasicSubtileRenderer::instance();
}

}