#define SETUP_IMPLEMENTATION
#include "engine/setup.hpp"
#undef SETUP_IMPLEMENTATION

#include "engine/engine.hpp"
#include "engine/subsystem/objects.hpp"
#include "engine/subsystem/light.hpp"

#include <iostream>
#include <exception>
#include <vector>

using namespace Engine;


Mesh *setupMesh(RenderEngine &engine) {
    // Quad
    std::vector<Vertex> vertices = {
        {
            {0.0f, 0.0f, 0.0f},
            {0.0f, 0.0f, 1.0f},
            {1.0f, 1.0f, 1.0f, 1.0f},
            {0.0f, 0.0f}
        },
        {
            {1.0f, 0.0f, 0.0f},
            {0.0f, 0.0f, 1.0f},
            {1.0f, 1.0f, 1.0f, 1.0f},
            {1.0f, 0.0f}
        },
        {
            {1.0f, 1.0f, 0.0f},
            {0.0f, 0.0f, 1.0f},
            {1.0f, 1.0f, 1.0f, 1.0f},
            {1.0f, 1.0f}
        },
        {
            {0.0f, 1.0f, 0.0f},
            {0.0f, 0.0f, 1.0f},
            {1.0f, 1.0f, 1.0f, 1.0f},
            {0.0f, 1.0f}
        },
    };

    std::vector<uint16_t> indices = {
        0, 1, 2,
        2, 3, 0
    };

    return engine.createStaticMesh<Engine::Vertex>("view")
        .withVertices(vertices)
        .withIndices(indices)
        .build();
}

int main() {
    RenderEngine engine;

    engine.addSubsystem(Subsystem::LightSubsystem::ID);
    engine.addSubsystem(Subsystem::ObjectSubsystem::ID);

    auto objects = engine.getSubsystem(Subsystem::ObjectSubsystem::ID);

    try {
        engine.initialize("Terrain Viewer");
    } catch (const std::exception& e) {
        std::cerr << "Failed to initialise the render engine." << std::endl;
        std::cerr << e.what() << std::endl;
        return false;
    }

    auto mesh = setupMesh(engine);
    auto material = engine.createMaterial({
        "outputMaterial",
        "internal.white",
        "",
        "",
        false,
        vk::Filter::eNearest,
        vk::Filter::eNearest,
    });

    auto obj = objects->createObject()
        .withMesh(mesh)
        .withMaterial(material)
        .withPosition({0,0,0})
        .build();

    Camera cam(CameraType::Perspective, {0.5, 0.5, 80}, {0, 0, -1}, {0, 1, 0});

    engine.setCamera(cam);
    
    auto &input = engine.getInputManager();
    input.releaseMouse();

    while (engine.beginFrame()) {
        // Frame timing
        if (input.wasPressed(Engine::Key::eEscape)) {
            break;
        }

        engine.render();
    }
}
