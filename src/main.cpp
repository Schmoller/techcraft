#define SETUP_IMPLEMENTATION
#include "engine/setup.hpp"
#undef SETUP_IMPLEMENTATION

#include "techcraft/game.hpp"

int main() {
    Techcraft::Game game;

    if (game.launch()) {
        return EXIT_SUCCESS;
    } else {
        return EXIT_FAILURE;
    }
}