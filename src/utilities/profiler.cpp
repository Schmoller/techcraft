#include "utilities/profiler.hpp"

#include <iostream>
#include <map>
#include <functional>

// Initialise the instance
Profiler Profiler::theInstance = Profiler();

void debugPrintProfilerTimings() {
    auto timings = Profiler::getTimings();
    
    std::map<std::chrono::duration<double, std::milli>, std::string, std::greater<std::chrono::duration<double, std::milli>>> worstPerformers;

    for (auto &pair : timings) {
        worstPerformers[pair.second] = pair.first;
    }

    std::cout << "Profiler timings:" << std::endl;
    int remain = 100;
    for (auto &pair : worstPerformers) {
        auto time = pair.first;

        std::cout << " " << pair.second << ": " << time.count() << "ms" << std::endl;
        --remain;
        if (remain <= 0) {
            break;
        }
    }
    std::cout << std::endl;
}