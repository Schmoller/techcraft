#include "utilities/workers.hpp"

#include <cmath>
#include <iostream>

namespace Utilities {

// WorkerTask
WorkerFleet::WorkerTask::WorkerTask(std::function<void()> &&callback)
: callback(callback)
{}

void WorkerFleet::WorkerTask::run() {
    callback();
}

// Worker
Worker::Worker(WorkerFleet &fleet)
    : fleet(fleet), isRunning(true)
{
    thread = std::thread(std::bind(&Worker::run, this));
}

void Worker::run() {
    while (isRunning) {
        auto task = fleet.popTask();
        if (!task) {
            break;
        }
        task->run();
    }
}

void Worker::shutdown() {
    isRunning = false;
}

void Worker::join() {
    thread.join();
}


// WorkerFleet
WorkerFleet::WorkerFleet(bool startBlocked, float coreUtilisationPercent)
    : nextId(0), isBlocked(startBlocked), isRunning(true)
{
    coreUtilisationPercent = std::min(std::max(coreUtilisationPercent, 0.0f), 1.0f);
    uint32_t workerCount = std::ceil(std::thread::hardware_concurrency() * coreUtilisationPercent);
    // uint32_t workerCount = 1;

    for (uint32_t i = 0; i < workerCount; ++i) {
        workers.emplace_back(*this);
    }
}

WorkerFleet::~WorkerFleet() {
    if (isRunning) {
        shutdown(true);
    }
}

uint32_t WorkerFleet::submit(std::function<void()> &&callback) {
    std::unique_lock lock(taskQueueLock);

    uint32_t id = nextId++;

    auto &task = taskQueue.emplace_back(std::move(callback));

    task.id = id;

    if (!isBlocked) {
        submitNotification.notify_one();
    }

    return id;
}

void WorkerFleet::cancel(uint32_t id) {
    std::unique_lock lock(taskQueueLock);

    for (auto it = taskQueue.begin(); it != taskQueue.end(); ++it) {
        if (it->id == id) {
            taskQueue.erase(it);
            break;
        }
    }
}

void WorkerFleet::cancelAll() {
    std::unique_lock lock(taskQueueLock);

    taskQueue.clear();
}

void WorkerFleet::block() {
    std::unique_lock lock(taskQueueLock);

    if (isBlocked) {
        return;
    }

    isBlocked = true;
    // Any that are blocked waiting for a task need to wake up
    // so they give us the block notification
    submitNotification.notify_all();
    unblockNotification.notify_all();

    // Now we need to wait until all have stopped.
    size_t stoppedWorkers = 0;
    blockedWorkers = 0;

    while (stoppedWorkers < workers.size()) {
        blockNotification.wait(lock);
        stoppedWorkers += blockedWorkers;
        blockedWorkers = 0;
    }

    // Everyone is now stopped
}

void WorkerFleet::unblock() {
    std::unique_lock lock(taskQueueLock);

    isBlocked = false;
    unblockNotification.notify_all();
}

void WorkerFleet::shutdown(bool now) {
    if (!isRunning) {
        return;
    }

    if (now) {
        cancelAll();
    }

    // Stops processing of tasks and gets them all into a defined spot
    block();

    isRunning = false;

    for (auto &worker : workers) {
        worker.shutdown();
    }

    // Wake all threads so they can shut down
    unblockNotification.notify_all();

    for (auto &worker : workers) {
        worker.join();
    }
}

std::optional<WorkerFleet::WorkerTask> WorkerFleet::popTask() {
    std::unique_lock lock(taskQueueLock);

    while (true) {
        if (!isRunning) {
            return {};
        }

        if (isBlocked) {
            ++blockedWorkers;
            blockNotification.notify_all();

            unblockNotification.wait(lock);
            continue; // For spurious wakes
        }

        if (taskQueue.empty()) {
            submitNotification.wait(lock);
            continue;
        }

        break;
    }

    auto task = taskQueue.front();
    taskQueue.pop_front();

    return task;
}

}