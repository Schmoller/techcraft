#include "utilities/lsystem.hpp"

#include <sstream>

#ifdef LSYSTEM_DEBUG
#include <iostream>
#endif

namespace Utilities::LSystem {

// Rule
size_t Rule::apply(std::string &string, size_t index) {
    string.erase(string.begin() + index);
    if (transformation.size() > 0) {
        string.insert(string.begin() + index, transformation.begin(), transformation.end());
    }

    return transformation.size();
}

bool Rule::operator<(const Rule &other) const {
    if (other.priority() > priority()) {
        return true;
    }

    if (match < other.match) {
        return true;
    }

    return false;
}

// BasicRule
bool BasicRule::doesApply(char current, const std::string &string, size_t index, std::mt19937 *rand) {
    return (current == match);
}
std::string BasicRule::toString() const {
    std::stringstream ss;

    ss << match << " -> " << std::string(transformation.data(), transformation.size());

    return ss.str();
}

// ProbabilisticRule
bool ProbabilisticRule::doesApply(char current, const std::string &string, size_t index, std::mt19937 *rand) {
    float randVal = (*rand)() / static_cast<float>(rand->max());

    return (current == match && randVal < probability);
}
std::string ProbabilisticRule::toString() const {
    std::stringstream ss;

    ss << match << " (" << probability << ") -> " << std::string(transformation.data(), transformation.size());

    return ss.str();
}

bool ContextualRule::doesApply(char current, const std::string &string, size_t index, std::mt19937 *rand) {
    if (current == match) {
        // Match the before string
        int32_t matchInd = before.size() - 1;
        for (int32_t absIndex = index - 1; absIndex >= 0 && matchInd >= 0; --absIndex, --matchInd) {
            if (string[absIndex] != before[matchInd]) {
                return false;
            }
        }

        if (matchInd >= 0) {
            // Didnt match whole string
            return false;
        }

        // Match the after string
        matchInd = 0;
        for (int32_t absIndex = index + 1; absIndex < string.size() && matchInd < after.size(); ++absIndex, ++matchInd) {
            if (string[absIndex] != before[matchInd]) {
                return false;
            }
        }

        if (matchInd < after.size()) {
            // Didnt match whole string
            return false;
        }

        return true;
    }

    return false;
}

std::string ContextualRule::toString() const {
    std::stringstream ss;

    if (!before.empty()) {
        ss << std::string(before.data(), before.size()) << " < ";
    }

    ss << match;
    
    if (!after.empty()) {
        ss << " > " << std::string(after.data(), after.size());
    }

    ss << " -> " << std::string(transformation.data(), transformation.size());

    return ss.str();
}

// LSystem
LSystem::LSystem() {}

LSystem::LSystem(const std::vector<std::shared_ptr<Rule>> &rules)
    : rules(rules)
{}

void LSystem::addRule(const std::shared_ptr<Rule> &rule) {
    if (rule->requireRandon()) {
        requireRand = true;
    }

    auto it = rules.begin();
    for (; it != rules.end(); ++it) {
        auto other = it->get();

        if (!((*rule.get()) < (*other))) {
            rules.insert(it, rule);
            return;
        }
    }

    rules.push_back(rule);
}

void LSystem::addRule(char match, const std::string &transform) {
    addRule(std::make_shared<BasicRule>(match, transform));
}

void LSystem::addRule(char match, float probability, const std::string &transform) {
    addRule(std::make_shared<ProbabilisticRule>(match, probability, transform));
}

void LSystem::addRule(char match, const std::string &before, const std::string &after, const std::string &transform) {
    addRule(std::make_shared<ContextualRule>(match, before, after, transform));
}

void LSystem::setRand(std::mt19937 &rand) {
    this->rand = &rand;
}

std::string LSystem::run(char axiom, uint32_t iterations) {
    return run(std::string(axiom,1), iterations);
}

std::string LSystem::run(const std::string &axiom, uint32_t iterations) {
    assert(!requireRand || rand);

    std::string string = axiom;

    for (uint32_t iter = 0; iter < iterations; ++iter) {
        #ifdef LSYSTEM_DEBUG
        std::cout << "Iter: " << iter << " S: " << std::string(string.data(), string.size()) << std::endl;
        #endif

        // TODO: This can most likely be optimised into some FSA or FSM
        for (uint32_t i = 0; i < string.size(); ++i) {
            for (auto &rule : rules) {
                // TODO: We need to handle the fallback case. More complex rules take precedence over less complex ones.
                // This means that its Contextual -> Stochastic -> Basic
                if (rule->doesApply(string[i], string, i, rand)) {
                    #ifdef LSYSTEM_DEBUG
                    std::cout << "Apply " << rule->toString() << " at " << i << std::endl;
                    std::cout << " before: " << std::string(string.data(), string.size()) << std::endl;
                    #endif
                    size_t amount = rule->apply(string, i);
                    // Skip over the newly added characters
                    i += amount - 1;

                    #ifdef LSYSTEM_DEBUG
                    std::cout << " after: " << std::string(string.data(), string.size()) << std::endl;
                    #endif
                    break;
                }
            }
        }
    }

    return string;
}

std::string LSystem::toString() const {
    std::stringstream ss;

    bool first = true;
    for (auto &rule : rules) {
        if (!first) {
            ss << ", ";
        }
        first = false;

        ss << "(" << rule->toString() << ")";
    }

    return ss.str();
}




}