#pragma once

#include "common.hpp"
#include "techcraft/common.hpp"

namespace Techcraft {

class ItemTubeTileEntity: public TileEntity {
    public:
    ItemTubeTileEntity(const TilePosition &pos);

    bool isSolidOn(TileDirection face) const override;

    bool isConnectedOn(TileDirection dir) const {
        return connectedOn[static_cast<int>(dir)];
    }

    void setConnectedOn(TileDirection dir, bool connected) {
        connectedOn[static_cast<int>(dir)] = connected;
        markDirty();
    }

    const bool *getConnectivity() const {
        return connectedOn;
    }

    TileDirection getAttachSide() const {
        return attachSide;
    }

    void setAttachSide(TileDirection dir) {
        attachSide = dir;
        markDirty();
    }

    bool isAttached() const {
        return attachSide != TileDirection::Self;
    }

    std::optional<BoundingBox> getBounds() const override;

    virtual void onPlace() override;
    virtual void onNeighbourUpdate(const TilePosition &neighbourPos, const Tile &neighbour) override;

    private:
    void updateAttachment();

    bool connectedOn[6] { false, false, false, false, false, false };
    TileDirection attachSide { TileDirection::Self };
};

}