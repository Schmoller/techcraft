#pragma once

#include "techcraft/common.hpp"

namespace Techcraft {

// In lieu of including
class Dimension;
class Tile;

/**
 * The basic tile entity, must be overridden to add behaviour
 */
class TileEntity {
    friend class Chunk;
    
    public:
    TileEntity(const TilePosition &pos);
    virtual ~TileEntity() = default;

    const TilePosition &getPosition() const;
    const Dimension &getDimension() const;

    virtual std::optional<BoundingBox> getBounds() const;
    /**
     * Checks if a face is solid or not
     */
    virtual bool isSolidOn(TileDirection face) const;

    bool isDirty() const {
        return dirty;
    }

    virtual void onPlace() {};
    virtual void onRemove() {};
    virtual void onNeighbourUpdate(const TilePosition &neighbourPos, const Tile &neighbour) {};

    protected:
    void markDirty();
    void markClean();

    private:
    void registerInto(Dimension *dimension);

    const TilePosition position;
    Dimension *dimension;

    bool dirty;
};

/**
 * Behaviour to indicate that a tile entity requires ticking
 */
class Tickable {
    public:
    virtual void doTick() = 0;
};

}