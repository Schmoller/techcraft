#pragma once

#include "common.hpp"
#include "techcraft/common.hpp"

// Slab tile entity
namespace Techcraft {

class SlabTileEntity : public TileEntity {
    public:
    SlabTileEntity(const TilePosition &pos);
    SlabTileEntity(const TilePosition &pos, TileDirection attachment);

    /**
     * Gets which side of the tile this slab it attached.
     * This is from the perspective of this tile.
     */
    TileDirection getAttached() const;
    /**
     * Sets the attach direction.
     * Must not be Self
     */
    void setAttached(TileDirection face);

    std::optional<BoundingBox> getBounds() const override;
    bool isSolidOn(TileDirection face) const override;

    private:
    // Face the slab is attached to. Self is not a valid value.
    // Direction is from own perspective
    TileDirection attached;
};

}