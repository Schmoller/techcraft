#pragma once

#include "common.hpp"

namespace Techcraft {

class BarrelTileEntity: public TileEntity {
    public:
    BarrelTileEntity(const TilePosition &pos);

    bool isSolidOn(TileDirection face) const override;
};

}