#pragma once

#include "base.hpp"
#include "utilities/registry.hpp"

namespace Techcraft {

// Forward declarations
class ItemRegistry;

class ItemRegistryBuilder: public Utilities::RegistryBuilder<ItemType, ItemRegistry> {
    public:
    ItemRegistryBuilder(ResourceManager &manager);

    virtual std::unique_ptr<ItemRegistry> build() override;

    protected:
    virtual void initializeItem(ItemType *item, size_t slotId) override;

    private:
    ResourceManager &manager;
};

/**
 * Allows the lookup of items
 */
class ItemRegistry: public Utilities::Registry<ItemType> {
    friend class ItemRegistryBuilder;

    private:
    ItemRegistry(std::unordered_map<std::string, std::shared_ptr<ItemType>>, std::vector<std::shared_ptr<ItemType>>);
};

}