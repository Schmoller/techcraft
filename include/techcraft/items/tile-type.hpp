#pragma once

#include "base.hpp"
#include "techcraft/tiles/base.hpp"

namespace Techcraft {

/**
 * Represents a TileType in as an ItemType.
 * Only TileTypes with a TileItemType instance are carryable in inventories,
 * and by extension, placeable by players (directly)
 */
class TileItemType : public ItemType {
    public:
    TileItemType(const TileType &tileType);

    const TileType &getTileType() const { return tileType; }

    const TileTexture *texture() const override;
    bool doPlace(ItemStack &item, LocalPlayer &player, Dimension &dimension) const override;

    Rendering::ItemRenderer &getRenderer() const override;

    private:
    const TileType &tileType;
};

}
