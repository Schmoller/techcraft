#pragma once

#include "base.hpp"

namespace Techcraft {

class DebugTreeGenItem: public ItemType {
    public:
    DebugTreeGenItem();
    
    const TileTexture *texture() const { return icon; }

    bool doPlace(ItemStack &item, LocalPlayer &player, Dimension &dimension) const override;

    private:
    void initialiseItem(ResourceManager &manager) override;

    const TileTexture *icon;
};

}