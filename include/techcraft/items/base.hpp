#pragma once

#include "techcraft/common.hpp"
#include "techcraft/resources/manager.hpp"
#include "techcraft/resources/tile-texture.hpp"
#include <string>

namespace Techcraft {

// Forward declarations
class ItemStack;
class LocalPlayer;
class Dimension;

namespace Rendering {
    class ItemRenderer;
}

class ItemType {
    friend class ItemRegistryBuilder;
    
    public:
    ItemType(const std::string_view &name);

    const std::string &id() const { return name; }

    /**
     * Fired when the item (held by a player) is attempted to be placed.
     * 
     * If placing the item should consume the item, then the `item` parameter may be modified.
     * @param item The item to be placed. Always has the same ItemType as this class
     * @param player The player placing the item
     * @param dimension The dimension being placed into
     * @returns true if the item was placed. false otherwise.
     */
    virtual bool doPlace(ItemStack &item, LocalPlayer &player, Dimension &dimension) const;

    /**
     * Fired when the item is held by the player, which looking at a tile or subtile.
     * 
     * This is used to render the placement guides (if applicable) for the given item.
     */
    virtual void renderPlacementHighlight(ItemStack &item, LocalPlayer &player, Dimension &dimension) const;

    /**
     * Gets the texture of this item.
     * If the item does not have a texture, this may return a nullptr
     */
    virtual const TileTexture *texture() const { return nullptr; };

    /**
     * Gets the renderer used to render this item
     */
    virtual Rendering::ItemRenderer &getRenderer() const;

    bool operator==(const ItemType &other) const;

    protected:
    virtual void initialiseItem(ResourceManager &manager) {};

    private:
    const std::string name;
};

}