#pragma once

#include "base.hpp"
#include "techcraft/subtiles/base.hpp"

namespace Techcraft {

class SubtileItemType: public ItemType {
    public:
    SubtileItemType(const SubtileType &type);

    const SubtileType &getSubtileType() const { return subtileType; }

    bool doPlace(ItemStack &item, LocalPlayer &player, Dimension &dimension) const override;

    void renderPlacementHighlight(ItemStack &item, LocalPlayer &player, Dimension &dimension) const override;

    Rendering::ItemRenderer &getRenderer() const override;

    private:
    const SubtileType &subtileType;

    bool hasCenterSlot { false };
    bool hasCornerSlot { false };
    bool hasEdgeSlot { false };
    bool hasFaceSlot { false };
};

}