#pragma once

#include <string>

namespace Techcraft::Commands {

/**
 * Interface representing something that can send commands to the game
 */
class CommandSender {
    public:
    virtual ~CommandSender() = default;

    virtual void printLine(const std::wstring_view &line) = 0;
};

/**
 * A command sender for a player
 */
class PlayerCommandSender: public CommandSender {
    public:
};

}