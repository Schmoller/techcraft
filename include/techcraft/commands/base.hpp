#pragma once

#include "sender.hpp"

#include <string>
#include <vector>

namespace Techcraft::Commands {

class Command {
    public:
    virtual std::wstring getName() const = 0;

    /**
     * Allow this command to be executed by the server console
     */
    virtual bool allowConsole() { return true; }
    /**
     * Allows this command to be executed by a player
     */
    virtual bool allowPlayer() { return true; }

    /**
     * Aliases allow the command to be used with multiple names
     */
    virtual std::vector<std::wstring> getAliases() const { return {}; };

    virtual bool execute(CommandSender &sender, const std::vector<std::wstring> &args) = 0;
    virtual std::vector<std::wstring> tabComplete(CommandSender &sender, const std::vector<std::wstring> &args, size_t index) = 0;
};

}