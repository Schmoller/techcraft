#pragma once

#include "sender.hpp"
#include "base.hpp"

#include <string>
#include <vector>
#include <memory>
#include <unordered_map>

namespace Techcraft::Commands {

class Executor {
    public:
    
    void addCommand(const std::shared_ptr<Command> &command);

    bool execute(CommandSender &sender, const std::wstring &commandLine);
    bool execute(CommandSender &sender, const std::wstring &command, const std::vector<std::wstring> &args);

    std::vector<std::wstring> autoComplete(CommandSender &sender, const std::wstring &commandLine, size_t cursor);
    std::vector<std::wstring> autoComplete(CommandSender &sender, const std::wstring &command, const std::vector<std::wstring> &commandLine, size_t arg);

    private:
    std::unordered_map<std::wstring, std::shared_ptr<Command>> commandMap;
    std::unordered_map<std::wstring, std::shared_ptr<Command>> aliasMap;
};

}