#pragma once

#include "base.hpp"

namespace Techcraft::Commands {

class TestCommand: public Command {
    public:
    std::wstring getName() const override { return L"test"; }

    bool execute(CommandSender &sender, const std::vector<std::wstring> &args) override;
    std::vector<std::wstring> tabComplete(CommandSender &sender, const std::vector<std::wstring> &args, size_t index) override;
};

}