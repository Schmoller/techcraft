#pragma once

#include "engine/inputmanager.hpp"
#include "utilities/flags.hpp"

#include <optional>

namespace Techcraft {

// Forward declarations
class BindingManager;
class KeyBinding;
class KeyBindingBuilder;

enum class KeybindContextFlag {
    None = 0,

    Inventory = 0x01,
    Console = 0x02,
};

typedef Utilities::Flags<KeybindContextFlag> KeybindContext;

inline KeybindContext operator|(KeybindContextFlag value1, KeybindContextFlag value2) {
    return KeybindContext(value1) | value2;
}
inline KeybindContext operator~(KeybindContextFlag value1) {
    return ~KeybindContext(value1);
}

/**
 * A key binding which can be used to get input from the player
 */
class KeyBinding {
    friend class BindingManager;

    public:
    class Definition {
        public:
        Definition() = default;
        Definition(const Definition &other);
        Definition(Engine::Key key, Engine::Modifier modifiers);

        bool isActive(Engine::InputManager &inputManager, Engine::Modifier modifiers) const;
        bool isContained(const Definition &other) const;

        Engine::Key key;
        Engine::Modifier modifiers;
    };

    KeyBinding(const std::string &name, KeybindContext mask, std::optional<Definition> binding = {}, std::optional<Definition> alternateBinding = {});

    const std::string &getName() const { return name; }

    /**
     * Sets the key binding.
     * If this key (or combination) is active, then this binding will be active
     */
    void setBinding(const Definition &bind);

    /**
     * Removes the binding
     */
    void unsetBinding();

    /**
     * Gets the key binding
     */
    std::optional<Definition> getBinding() const { return binding; }

    /**
     * Sets another key binding.
     * If this key (or combination) is active, then this binding will be active
     */
    void setAlternateBinding(const Definition &bind);

    /**
     * Removes the alternate binding
     */
    void unsetAlternateBinding();

    /**
     * Gets the alternate key binding
     */
    std::optional<Definition> getAlternateBinding() const { return alternateBinding; }

    /**
     * Checks if the binding was pressed since the last tick
     */
    bool wasPressed();

    /**
     * Checks if the binding was released since the last tick
     */
    bool wasReleased();

    /**
     * Checks if the binding is currently pressed
     */
    bool isPressed();

    KeybindContext contextMask() const { return mask; }

    bool isEnabledIn(KeybindContext context) const;

    private:
    void update(bool pressed);
    void tick();

    const std::string name;

    std::optional<Definition> binding;
    std::optional<Definition> alternateBinding;

    KeybindContext mask {};
    bool pressed { false };
    bool lastPressed { false };

    bool modified { false };
};

class BindingManager {
    friend class KeyBindingBuilder;

    public:
    BindingManager(Engine::InputManager &inputManager);

    bool isEnabled() const { return enabled; }
    void setEnabled(bool enabled);

    KeyBindingBuilder createBinding(const std::string_view &name);

    std::shared_ptr<KeyBinding> getBinding(const std::string_view &name) const;

    void doTick();

    bool forceCheckBindingActive(const KeyBinding &binding);
    bool forceCheckBindingActive(const std::string_view &name);

    void setContext(KeybindContextFlag context);
    KeybindContextFlag getContext() const { return currentContext; }

    private:
    void addBinding(const std::shared_ptr<KeyBinding> &binding);
    void onInputUpdate(Engine::Key key, Engine::Action action, const Engine::Modifier &modifiers);

    KeybindContextFlag currentContext { KeybindContextFlag::None };

    Engine::InputManager &inputManager;
    bool enabled { true };

    std::vector<std::shared_ptr<KeyBinding>> bindings;

};

/**
 * Allows the construction of new key bindings
 */
class KeyBindingBuilder {
    friend class BindingManager;
    public:
    /**
     * Sets the default key settings for this key bind
     */
    KeyBindingBuilder &withKey(Engine::Key key, Engine::Modifier modifiers = {});
    /**
     * Sets the default fallback key setting for this key bind
     */
    KeyBindingBuilder &withAltKey(Engine::Key key, Engine::Modifier modifiers = {});

    /**
     * Masks allow for the keybinding to work in a context.
     * If no mask is provided, when in a context, the keybinding will be disabled
     */
    KeyBindingBuilder &withContextMask(KeybindContext mask);

    /**
     * Creates the key binding
     */
    std::shared_ptr<KeyBinding> build();

    private:
    KeyBindingBuilder(const std::string_view &name, BindingManager &bindingManager);

    std::string name;
    BindingManager &bindingManager;

    std::optional<KeyBinding::Definition> binding {};
    std::optional<KeyBinding::Definition> alternate {};
    KeybindContext mask;
};


}