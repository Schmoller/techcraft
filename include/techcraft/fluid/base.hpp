#pragma once

#include "techcraft/common.hpp"
#include "techcraft/lighting.hpp"
#include "techcraft/resources/manager.hpp"
#include "techcraft/resources/tile-texture.hpp"

#include <string>

namespace Techcraft {

const uint8_t MAX_FLUID_AMOUNT = 10_u8;

// In lieu of including
namespace Rendering {
    class FluidRenderer;
}


class FluidType {
    friend class FluidRegistryBuilder;
    public:
    FluidType(const std::string &id)
        : name(id)
    {}

    inline const std::string &id() const {
        return name;
    }

    bool operator==(const FluidType &other) const;
    bool operator!=(const FluidType &other) const;

    inline uint32_t getSpread() const {
        return spread;
    }

    inline uint32_t getViscosity() const {
        return viscosity;
    }

    inline uint8_t getLight(TileLightPallet pallet) const {
        return tileLight[static_cast<int>(pallet)];
    }

    inline const uint8_t *getLightAll() const {
        return tileLight;
    }

    inline bool hasLight() const {
        return emitsLight;
    }

    inline const TileTexture *textureOf(TileDirection direction) const {
        return faceTextures[static_cast<int>(direction)];
    }

    /**
     * Gets the renderer that is used to render this fluid type
     */
    virtual Rendering::FluidRenderer &getRenderer() const;

    protected:
    virtual void initialize(ResourceManager &manager) = 0;
    void setSpread(uint32_t spread) {
        this->spread = spread;
    }
    void setViscosity(uint32_t viscosity) {
        if (viscosity == 0) {
            viscosity = 1;
        }
        this->viscosity = viscosity;
    }
    void setTextureAll(const TileTexture *texture);
    void setTexture(const TileTexture *texture, TileDirection direction);
    void setLightEmission(TileLightPallet pallet, uint8_t intensity);

    private:
    std::string name;
    uint32_t slotId {0};
    uint32_t spread {0};
    uint32_t viscosity {1};
    const TileTexture *faceTextures[6] {};
    uint8_t tileLight[TILE_LIGHT_PALLET_COUNT] {};
    bool emitsLight {false};
};


/**
 * Represents an instance of a fluid in the world
 */
class FluidTile {
    public:
    inline FluidTile() = default;
    inline FluidTile(const FluidType *type, uint16_t amount, bool flowing = false)
        : type(type)
    {
        setAmount(amount);
        setFlowing(flowing);
    }

    inline bool isEmpty() const {
        return !type;
    }

    inline const FluidType *getType() const {
        return type;
    }

    inline const bool contains(const FluidType *type) const {
        return this->type == type;
    }

    std::optional<BoundingBox> getBounds() const;

    inline bool isFlowing() const {
        return amount & 0x8000;
    }

    inline void setFlowing(bool flowing) {
        if (flowing) {
            amount |= 0x8000;
        } else {
            amount &= ~(0x8000);
        }
    }

    inline void setAmount(uint16_t amount) {
        this->amount = ~(0x8000) & amount;
    }

    inline uint16_t getAmount() const {
        return this->amount & ~(0x8000);
    }

    private:
    const FluidType *type {nullptr};
    uint16_t amount {0};
};


}