#pragma once

#include "base.hpp"
#include "utilities/registry.hpp"
#include "techcraft/resources/resource.hpp"

namespace Techcraft {


/**
 * Used for making new fluid types
 */
class FluidTypeDefinition {
    friend class FluidRegistryBuilder;

    public:
    FluidTypeDefinition(const std::string &name)
        : name(name)
    {}

    void setSpread(uint32_t spread) {
        this->spread = spread;
    }
    void setViscosity(uint32_t viscosity) {
        this->viscosity = viscosity;
    }
    void setTextureAll(const Resource &resource);
    void setTexture(const Resource &resource, TileDirection direction);
    void setLightEmission(TileLightPallet pallet, uint8_t intensity);

    private:
    uint32_t spread {10};
    uint32_t viscosity {1};
    std::string name;
    Resource faceTextures[6];
    uint8_t tileLight[TILE_LIGHT_PALLET_COUNT] = {};
};


class BasicFluidType : public FluidType {
    public:
    BasicFluidType(
        const std::string &id,
        uint32_t spread,
        uint32_t viscosity,
        const TileTexture *faceTextures[6],
        const uint8_t tileLight[TILE_LIGHT_PALLET_COUNT]
    );

    protected:
    void initialize(ResourceManager &manager);
};

class FluidRegistry;

/**
 * Used to create tile registries
 */
class FluidRegistryBuilder: public Utilities::RegistryBuilder<FluidType, FluidRegistry> {
    public:
    FluidRegistryBuilder(ResourceManager &manager);

    virtual std::unique_ptr<FluidRegistry> build() override;

    const FluidType *add(const FluidTypeDefinition &definition);

    using Utilities::RegistryBuilder<FluidType, FluidRegistry>::add;
    protected:
    virtual void initializeItem(FluidType *item, size_t slotId) override;

    private:
    ResourceManager &manager;
};

/**
 * Allows the lookup of types of fluid
 */
class FluidRegistry: public Utilities::Registry<FluidType> {
    friend class FluidRegistryBuilder;

    private:
    FluidRegistry(std::unordered_map<std::string, std::shared_ptr<FluidType>>, std::vector<std::shared_ptr<FluidType>>);
};


class FluidTypes {
    public:

    static inline const FluidRegistry *registry() { return fluidRegistry.get(); }

    static void initialize(ResourceManager &manager);

    // Common liquid
    static const FluidType *Water;
    static const FluidType *Lava;

    private:
    static std::unique_ptr<FluidRegistry> fluidRegistry;
};

}