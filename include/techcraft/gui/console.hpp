#pragma once

#include "techcraft/console.hpp"
#include "techcraft/gui/window.hpp"
#include "techcraft/keybinding.hpp"

namespace Techcraft {
namespace _G = Engine::Gui;

class GuiConsole: public Window {
    public:
    GuiConsole(const _G::Rect &bounds, Console &console, Engine::Font *font);

    KeybindContextFlag keybindContext() const override { return KeybindContextFlag::Console; }

    bool doesCaptureInput() const override { return true; }
    
    void onKeyTyped(wchar_t ch) override;
    void onKeyPress(Engine::Key key, Engine::Modifier modifiers) override;

    Console &getConsole() const { return console; }

    protected:
    void onPaint(_G::Drawer &drawer) override;
    void onFrameUpdate() override;

    private:
    Console &console;
    Engine::Font *font;
    std::shared_ptr<KeyBinding> consoleKeybind;

    uint32_t scrollOffset { 0 };
};

}