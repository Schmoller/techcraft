#pragma once

#include "techcraft/gui/window.hpp"
#include "techcraft/inventory/player.hpp"

#include "techcraft/gui/components/invslot.hpp"

#include <vector>

namespace Techcraft {

class GuiPlayerInventory: public Window {
    public:
    GuiPlayerInventory(PlayerInventory &playerInventory);

    KeybindContextFlag keybindContext() const override { return KeybindContextFlag::Inventory; }

    bool doesCaptureInput() const override { return true; }
    void onKeyPress(Engine::Key key, Engine::Modifier modifiers) override;
    void onKeyRelease(Engine::Key key, Engine::Modifier modifiers) override;
    void onKeyTyped(wchar_t ch) override;
    
    void onMousePress(Engine::MouseButton button, double x, double y, Engine::Modifier modifiers) override;
    void onMouseRelease(Engine::MouseButton button, double x, double y, Engine::Modifier modifiers) override;
    void onMouseMove(Engine::MouseButtons buttons, double x, double y, Engine::Modifier modifiers) override;
    void onMouseScroll(double scrollX, double scrollY) override;

    protected:
    void onPaint(_G::Drawer &drawer) override;
    void onFrameUpdate() override;

    private:
    GuiInventorySlot *getSlotAt(float x, float y) const;
    bool isDropZone(float x, float y) const;
    bool isDeleteZone(float x, float y) const;

    PlayerInventory &playerInventory;

    OptionalItemStack heldItemstack;
    float cursorX, cursorY;

    std::vector<std::shared_ptr<GuiInventorySlot>> slots;

    GuiInventorySlot *lastHoverSlot { nullptr };
};

}