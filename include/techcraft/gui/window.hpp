#pragma once

#include "techcraft/keybinding.hpp"

#include "engine/gui/common.hpp"
#include "engine/inputmanager.hpp"

namespace Techcraft {
namespace _G = Engine::Gui;

class Window: public _G::BaseComponent {
    public:
    Window(const _G::Rect &bounds);

    virtual bool doesCaptureInput() const { return false; }
    /**
     * Used when input is captured to allow or deny ALL keybinds from
     * still executing.
     * @returns false if the keybinds should not be executed
     */
    virtual bool allowKeybinds() const { return true; }

    /**
     * Sets the keybind context, for masking out specific keybindings from triggering
     */
    virtual KeybindContextFlag keybindContext() const { return KeybindContextFlag::None; }

    virtual void onKeyPress(Engine::Key key, Engine::Modifier modifiers) {};
    virtual void onKeyRelease(Engine::Key key, Engine::Modifier modifiers) {};
    virtual void onKeyTyped(wchar_t ch) {};

    virtual void onMousePress(Engine::MouseButton button, double x, double y, Engine::Modifier modifiers) {};
    virtual void onMouseRelease(Engine::MouseButton button, double x, double y, Engine::Modifier modifiers) {};
    virtual void onMouseMove(Engine::MouseButtons buttons, double x, double y, Engine::Modifier modifiers) {};
    virtual void onMouseScroll(double scrollX, double scrollY) {};

    /**
     * Called when the window is shown.
     * If it was closed because another window was shown, that window will be passed
     * in as `previous`
     */
    virtual void onShow(Window *previous) {};
    /**
     * Called when the window is hidden
     */
    virtual void onHide() {};
};

}