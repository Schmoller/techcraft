#pragma once

#include "techcraft/inventory/base.hpp"
#include "engine/gui/common.hpp"

#define DefaultInvSlotSize {52,52}

namespace Techcraft {
namespace _G = Engine::Gui;

class GuiInventorySlot: public _G::BaseComponent {
    public:
    GuiInventorySlot(Inventory &inventory, uint32_t slot, const glm::vec2 &coords, const glm::vec2 &size = DefaultInvSlotSize);

    void setHover(bool hover);
    bool isHovered() const { return hover; }

    OptionalItemStack getItem() const { return inventory[slot]; }
    void setItem(const OptionalItemStack &item);

    protected:
    virtual void onPaint(_G::Drawer &drawer) override;
    virtual void onFrameUpdate() override;

    private:
    Inventory &inventory;
    const uint32_t slot;

    bool hover { false };
};

}
