#pragma once

#include "engine/gui/common.hpp"

namespace Techcraft {
namespace _G = Engine::Gui;

// In lieu of including
class Game;

class GuiDebug : public _G::BaseComponent {
    public:
    GuiDebug(Game *game, const _G::Rect &bounds);

    protected:
    void onFrameUpdate() override;
    void onPaint(_G::Drawer &drawer) override;

    private:
    Game *game;
};

}