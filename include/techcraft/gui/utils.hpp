#pragma once

#include "engine/gui/drawer.hpp"
#include "engine/font.hpp"
#include "techcraft/inventory/itemstack.hpp"

namespace Techcraft {

void drawItemStack(Engine::Gui::Drawer &drawer, const ItemStack &item, Engine::Font *font, float x, float y, float width = 32, float height = 32);

}