#pragma once

#include "techcraft/entities/player.hpp"
#include "techcraft/inventory/player.hpp"
#include "engine/gui/common.hpp"
#include "engine/texturemanager.hpp"
#include "engine/font.hpp"

#include <array>

namespace Techcraft {
namespace _G = Engine::Gui;

class GuiHUD : public _G::BaseComponent {
    public:
    GuiHUD(const _G::Rect &bounds, LocalPlayer *player, Engine::TextureManager &textureManager, Engine::Font *itemFont);


    protected:
    void onFrameUpdate() override;
    void onPaint(_G::Drawer &drawer) override;

    private:
    void drawToolbarSlot(_G::Drawer &drawer, float x, float y, bool selected, const std::string &name);

    LocalPlayer *player;
    Engine::Texture *hudTexture;
    const Engine::Texture *whiteTexture;
    Engine::Font *itemFont;

    // Last known state
    uint8_t lastSelectedSlot;
    std::array<OptionalItemStack, 10> lastItems;
};

}