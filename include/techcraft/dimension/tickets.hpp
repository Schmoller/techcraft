#pragma once

#include "techcraft/forward.hpp"
#include "techcraft/common/position.hpp"

namespace Techcraft {

typedef int AgentType;

enum class ChunkLoadCause {
    PlayerNear,
    PlayerForced,
    Forced,
};

/**
 * A ticket that indicates that a chunk should be loaded
 */
struct LoadTicket {
    ChunkPosition coord;
    ChunkLoadCause cause; // Why the ticket exists
    std::optional<AgentType> agent; // Some agent that is responsible for the ticket.

    bool operator==(const LoadTicket &other) const {
        return (
            coord == other.coord &&
                cause == other.cause &&
                (
                    (agent && other.agent && *agent == *other.agent) ||
                        (!agent && !other.agent)
                )
        );
    }
};


/**
 * Produces chunk loading tickets for the view range of a player as it moves around.
 */
class AreaTicketer {
public:
    AreaTicketer(ChunkLoader &chunkLoader, AgentType agentId, ChunkLoadCause cause, ChunkCoordRes viewRange);

    void setViewRange(ChunkCoordRes viewRange);
    const ChunkCoordRes &getViewRange() const;

    void updatePosition(const Position &position);

private:
    void updateTickets(const ChunkPosition &newChunk, ChunkCoordRes newViewRange);

    // Provided
    ChunkLoader &chunkLoader;
    AgentType agentId;
    ChunkLoadCause cause;

    // State
    ChunkPosition lastChunk;
    ChunkCoordRes viewRange;
    bool hasTicketed;
};

}

namespace std {
template<> struct hash<Techcraft::LoadTicket> {
    std::size_t operator()(Techcraft::LoadTicket const& ticket) const noexcept {
        auto ticketHash = std::hash<Techcraft::ChunkPosition>()(ticket.coord);
        ticketHash = ticketHash ^ (std::hash<Techcraft::ChunkLoadCause>()(ticket.cause) << 1);
        ticketHash = ticketHash ^ (std::hash<std::optional<Techcraft::AgentType>>()(ticket.agent) << 1);

        return ticketHash;
    }
};
}
