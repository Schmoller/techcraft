#pragma once

constexpr int32_t ChunkBits = 4;
constexpr int32_t ChunkSize = 1 << ChunkBits;
constexpr int32_t ChunkMask = ChunkSize - 1;
constexpr size_t ChunkVolume = ChunkSize * ChunkSize * ChunkSize;
constexpr size_t ChunkArea = ChunkSize * ChunkSize;

