#pragma once

#include "techcraft/common.hpp"
#include "techcraft/forward.hpp"
#include "techcraft/tiles/base.hpp"
#include "techcraft/fluid/base.hpp"
#include "techcraft/region.hpp"
#include "techcraft/lighting.hpp"
#include "techcraft/tileentities/common.hpp"
#include "utilities/bitset.hpp"
#include "techcraft/rendering/renderstate.hpp"
#include "size.hpp"

#include <functional>
#include <deque>
#include <optional>
#include <boost/intrusive/list.hpp>

namespace Techcraft {

#define NOT_PRESENT -1

class Chunk final : public Region {
    friend class Dimension;
    friend class ChunkLoader;
    friend class LightingUpdater;
    friend class Column;
    friend class DimensionRenderer;

    typedef std::function<void()> DirtyCallback;

public:
    Chunk(const ChunkPosition &coord, const Tile &initialFill);

    const ChunkPosition &getPosition() const { return coord; }

    void setTile(uint8_t x, uint8_t y, uint8_t z, const Tile &tile);
    const Tile &getTile(uint8_t x, uint8_t y, uint8_t z) const;
    void setTileLight(uint8_t x, uint8_t y, uint8_t z, const TileLighting &light);
    const TileLighting &getTileLight(uint8_t x, uint8_t y, uint8_t z) const;
    void setFluid(uint8_t x, uint8_t y, uint8_t z, const FluidTile &fluid);
    const FluidTile &getFluid(uint8_t x, uint8_t y, uint8_t z) const;
    void markFluidActive(uint8_t x, uint8_t y, uint8_t z);

    // Overriding Region methods
    Extent size() const {
        return {
            ChunkSize,
            ChunkSize,
            ChunkSize
        };
    }

    void setTile(const TilePosition &location, const Tile &tile);
    const Tile &getTile(const TilePosition &location) const;
    void setFluid(const TilePosition &location, const FluidTile &fluid) override;
    const FluidTile &getFluid(const TilePosition &location) const override;

    void setTileLight(const TilePosition &location, const TileLighting &light);
    const TileLighting &getTileLight(const TilePosition &location) const;

    void setSubtile(const TilePosition &location, const SubtileSlot &slot, const Subtile &subtile) override;
    const Subtile &getSubtile(const TilePosition &location, const SubtileSlot &slot) const override;

    void markDirty();
    void markClean();

    bool isDirty() const {
        return dirty;
    }

    bool isReady() const {
        return ready;
    }

    void setReady() {
        ready = true;

        activeFluidTiles.clear();
    }

    /**
     * Gets the z coordinate of the highest solid tile in the XY position in this chunk.
     * If no such tile exists, then NOT_PRESENT will be returned
     */
    int8_t getTop(uint8_t x, uint8_t y);

    void doTick();

    /**
     * Performs some action for every active fluid tile in the chunk.
     */
    void forEachActiveFluid(
        bool clear, const std::function<void(const FluidTile &, uint8_t x, uint8_t y, uint8_t z)> &callback
    );

    bool operator==(const Chunk &) const;

    boost::intrusive::list_member_hook<> activeRenderHook;
protected:
    void updateDirtyNotifier(DirtyCallback callback);
    void onAcceptIntoDimension(std::shared_ptr<Column> column, Dimension *dimension);

private:
    ChunkPosition coord;
    std::shared_ptr<Column> column;
    Dimension *dimension;
    Tile tilesByXYZ[ChunkVolume];
    FluidTile fluidByXYZ[ChunkVolume];
    TileLighting lightingByXYZ[ChunkVolume];
    int8_t highestByXY[ChunkArea];

    std::vector<std::weak_ptr<Tickable>> tickableTileEntities;
    std::vector<std::shared_ptr<TileEntity>> allTileEntities;

    Utilities::BitSet<ChunkVolume> activeFluidTiles;

    bool dirty;
    DirtyCallback dirtyNotifier;

    bool ready;

    // For DimensionRenderer
    std::optional<ChunkRenderState> renderState;
};

}