#pragma once

#include "techcraft/forward.hpp"
#include "techcraft/common.hpp"
#include "techcraft/lighting.hpp"
#include "size.hpp"
#include <deque>

namespace Techcraft {

/**
 * A column holds information that exist only on the 2D plane.
 */
class Column {
public:
    Column(ChunkCoordRes x, ChunkCoordRes y, LightingUpdater &lighting);
    /**
     * Gets the z coordinate of the highest solid tile in the XY position in this chunk.
     * If no such tile exists, then NOT_PRESENT will be returned
     */
    tcsize getTop(uint8_t x, uint8_t y) const;

    void updateHighestFail(uint8_t x, uint8_t y, ChunkCoordRes chunkZ);
    void updateHighest(uint8_t x, uint8_t y, tcsize newTop);

    void addChunk(const std::shared_ptr<Chunk> &chunk);
    void removeChunk(const std::shared_ptr<Chunk> &chunk);

    bool isEmpty() const;

private:
    LightingUpdater &lighting;
    ChunkCoordRes columnX;
    ChunkCoordRes columnY;

    std::deque<std::shared_ptr<Chunk>> verticalChunks;
    ChunkCoordRes firstZ;
    tcsize highestByXY[ChunkSize * ChunkSize];
};

}