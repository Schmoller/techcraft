#pragma once

#include <cstdint>
#include <limits>

// Forward declarations
namespace Techcraft {
    enum class DimensionEvent;
}

#include "techcraft/forward.hpp"
#include "techcraft/common.hpp"
#include "techcraft/tiles/base.hpp"
#include "techcraft/fluid/base.hpp"
#include "techcraft/subtiles/base.hpp"
#include "techcraft/subtiles/slot.hpp"
#include "utilities/event.hpp"
#include "utilities/workers.hpp"
#include "utilities/octree.hpp"
#include "techcraft/entities/common.hpp"
#include "techcraft/physics.hpp"
#include "techcraft/dimension/chunkloader.hpp"
#include "techcraft/lighting.hpp"
#include "techcraft/fluidsim.hpp"

#include <vector>
#include <deque>
#include <unordered_set>
#include <optional>

namespace Techcraft {

enum class DimensionEvent {
    ChunkDirty,
    ChunkLoad,
    ChunkUnload,
    EntityAdd,
    EntityRemove,
    TileAdd,
    TileRemove,
    TileUpdate
};

enum class SubtileResponse {
    Success,
    NoContainer,
    NoSlot,
    Blocked
};

constexpr size_t OctreeMaxDepth = 16;

/**
 * A dimension is a simulatable and visitable "world".
 */
class Dimension {
    friend class ChunkLoader;
    public:
    typedef Utilities::EventHandler<DimensionEvent::ChunkDirty, ChunkPosition> EventHandlerDirty;
    typedef Utilities::EventHandler<DimensionEvent::ChunkLoad, ChunkPosition, const std::shared_ptr<Chunk>&> ChunkLoadedEvent;
    typedef Utilities::EventHandler<DimensionEvent::ChunkUnload, ChunkPosition, const std::shared_ptr<Chunk>&> ChunkUnloadedEvent;
    typedef Utilities::EventHandler<DimensionEvent::EntityAdd, Entity*> EntityAddEvent;
    typedef Utilities::EventHandler<DimensionEvent::EntityRemove, Entity*> EntityRemoveEvent;
    typedef Utilities::EventHandler<DimensionEvent::TileAdd, const TilePosition&, const Tile&> TileAddEvent;
    typedef Utilities::EventHandler<DimensionEvent::TileRemove, const TilePosition&, const Tile&> TileRemoveEvent;
    typedef Utilities::EventHandler<DimensionEvent::TileUpdate, const TilePosition&, const Tile&> TileUpdateEvent;
    
    /**
     * Constructs a new dimension.
     * @param heightChunks The number of chunks vertically the dimension can use. This
     *      defines the maximum height of the dimension
     * @param chunkDistanceLimit The maximum chuRayTargetnk coordinate that can exist horizontally.
     */
    Dimension(
        uint32_t id,
        std::shared_ptr<Generation::ChunkGenerator> generator,
        Utilities::WorkerFleet &workers,
        uint16_t heightChunks,
        uint16_t chunkDistanceLimit = std::numeric_limits<uint16_t>::max()
    );

    ~Dimension();

    uint32_t getId() const;

    ChunkLoader &getChunkLoader();
    LightingUpdater &getLightingUpdater();
    PhysicsSimulation &getPhysicsSimulation();

    void setChunkLoadDistance(ChunkCoordRes distance);
    ChunkCoordRes getChunkLoadDistance() const;

    /**
     * Retrieves a chunk at the given tile location.
     * NOTE: This does not load or generate the chunk if it is not present
     */
    std::shared_ptr<Chunk> getChunkAt(const TilePosition &pos) const;
    /**
     * Retrieves a chunk at the given tile location.
     * NOTE: This does not load or generate the chunk if it is not present
     */
    std::shared_ptr<Chunk> getChunkAt(tcsize x, tcsize y, tcsize z) const;

    /**
     * Retrieves a chunk at the given chunk coordinate.
     * NOTE: This does not load or generate the chunk if it is not present
     */
    std::shared_ptr<Chunk> getChunk(ChunkCoordRes x, ChunkCoordRes y, ChunkCoordRes z) const;

    /**
     * Retrieves a tile at the given tile location.
     * NOTE: This does not load or generate the chunk if it is not present
     */
    const Tile &getTile(const TilePosition &location) const;
    /**
     * Retrieves a tile at the given tile location.
     * NOTE: This does not load or generate the chunk if it is not present
     */
    const Tile &getTile(tcsize x, tcsize y, tcsize z) const;

    /**
     * Replaces a tile a the given tile location.
     * If the location is not in a currently loaded chunk, a false value is returned.
     * NOTE: This does not load or generate the chunk if it is not present
     */
    bool setTile(const TilePosition &location, const Tile &tile);

    /**
     * Retrieves a fluid tile at the given tile location.
     * NOTE: This does not load or generate the chunk if it is not present
     */
    const FluidTile &getFluid(const TilePosition &location) const;
    /**
     * Retrieves a fluid tile at the given tile location.
     * NOTE: This does not load or generate the chunk if it is not present
     */
    const FluidTile &getFluid(tcsize x, tcsize y, tcsize z) const;
    /**
     * Replaces a fluid tile a the given tile location.
     * If the location is not in a currently loaded chunk, a false value is returned.
     * NOTE: This does not load or generate the chunk if it is not present
     */
    bool setFluid(const TilePosition &location, const FluidTile &fluid);

    /**
     * Retrieves a subtile within the given tile location and slot.
     * If there is no container at that location or the slot doesnt exist,
     * the return value will be a blank subtile
     */
    const Subtile &getSubtile(const TilePosition &location, const SubtileSlot &slot) const;

    /**
     * Sets a subtile within the given tile location and slot.
     * If the location does not have a container or no container can be made, then SubtileResponse::NoContainer will be returned.
     * If the slot is not valid for the given container, then SubtileResponse::NoSlot will be returned.
     * If there is already a subtile in the given slot and location, then SubtileResponse::Blocked will be returned.
     */
    SubtileResponse setSubtile(const TilePosition &location, const SubtileSlot &slot, const Subtile &subtile);

    /**
     * Gets the location of the highest tile at the given location
     */
    std::optional<TilePosition> getTop(const TilePosition &location);
    /**
     * Gets the location of the highest tile at the given location
     */
    std::optional<TilePosition> getTop(tcsize x, tcsize y);

    // Entities
    template <typename T, typename... Args>
    T *addEntity(const Position &position, Args&... args);
    EntityID addEntity(std::unique_ptr<Entity> &&entity, const Position &position);
    Entity *getEntity(EntityID id);
    void removeEntity(EntityID id);
    void transferEntity(EntityID id, Dimension &owner, const Position &target);

    // Events
    EventHandlerDirty &eventChunkDirty();
    ChunkLoadedEvent &getChunkLoadedEvent();
    ChunkUnloadedEvent &getChunkUnloadedEvent();
    EntityAddEvent &getEntityAddEvent();
    EntityRemoveEvent &getEntityRemoveEvent();
    TileAddEvent &getTileAddEvent();
    TileRemoveEvent &getTileRemoveEvent();
    TileUpdateEvent &getTileUpdateEvent();

    // Updates
    void doTick();
    void doUpdate();

    // Iterator methods
    void forEachLoadedChunk(const std::function<void(const ChunkPosition &, const std::shared_ptr<Chunk> &)> &callback);

    private:
    void acceptChunk(const ChunkPosition &coord, const std::shared_ptr<Chunk> &chunk);
    std::shared_ptr<Chunk> disownChunk(const ChunkPosition &coord);

    std::shared_ptr<Column> getOrMakeColumn(const ChunkPosition &coord);

    void notifyNeighboursOfUpdate(const TilePosition &pos);

    // Provided
    uint32_t id;

    // Owned
    ChunkLoader chunkLoader;
    LightingUpdater lighting;
    PhysicsSimulation physicsSim;
    FluidSimulation fluidSim;

    std::unordered_map<EntityID, std::unique_ptr<AreaTicketer>> entityChunkTicketers;

    // Events
    EventHandlerDirty dirtyEvent;
    ChunkLoadedEvent chunkLoadedEvent;
    ChunkUnloadedEvent chunkUnloadedEvent;
    EntityAddEvent entityAddEvent;
    EntityRemoveEvent entityRemoveEvent;
    TileAddEvent tileAddEvent;
    TileRemoveEvent tileRemoveEvent;
    TileUpdateEvent tileUpdateEvent;

    // Chunks
    Utilities::Octree<Chunk, OctreeMaxDepth> chunks;
    // Map of 0xXXXXYYYY packed chunk coordinates to the columns
    std::unordered_map<uint32_t, std::shared_ptr<Column>> chunkCoordToColumn;
    uint16_t maxHeightChunks;
    uint16_t maxChunkCoord;
    ChunkCoordRes playerChunkLoadRange;

    // Entities
    std::unordered_map<EntityID, std::unique_ptr<Entity>> entities;
    EntityID nextEntityId = 0;

    // Special
    const Tile voidTile;
    const FluidTile voidFluid;
    const Subtile voidSubtile;
};

template <typename T, typename... Args>
T *Dimension::addEntity(const Position &position, Args&... args) {
    std::unique_ptr<Entity> entity = std::make_unique<T>(args...);
    T *entityPtr = static_cast<T*>(entity.get());

    addEntity(std::move(entity), position);

    return entityPtr;
}

}