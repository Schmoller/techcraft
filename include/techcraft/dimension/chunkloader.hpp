#pragma once

#include "techcraft/forward.hpp"
#include "techcraft/common.hpp"
#include "techcraft/generation/base.hpp"
#include "utilities/event.hpp"
#include "utilities/multithreading.hpp"
#include "utilities/workers.hpp"
#include "tickets.hpp"

#include <unordered_map>
#include <optional>
#include <functional>
#include <unordered_set>
#include <boost/pool/pool_alloc.hpp>

namespace Techcraft {

/**
 * Handles loading (also generation) and unloading of chunks within a single dimension
 */
class ChunkLoader {
    struct ChunkLoadState {
        ChunkPosition coord;
        std::vector<LoadTicket> tickets;
        std::optional<std::chrono::time_point<std::chrono::system_clock>> unloadIfEmptyAt;
    };

    public:
    ChunkLoader(Dimension &, const Tile &voidTile, std::shared_ptr<Generation::ChunkGenerator> chunkGenerator, Utilities::WorkerFleet &workers);
    ~ChunkLoader();

    /**
     * Adds a request to have a chunk loaded.
     * 
     * @param coord The coordinates of the chunk to load
     * @param cause The cause of the request
     */
    void addRequest(const ChunkPosition &coord, const ChunkLoadCause &cause);
    /**
     * Adds a request to have a chunk loaded.
     * 
     * @param coord The coordinates of the chunk to load
     * @param cause The cause of the request
     * @param agent The agent behind the request
     */
    void addRequest(const ChunkPosition &coord, const ChunkLoadCause &cause, AgentType agent);

    /**
     * Removes a load request.
     * If there are no other requests for this chunk, then it will be unloaded at some point.
     * 
     * @param coord The coordinates of the chunk to unload
     * @param cause The cause being rescinded.
     */
    void removeRequest(const ChunkPosition &coord, const ChunkLoadCause &cause);
    /**
     * Removes a load request.
     * If there are no other requests for this chunk, then it will be unloaded at some point.
     * 
     * @param coord The coordinates of the chunk to unload
     * @param cause The cause being rescinded.
     * @param agent The agent behind the request
     */
    void removeRequest(const ChunkPosition &coord, const ChunkLoadCause &cause, AgentType agent);

    /**
     * Removes all requests by the given agent.
     * Chunks will only be unloaded if there are no other requests for that chunk.
     *
     * @param agent The agent behind the requests.
     */
    void removeAllFromAgent(AgentType agent);

    /**
     * Removes all requests by the given agent which have the given cause.
     * 
     * @param agent The agent behind the requests.
     * @param cause The cause to match
     */
    void removeAllFromAgent(AgentType agent, ChunkLoadCause cause);

    void update();

    private:
    // Thread only methods
    std::shared_ptr<Chunk> allocateChunk(const ChunkPosition &coord);
    void loadOrGenerateChunk(const ChunkPosition &coord);

    void unloadChunk(const ChunkPosition &coord, const std::shared_ptr<Chunk> &chunk);

    void queueChunkLoad(const ChunkPosition &coord);
    void queueChunkUnload(const ChunkPosition &coord, const std::shared_ptr<Chunk> &chunk);

    void processChunkQueue();

    void processUnloadRequests();

    // Provided
    Dimension &dimension;
    // FIXME: This reference causes a segfault in new Chunk() on exit if there are chunks being generated on quit.
    // This is because the chunkloader is asynchronous. The dimension (which owns this) is gone.
    const Tile &voidTile;
    std::shared_ptr<Generation::ChunkGenerator> chunkGenerator;
    Utilities::WorkerFleet &workers;

    std::chrono::seconds unloadGracePeriod { 5 };

    // Tickets
    std::unordered_map<ChunkPosition, std::shared_ptr<ChunkLoadState>> chunkTickets;
    std::unordered_set<LoadTicket> activeTickets;

    std::deque<ChunkPosition> chunksToLoad;
    std::deque<std::shared_ptr<ChunkLoadState>> chunksToCheck;

    boost::pool_allocator<ChunkLoadState> chunkStateAllocator;

    // Processing
    Utilities::SharedVector<std::pair<ChunkPosition, std::shared_ptr<Chunk>>> loadedChunks;
};

}