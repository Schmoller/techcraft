#pragma once

#include <random>
#include <vector>
#include <array>

namespace Techcraft {

class Random {
    public:
    Random(uint64_t seed);

    uint64_t randRange(uint64_t min, uint64_t max);
    float randFloat();

    template <typename T>
    inline const T &randIn(const std::vector<T> &values) {
        size_t index = randRange(0, values.size());
        return values[index];
    }

    template <typename T, int _S>
    inline const T &randIn(const std::array<T, _S> &values) {
        size_t index = randRange(0, values.size());
        return values[index];
    }

    template <typename T, int _S>
    inline const T &randIn(const T values[_S]) {
        size_t index = randRange(0, _S);
        return values[index];
    }

    template <typename T>
    inline const T &randIn(const T *values, size_t size) {
        size_t index = randRange(0, size);
        return values[index];
    }

    inline bool randChance(uint64_t size) {
        auto value = internalRand();
        return (value % size) == 0;
    }

    inline bool randChance(uint64_t chance, uint64_t inSize) {
        auto value = internalRand();
        return (value % inSize) < chance;
    }

    private:
    std::mt19937 internalRand;
};

}