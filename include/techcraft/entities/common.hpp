#pragma once

// Forward declarations
namespace Techcraft {
    class EntityType;
    class Entity;
    class Rotatable;
    class PhysicsObject;
}

#include "techcraft/common.hpp"
#include "techcraft/dimension/tickets.hpp"

#include <string>

namespace Techcraft {
typedef uint32_t EntityID;

// Prototype in lieu of include
class Dimension;

/**
 * An empty entity which can be built upon to add functionality
 */
class Entity {
    friend class Dimension;

    public:
    Entity();
    virtual ~Entity() = default;
    virtual const EntityType &getType() const = 0;

    /**
     * Called every dimension tick, while the entity is loaded
     */
    virtual void onTick() {}

    inline EntityID getID() {
        return id;
    }

    inline Dimension *getDimension() const {
        return dimension;
    }

    const Position &getPosition() const;
    virtual void setPosition(const Position &position);
    inline void setPosition(tcfloat x, tcfloat y, tcfloat z) {
        setPosition({x, y, z});
    }

    const BoundingBox &getBounds() const;
    void setBounds(const BoundingBox &bounds);

    protected:
    void takeOwnership(Dimension *dimension, EntityID id);

    private:
    Position position;
    BoundingBox bounds;

    EntityID id;
    Dimension *dimension;
};

class EntityType {
    public:

    private:
    std::string id;
};

// Generic behaviours
class Rotatable {
    public:
    Rotatable();
    
    void setYaw(float yaw);
    float getYaw() const;

    void setPitch(float pitch);
    float getPitch() const;

    glm::vec3 getForward() const;

    private:
    float yaw;
    float pitch;
};

class PhysicsObject {
    friend class PhysicsSimulation;
    public:
    PhysicsObject() = default;
    virtual ~PhysicsObject() = default;

    // Configuration
    inline bool isPhysicsEnabled() const {
        return enablePhysics;
    }

    inline void setPhysicsEnable(bool enable) {
        enablePhysics = enable;
    }

    inline bool isGravityEnabled() const {
        return enableGravity;
    }

    inline void setGravityEnable(bool enable) {
        enableGravity = enable;
    }

    inline bool isCollidable() const {
        return enableCollision;
    }

    inline void setCollidable(bool enable) {
        enableCollision = enable;
    }

    inline double getStepUp() const {
        return stepUp;
    }

    inline void setStepUp(double stepUp) {
        this->stepUp = std::max(0.0, stepUp);
    }

    inline bool getPreventDropoff() const {
        return preventDropoff;
    }

    inline void setPreventDropoff(bool prevent) { 
        preventDropoff = prevent;
    }

    inline double getGravityInfuence() const {
        return gravityInfluence;
    }

    inline void setGravityInfuence(double infuence) {
        gravityInfluence = infuence;
    }

    // Current state
    inline bool isOnGround() const {
        return standing;
    }

    inline bool isInFluid() const {
        return inFluid;
    }

    inline void setVelocity(Position velocity) {
        this->velocity = velocity;
    }

    inline const Position &getVelocity() const {
        return velocity;
    }

    virtual glm::highp_dvec3 getDrag() const {
        return {};
    }

    private:
    // Configuration
    bool enablePhysics { true };
    bool enableGravity { true };
    bool enableCollision { false };
    bool preventDropoff { false };
    double stepUp {0};
    double gravityInfluence {1};

    // State
    Position velocity {};
    bool standing;
    bool inFluid;
};

/**
 * A behaviour that causes chunks to be loaded around it
 * automatically.
 */
class ChunkTicketing {
    public:
    virtual AgentType getTicketAgentId() = 0;
    virtual ChunkLoadCause getTicketReason() = 0;
};

}