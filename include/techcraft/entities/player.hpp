#pragma once

#include "techcraft/common/raycast.hpp"
#include "techcraft/entities/common.hpp"
#include "techcraft/inventory/player.hpp"
#include "techcraft/keybinding.hpp"

#include "techcraft/commands/sender.hpp"

#include "engine/camera.hpp"
#include "engine/inputmanager.hpp"

namespace Techcraft {

/**
 * Entity which represents the local player.
 * A local player is client side and is controllable directly by 
 * the input scheme of the client.
 */
class LocalPlayer : public Entity, public Rotatable, public PhysicsObject, public ChunkTicketing, public Commands::PlayerCommandSender {
    public:
    class LocalPlayerType: public EntityType {
        public:
        std::string id() {
            return "player.local";
        }
    };
    static const LocalPlayerType Type;

    LocalPlayer(BindingManager &manager, Engine::InputManager &input);

    const EntityType &getType() const;

    void setPosition(const Position &position);
    void setCrouching(bool crouch);
    bool isCrouching() const;
    void setFlying(bool flying);
    bool isFlying() const { return flying; }

    void processInputs();

    Position getEyePosition() const;

    RaycastResult getLookAtTile() const;

    // Configuration
    float getSensitiviy() const { return confSensitiviy; }
    double getSpeed() const { return confSpeed; }
    double getSwimSpeed() const { return confSwimSpeed; }
    double getFlySpeed() const { return confFlySpeed; }
    double getFluidCrouchSpeed() const { return confFluidCrouchSpeed; }
    double getCrouchSpeed() const { return confCrouchSpeed; }
    float getReach() const { return confReach; }

    // ChunkTicketing members
    AgentType getTicketAgentId();
    ChunkLoadCause getTicketReason();

    PlayerInventory &getInventory();

    // PhysicsObject members
    glm::highp_dvec3 getDrag() const override;

    // PlayerCommandSender members
    void printLine(const std::wstring_view &line) override;

    private:

    std::shared_ptr<KeyBinding> bindMoveForward;
    std::shared_ptr<KeyBinding> bindMoveBackward;
    std::shared_ptr<KeyBinding> bindStrafeLeft;
    std::shared_ptr<KeyBinding> bindStrafeRight;

    std::shared_ptr<KeyBinding> bindCrouch;
    std::shared_ptr<KeyBinding> bindJump;

    std::shared_ptr<KeyBinding> bindSwitchSlot0;
    std::shared_ptr<KeyBinding> bindSwitchSlot1;
    std::shared_ptr<KeyBinding> bindSwitchSlot2;
    std::shared_ptr<KeyBinding> bindSwitchSlot3;
    std::shared_ptr<KeyBinding> bindSwitchSlot4;
    std::shared_ptr<KeyBinding> bindSwitchSlot5;
    std::shared_ptr<KeyBinding> bindSwitchSlot6;
    std::shared_ptr<KeyBinding> bindSwitchSlot7;
    std::shared_ptr<KeyBinding> bindSwitchSlot8;
    std::shared_ptr<KeyBinding> bindSwitchSlot9;
    std::shared_ptr<KeyBinding> bindOpenBackpack;

    std::shared_ptr<KeyBinding> bindBreakTile;
    std::shared_ptr<KeyBinding> bindPlaceTile;
    
    Engine::InputManager &input;
    PlayerInventory inventory;
    bool hasInventoryOpen { false };

    // Configuration
    float confSensitiviy = 0.1f;
    double confSpeed = 5.2f;
    double confSwimSpeed = 2.7f;
    double confFluidCrouchSpeed = 1.6f;
    double confCrouchSpeed = 3.1f;
    double confFlySpeed = 20.0f;
    float confReach = 4.5f;

    // State
    bool crouching { false };
    bool hasMovementInput { false };
    bool flying { false };
};

}
