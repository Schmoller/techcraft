#pragma once

#include "techcraft/commands/sender.hpp"
#include "techcraft/commands/executor.hpp"

#include <string>
#include <vector>
#include <deque>


namespace Techcraft {

class Console {
    public:
    Console(Commands::Executor &executor);

    bool wasModifiedSince();

    uint32_t getMaxHistoryLines() const { return maxHistoryLines; }
    void setMaxHistoryLines(uint32_t maxLines);

    const std::deque<std::wstring> &getHistory() const { return commandHistory; }
    void clearHistory();

    uint32_t getCursorPos() const { return cursorPos; }
    void setCursorPos(uint32_t pos);

    const std::wstring &getCommandLine() const { return commandHistory[currentHistoryLine]; }
    void setCommandLine(const std::wstring &commandLine);

    const std::vector<std::wstring> &getSuggestions() const { return suggestions; };
    void setSuggestions(const std::vector<std::wstring> &suggestions);

    const std::deque<std::wstring> &getScrollback() const { return scrollback; }
    uint32_t getMaxScrollback() const { return maxScrollback; }
    void setMaxScrollback(uint32_t maxScrollback);

    void type(wchar_t ch);
    void backspace(bool word);
    void del(bool word);
    void submit(Commands::CommandSender &sender);
    void cursorLeft();
    void cursorRight();
    void tabComplete();

    void historyPrevious();
    void historyNext();

    void print(const std::wstring &string);

    private:
    inline std::wstring &commandLine() { return commandHistory[currentHistoryLine]; }

    Commands::Executor &executor;
    
    uint32_t cursorPos { 0 };

    // History is the command lines that were entered
    uint32_t maxHistoryLines { 100 };
    std::deque<std::wstring> commandHistory;
    int32_t currentHistoryLine { 0 };
    bool isLookingAtHistory { false };

    // Scrollback is the output from executed commands
    uint32_t maxScrollback { 9999 };
    std::deque<std::wstring> scrollback;

    std::vector<std::wstring> suggestions;

    bool isModified { false };
};

}