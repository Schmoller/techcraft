#pragma once

#include "techcraft/game.hpp"

#include <memory>

namespace Techcraft {

class Debug {
    public:
    static void initialize(Game *game);
    static Debug *getInstance() { return instance.get(); }

    void doUpdate();

    // Configuration
    void setShowChunkBorders(bool show) { showChunkBorders = show; }
    bool getShowChunkBorders() const { return showChunkBorders; }

    void setShowDebugUI(bool show) { showDebugUI = show; }
    bool getShowDebugUI() const { return showDebugUI; }

    private:
    static std::unique_ptr<Debug> instance;
    Debug(Game *game);

    Game *game;

    bool showChunkBorders { true };
    bool showDebugUI { true };
};

}