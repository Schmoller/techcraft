#pragma once

#include <string>

namespace Techcraft::Default {

/**
 * Default keybinding names
 */
class KeyBindings {
    public:

    static constexpr std::string_view MOVE_FORWARD = "Move Forward";
    static constexpr std::string_view MOVE_BACKWARD = "Move Backward";
    static constexpr std::string_view STRAFE_LEFT = "Strafe Left";
    static constexpr std::string_view STRAFE_RIGHT = "Strafe Right";
    static constexpr std::string_view CROUCH = "Crouch";
    static constexpr std::string_view JUMP = "Jump";
    static constexpr std::string_view SWITCH_SLOT_0 = "Switch Slot 0";
    static constexpr std::string_view SWITCH_SLOT_1 = "Switch Slot 1";
    static constexpr std::string_view SWITCH_SLOT_2 = "Switch Slot 2";
    static constexpr std::string_view SWITCH_SLOT_3 = "Switch Slot 3";
    static constexpr std::string_view SWITCH_SLOT_4 = "Switch Slot 4";
    static constexpr std::string_view SWITCH_SLOT_5 = "Switch Slot 5";
    static constexpr std::string_view SWITCH_SLOT_6 = "Switch Slot 6";
    static constexpr std::string_view SWITCH_SLOT_7 = "Switch Slot 7";
    static constexpr std::string_view SWITCH_SLOT_8 = "Switch Slot 8";
    static constexpr std::string_view SWITCH_SLOT_9 = "Switch Slot 9";
    static constexpr std::string_view OPEN_BACKPACK = "Open Backpack";
    static constexpr std::string_view BREAK_TILE = "Break Tile";
    static constexpr std::string_view PLACE_TILE = "Place Tile";
    static constexpr std::string_view OPEN_CONSOLE = "Open Console";
};

}