#pragma once

#include "techcraft/tiles/base.hpp"

namespace Techcraft {

class ItemTubeTileType: public TileType {
    public:
    ItemTubeTileType();

    std::optional<BoundingBox> getBounds() const override;
    std::optional<BoundingBox> getBoundsForPlacement(RaycastResult hit, const Dimension &dimension) const override;

    bool doPlaceBlock(const TilePosition &pos, RaycastResult hit, Dimension &dimension) const override;
    std::shared_ptr<TileEntity> createTileEntity(const TilePosition &pos) const override;
    Rendering::TileRenderer &getRenderer() const override;

    bool isFluidPassable() const override;

    protected:
    void initialize(ResourceManager &manager) override;
};

}