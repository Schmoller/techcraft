#pragma once

#include "techcraft/common.hpp"
#include "techcraft/tileentities/common.hpp"
#include "techcraft/subtiles/container.hpp"
#include "techcraft/resources/manager.hpp"
#include "techcraft/resources/tile-texture.hpp"

#include "techcraft/settings.hpp"

#include <memory>
#include <functional>
#include <variant>

namespace Techcraft {

// In lieu of including
class Dimension;
class RaycastResult;
class Fluid;

namespace Rendering {
    class TileRenderer;
}

/**
 * The representation of the type for a tile.
 */
class TileType {
    friend class TileRegistryBuilder;
    friend std::hash<TileType>;

    public:
    TileType(const std::string &id);

    const std::string &id() const;

    bool operator==(const TileType &other) const;
    bool operator!=(const TileType &other) const;

    inline const TileTexture *textureOf(TileDirection direction) const {
        return faceTextures[static_cast<int>(direction)];
    }

    inline uint8_t getLight(TileLightPallet pallet) const {
        return tileLight[static_cast<int>(pallet)];
    }

    inline const uint8_t *getLightAll() const {
        return tileLight;
    }

    inline bool isTransparent() const {
        return transparent;
    }

    inline bool hasLight() const {
        return emitsLight;
    }

    virtual std::optional<BoundingBox> getBounds() const;
    
    /**
     * Gets a bounding box for use with placement tests.
     * This bounding box should not be offset by the position.
     */
    virtual std::optional<BoundingBox> getBoundsForPlacement(RaycastResult hit, const Dimension &dimension) const;
    virtual bool doPlaceBlock(const TilePosition &pos, RaycastResult hit, Dimension &dimension) const;

    /**
     * For types that have an entity component, override this to construct the 
     * appropriate tile entity.
     * By default, no tile entity is created.
     */
    virtual std::shared_ptr<TileEntity> createTileEntity(const TilePosition &pos) const;

    /**
     * Gets the renderer that is used to render this tile
     */
    virtual Rendering::TileRenderer &getRenderer() const;

    /**
     * Empty tiles act like air
     */
    virtual bool isEmpty() const;

    /**
     * Empty tiles act like air
     */
    virtual bool isEmpty(const Tile &tile) const { return isEmpty(); };

    /**
     * When true, fluid can pass through it, destroying this as it goes
     */
    virtual bool isFluidReplaceable() const;

    /**
     *  When true, this tile type can have fluid pass through it.
     */
    virtual bool isFluidPassable() const;

    /**
     * Called when the tile is to be replaced as a result of fluid passing through
     */
    virtual void doFluidReplace(const TilePosition &pos, Dimension &dimension, const Fluid &fluid) const;

    /**
     * Attempts to coerce the tile into a subtile-containing tile.
     * NOTE: This is never called for TileEntities. A TileEntity must inherit from SubtileContainer if it
     * wants to use subtiles.
     */
    virtual std::shared_ptr<SubtileContainer> coerseIntoSubtileContainer(const TilePosition &pos, const Tile &tile) const;

    protected:
    virtual void initialize(ResourceManager &manager) = 0;
    void setTextureAll(const TileTexture *texture);
    void setTexture(const TileTexture *texture, TileDirection direction);
    void setLightEmission(TileLightPallet pallet, uint8_t intensity);
    void setTransparent() {
        transparent = true;
    }

    private:
    std::string name;
    uint32_t slotId;
    const TileTexture *faceTextures[6];
    uint8_t tileLight[TILE_LIGHT_PALLET_COUNT];
    bool emitsLight;
    bool transparent;
};

/**
 * Represents an instance of a tile in the world
 */
class Tile {
    public:
    const TileType *type;

    std::variant<std::shared_ptr<TileEntity>,std::shared_ptr<SubtileContainer>> data;

    std::shared_ptr<TileEntity> getTileEntity() const;
    std::shared_ptr<SubtileContainer> getSubtileContainer() const;

    std::optional<BoundingBox> getBounds() const;
    bool isSolidOnSide(TileDirection face) const;
    bool isEmpty() const;
};

/**
 * Represents the lighting information for a tile in the world
 */
struct TileLighting {
    // These intensity bytes are layed out like so: 0bAAAAABBB
    // Where A = intensity value and B = source direction (TileDirection)
    uint8_t tileIntensity[TILE_LIGHT_PALLET_COUNT];
    uint8_t skyIntensity;
};
}

namespace std {
    template<> struct hash<Techcraft::TileType> {
        std::size_t operator()(Techcraft::TileType const& type) const noexcept {
            return std::hash<uint32_t>()(type.slotId);
        }
    };
}