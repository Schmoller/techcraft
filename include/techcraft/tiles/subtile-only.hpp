#pragma once

#include "techcraft/tiles/base.hpp"

namespace Techcraft {

class SubtileOnlyTileType: public TileType {
    public:
    SubtileOnlyTileType(const std::string &id);

    bool doPlaceBlock(const TilePosition &pos, RaycastResult hit, Dimension &dimension) const override;

    Rendering::TileRenderer &getRenderer() const override;

    bool isEmpty(const Tile &tile) const override;

    protected:
    void initialize(ResourceManager &manager) override;
};

}