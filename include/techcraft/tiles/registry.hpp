#pragma once

#include "base.hpp"
#include "utilities/registry.hpp"
#include "techcraft/resources/manager.hpp"
#include "techcraft/resources/tile-texture.hpp"
#include "techcraft/resources/resource.hpp"

#include <memory>
#include <optional>
#include <string>
#include <unordered_map>

namespace Techcraft {

/**
 * Used for making new tile types
 */
class TileTypeDefinition {
    friend class TileRegistryBuilder;

    public:
    TileTypeDefinition(const std::string &name);

    void setTextureAll(const Resource &resource);
    void setTexture(const Resource &resource, TileDirection direction);
    void setLightEmission(TileLightPallet pallet, uint8_t intensity);
    void setTransparent() {
        transparent = true;
    }
    void setEmpty() {
        empty = true;
    }
    void setRenderer(Rendering::TileRenderer &renderer) {
        this->renderer = &renderer;
    }

    private:
    std::string name;
    Resource faceTextures[6];
    uint8_t tileLight[TILE_LIGHT_PALLET_COUNT];
    bool transparent;
    bool empty {false};
    Rendering::TileRenderer *renderer { nullptr };
};

class BasicTileType : public TileType {
    public:
    BasicTileType(
        const std::string &id,
        const TileTexture *faceTextures[6],
        const uint8_t tileLight[TILE_LIGHT_PALLET_COUNT],
        bool transparent,
        Rendering::TileRenderer &renderer,
        bool empty = false
    );

    std::optional<BoundingBox> getBounds() const override;
    Rendering::TileRenderer &getRenderer() const override { return renderer; }

    bool isEmpty() const override;

    protected:
    void initialize(ResourceManager &manager) override;

    private:
    std::optional<BoundingBox> bounds;
    Rendering::TileRenderer &renderer;
    bool empty {false};
};

/**
 * Allows the lookup of types of tiles
 */
class TileRegistry: public Utilities::Registry<TileType> {
    friend class TileRegistryBuilder;

    public:
    /**
     * Helper to get the built in air type quickly
     */
    const TileType *air() const;
    private:
    TileRegistry(std::unordered_map<std::string, std::shared_ptr<TileType>>, std::vector<std::shared_ptr<TileType>>);
};

/**
 * Used to create tile registries
 */
class TileRegistryBuilder: public Utilities::RegistryBuilder<TileType, TileRegistry> {
    public:
    TileRegistryBuilder(ResourceManager &manager);

    const TileType *add(const TileTypeDefinition &definition);
    virtual std::unique_ptr<TileRegistry> build() override;

    using Utilities::RegistryBuilder<TileType, TileRegistry>::add;
    protected:
    virtual void initializeItem(TileType *item, size_t slotId) override;

    private:
    ResourceManager &manager;
};


class TileTypes {
    public:

    static inline const TileRegistry *registry() { return tileRegistry.get(); }

    static void initialize(ResourceManager &manager);

    // Basic tiles
    static const TileType *Dirt;
    static const TileType *Stone;
    static const TileType *Grass;
    static const TileType *Log1;
    static const TileType *Leaves1;
    static const TileType *Log2;
    static const TileType *Leaves2;

    // Tier 1
    static const TileType *Tube;
    static const TileType *Barrel;

    // Slabs
    static const TileType *StoneSlab;
    static const TileType *DirtSlab;

    // Internal blocks. Not player usable
    static const TileType *InternalSubtileContainer;
    static const TileType *Air;

    // Debug blocks
    static const TileType *Reference;
    static const TileType *Reference2;
    static const TileType *ConnectedTest;

    private:
    static std::unique_ptr<TileRegistry> tileRegistry;
};

}