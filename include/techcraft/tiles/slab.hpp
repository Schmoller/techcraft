#pragma once

#include "techcraft/tiles/base.hpp"

// Other parts
#include "techcraft/rendering/tile/slab.hpp"
#include "techcraft/tileentities/slab.hpp"

namespace Techcraft {

/**
 * Slab tiles occupy half of a tile in any direction
 */
class SlabTileType : public TileType {
    public:
    SlabTileType(const std::string &id, const TileType *fullTile);

    std::optional<BoundingBox> getBounds() const;
    std::optional<BoundingBox> getBoundsForPlacement(RaycastResult hit, const Dimension &dimension) const override;
    bool doPlaceBlock(const TilePosition &pos, RaycastResult hit, Dimension &dimension) const override;

    std::shared_ptr<TileEntity> createTileEntity(const TilePosition &pos) const override;

    Rendering::TileRenderer &getRenderer() const override;

    protected:
    void initialize(ResourceManager &manager) override;

    private:
    const TileType *fullTile;
};

}