#pragma once

#include "techcraft/tiles/base.hpp"

namespace Techcraft {

class BarrelTileType: public TileType {
    public:
    BarrelTileType();

    std::optional<BoundingBox> getBounds() const override;
    bool doPlaceBlock(const TilePosition &pos, RaycastResult hit, Dimension &dimension) const override;
    std::shared_ptr<TileEntity> createTileEntity(const TilePosition &pos) const override;
    Rendering::TileRenderer &getRenderer() const override;

    protected:
    void initialize(ResourceManager &manager) override;
};

}