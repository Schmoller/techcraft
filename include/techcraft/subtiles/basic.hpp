#pragma once

#include "base.hpp"

namespace Techcraft {

/**
 * A basic subtile
 */
class BasicSubtileType: public SubtileType {
    public:
    BasicSubtileType(
        const std::string &id,
        const std::vector<const SubtileSlot *> &slots,
        const TileTexture *faceTextures[6],
        bool transparent,
        std::optional<BoundingBox> bounds = {},
        bool empty = false
    );

    // override SubtileType
    bool isEmpty() const override { return empty; }
    std::optional<BoundingBox> getBounds(const SubtileSlot &slot) const override { return bounds; }

    protected:
    void initialize(ResourceManager &manager) override {};

    private:
    std::optional<BoundingBox> bounds;
    bool empty {false};
};


}