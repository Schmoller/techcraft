#pragma once

#include "base.hpp"

#include "techcraft/resources/resource.hpp"
#include "techcraft/common.hpp"
#include "utilities/registry.hpp"

namespace Techcraft {

/**
 * Used for making new tile types
 */
class SubtileTypeDefinition {
    friend class SubtileRegistryBuilder;

    public:
    SubtileTypeDefinition(const std::string &name);

    void setTextureAll(const Resource &resource);
    void setTexture(const Resource &resource, TileDirection direction);
    void setTransparent() {
        transparent = true;
    }
    void setEmpty() {
        empty = true;
    }
    void setBoundingBox(const std::optional<BoundingBox> &bounds) {
        this->bounds = bounds;
    }
    void setSlots(const std::vector<const SubtileSlot *> &slots) {
        this->slots = slots;
    }
    template <typename... Slots>
    void setSlots(const Slots&... slots) {
        std::vector<const SubtileSlot *> slotVector = {{&slots...}};
        setSlots(slotVector);
    }

    private:
    std::string name {};
    Resource faceTextures[6] {};
    bool transparent { false };
    bool empty { false };
    std::optional<BoundingBox> bounds {};
    std::vector<const SubtileSlot *> slots {};
};

/**
 * Registry of all availble subtile types.
 */
class SubtileRegistry: public Utilities::Registry<SubtileType> {
    friend class SubtileRegistryBuilder;

    public:

    private:
    SubtileRegistry(std::unordered_map<std::string, std::shared_ptr<SubtileType>>, std::vector<std::shared_ptr<SubtileType>>);
};

class SubtileRegistryBuilder: public Utilities::RegistryBuilder<SubtileType, SubtileRegistry> {
    public:
    SubtileRegistryBuilder(ResourceManager &manager);

    const SubtileType *add(const SubtileTypeDefinition &definition);
    virtual std::unique_ptr<SubtileRegistry> build() override;

    using Utilities::RegistryBuilder<SubtileType, SubtileRegistry>::add;
    protected:
    virtual void initializeItem(SubtileType *item, size_t slotId) override;

    private:
    ResourceManager &manager;
};

class SubtileTypes {
    public:
    static inline const SubtileRegistry *registry() { return subtileRegistry.get(); }

    static void initialize(ResourceManager &manager);

    private:
    static std::unique_ptr<SubtileRegistry> subtileRegistry;
};

}