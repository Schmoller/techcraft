#pragma once

#include "slot.hpp"

#include <iostream>

namespace Techcraft {

inline std::ostream &operator<<(std::ostream &os, const SubtileSlot &slot) {
    return os << slot.name();
}

}