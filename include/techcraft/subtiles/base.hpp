#pragma once

#include "slot.hpp"

#include "techcraft/common.hpp"
#include "techcraft/resources/manager.hpp"
#include "techcraft/resources/tile-texture.hpp"

namespace Techcraft {

// Forward declaration
namespace Rendering {
    class SubtileRenderer;
}

/**
 * Similar to a TileType, but only for subtiles
 */
class SubtileType {
    friend class SubtileRegistryBuilder;

    public:
    SubtileType(const std::string_view &id);

    const std::string &id() const { return name; };

    bool operator==(const SubtileType &other) const;
    bool operator!=(const SubtileType &other) const;

    /**
     * The slots this subtile can be placed into
     */
    const std::vector<const SubtileSlot *> &getSlots() const { return slots; }

    /**
     * Retrieves the textures for the given face
     */
    inline const TileTexture *textureOf(TileDirection direction) const {
        return faceTextures[static_cast<int>(direction)];
    }

    /**
     * Is the subtile transparent for lighting
     */
    inline bool isTransparent() const {
        return transparent;
    }

    /**
     * Empty tiles act like air
     */
    virtual bool isEmpty() const {
        return transparent;
    }

    /**
     * Retreives the bounding box (non-located) for this subtile
     */
    virtual std::optional<BoundingBox> getBounds(const SubtileSlot &slot) const { return {}; }

    /**
     * Retrieves the renderer that can display this subtile.
     * Subtiles may either use a static renderer or dynamic renderer.
     * 
     * Rendering is not invoked on any subtile that isEmpty() returns true on.
     */
    virtual Rendering::SubtileRenderer &getRenderer() const;

    protected:
    virtual void initialize(ResourceManager &manager) = 0;
    void setTextureAll(const TileTexture *texture);
    void setTexture(const TileTexture *texture, TileDirection direction);
    void setTransparent() {
        transparent = true;
    }
    void setSlots(const std::vector<const SubtileSlot *> &slots);
    template <typename... Slots>
    void setSlots(const Slots&... slots) {
        std::vector<const SubtileSlot *> slotVector = {{&slots...}};
        setSlots(slotVector);
    }

    private:
    std::string name;

    const TileTexture *faceTextures[6] {};
    bool transparent { false };
    std::vector<const SubtileSlot *> slots {};
};

class Subtile {
    public:
    const SubtileType *type { nullptr };

    // std::shared_ptr<SubtileEntity> entity;
};

}