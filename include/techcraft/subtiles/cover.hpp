#pragma once

#include "base.hpp"

#include "techcraft/tiles/base.hpp"

namespace Techcraft {

enum class CoverSize {
    VeryThin,
    Thin,
    Slab,
    Thick,
    VeryThick
};

class CoverSubtileType: public SubtileType {
    public:
    CoverSubtileType(const std::string_view &name, const TileType &tileType, CoverSize size);

    CoverSize getSize() const { return size; }
    const TileType &getTileType() const { return tileType; }

    /**
     * Retreives the bounding box (non-located) for this subtile
     */
    std::optional<BoundingBox> getBounds(const SubtileSlot &slot) const override;

    protected:
    void initialize(ResourceManager &manager);

    private:
    const TileType &tileType;
    const CoverSize size;
};

}