#pragma once

#include "slot.hpp"
#include "base.hpp"

#include <vector>
#include <array>
#include <functional>

#include <iostream>

namespace Techcraft {

/**
 * A subtile container holds the subtile data. Both for plain subtiles (like plain tiles with no tile entity)
 * and tile entity based subtiles.
 * 
 * This should not be used directly, but through a Dimension.
 */
class SubtileContainer {
    public:
    virtual ~SubtileContainer() = default;

    /**
     * Sets the subtile in a given slot.
     * NOTE: This should not be used directly, as it will not perform any needed updates
     */
    virtual void setSubtile(const SubtileSlot &slot, const Subtile &subtile) = 0;

    /**
     * Gets the subtile in a given slot
     */
    virtual const Subtile &getSubtile(const SubtileSlot &slot) const = 0;

    /**
     * Checks if the given slot exists
     */
    virtual bool hasSlot(const SubtileSlot &slot) const = 0;

    /**
     * Checks if the tile has a solid face on the given side
     */
    virtual bool isSolidOn(TileDirection face) const = 0;

    /**
     * Executes callback for each subtile
     */
    virtual void forEach(const std::function<void(const SubtileSlot &slot, const Subtile &)> &callback) = 0;
}; 


const uint32_t NoSlot = 0xFFFFFFFF;

/**
 * This is a template based implementation of the SubtileContainer.
 * This allows you to specify as the template arguments, the slots that are available.
 */
template <const auto&... Slots>
class SubtileContainerImpl: public SubtileContainer {
    public:
    SubtileContainerImpl() = default;
    virtual ~SubtileContainerImpl() = default;

    /**
     * Sets the subtile in a given slot.
     * NOTE: This should not be used directly, as it will not perform any needed updates
     */
    void setSubtile(const SubtileSlot &slot, const Subtile &subtile) {
        auto id = getSlotID(slot);
        assert(id != NoSlot);

        slots[id] = subtile;
    }

    /**
     * Gets the subtile in a given slot
     */
    const Subtile &getSubtile(const SubtileSlot &slot) const {
        auto id = getSlotID(slot);
        assert(id != NoSlot);

        return slots[id];
    }

    /**
     * Checks if the given slot exists
     */
    bool hasSlot(const SubtileSlot &slot) const {
        return (getSlotID(slot) != NoSlot);
    }

    /**
     * Gets all the slots available for use
     */
    constexpr std::array<const SubtileSlot *, sizeof...(Slots)> getSlots() const {
        return {&Slots...};
    };

    /**
     * Checks if the tile has a solid face on the given side
     */
    bool isSolidOn(TileDirection face) const {
        // TODO: This is temporary. Eventually we need some logic to handle this
        return false;
    }

    /**
     * Executes callback for each subtile
     */
    void forEach(const std::function<void(const SubtileSlot &slot, const Subtile &)> &callback) override {
        for (size_t i = 0; i < slots.size(); ++i) {
            callback(*getSlots()[i], slots[i]);
        }
    }

    protected:
    const uint32_t slotCount { sizeof...(Slots) };

    private:
    std::array<Subtile, sizeof...(Slots)> slots;

    constexpr uint32_t getSlotID(const SubtileSlot &slot) const {
        int index = 0;
        for (auto other : getSlots()) {
            if (other == &slot) {
                return index;
            }
            ++index;
        }

        return NoSlot;
    }
};

typedef SubtileContainerImpl<AllCornerSlots, AllEdgeSlots, AllFaceSlots, CenterSlot::Center> StandardSubtileContainer;

}
