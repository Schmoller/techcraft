#pragma once

#include "base.hpp"

#include "techcraft/tiles/base.hpp"

namespace Techcraft {

enum class CornerSize {
    VeryThin,
    Thin,
    Slab,
    Thick,
    VeryThick
};

class CornerSubtileType: public SubtileType {
    public:
    CornerSubtileType(const std::string_view &name, const TileType &tileType, CornerSize size);

    CornerSize getSize() const { return size; }
    const TileType &getTileType() const { return tileType; }

    /**
     * Retreives the bounding box (non-located) for this subtile
     */
    std::optional<BoundingBox> getBounds(const SubtileSlot &slot) const override;

    protected:
    void initialize(ResourceManager &manager);

    private:
    const TileType &tileType;
    const CornerSize size;
};

}