#pragma once

#include "techcraft/common.hpp"
#include "slot.hpp"

namespace Techcraft {

enum class SubtilePlaceHit {
    EdgeLeft,
    EdgeRight,
    EdgeTop,
    EdgeBottom,
    CornerTopLeft,
    CornerTopRight,
    CornerBottomLeft,
    CornerBottomRight,
    Center,
    CenterFace,
    None
};

/**
 * Determines which placement marker was hit
 */
SubtilePlaceHit computeHitLocation(
    double hitX,
    double hitY,
    bool hasCenterSlot,
    bool hasCornerSlot,
    bool hasEdgeSlot
);

/**
 * Draws a placement hint for placing subtiles
 */
void drawPlacementHint(
    TileDirection face,
    const BoundingBox &onBounds,
    bool allowCenter,
    bool allowEdge,
    bool allowCorner
);

/**
 * Determines which slot the placement hit should go to
 */
const SubtileSlot *computeSlotFromHit(
    SubtilePlaceHit hit,
    TileDirection hitFace,
    bool adjacent,
    bool allowCenter,
    bool allowEdge,
    bool allowCorner,
    bool allowFace
);

const SubtileSlot *computeEdgeSlotFromHit(SubtilePlaceHit hit, TileDirection hitFace, bool adjacent);
const SubtileSlot *computeCornerSlotFromHit(SubtilePlaceHit hit, TileDirection hitFace, bool adjacent);
const SubtileSlot *computeFaceSlotFromHit(SubtilePlaceHit hit, TileDirection hitFace, bool adjacent);
}