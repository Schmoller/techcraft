#pragma once

#include "techcraft/common.hpp"

namespace Techcraft {

/**
 * A slot is a insertion point for a subtile.
 */
class SubtileSlot {
    public:
    bool operator==(const SubtileSlot &other) { return &other == this; }

    constexpr const char *name() const { return _name; }

    protected:
    constexpr SubtileSlot(const char *name)
        : _name(name)
    {}

    private:
    const char *_name;

};

/**
 * Represents a slot on one of the 8 corners of a tile
 */
class CornerSlot: public SubtileSlot {
    public:
    static CornerSlot NorthEastUp;
    static CornerSlot NorthEastDown;
    static CornerSlot NorthWestUp;
    static CornerSlot NorthWestDown;
    static CornerSlot SouthEastUp;
    static CornerSlot SouthEastDown;
    static CornerSlot SouthWestUp;
    static CornerSlot SouthWestDown;

    private:
    constexpr CornerSlot(const char *name)
        : SubtileSlot(name)
    {}

    CornerSlot(const CornerSlot &other) = delete;
    CornerSlot(CornerSlot &&other) = delete;
    void operator=(const CornerSlot &other) = delete;
};

bool isCornerSlot(const SubtileSlot *slot);

#define AllCornerSlots CornerSlot::NorthEastUp, CornerSlot::NorthEastDown, CornerSlot::NorthWestUp, CornerSlot::NorthWestDown, CornerSlot::SouthEastUp, CornerSlot::SouthEastDown, CornerSlot::SouthWestUp, CornerSlot::SouthWestDown

/**
 * Represents a slot on one of the 12 edges of a tile
 */
class EdgeSlot: public SubtileSlot {
    public:
    static EdgeSlot NorthDown;
    static EdgeSlot SouthDown;
    static EdgeSlot EastDown;
    static EdgeSlot WestDown;

    static EdgeSlot NorthUp;
    static EdgeSlot SouthUp;
    static EdgeSlot EastUp;
    static EdgeSlot WestUp;

    static EdgeSlot NorthEast;
    static EdgeSlot NorthWest;
    static EdgeSlot SouthEast;
    static EdgeSlot SouthWest;

    private:
    constexpr EdgeSlot(const char *name)
        : SubtileSlot(name)
    {}

    EdgeSlot(const EdgeSlot &other) = delete;
    EdgeSlot(EdgeSlot &&other) = delete;
    void operator=(const EdgeSlot &other) = delete;
};

bool isEdgeSlot(const SubtileSlot *slot);

#define AllEdgeSlots EdgeSlot::NorthDown, EdgeSlot::SouthDown, EdgeSlot::EastDown, EdgeSlot::WestDown, EdgeSlot::NorthUp, EdgeSlot::SouthUp, EdgeSlot::EastUp, EdgeSlot::WestUp, EdgeSlot::NorthEast, EdgeSlot::NorthWest, EdgeSlot::SouthEast, EdgeSlot::SouthWest

/**
 * Represents a slot on one of the 6 faces of a tile
 */
class FaceSlot: public SubtileSlot {
    public:
    static FaceSlot North;
    static FaceSlot South;
    static FaceSlot East;
    static FaceSlot West;
    static FaceSlot Up;
    static FaceSlot Down;

    private:
    constexpr FaceSlot(const char *name)
        : SubtileSlot(name)
    {}

    FaceSlot(const FaceSlot &other) = delete;
    FaceSlot(FaceSlot &&other) = delete;
    void operator=(const FaceSlot &other) = delete;
};

bool isFaceSlot(const SubtileSlot *slot);
const SubtileSlot &faceSlotFrom(TileDirection dir);

#define AllFaceSlots FaceSlot::North, FaceSlot::South, FaceSlot::East, FaceSlot::West, FaceSlot::Up, FaceSlot::Down

/**
 * Represents a slot in the center of the tile.
 */
class CenterSlot: public SubtileSlot {
    public:
    static CenterSlot Center;

    private:
    constexpr CenterSlot(const char *name)
        : SubtileSlot(name)
    {}

    CenterSlot(const CenterSlot &other) = delete;
    CenterSlot(CenterSlot &&other) = delete;
    void operator=(const CenterSlot &other) = delete;
};

bool isCenterSlot(const SubtileSlot *slot);
}