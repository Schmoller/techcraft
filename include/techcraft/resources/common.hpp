#pragma once

namespace Techcraft {

enum class TextureFlipMode {
    None,
    Vertical,
    Horizontal,
    Both
};

}