#pragma once

#include "engine/texturemanager.hpp"
#include "resource.hpp"
#include "manifest.hpp"

#include "tile-texture.hpp"

#include <memory>
#include <unordered_map>
#include <nlohmann/json.hpp>

namespace Techcraft {

/**
 * Manages resource loading for all registerable things.
 */
class ResourceManager {
    public:
    ResourceManager(Engine::TextureManager &textureManager);

    /**
     * Adds a texture resource to be managed.
     * 
     * If called before finalisation, this will not load the resource at this point.
     * Resources cannot be added after finalisation
     */
    const TileTexture *asTileTexture(const Resource &resource);

    /**
     * Adds a texture resource to be managed.
     * 
     * If called before finalisation, this will not load the resource at this point.
     * Resources cannot be added after finalisation
     */
    void asTexture(const Resource &resource);
    
    /**
     * Adds a model resource to be managed.
     * 
     * If called before finalisation, this will not load the resource at this point.
     * Resources cannot be added after finalisation
     */
    void asModel(const Resource &resource);

    /**
     * Adds a font resource to be managed.
     * 
     * If called before finalisation, this will not load the resource at this point.
     * Resources cannot be added after finalisation
     */
    void asFont(const Resource &resource);

    /**
     * Takes all resource load requests, and attempts to turn them into the actual resources.
     * This should be called after all required resources have been added with addResource.
     * 
     * This must only be called once.
     */
    void finaliseResources();

    private:
    std::unordered_map<Resource, std::unique_ptr<TileTexture>> tileTextureResources;
    std::unordered_map<std::string, const Engine::Texture *> texturesByFile;

    Engine::TextureManager &textureManager;

    void loadAllTextures();

    const Engine::Texture *getTextureByFilename(const std::string &name);

    void getAllTextures(const std::vector<TileTextureManifestDefinition::Image>&,
        const std::filesystem::path&,
        const TileTextureManifestDefinition&,
        const Resource&,
        TileTexture::TextureVector&
    );

    std::shared_ptr<Manifest> loadManifest(const std::string_view &base, const std::string_view &path);
};

}