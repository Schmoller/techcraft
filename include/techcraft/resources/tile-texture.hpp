#pragma once

#include "common.hpp"
#include "engine/texturemanager.hpp"
#include "techcraft/common.hpp"

#include <vector>

namespace Techcraft {

struct TextureInfo {
    double u1, v1;
    double u2, v2;
};

/**
 * A loaded texture which can change based on various things.
 * 
 * Can be "random", tiled, connected textures, rotated, animated, etc.
 */
class TileTexture {
    friend class ResourceManager;
    public:
    typedef std::pair<const Engine::Texture *, TextureInfo> TexturePair;
    typedef std::vector<TexturePair> TextureVector;

    enum class SubFace {
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight
    };

    struct Connectivity {
        SubFace subFace;
        bool vertical;
        bool horizontal;
        bool diagonal;

        // When these are false, texture coordinates
        // are split by the mid point depending on the sub face.
        // When true, it extends to the the entire width and or height.
        bool fullVertical { false };
        bool fullHorizontal { false };
    };

    TileTexture(const Engine::Texture *initialTexture, const TextureInfo &info);

    /**
     * Retrieves the current engine texture.
     * @param pos The tile position. Used for texture selection on some types of textures
     * @param info Outputs the texture coordinates
     */
    const Engine::Texture *asTexture(const TilePosition &pos, TextureInfo *info) const;
    /**
     * Retrieves the current engine texture taking into consideration connected texture stuff
     */
    const Engine::Texture *asTexture(const TilePosition &pos, Connectivity connectivity, TextureInfo *info) const;

    // TODO: Need asTexture overload with the connected texture info. This would require which quadrant to render and the connectivity details

    /**
     * Checks if this texture is a connected texture, and requires extra information
     * to resolve to a texture
     */
    bool isConnectedTexture() const;

    private:

    // Standard / Connected end texture
    TextureVector standardTextures;
    // Connected only
    TextureVector middleTextures;
    TextureVector verticalTextures;
    TextureVector horizontalTextures;
    TextureVector crossTextures;
    bool usingConnected { false };

    TextureFlipMode flip { TextureFlipMode::None };

    const Engine::Texture *selectTexture(const TextureVector &textures, const TilePosition &pos, TextureInfo *info) const;

    // For resource manager to use
    void updateTexture(const Engine::Texture *texture, const TextureInfo &info);
    void updateTextures(const TextureVector &textures);
    void setFlipMode(TextureFlipMode mode);
    void updateConnectedTextures(
        const TextureVector &end,
        const TextureVector &middle,
        const TextureVector &vertical,
        const TextureVector &horizontal,
        const TextureVector &cross
    );
};

}