#pragma once

#include <string>
#include <functional>
#include <ostream>

namespace Techcraft {

/**
 * Represents some resource.
 */
class Resource {
    public:
    Resource(const std::string_view &path, const std::string_view &subResource)
        : path(path), subResource(subResource)
    {}

    Resource(const std::string_view &path)
        : path(path), subResource("")
    {}

    Resource()
        : path(""), subResource("")
    {}

    bool isValid() const { return !path.empty(); }

    bool operator==(const Resource &other) const {
        return (
            path == other.path &&
            subResource == other.subResource
        );
    }

    std::string path;
    std::string subResource;
};

inline std::ostream &operator<<(std::ostream &os, const Resource &resource) {
    if (resource.subResource.empty()) {
        return os << resource.path;
    } else {
        return os << resource.path << "#" << resource.subResource;
    }
}


}

namespace std {
    template<> struct hash<Techcraft::Resource> {
        std::size_t operator()(Techcraft::Resource const& resource) const noexcept {
            auto resourceHash = std::hash<std::string>()(resource.path);
            resourceHash = resourceHash ^ (std::hash<std::string>()(resource.subResource) << 1);

            return resourceHash;
        }
    };
}