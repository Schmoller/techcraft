#pragma once

#include "common.hpp"
#include "engine/font.hpp"

#include <memory>
#include <string>
#include <vector>
#include <unordered_map>
#include <istream>
#include <filesystem>

#include <nlohmann/json.hpp>

namespace Techcraft {

class InvalidManifestError: public std::exception {
    public:
    InvalidManifestError(const std::string &error) : error(error) {}

    virtual const char *what() const noexcept override { return error.c_str(); }

    const std::string error;
};

class ManifestDefinition {
    public:
    enum class AssetType {
        TileTexture,
        Texture,
        Model,
        Font
    };

    virtual AssetType type() const = 0;
};

class TileTextureManifestDefinition: public ManifestDefinition {
    public:
    enum class Mode {
        Standard,
        Connected
    };

    struct Image {
        std::string file;
        uint32_t index { 0 };
    };

    TileTextureManifestDefinition(const nlohmann::json &raw);

    AssetType type() const override { return AssetType::TileTexture; }

    Mode mode { Mode::Standard };
    TextureFlipMode flip { TextureFlipMode::None };

    std::vector<Image> images;
    uint32_t width { 0 };
    uint32_t height { 0 };

    struct {
        std::vector<Image> end;
        std::vector<Image> middle;
        std::vector<Image> vertical;
        std::vector<Image> horizontal;
        std::vector<Image> cross;
    } connectedTextures;

    private:
    Image parseImage(const nlohmann::json &raw) const;
    void parseImages(const nlohmann::json &raw, std::vector<Image> &images) const;
};

class TextureManifestDefinition: public ManifestDefinition {
    public:
    TextureManifestDefinition(const nlohmann::json &raw);
    AssetType type() const override { return AssetType::Texture; }

    std::string file;
};

class ModelManifestDefinition: public ManifestDefinition {
    public:
    ModelManifestDefinition(const nlohmann::json &raw);
    AssetType type() const override { return AssetType::Model; }

    std::string file;
    std::string texture;
    std::string object;
};

class FontManifestDefinition: public ManifestDefinition {
    public:
    struct Style {
        std::string file;
        Engine::FontStyle style;
    };

    FontManifestDefinition(const nlohmann::json &raw);

    AssetType type() const override { return AssetType::Font; }

    std::vector<Style> styles;
    std::vector<uint32_t> sizes;

    private:
    Style parseStyle(const nlohmann::json &raw) const;
};

class Manifest {
    public:
    typedef std::shared_ptr<ManifestDefinition> SharedDefinition;
    
    Manifest(std::istream &input, const std::filesystem::path &path);

    SharedDefinition get(const std::string &name) const;
    SharedDefinition get() const;

    std::vector<std::string> ids() const;

    inline bool isValid() { return rootDefinition || !definitions.empty(); }

    const std::filesystem::path &path() const { return filePath; }

    private:
    void load(std::istream &input);

    SharedDefinition rootDefinition;
    std::unordered_map<std::string, SharedDefinition> definitions;

    std::filesystem::path filePath;
};

}