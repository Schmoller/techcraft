#pragma once

#include "common/types.hpp"
#include "common/position.hpp"
#include "common/boundingbox.hpp"
#include "common/direction.hpp"
#include "common/extent.hpp"

#include <cstdint>

#define ERROR_TEXTURE_NAME "tc:error"

// Uncomment this to have chunk loading debug logs
// #define DEBUG_CHUNKLOAD
