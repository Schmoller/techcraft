#pragma once

#include "techcraft/common.hpp"

#include "engine/object.hpp"

#include <memory>
#include <unordered_map>

namespace Techcraft {
// This needs to be includded if the tile animation controller is used
class Tile;

// These need to be included if the subtile animation controller is used
class Subtile;
class SubtileSlot;

namespace Rendering {

/**
 * Handles animations for a specific object representing a part of a tile
 */
class AnimationController {
    public:
    AnimationController(const TilePosition &position);
    virtual ~AnimationController() = default;

    void acceptPart(uint32_t partID, std::shared_ptr<Engine::Object> object) {
        parts[partID] = object;
    }

    void removePart(uint32_t partID) {
        parts.erase(partID);
    }

    inline bool isValid() const {
        return parts.size() != 0;
    }

    /**
     * Updates the animation.
     * 
     * @param deltaSeconds The time difference in seconds between the last frame and this frace
     * @param clientTime The time in seconds. Value is not absolute time, but a continuous time that always increases
     */
    virtual void updateFrame(double deltaSeconds, double clientTime) = 0;

    inline const TilePosition &getPosition() const { return position; }

    /**
     * Retrieves the first part being animated by this controller.
     * If only one part is animated, then this will retrieve that part.
     */
    inline const Engine::Object &getObject() const {
        return *parts.begin()->second;
    }

    /**
     * Retrieves the first part being animated by this controller.
     * If only one part is animated, then this will retrieve that part.
     */
    inline Engine::Object &getObject() {
        return *parts.begin()->second;
    }

    /**
     * Retrieves the a specific part being animated by this controller.
     * @param partID the ID of the part. This is the same ID used to create the animation in the first place.
     * @returns The object that is the part, or nullptr for invalid part IDs
     */
    inline const Engine::Object *getPart(uint32_t partID) const {
        auto it = parts.find(partID);
        if (it == parts.end()) {
            return nullptr;
        }
        return it->second.get();
    }

    /**
     * Retrieves the a specific part being animated by this controller.
     * @param partID the ID of the part. This is the same ID used to create the animation in the first place.
     * @returns The object that is the part, or nullptr for invalid part IDs
     */
    inline Engine::Object *getPart(uint32_t partID) {
        auto it = parts.find(partID);
        if (it == parts.end()) {
            return nullptr;
        }
        return it->second.get();
    }

    private:
    TilePosition position;
    
    std::unordered_map<uint32_t, std::shared_ptr<Engine::Object>> parts;
};

class TileAnimationController: public AnimationController {
    public:
    TileAnimationController(const TilePosition &position, const Tile &tile);

    inline const Tile &getTile() const { return *tile; }

    private:
    const Tile *tile;
};

class SubtileAnimationController: public AnimationController {
    public:
    SubtileAnimationController(const TilePosition &position, const SubtileSlot &slot, const Subtile &subtile);

    inline const Subtile &getSubtile() const { return *subtile; }
    inline const SubtileSlot &getSlot() const { return *slot; };

    private:
    const Subtile *subtile;
    const SubtileSlot *slot;
};

}
}