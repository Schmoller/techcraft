#pragma once

#include "animation.hpp"
#include "techcraft/common.hpp"
#include "techcraft/tiles/base.hpp"
#include "techcraft/fluid/base.hpp"
#include "techcraft/subtiles/base.hpp"
#include "techcraft/resources/tile-texture.hpp"

#include "engine/subsystem/terrain.hpp"

#include <concepts>

namespace Techcraft::Rendering {

using Engine::Subsystem::TileVertex;

/**
 * An interface for statically rendering terrain meshes
 */
class StaticRenderContext {
    public:
    /**
     * Outputs a face that has a Opaque texture.
     * If there is any transparency or translucency, use outputTransparentFace
     */
    virtual void outputSolidFace(
        TileDirection face,
        const TileTexture *texture,
        TileVertex tlVertex,
        TileVertex trVertex,
        TileVertex brVertex,
        TileVertex blVertex,
        bool autoTextureCoords = true
    ) = 0;
    /**
     * Outputs a face that contains transparency or translucency
     */
    virtual void outputTransparentFace(
        TileDirection face,
        const TileTexture *texture,
        TileVertex tlVertex,
        TileVertex trVertex,
        TileVertex brVertex,
        TileVertex blVertex,
        bool autoTextureCoords = true
    ) = 0;

    virtual const Tile &getTile(const TilePosition &position) const = 0;
    virtual const FluidTile &getFluid(const TilePosition &position) const = 0;
    virtual const Subtile &getSubtile(const TilePosition &position, const SubtileSlot &slot) const = 0;
    virtual const TileLighting &getTileLight(const TilePosition &position) const = 0;
};

/**
 * Contains everything needed to be able to render models to the scene
 */
// template <typename T> requires std::derived_from<T, AnimationController>
template <typename T>
class DynamicRenderContext {
    public:
    /**
     * Renders a new Part.
     * @param partID A unique ID for this part (to any paricular tile). The value can be anything, and only has meaning to the specific renderer.
     * @param mesh The mesh to render
     * @param material The material to render with
     * @param position The position to render at.
     * @returns An Object, which can be used to update additional parameters.
     */
    virtual std::shared_ptr<Engine::Object> renderPart(uint32_t partID, const Engine::Mesh *mesh, const Engine::Material *material, const Position &position) = 0;
    /**
     * Renders a new animated Part.
     * @param partID A unique ID for this part (to any paricular tile). The value can be anything, and only has meaning to the specific renderer.
     * @param mesh The mesh to render
     * @param material The material to render with
     * @param position The position to render at.
     * @param animation The animation controller
     */
    virtual void renderAnimation(uint32_t partID, const Engine::Mesh *mesh, const Engine::Material *material, const Position &position, std::shared_ptr<T> animation) = 0;

    /**
     * Retrieves the object for the given part ID.
     * Allows you to update additional parameters for the model.
     * This includes:
     * - Changing position, rotation, and scale
     * - Changing texture
     * - Changing mesh
     * @param partID The ID of the part.
     * @returns The Object if the part ID is valid.
     */
    virtual std::shared_ptr<Engine::Object> getPart(uint32_t partID) = 0;

    /**
     * Removes a part that was created with @see renderPart or @see getPart
     * @param partID The ID of the part.
     */
    virtual void unrender(uint32_t partID) = 0;
};


}