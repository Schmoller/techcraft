#pragma once

#include "techcraft/common/position.hpp"
#include "engine/engine.hpp"
#include "regionrenderer.hpp"
#include "dynamicrenderer.hpp"
#include <atomic>

namespace Techcraft {

class ChunkRenderState {
public:
    ChunkRenderState(const std::shared_ptr<Chunk> &chunk, Engine::RenderEngine &engine, const TilePosition &position);

    const ChunkPosition position;
    std::atomic_bool rendering { false };
    RegionRenderer staticRenderer;
    DynamicRegionRenderer dynamicRenderer;
    std::atomic_bool dirty { false };
    std::atomic_bool removed { false };
};

}