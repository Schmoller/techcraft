#pragma once

#include <glm/glm.hpp>

namespace Techcraft::Rendering {

// TODO: I should only calculate this once and reuse the result
const glm::mat4 &gui3dTransform();

inline glm::vec3 transform(const glm::vec3 &pos, const glm::mat4 &transform) {
    return transform * glm::vec4(pos, 1.0f);
}


}