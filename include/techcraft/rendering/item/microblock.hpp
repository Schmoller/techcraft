#pragma once

#include "base.hpp"

namespace Techcraft::Rendering {

class MicroblockItemRenderer: public TESRItemRenderer {
    public:
    bool renderForGUI(const ItemStack &item, const ItemType &type, Engine::Gui::Drawer &drawer) override;

    void init(Engine::RenderEngine &engine) override;

    static MicroblockItemRenderer &instance();

    private:
    static MicroblockItemRenderer inst;

    Engine::Texture *microblockIcon { nullptr };
};

}