#pragma once

#include "base.hpp"

namespace Techcraft::Rendering {

/**
 * Flatly renders the texture of the item
 */
class FlatItemRenderer: public TESRItemRenderer {
    public:
    bool renderForGUI(const ItemStack &item, const ItemType &type, Engine::Gui::Drawer &drawer) override;

    static FlatItemRenderer &instance();

    private:
    static FlatItemRenderer inst;
};

}