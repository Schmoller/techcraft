#pragma once

#include "base.hpp"

namespace Techcraft::Rendering {

class TileItemRenderer: public TESRItemRenderer {
    public:
    bool renderForGUI(const ItemStack &item, const ItemType &type, Engine::Gui::Drawer &drawer) override;

    static TileItemRenderer &instance();

    private:
    static TileItemRenderer inst;
};

}