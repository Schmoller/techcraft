#pragma once

#include "techcraft/common.hpp"
#include "techcraft/items/base.hpp"
#include "engine/engine.hpp"

namespace Techcraft::Rendering {

enum class ItemRendererType {
    Mesh,
    TESR
};

class ItemRenderer {
    public:
    virtual ~ItemRenderer() = default;

    /**
     * Called to initialise resources required by this renderer
     * such as models or textures.
     */
    virtual void init(Engine::RenderEngine &engine) {};

    virtual ItemRendererType type() = 0;
};

/**
 * Renders an item as a mesh. This may either produce a mesh or provide an
 * existing mesh.
 */
class MeshItemRenderer: public ItemRenderer {
    public:
    ItemRendererType type() final override { return ItemRendererType::Mesh; }

    virtual Engine::Mesh *getOrCreateMesh(const ItemStack &item, const ItemType &type) = 0;

    /**
     * Allows you to set how the mesh is rendered in the GUI.
     * By default, it is rendered with the expectation that the mesh covers the
     * range {-0.5 - 0.5} on all axes, looking at it along the Y axis.
     */
    virtual bool renderForGUI(const ItemStack &item, const ItemType &type, glm::vec3 *outScale, glm::quat *outRotation);
};

/**
 * Renders the item...
 */
class TESRItemRenderer: public ItemRenderer {
    public:
    ItemRendererType type() final override { return ItemRendererType::TESR; }

    // TODO: How do we render this for the world? idk
    /*
     * It would probably need to be rendered as a mesh, but meshes dont specify textures, and objects can only have one material.
     * We would need to offer the ability to have multiple materials (which apply to specific index ranges) on objects
     */
    // virtual bool renderForWorld(const ItemStack &item, const ItemType &type, Engine::StaticMeshBuilder<Engine::Vertex> &output) = 0;

    /**
     * Renders this item in the GUI.
     * 
     * Items must be rendered within the range of {-0.5 .. 0.5} in all axes.
     * This will be transformed to whatever size and location is desired on the output.
     * 
     * @returns true if the item was drawn. false if the item cannot be rendered.
     */
    virtual bool renderForGUI(const ItemStack &item, const ItemType &type, Engine::Gui::Drawer &drawer) = 0;
};

}