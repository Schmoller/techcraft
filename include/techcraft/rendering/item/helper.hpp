#pragma once

#include "base.hpp"
#include "techcraft/items/base.hpp"
#include "engine/engine.hpp"

#include <unordered_map>

namespace Techcraft::Rendering {

/**
 * Deals with rendering and re-rendering of items as needed
 */
class ItemRenderHelper {
    public:
    ItemRenderHelper(Engine::RenderEngine &engine);

    void update();

    /**
     * Draws an item for GUI display.
     */
    void drawItemGUI(Engine::Gui::Drawer &drawer, const ItemStack &item, float x, float y);
    /**
     * Draws an item for GUI display.
     * This overload can also change the output size
     */
    void drawItemGUI(Engine::Gui::Drawer &drawer, const ItemStack &item, float x, float y, float width, float height);

    private:
    Engine::RenderEngine &engine;
};

}