#pragma once

#include "base.hpp"
#include "techcraft/fluid/base.hpp"

namespace Techcraft::Rendering {

class BasicFluidRenderer : public FluidRenderer {
    public:
    virtual void render(tcsize x, tcsize y, tcsize z, const FluidTile &fluid, StaticRenderContext &renderer) override;

    static BasicFluidRenderer &instance();

    private:
    static BasicFluidRenderer inst;
};

}