#pragma once

#include "techcraft/common.hpp"
#include "techcraft/fluid/base.hpp"
#include "techcraft/rendering/context.hpp"

namespace Techcraft::Rendering {

/**
 * A renderer that handles rendering of fluids
 */
class FluidRenderer {
    public:
    virtual void render(tcsize x, tcsize y, tcsize z, const FluidTile &fluid, StaticRenderContext &renderer) = 0;
};

}