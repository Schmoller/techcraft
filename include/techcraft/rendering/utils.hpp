#pragma once

#include "techcraft/tiles/base.hpp"

namespace Techcraft {

const float DirectionalOcclusionShade[6] = {
    1.0f, // Up
    0.7f, // Down
    0.85f, // North
    0.85f, // South
    0.9f, // East
    0.9f, // West
};

glm::vec3 mergeTile(const TileLighting &self, const TileLighting &tl1, const Tile &t1, const TileLighting &tl2, const Tile &t2, const TileLighting &tl3, const Tile &t3);

glm::vec3 mergeSky(const TileLighting &self, const TileLighting &tl1, const Tile &t1, const TileLighting &tl2, const Tile &t2, const TileLighting &tl3, const Tile &t3);

glm::vec3 occlude(const Tile &t1, const Tile &t2, const Tile &t3, const glm::vec3 &tint);

}