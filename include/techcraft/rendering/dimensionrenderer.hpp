#pragma once

#include <glm/glm.hpp>
#include "techcraft/dimension/dimension.hpp"
#include "techcraft/dimension/chunk.hpp"
#include "techcraft/rendering/regionrenderer.hpp"
#include "techcraft/rendering/dynamicrenderer.hpp"
#include "engine/engine.hpp"
#include "engine/object.hpp"
#include "utilities/event.hpp"
#include "utilities/workers.hpp"

#include <deque>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <mutex>
#include <vector>
#include <boost/pool/pool_alloc.hpp>
#include <boost/intrusive/list.hpp>

namespace Techcraft {

/**
 * The dimension renderer handles rendering chunks around a viewer
 */
class DimensionRenderer {
    using ChunkRenderHookMember = boost::intrusive::member_hook<Chunk, boost::intrusive::list_member_hook<>, &Chunk::activeRenderHook>;

    public:
    DimensionRenderer(
        Dimension *dimension,
        Engine::RenderEngine &engine,
        Utilities::WorkerFleet &workers,
        uint16_t viewDistance = 3
    );
    ~DimensionRenderer();

    /**
     * Updates the viewport that the dimension will be rendered from.
     * As the viewpoint changes, the visible chunks will change.
     */
    void setViewpoint(const Position &point, const glm::vec3 &forward);
    /**
     * Removes the viewpoint, no chunks will be rendered
     */
    void clearViewpoint();

    void setViewRange(uint16_t viewRange);
    uint16_t getViewRange() const;

    void update(double deltaTime);

    private:
    void addToRender(const std::shared_ptr<Chunk> &chunk, bool withBackLink);
    void removeFromRender(Chunk &chunk);

    void updateActiveChunks();
    void processPending();

    void queueGenerate(const ChunkPosition &chunk);

    // Event listeners
    void onChunkDirty(DimensionEvent event, const ChunkPosition &coord);
    std::shared_ptr<Dimension::EventHandlerDirty::Connector> chunkDirtyHandler;
    void onChunkLoad(DimensionEvent event, const ChunkPosition &coord, const std::shared_ptr<Chunk> &chunk);
    std::shared_ptr<Dimension::ChunkLoadedEvent::Connector> chunkLoadHandler;
    void onChunkUnload(DimensionEvent event, const ChunkPosition &coord, const std::shared_ptr<Chunk> &chunk);
    std::shared_ptr<Dimension::ChunkUnloadedEvent::Connector> chunkUnloadHandler;
    void onTileEvent(DimensionEvent event, const TilePosition &position, const Tile &tile);
    std::shared_ptr<Dimension::TileAddEvent::Connector> tileAddHandler;
    std::shared_ptr<Dimension::TileRemoveEvent::Connector> tileRemoveHandler;
    std::shared_ptr<Dimension::TileUpdateEvent::Connector> tileUpdateHandler;
    
    // Provided
    Dimension *dimension;
    Engine::RenderEngine &engine;
    Utilities::WorkerFleet &workers;

    // State
    uint16_t chunkViewDistance;
    uint16_t lastChunkViewDistance;

    ChunkPosition lastCenterChunk;
    bool lastRenderActive;

    ChunkPosition centerChunk;
    bool renderActive;


    // Chunks that should be rendered
    boost::intrusive::list<Chunk, ChunkRenderHookMember> activeChunks;
    std::unordered_set<ChunkPosition, std::hash<ChunkPosition>, std::equal_to<ChunkPosition>, boost::pool_allocator<ChunkPosition>> toBeUpdated;

    std::mutex uploadLock;
    std::vector<std::weak_ptr<Chunk>> uploadQueue;

    double lastReport { 0 };
};

}