#pragma once

#include "techcraft/region.hpp"
#include "techcraft/tiles/base.hpp"
#include "techcraft/common.hpp"
#include "context.hpp"
#include "animation.hpp"
#include "engine/subsystem/objects.hpp"
#include "engine/object.hpp"

#include <unordered_map>
#include <vector>

namespace Techcraft {

class DynamicRegionRenderer : private Rendering::DynamicRenderContext<Rendering::TileAnimationController> {
    using ObjectSubsystem = Engine::Subsystem::ObjectSubsystem;

    struct DynamicState {
        std::unordered_map<uint32_t, std::shared_ptr<Engine::Object>> parts;
        std::unordered_map<uint32_t, std::shared_ptr<Rendering::TileAnimationController>> animationControllers;
    };

    public:
    DynamicRegionRenderer(
        std::shared_ptr<Region> region,
        ObjectSubsystem *objectSubsystem,
        const TilePosition &offset
    );

    ~DynamicRegionRenderer();

    void addTile(const TilePosition &pos, const Tile &tile);
    void removeTile(const TilePosition &pos, const Tile &tile);
    void updateTile(const TilePosition &pos, const Tile &tile);

    void render();
    void frameUpdate(double deltaTime);

    // Do not use Self in any of the following
    void link(std::shared_ptr<Region> region, TileDirection direction);
    void unlink(TileDirection direction);
    void link(std::shared_ptr<Region>region, TileDirectionExtended direction);
    void unlink(TileDirectionExtended direction);

    private:
    void setTileForRendering(const TilePosition &pos);

    const Tile &getTile(int32_t x, int32_t y, int32_t z) const;
    const TileLighting &getTileLight(int32_t x, int32_t y, int32_t z) const;
    void genLightCubes(int32_t x, int32_t y, int32_t z, Engine::LightCube &tileLight, Engine::LightCube &skyTint, Engine::LightCube &occlusion);

    // Implementing DynamicRenderContext
    std::shared_ptr<Engine::Object> renderPart(uint32_t partID, const Engine::Mesh *mesh, const Engine::Material *material, const Position &position) override;
    void renderAnimation(uint32_t partID, const Engine::Mesh *mesh, const Engine::Material *material, const Position &position, std::shared_ptr<Rendering::TileAnimationController> animation) override;
    std::shared_ptr<Engine::Object> getPart(uint32_t partID) override;
    void unrender(uint32_t partID) override;

    // Provided
    std::shared_ptr<Region> region;
    ObjectSubsystem *objectSubsystem;
    TilePosition offset;

    Tile emptyTile;
    TileLighting emptyTileLight;

    // State
    TilePosition currentTile;
    Engine::LightCube tileLight;
    Engine::LightCube skyTint;
    Engine::LightCube occlusion;
    DynamicState *currentTileState;
    std::unordered_map<TilePosition, DynamicState> tileState;
    std::vector<Rendering::TileAnimationController *> allAnimationControllers;
    double animationTime;

    std::array<std::shared_ptr<Region>, 26> nextRegion;
};

}