#pragma once

#include "../region.hpp"
#include "../tiles/base.hpp"
#include "../fluid/base.hpp"
#include "context.hpp"
#include "engine/mesh.hpp"
#include "engine/texturemanager.hpp"
#include "engine/subsystem/terrain.hpp"
#include <memory>
#include <functional>
#include <unordered_map>
#include <atomic>

namespace Techcraft {

class RegionRenderer : private Rendering::StaticRenderContext {
    using TerrainSubsystem = Engine::Subsystem::TerrainSubsystem;
    using TileVertex = Engine::Subsystem::TileVertex;
    static constexpr uint32_t NO_SEGMENT { 0xFFFFFFFF };

    struct Segment {
        uint32_t textureArrayId;

        uint32_t id { NO_SEGMENT };
        std::vector<TileVertex> vertices;
        std::vector<uint16_t> indices;

        bool modified { false };
    };

    enum class CurrentMode {
        Tile,
        Subtile,
        Fluid
    };

    public:
    RegionRenderer(
        std::shared_ptr<Region> region,
        TerrainSubsystem *terrainSystem,
        const TilePosition &offset
    );
    ~RegionRenderer();

    // Do not use Self in any of the following
    void link(std::shared_ptr<Region> region, TileDirection direction);
    void unlink(TileDirection direction);
    void link(std::shared_ptr<Region> region, TileDirectionExtended direction);
    void unlink(TileDirectionExtended direction);

    void generateMesh();
    void finishGeneration();

    private:
    Tile emptyTile;
    FluidTile emptyFluid;
    TileLighting emptyTileLight;
    Subtile emptySubtile;

    // Provided
    TerrainSubsystem *terrainSystem;
    std::shared_ptr<Region> region;
    TilePosition offset;

    TilePosition current;
    const Tile *currentTile { nullptr };
    const Subtile *currentSubtile { nullptr };
    const SubtileSlot *currentSubtileSlot { nullptr };
    const FluidTile *currentFluid { nullptr };
    CurrentMode currentMode;

    // Generated
    // Maps of texture array id to Segment
    std::unordered_map<uint32_t, Segment> opaqueSegemnts;
    std::unordered_map<uint32_t, Segment> transparentSegments;

    std::array<std::shared_ptr<Region>, 26> linkedRegionsForWriting { nullptr };
    std::array<std::shared_ptr<Region>, 26> linkedRegionsForReading { nullptr };
    std::atomic_bool linkedRegionsIsModified { false };

    void beginMesh();
    void endMesh();


    // Implementing StaticRenderContext
    /**
     * Outputs a face that has a Opaque texture.
     * If there is any transparency or translucency, use outputTransparentFace
     */
    void outputSolidFace(
        TileDirection face,
        const TileTexture *texture,
        TileVertex tlVertex,
        TileVertex trVertex,
        TileVertex brVertex,
        TileVertex blVertex,
        bool autoTextureCoords = true
    ) override;
    /**
     * Outputs a face that contains transparency or translucency
     */
    void outputTransparentFace(
        TileDirection face,
        const TileTexture *texture,
        TileVertex tlVertex,
        TileVertex trVertex,
        TileVertex brVertex,
        TileVertex blVertex,
        bool autoTextureCoords = true
    ) override;

    void outputFace(
        std::unordered_map<uint32_t, Segment> &segmentMap,
        TileDirection face,
        const TileTexture *texture,
        TileVertex tlVertex,
        TileVertex trVertex,
        TileVertex brVertex,
        TileVertex blVertex,
        bool autoTextureCoords
    );

    void outputFinalFace(
        std::unordered_map<uint32_t, Segment> &segmentMap,
        TileDirection face,
        const Engine::Texture *texture,
        const TextureInfo &info,
        TileVertex tlVertex,
        TileVertex trVertex,
        TileVertex brVertex,
        TileVertex blVertex,
        bool autoTextureCoords
    );

    const Tile &getTile(const TilePosition &position) const override;
    const FluidTile &getFluid(const TilePosition &position) const override;
    const Subtile &getSubtile(const TilePosition &position, const SubtileSlot &slot) const override;
    const TileLighting &getTileLight(const TilePosition &position) const override;

    bool textureCanConnect(const TilePosition &position, TileDirection face, const TileTexture *match) const;

    void lockInLinkedRegionChanges();
};

}