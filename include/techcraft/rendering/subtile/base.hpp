#pragma once

#include "techcraft/common.hpp"
#include "techcraft/subtiles/base.hpp"
#include "techcraft/rendering/context.hpp"
#include "techcraft/rendering/animation.hpp"
#include "engine/engine.hpp"

namespace Techcraft::Rendering {

class SubtileRenderer {
    public:
    virtual ~SubtileRenderer() = default;

    virtual bool isStatic() = 0;
};

/**
 * A statically rendered subtile.
 * These are rendered into the terrain mesh and cannot have animations.
 */
class StaticSubtileRenderer: public SubtileRenderer {
    public:
    virtual void render(tcsize x, tcsize y, tcsize z, const SubtileSlot &slot, const Subtile &subtile, StaticRenderContext &renderer) = 0;

    bool isStatic() final override {
        return true;
    }
};

/**
 * A dynamically rendered subtile.
 * These are rendered into objects which can be modified as needed.
 */
class DynamicSubtileRenderer: public SubtileRenderer {
    public:
    /**
     * Called to initialise resources required by this renderer
     * such as models or textures.
     */
    virtual void init(Engine::RenderEngine &engine) {};

    /**
     * Called to initialise a tile for rendering.
     * This is only called once for a particular tile.
     * Use this to set up the model parts.
     * @param x The x coord of the tile
     * @param y The y coord of the tile
     * @param z The z coord of the tile
     * @param slot The slot of the subtile
     * @param tile The tile data being initialised
     * @param context The context object for initialising the renderer
     */
    virtual void initTile(tcsize x, tcsize y, tcsize z, const SubtileSlot &slot, const Subtile &subtile, DynamicRenderContext<SubtileAnimationController> &context) = 0;

    /**
     * Called to update the rendered tile.
     * This is only called if a tile needs to be re-rendered.
     * @param x The x coord of the tile
     * @param y The y coord of the tile
     * @param z The z coord of the tile
     * @param slot The slot of the subtile
     * @param tile The tile data being rendered
     * @param context The context object for modifying settings
     */
    virtual void update(tcsize x, tcsize y, tcsize z, const SubtileSlot &slot, const Subtile &subtile, DynamicRenderContext<SubtileAnimationController> &context) {};

    bool isStatic() final override {
        return false;
    }
};

}