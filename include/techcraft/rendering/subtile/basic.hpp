#pragma once

#include "base.hpp"

namespace Techcraft::Rendering {

class BasicSubtileRenderer: public StaticSubtileRenderer {
    public:
    static BasicSubtileRenderer &instance();

    void render(tcsize x, tcsize y, tcsize z, const SubtileSlot &slot, const Subtile &subtile, StaticRenderContext &renderer) override;

    private:
    static BasicSubtileRenderer inst;
};

}