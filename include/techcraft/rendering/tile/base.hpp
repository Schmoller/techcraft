#pragma once

#include "techcraft/common.hpp"
#include "techcraft/tiles/base.hpp"
#include "techcraft/rendering/context.hpp"
#include "techcraft/rendering/animation.hpp"
#include "engine/engine.hpp"

namespace Techcraft::Rendering {

/**
 * Tile renderers tell the game how to render the given tile.
 * This comes in two variants:
 * - static rendering
 * - dynamic rendering
 * 
 * Static rendering (@see StaticTileRenderer) renders a mesh into the terrain mesh.
 * This should only be used for simple meshes that are not animated.
 * 
 * Dynamic rendering (@see DynamicTileRenderer) renders one or more meshes into the scenegraph.
 * This should be used for complex meshes or animations.
 */
class TileRenderer {
    public:
    virtual ~TileRenderer() = default;

    virtual bool isStatic() = 0;
};

/**
 * An interface that when implemented, allows the static rendering of tiles
 */
class StaticTileRenderer: public TileRenderer {
    public:
    virtual void render(tcsize x, tcsize y, tcsize z, const Tile &tile, StaticRenderContext &renderer) = 0;

    bool isStatic() final override {
        return true;
    }
};

/**
 * The base interface for all dynamically rendered tiles
 */
class DynamicTileRenderer: public TileRenderer {
    public:
    /**
     * Called to initialise resources required by this renderer
     * such as models or textures.
     */
    virtual void init(Engine::RenderEngine &engine) {};

    /**
     * Called to initialise a tile for rendering.
     * This is only called once for a particular tile.
     * Use this to set up the model parts.
     * @param x The x coord of the tile
     * @param y The y coord of the tile
     * @param z The z coord of the tile
     * @param tile The tile data being initialised
     * @param context The context object for initialising the renderer
     */
    virtual void initTile(tcsize x, tcsize y, tcsize z, const Tile &tile, DynamicRenderContext<TileAnimationController> &context) = 0;

    /**
     * Called to update the rendered tile.
     * This is only called if a tile needs to be re-rendered.
     * @param x The x coord of the tile
     * @param y The y coord of the tile
     * @param z The z coord of the tile
     * @param tile The tile data being rendered
     * @param context The context object for modifying settings
     */
    virtual void update(tcsize x, tcsize y, tcsize z, const Tile &tile, DynamicRenderContext<TileAnimationController> &context) {};

    bool isStatic() final override {
        return false;
    }
};


}