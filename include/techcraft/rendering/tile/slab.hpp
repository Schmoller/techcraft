#pragma once

#include "base.hpp"

namespace Techcraft::Rendering {

/**
 * Renders slab tiles.
 * NOTE: This renderer assumes an attached SlabTileEntity.
 */
class SlabTileRenderer : public StaticTileRenderer {
    public:
    void render(tcsize x, tcsize y, tcsize z, const Tile &tile, StaticRenderContext &context) override;

    static SlabTileRenderer &instance();

    private:
    static SlabTileRenderer inst;
};

}