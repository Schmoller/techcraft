#pragma once

#include "base.hpp"

namespace Techcraft::Rendering {

class BarrelTileRenderer: public DynamicTileRenderer {
    public:
    void init(Engine::RenderEngine &engine) override;

    void initTile(tcsize x, tcsize y, tcsize z, const Tile &tile, DynamicRenderContext<TileAnimationController> &context) override;
    void update(tcsize x, tcsize y, tcsize z, const Tile &tile, DynamicRenderContext<TileAnimationController> &context) override;

    static BarrelTileRenderer &instance();

    private:
    static BarrelTileRenderer inst;

    const Engine::StaticMesh *mesh;
    Engine::Material *material;
};

class BarrelAnimator: public TileAnimationController {
    public:
    BarrelAnimator(const TilePosition &position, const Tile &tile);

    void updateFrame(double deltaSeconds, double clientTime) override;
};

}