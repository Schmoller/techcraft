#pragma once

#include "base.hpp"

namespace Techcraft::Rendering {

class BasicTransparentTileRenderer : public StaticTileRenderer {
    public:
    void render(tcsize x, tcsize y, tcsize z, const Tile &tile, StaticRenderContext &context) override;

    static BasicTransparentTileRenderer &instance();

    private:
    static BasicTransparentTileRenderer inst;
};

}
