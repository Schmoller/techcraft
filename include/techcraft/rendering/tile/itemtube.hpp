#pragma once

#include "base.hpp"

namespace Techcraft::Rendering {

class ItemTubeRenderer: public DynamicTileRenderer {
    public:
    void init(Engine::RenderEngine &engine) override;

    void initTile(tcsize x, tcsize y, tcsize z, const Tile &tile, DynamicRenderContext<TileAnimationController> &context) override;
    void update(tcsize x, tcsize y, tcsize z, const Tile &tile, DynamicRenderContext<TileAnimationController> &context) override;

    static ItemTubeRenderer &instance();

    private:
    static ItemTubeRenderer inst;

    void getMeshAndTransform(
        const bool connectivity[6],
        TileDirection attachSide,
        const Engine::StaticMesh **mesh,
        glm::mat4 *transform,
        glm::mat4 *connectorTransform,
        bool *allowConnector
    ) const;

    struct {
        const Engine::StaticMesh *straight;
        const Engine::StaticMesh *corner;
        const Engine::StaticMesh *tee;
        const Engine::StaticMesh *teeCorner;
        const Engine::StaticMesh *teeCross;
        const Engine::StaticMesh *crossUp;
        const Engine::StaticMesh *cross6Way;
        const Engine::StaticMesh *cross4Way;
        const Engine::StaticMesh *none;
        const Engine::StaticMesh *cap;
        const Engine::StaticMesh *connector;
    } meshes;

    Engine::Material *materialBasic;
};

}