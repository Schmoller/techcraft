#pragma once

#include "base.hpp"

namespace Techcraft::Rendering {

/**
 * Default tile renderer.
 * Able to render solid tiles.
 */
class BasicTileRenderer : public StaticTileRenderer {
    public:
    void render(tcsize x, tcsize y, tcsize z, const Tile &tile, StaticRenderContext &context) override;

    static BasicTileRenderer &instance();

    private:
    static BasicTileRenderer inst;
};

}
