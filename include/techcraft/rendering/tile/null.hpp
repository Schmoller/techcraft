#pragma once

#include "base.hpp"

namespace Techcraft::Rendering {

class NullTileRenderer: public StaticTileRenderer {
    public:
    void render(tcsize x, tcsize y, tcsize z, const Tile &tile, StaticRenderContext &context) override;

    static NullTileRenderer &instance();

    private:
    static NullTileRenderer inst;
};

}