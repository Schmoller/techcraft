#pragma once

#include <glm/glm.hpp>

namespace Techcraft {
    #define TILE_LIGHT_PALLET_COUNT 2
    const glm::vec3 TILE_LIGHTING_PALLETS[TILE_LIGHT_PALLET_COUNT] = {
        {1, 1, 0.8},
        {0.0, 0.3, 1.0}
    };

    enum class TileLightPallet {
        White = 0,
        Blue
    };

    const TileLightPallet AllPallets[TILE_LIGHT_PALLET_COUNT] = {
        TileLightPallet::White,
        TileLightPallet::Blue
    };
}