#pragma once

#include "techcraft/dimension/dimension.hpp"
#include "techcraft/dimension/chunkloader.hpp"
#include "keybinding.hpp"
#include "utilities/workers.hpp"

#include "console.hpp"
#include "gui/console.hpp"

#include <unordered_map>
#include <chrono>

// Client
#include "rendering/dimensionrenderer.hpp"
#include "engine/camera.hpp"
#include "engine/inputmanager.hpp"
#include "entities/player.hpp"

namespace Techcraft {

const uint32_t TICKS_PER_SECOND = 20;
const std::chrono::duration<double, std::nano> TICK_DURATION = std::chrono::duration<double>(1.0/TICKS_PER_SECOND);

/**
 * A universe is a collection of dimensions representing an entire save game.
 */
class Universe {
    public:
    Universe(Utilities::WorkerFleet &workers);
    Dimension *getDimension(uint32_t id);

    virtual void update(double deltaTime);

    /**
     * Pauses and unpauses the universe simulation
     */
    void setPause(bool paused);
    /**
     * When true, the universe simulation is paused
     */
    bool isPaused();

    void setChunkLoadDistance(ChunkCoordRes distance);
    ChunkCoordRes getChunkLoadDistance() const;

    protected:
    virtual void doTick();
    virtual void doUpdate();

    Utilities::WorkerFleet &getWorkers();

    private:
    typedef std::chrono::time_point<std::chrono::high_resolution_clock, std::chrono::duration<double, std::nano>> TimePoint;
    std::unordered_map<uint32_t, std::unique_ptr<Dimension>> dimensions;

    Utilities::WorkerFleet &workers;

    // Config
    ChunkCoordRes chunkLoadRange {5};

    // Ticking
    bool pauseState { false };
    TimePoint lastTickStart;
};

class ClientUniverse : public Universe {
    public:
    ClientUniverse(
        BindingManager *bindingManager,
        Utilities::WorkerFleet &workers,
        Engine::RenderEngine &renderEngine,
        Commands::Executor &executor,
        uint16_t viewDistance = 3
    );

    virtual void update(double deltaTime);

    void setLocalPlayer(LocalPlayer *player);
    LocalPlayer *getLocalPlayer() const { return localPlayer; }

    void setChunkViewDistance(uint16_t viewDistance);
    uint16_t getChunkViewDistance() const;

    BindingManager &getBindingManager() {
        return *bindingManager;
    }

    Console &getConsole() { return console; }
    
    void showConsole();
    void hideConsole();

    private:
    void drawLookTile();

    Engine::RenderEngine &renderEngine;
    Engine::InputManager &inputManager;
    Engine::Camera camera;
    BindingManager *bindingManager;

    Console console;
    std::shared_ptr<KeyBinding> consoleKey;
    std::shared_ptr<GuiConsole> consoleGui;

    uint16_t chunkViewDistance;

    std::unordered_map<uint32_t, std::unique_ptr<DimensionRenderer>> dimensionRenderers;

    LocalPlayer *localPlayer;
    Dimension *lastPlayerDimension;
};

}