#pragma once

#include "base.hpp"

namespace Techcraft {

class PlayerInventory : public Inventory {
    public:
    PlayerInventory();

    uint8_t getSelected() const;
    void setSelected(uint8_t slot);

    OptionalItemStack getSelectedItem() const;

    private:

    uint8_t selectedSlot;
};

}