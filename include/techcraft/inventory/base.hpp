#pragma once

#include "itemstack.hpp"

namespace Techcraft {

/**
 * A generic container for item stacks.
 */
class Inventory {
    public:
    Inventory(uint16_t slotCount);
    uint16_t getSize() const;

    /**
     * Inserts an item stack into this inventory.
     * If the inventory cannot hold the given stack, it will be returned.
     * If the inventory was able to hold some of the item stack, the remainder will be returned.
     * Nothing will be returned if the inventory holds all of the item given.
     * 
     * @param stack The item stack to insert
     * @param simulate When true, the item is not actually inserted, but the logic is still checked
     * @returns Any remaining items that didnt fit into the inventory.
     */
    OptionalItemStack insert(const ItemStack &stack, bool simulate = false);

    OptionalItemStack operator[](uint16_t slot) const;
    OptionalItemStack &operator[](uint16_t slot);

    private:
    std::vector<OptionalItemStack> slots;
};

}