#pragma once

#include "techcraft/items/base.hpp"

namespace Techcraft {

/**
 * An item stack is a part of inventories which contains solid items.
 */
class ItemStack {
    public:
    /**
     * Creates a new ItemStack from the given ItemType and quantity
     */
    ItemStack(const ItemType &type, uint16_t quantity);
    /**
     * Creates a new ItemStack as an exact copy of another ItemStack
     */
    ItemStack(const ItemStack &other);

    /**
     * Retrieves the type of item held within.
     */
    const ItemType &getType() const { return *type; }

    /**
     * Retrieves the number of items in this stack
     */
    uint16_t getQuantity() const { return quantity; }

    /**
     * Sets the quantity of the stack
     */
    void setQuantity(uint16_t quantity);

    /**
     * Checks if this ItemStack contains no items
     */
    bool isEmpty() const { return quantity == 0; }

    /**
     * Returns true if this item stack could be merged into the given stack,
     * or viceversa.
     */
    bool canStackWith(const ItemStack &other) const;

    /**
     * Returns the maximum number of items that may be stored within this itemstack
     */
    uint16_t getMaximumStackSize() const;

    /**
     * Splits this ItemStack after the given number.
     * Afterwards, this ItemStack will contain no more than `amount`,
     * and the returned stack will contain the remainder, if any.
     * @param amount The split point.
     * @returns The remaining items, or nothing
     */
    std::optional<ItemStack> splitAfter(uint16_t amount);

    /**
     * Removes up to `amount` items from this stack and returns them.
     * The quantity in this stack is whatever remains.
     * @param amount The amount to remove
     * @returns The items that were removed.
     */
    ItemStack splitOff(uint16_t amount);

    /**
     * Merges the other ItemStack into this ItemStack.
     * This will obey all stacking restrictions including maximum quantity.
     * @param other The ItemStack to merge into this ItemStack. 
     *              Its quantity will be updated.
     * @returns true If any items were able to be moved accross.
     */
    bool merge(ItemStack &other);

    /**
     * Merges the other ItemStack into this ItemStack.
     * This will obey all stacking restrictions including maximum quantity.
     * @param other The ItemStack to merge into this ItemStack. 
     * @returns true The remaining quantity that cannot be merged.
     */
    uint16_t combine(const ItemStack &other);

    /**
     * Produces a copy of this ItemStack with the quantity overridden
     * @param quantity The quantity for the returned ItemStack
     * @returns The new ItemStack with the given quantity.
     */
    ItemStack copy(uint16_t quantity);

    /**
     * Checks if the given ItemStack holds the same type but not necessarily the 
     * same quantity as this ItemStack
     */
    bool isSimilar(const ItemStack &other) const;

    /**
     * Checks if the given ItemStack holds the same type and quantity as this
     * ItemStack
     */
    bool operator==(const ItemStack &other) const;

    private:
    const ItemType *type;
    uint16_t quantity;
};

typedef std::optional<ItemStack> OptionalItemStack;


}