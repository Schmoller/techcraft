#pragma once

#include "fluid/base.hpp"

namespace Techcraft {

// in lieu of including
class Dimension;

class FluidSimulation {
    public:
    FluidSimulation(Dimension &dimension);

    void fluidStep();

    private:
    bool needsUpdate(const TilePosition &position, const FluidTile &fluid, uint16_t *newAmount);

    // Provided
    Dimension &dimension;

    // State
    uint32_t tickCount {0};
};

}