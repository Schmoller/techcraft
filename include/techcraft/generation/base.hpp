#pragma once

#include "biome/base.hpp"

#include "techcraft/common.hpp"
#include "techcraft/forward.hpp"

#include <vector>

namespace Techcraft::Generation {

/**
 * A generic chunk generator solution that populates chunks.
 * NOTE: Chunk generators run on threads. They must be thread safe.
 */
class ChunkGenerator {
    public:
    ChunkGenerator();

    virtual void populate(const ChunkPosition &coord, const std::shared_ptr<Chunk> &chunk) = 0;

    void addBiome(std::unique_ptr<Biome> &&biome);

    const std::vector<std::unique_ptr<Biome>> &getBiomes() const { return biomes; }

    protected:
    std::vector<std::unique_ptr<Biome>> biomes;
    std::vector<double> biomeBoundaries;
    double maxWeight { 0 };
    // Needed for queries, useful to reduce need to do divisions
    double halfWeight { 0 };
};

}