#pragma once

#include "base.hpp"

namespace Techcraft::Generation {

class DebugChunkGenerator : public ChunkGenerator {
    public:  
    void populate(const ChunkPosition &coord, Chunk *chunk);
};

}