#pragma once

#include "base.hpp"
#include <libnoise/noise.h>

namespace Techcraft::Generation {

class OverworldGenerator : public ChunkGenerator {
    public:
    explicit OverworldGenerator(int seed);
    
    void populate(const ChunkPosition &coord, const std::shared_ptr<Chunk> &chunk) override;

    private:
    noise::module::Perlin highFrequencyGen;
    noise::module::Perlin mediumFrequencyGen;
    noise::module::Perlin lowFrequencyGen;
    noise::module::Perlin biomeGen;

    double sampleTerrainHeight(const Biome &biome, double x, double y) const;
};

}