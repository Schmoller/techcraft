#pragma once

#include "base.hpp"

namespace Techcraft::Generation {

class OceanBiome : public Biome {
    public:
    OceanBiome();

    virtual void decorateColumn(const std::shared_ptr<Chunk> &chunk, const ChunkPosition &chunkCoord, int x, int y, int realX, int realY, tcsize terrainHeight) override;
};

}