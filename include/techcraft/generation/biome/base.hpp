#pragma once

#include "techcraft/common.hpp"
#include "techcraft/forward.hpp"
#include <memory>

namespace Techcraft::Generation {

class Biome {
    public:
    float minimumElevation { 0 };
    float maximumElevation { 100 };

    float lowFreqFactor { 1 };
    float medFreqFactor { 1 };
    float highFreqFactor { 1 };

    float weight { 1 };

    virtual void decorateColumn(const std::shared_ptr<Chunk> &chunk, const ChunkPosition &chunkCoord, int x, int y, int realX, int realY, tcsize terrainHeight) {};
};

}