#pragma once

#include "plains.hpp"

namespace Techcraft::Generation {

class HillsBiome : public PlainsBiome {
    public:
    HillsBiome();
};

}