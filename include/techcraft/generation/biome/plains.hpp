#pragma once

#include "base.hpp"

namespace Techcraft::Generation {

class PlainsBiome : public Biome {
    public:
    PlainsBiome();

    virtual void decorateColumn(const std::shared_ptr<Chunk> &chunk, const ChunkPosition &chunkCoord, int x, int y, int realX, int realY, tcsize terrainHeight) override;
};

}