#pragma once

#include "techcraft/common.hpp"
#include "techcraft/dimension/dimension.hpp"

#include <random>

namespace Techcraft::Generation {

template <typename T>
class GenerationParameter {
    public:
    GenerationParameter(T min, T max, T variance = 0, T bias = 0):
        minimum(min), maximum(max), variance(variance), bias(bias) {};

    GenerationParameter(T value):
        minimum(value), maximum(value), variance(variance), bias(bias) {};

    T randomValue(std::mt19937 &random) const {
        auto intValue = random();
        float floatValue = static_cast<float>(intValue - random.min()) / static_cast<float>(random.max() - random.min());

        // TODO: Variance and bias
        return std::round(floatValue * (maximum - minimum) + minimum);
    }

    T minimum;
    T maximum;
    T variance;
    T bias;
};

typedef GenerationParameter<int32_t> IntParameter;
typedef GenerationParameter<float> FloatParameter;

class TreeGenerator {
    public:
    TreeGenerator();

    void generateAt(Dimension &dimension, const TilePosition &position);

    private:
    IntParameter trunkSize { 1, 1 };
    IntParameter height { 6, 25 };
    const TileType *trunkType { nullptr };
    const TileType *branchType { nullptr };
    const TileType *leafType { nullptr };

    size_t placeBy(const std::string &dna, size_t offset, Dimension &dimension, TilePosition startPos, TileDirection startDir);
};

}