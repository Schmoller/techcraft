#pragma once

#include "techcraft/common.hpp"
#include "techcraft/dimension/dimension.hpp"
#include "techcraft/rand.hpp"

namespace Techcraft::Generation {

class BushyTreeGenerator {
    public:
    BushyTreeGenerator();

    void generateAt(Dimension &dimension, const TilePosition &position, Random &random);

    private:
    const TileType *trunkType { nullptr };
    const TileType *branchType { nullptr };
    const TileType *leafType { nullptr };

    void placeLogAt(Dimension &dimension, const TilePosition &position);
    void placeLeafAt(Dimension &dimension, const TilePosition &position);
    void placeLeafHanging(Dimension &dimension, const TilePosition &position, tcsize length);
};

}