#pragma once

#include "types.hpp"
#include <glm/glm.hpp>

#include <functional>
#include <techcraft/dimension/size.hpp>

namespace Techcraft {

class Position;

/**
 * Represnts a tile snapped position.
 * Useful for locating anything within the tile grid
 */
class TilePosition {
    public:
    tcsize x;
    tcsize y;
    tcsize z;

    constexpr inline TilePosition() : TilePosition(0,0,0) {}
    constexpr inline TilePosition(tcsize x, tcsize y, tcsize z)
        : x(x), y(y), z(z)
    {}

    constexpr inline TilePosition operator+(const TilePosition &other) const {
        return {x + other.x, y + other.y, z + other.z};
    }

    constexpr inline TilePosition operator-(const TilePosition &other) const {
        return {x - other.x, y - other.y, z - other.z};
    }

    constexpr inline bool operator==(const TilePosition &other) const {
        return (
            x == other.x &&
            y == other.y &&
            z == other.z
        );
    }
};

/**
 * Represents granular positions.
 * Useful for precicely locating something within space
 */
class Position {
    public:
    tcfloat x;
    tcfloat y;
    tcfloat z;

    constexpr inline Position() : Position(0, 0, 0) {}
    constexpr inline Position(tcfloat x, tcfloat y, tcfloat z)
        : x(x), y(y), z(z)
    {}
    constexpr inline Position(tcsize x, tcsize y, tcsize z)
        : Position(
            static_cast<tcfloat>(x),
            static_cast<tcfloat>(y),
            static_cast<tcfloat>(z)
        )
    {}

    constexpr inline Position(const glm::vec3 &vector)
        : Position(vector.x, vector.y, vector.z)
    {}

    constexpr inline Position(const glm::highp_dvec3 &vector)
        : Position(vector.x, vector.y, vector.z)
    {}

    constexpr inline Position(const TilePosition &position)
        : Position(position.x, position.y, position.z)
    {}

    constexpr inline Position operator+(const Position &other) const {
        return {
            x + other.x,
            y + other.y,
            z + other.z
        };
    }

    constexpr inline Position operator-(const Position &other) const {
        return {
            x - other.x,
            y - other.y,
            z - other.z
        };
    }

    constexpr inline Position operator*(tcfloat scale) const {
        return {
            x * scale,
            y * scale,
            z * scale
        };
    }

    constexpr inline bool operator==(const Position &other) const {
        return (
            x == other.x &&
            y == other.y &&
            z == other.z
        );
    }

    constexpr inline operator glm::vec3() const {
        return {
            static_cast<float>(x),
            static_cast<float>(y),
            static_cast<float>(z),
        };
    }
    constexpr inline operator glm::highp_dvec3() const {
        return {x, y, z};
    }

    constexpr inline operator TilePosition() const {
        return {
            static_cast<tcsize>(std::floor(x)),
            static_cast<tcsize>(std::floor(y)),
            static_cast<tcsize>(std::floor(z))
        };
    }
};

class ChunkPosition {
    public:
    ChunkCoordRes x;
    ChunkCoordRes y;
    ChunkCoordRes z;

    constexpr inline ChunkPosition() : ChunkPosition(0,0,0) {}
    constexpr inline ChunkPosition(ChunkCoordRes x, ChunkCoordRes y, ChunkCoordRes z)
        : x(x), y(y), z(z)
    {}
    constexpr inline ChunkPosition(const TilePosition &position)
        : ChunkPosition(
            static_cast<ChunkCoordRes>(position.x >> ChunkBits),
            static_cast<ChunkCoordRes>(position.y >> ChunkBits),
            static_cast<ChunkCoordRes>(position.z >> ChunkBits)
        )
    {}

    constexpr inline ChunkPosition(const Position &position)
        : ChunkPosition(
            static_cast<ChunkCoordRes>(static_cast<tcsize>(std::floor(position.x)) >> ChunkBits),
            static_cast<ChunkCoordRes>(static_cast<tcsize>(std::floor(position.y)) >> ChunkBits),
            static_cast<ChunkCoordRes>(static_cast<tcsize>(std::floor(position.z)) >> ChunkBits)
        )
    {}

    constexpr inline ChunkPosition operator+(const ChunkPosition &other) const {
        return {
            x + other.x,
            y + other.y,
            z + other.z
        };
    }

    constexpr inline ChunkPosition operator-(const ChunkPosition &other) const {
        return {
            x - other.x,
            y - other.y,
            z - other.z
        };
    }

    constexpr inline bool operator==(const ChunkPosition &other) const {
        return (
            other.x == x &&
            other.y == y &&
            other.z == z
        );
    }

    constexpr inline bool operator!=(const ChunkPosition &other) const {
        return (
            other.x != x ||
            other.y != y ||
            other.z != z
        );
    }

    inline operator TilePosition() const {
        return {
            static_cast<ChunkCoordRes>(x << ChunkBits),
            static_cast<ChunkCoordRes>(y << ChunkBits),
            static_cast<ChunkCoordRes>(z << ChunkBits)
        };
    }

    /**
     * Directly casts to a ChunkPosition without transforming the coordinates.
     */
    static constexpr inline ChunkPosition direct(const TilePosition &other) {
        return {
            static_cast<ChunkCoordRes>(other.x),
            static_cast<ChunkCoordRes>(other.y),
            static_cast<ChunkCoordRes>(other.z)
        };
    }
};

}

// Hash methods to allow positions to be used as keys
namespace std {
    template<> struct hash<Techcraft::ChunkPosition> {
        std::size_t operator()(Techcraft::ChunkPosition const& coord) const noexcept {
            auto coordHash = std::hash<ChunkCoordRes>()(coord.x);
            coordHash = coordHash ^ (std::hash<ChunkCoordRes>()(coord.y) << 1);
            coordHash = coordHash ^ (std::hash<ChunkCoordRes>()(coord.z) << 1);

            return coordHash;
        }
    };
    template<> struct hash<Techcraft::TilePosition> {
        std::size_t operator()(Techcraft::TilePosition const& coord) const noexcept {
            auto coordHash = std::hash<tcsize>()(coord.x);
            coordHash = coordHash ^ (std::hash<tcsize>()(coord.y) << 1);
            coordHash = coordHash ^ (std::hash<tcsize>()(coord.z) << 1);

            return coordHash;
        }
    };
}