#pragma once

#include "position.hpp"
#include "direction.hpp"

namespace Techcraft {

class BoundingBox {
    public:
    double xMin;
    double yMin;
    double zMin;
    double xMax;
    double yMax;
    double zMax;

    BoundingBox() = default;
    BoundingBox(const Position &a, const Position &b);
    BoundingBox(double xMin, double yMin, double zMin, double xMax, double yMax, double zMax);
    BoundingBox(double width, double depth, double height, bool centered = true);

    bool operator==(const BoundingBox &other) const;
    BoundingBox operator+(const Position &position) const;
    BoundingBox operator-(const Position &position) const;
    BoundingBox &operator+=(const Position &position);
    BoundingBox &operator-=(const Position &position);

    BoundingBox include(const Position &position) const;
    BoundingBox &includeSelf(const Position &position);

    BoundingBox &offsetX(double x);
    BoundingBox &offsetY(double y);
    BoundingBox &offsetZ(double z);

    BoundingBox expand(double x, double y, double z) const;
    BoundingBox expand(double all) const;
    
    BoundingBox expandSkew(double x, double y, double z) const;
    BoundingBox &expandSkewSelf(double x, double y, double z);
    BoundingBox shrinkSkew(double x, double y, double z) const;
    BoundingBox &shrinkSkewSelf(double x, double y, double z);

    bool contains(const BoundingBox &other) const;
    bool contains(const Position &point) const;

    bool intersects(const BoundingBox &other) const;

    /**
     * Checks for an intersection of this bounding box by an infinitely long ray.
     * This doesnt need to check the end of the ray because the use case already
     * limits the test.
     */
    bool intersectedBy(const Position &rayOrigin, const Position &rayDirection, TileDirection &outHitFace, double *outDistance = nullptr);
    
    Position center() const;
    double width() const;
    double depth() const;
    double height() const;

};

}