#pragma once

#include "position.hpp"
#include "direction.hpp"
#include "boundingbox.hpp"

#include <iostream>

namespace Techcraft {

std::ostream &operator<<(std::ostream &os, const BoundingBox &box);

std::string toString(TileDirection direction);
std::string toString(TileDirectionExtended direction);
std::ostream &operator<<(std::ostream &os, const TileDirection &direction);
std::ostream &operator<<(std::ostream &os, const TileDirectionExtended &direction);

std::ostream &operator<<(std::ostream &os, const TilePosition &pos);
std::ostream &operator<<(std::ostream &os, const Position &pos);
std::ostream &operator<<(std::ostream &os, const ChunkPosition &pos);

}
