#pragma once

#include <cstdint>

typedef int32_t tcsize;
typedef double tcfloat;
typedef int32_t ChunkCoordRes;

// Special literals
inline constexpr tcsize operator "" _tcs(unsigned long long value) noexcept {
    return static_cast<tcsize>(value);
}
inline constexpr tcfloat operator "" _tcf(long double value) noexcept {
    return static_cast<tcfloat>(value);
}
inline constexpr ChunkCoordRes operator "" _tcc(unsigned long long value) noexcept {
    return static_cast<ChunkCoordRes>(value);
}
inline constexpr uint8_t operator "" _u8(unsigned long long value) noexcept {
    return static_cast<uint8_t>(value);
}
inline constexpr int8_t operator "" _i8(unsigned long long value) noexcept {
    return static_cast<int8_t>(value);
}
inline constexpr uint16_t operator "" _u16(unsigned long long value) noexcept {
    return static_cast<uint16_t>(value);
}
inline constexpr int16_t operator "" _i16(unsigned long long value) noexcept {
    return static_cast<int16_t>(value);
}