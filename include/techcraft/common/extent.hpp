#pragma once

#include "position.hpp"

namespace Techcraft {

struct Extent {
    tcsize width;
    tcsize depth;
    tcsize height;

    inline bool contains(const TilePosition &location) const {
        return (
            location.x >= 0 &&
            location.y >= 0 &&
            location.z >= 0 && 
            location.x < width &&
            location.y < depth &&
            location.z < height
        );
    }

    inline tcsize volume() const {
        return width * depth * height;
    }
};

}