#pragma once

#include "techcraft/tiles/base.hpp"
#include "techcraft/fluid/base.hpp"
#include "techcraft/subtiles/base.hpp"

#include <unordered_set>
#include <optional>

namespace Techcraft {

enum class HitType {
    Nothing,
    Tile,
    Subtile,
    Fluid,
    Entity
};

class RaycastResult {
    public:
    RaycastResult(
        const TilePosition &tilePosition,
        const Position &position,
        const Tile *tile,
        const FluidTile *fluid,
        const Subtile *subtile,
        HitType type,
        TileDirection direction = TileDirection::Self,
        const SubtileSlot *slot = nullptr
    );

    /**
     * The result of the raycast
     */
    inline HitType result() const { return hitResult; }

    /**
     * Did the raycast hit anything at all?
     */
    inline bool didHitAny() const { return hitResult != HitType::Nothing; }

    /**
     * The final position of the ray. This is either where the hit was (if there was a hit) 
     * or the last position before terminating.
     */
    inline const TilePosition &tilePosition() const { return hitTilePosition; }

    /**
     * The final position of the ray. This is either where the hit was (if there was a hit) 
     * or the last position before terminating.
     */
    inline const Position &position() const { return hitPosition; }

    /**
     * The final position of the ray relative to the bounding box of the tile or subtile it hit (or landed in).
     * If there is no bounding box, then the position will be relative to the tile coord.
     */
    Position relativePosition() const;

    /**
     * Gets the hit things bounding box if it exists
     */
    std::optional<BoundingBox> bounds() const;

    /**
     * The face that was hit.
     * Present with HitType::Tile, HitType::Subtile, or HitType::Fluid
     */
    inline TileDirection face() const { return hitFace; }

    inline bool hasTile() const { return hitTile != nullptr; }
    inline bool hasSubtile() const { return hitSubtile != nullptr; }
    inline bool hasFluid() const { return hitFluid != nullptr; }

    /**
     * The tile that was hit, or is present at the hit location.
     * Present with any HitType::Tile or HitType::Subtile
     */
    inline const Tile *tile() const { return hitTile; }
    /**
     * The slot that the Subtile (if present) is within
     */
    inline const SubtileSlot &slot() const { return *hitSlot; }
    /**
     * The subtile that was hit.
     * Only present with HitType::Subtile
     */
    inline const Subtile *subTile() const { return hitSubtile; }
    /**
     * The fluid that was hit, or is present at the hit location.
     * Present with any HitType.
     */
    inline const FluidTile *fluid() const { return hitFluid; }

    private:
    HitType hitResult { HitType::Nothing };

    TilePosition hitTilePosition;
    Position hitPosition;
    const SubtileSlot *hitSlot { nullptr };
    TileDirection hitFace { TileDirection::Self };

    const Tile *hitTile { 0 };
    const Subtile *hitSubtile { 0 };
    const FluidTile *hitFluid { 0 };
};


class Raycast {
    public:
    Raycast(const Position &start, const Position &end);
    Raycast(const Position &start, const Position &direction, float maxRange);

    Raycast &onlyTiles();
    Raycast &onlyFluid();
    Raycast &onlySubtiles();
    Raycast &onlyEntities();

    Raycast &ignoreTiles();
    Raycast &ignoreFluid();
    Raycast &ignoreSubtiles();
    Raycast &ignoreEntities();

    Raycast &ignore(const TileType *type);
    Raycast &ignore(const FluidType *type);
    Raycast &ignore(const SubtileType *type);

    RaycastResult cast(Dimension &dimension);

    private:
    Position start;
    Position direction;
    Position end;

    bool allowTiles { true };
    bool allowFluid { true };
    bool allowEntities { true };
    bool allowSubtiles { true };

    std::unordered_set<const TileType*> ignoredTileTypes;
    std::unordered_set<const FluidType*> ignoredFluidTypes;
    std::unordered_set<const SubtileType*> ignoredSubtileTypes;

    std::optional<RaycastResult> testTile(Dimension &dimension, const TilePosition &current, TileDirection face);
};

}