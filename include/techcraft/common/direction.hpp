#pragma once

#include "position.hpp"

namespace Techcraft {

enum class TileDirection {
    Up = 0,
    Down,
    North,
    South,
    East,
    West,
    Self
};

const TileDirection AllDirections[6] = {
    TileDirection::Up,
    TileDirection::Down,
    TileDirection::North,
    TileDirection::South,
    TileDirection::East,
    TileDirection::West
};

const TileDirection Horizontal[4] = {
    TileDirection::North,
    TileDirection::South,
    TileDirection::East,
    TileDirection::West
};

const TileDirection Vertical[2] = {
    TileDirection::Up,
    TileDirection::Down
};

enum class TileDirectionExtended {
    Up = 0,
    Down,
    North,
    South,
    East,
    West,
    NorthEast,
    NorthWest,
    SouthEast,
    SouthWest,
    UpEast,
    UpWest,
    UpNorth,
    UpSouth,
    UpNorthEast,
    UpNorthWest,
    UpSouthEast,
    UpSouthWest,
    DownEast,
    DownWest,
    DownNorth,
    DownSouth,
    DownNorthEast,
    DownNorthWest,
    DownSouthEast,
    DownSouthWest,
    Self
};

const TileDirectionExtended AllExtendedDirections[26] = {
    TileDirectionExtended::Up,
    TileDirectionExtended::Down,
    TileDirectionExtended::North,
    TileDirectionExtended::South,
    TileDirectionExtended::East,
    TileDirectionExtended::West,
    TileDirectionExtended::NorthEast,
    TileDirectionExtended::NorthWest,
    TileDirectionExtended::SouthEast,
    TileDirectionExtended::SouthWest,
    TileDirectionExtended::UpEast,
    TileDirectionExtended::UpWest,
    TileDirectionExtended::UpNorth,
    TileDirectionExtended::UpSouth,
    TileDirectionExtended::UpNorthEast,
    TileDirectionExtended::UpNorthWest,
    TileDirectionExtended::UpSouthEast,
    TileDirectionExtended::UpSouthWest,
    TileDirectionExtended::DownEast,
    TileDirectionExtended::DownWest,
    TileDirectionExtended::DownNorth,
    TileDirectionExtended::DownSouth,
    TileDirectionExtended::DownNorthEast,
    TileDirectionExtended::DownNorthWest,
    TileDirectionExtended::DownSouthEast,
    TileDirectionExtended::DownSouthWest
};

const TileDirectionExtended ExtendedHorizontal[8] = {
    TileDirectionExtended::North,
    TileDirectionExtended::South,
    TileDirectionExtended::East,
    TileDirectionExtended::West,
    TileDirectionExtended::NorthEast,
    TileDirectionExtended::NorthWest,
    TileDirectionExtended::SouthEast,
    TileDirectionExtended::SouthWest,
};

const TileDirectionExtended ExtendedVertical[2] = {
    TileDirectionExtended::Up,
    TileDirectionExtended::Down
};

class TileDirectionOffset {
public:
    static constexpr TilePosition Up = {0, 0, 1};
    static constexpr TilePosition Down = {0, 0, -1};
    static constexpr TilePosition North = {0, 1, 0};
    static constexpr TilePosition South = {0, -1, 0};
    static constexpr TilePosition East = {1, 0, 0};
    static constexpr TilePosition West = {-1, 0, 0};
    static constexpr TilePosition Self = {0, 0, 0};

    static constexpr TilePosition NorthEast = {1, 1, 0};
    static constexpr TilePosition NorthWest = {-1, 1, 0};
    static constexpr TilePosition SouthEast = {1, -1, 0};
    static constexpr TilePosition SouthWest = {-1, -1, 0};
    static constexpr TilePosition UpEast = {1, 0, 1};
    static constexpr TilePosition UpWest = {-1, 0, 1};
    static constexpr TilePosition UpNorth = {0, 1, 1};
    static constexpr TilePosition UpSouth = {0, -1, 1};
    static constexpr TilePosition UpNorthEast = {1, 1, 1};
    static constexpr TilePosition UpNorthWest = {-1, 1, 1};
    static constexpr TilePosition UpSouthEast = {1, -1, 1};
    static constexpr TilePosition UpSouthWest = {-1, -1, 1};
    static constexpr TilePosition DownEast = {1, 0, -1};
    static constexpr TilePosition DownWest = {-1, 0, -1};
    static constexpr TilePosition DownNorth = {0, 1, -1};
    static constexpr TilePosition DownSouth = {0, -1, -1};
    static constexpr TilePosition DownNorthEast = {1, 1, -1};
    static constexpr TilePosition DownNorthWest = {-1, 1, -1};
    static constexpr TilePosition DownSouthEast = {1, -1, -1};
    static constexpr TilePosition DownSouthWest = {-1, -1, -1};

    static TilePosition from(TileDirection direction) {
        return Directions[static_cast<int>(direction)];
    }
    static TilePosition from(TileDirectionExtended direction) {
        return ExtendedDirections[static_cast<int>(direction)];
    }
    
private:
    static constexpr TilePosition Directions[7] = {
        Up,
        Down,
        North,
        South,
        East,
        West,
        Self
    };

    static constexpr TilePosition ExtendedDirections[27] = {
        Up,
        Down,
        North,
        South,
        East,
        West,
        NorthEast,
        NorthWest,
        SouthEast,
        SouthWest,
        UpEast,
        UpWest,
        UpNorth,
        UpSouth,
        UpNorthEast,
        UpNorthWest,
        UpSouthEast,
        UpSouthWest,
        DownEast,
        DownWest,
        DownNorth,
        DownSouth,
        DownNorthEast,
        DownNorthWest,
        DownSouthEast,
        DownSouthWest,
        Self
    };
};

// Utility methods
TileDirection opposite(TileDirection direction);
TileDirectionExtended opposite(TileDirectionExtended direction);
TileDirectionExtended toDirection(int x, int y, int z);
}