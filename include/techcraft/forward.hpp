#pragma once

namespace Techcraft {

class Region;

class Chunk;
class Column;
class ChunkLoader;
class AreaTicketer;
class Dimension;
class Universe;
class ClientUniverse;
class Game;
class FluidSimulation;
class Tile;
class TileEntity;
class Subtile;

class Console;
class Debug;

class KeyBinding;
class BindingManager;
class PhysicsSimulation;
class Random;
class ResourceManager;
class Resource;
class ChunkRenderState;
}
