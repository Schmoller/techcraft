#pragma once

// Forward declarations
namespace Techcraft {
    class PhysicsSimulation;
}

#include "entities/common.hpp"
#include "engine/subsystem/debug.hpp"

#include <vector>
#include <chrono>
#include <functional>

namespace Techcraft {

// Prototype in lieu of include
class Dimension;

class PhysicsSimulation {
    public:
    PhysicsSimulation(Dimension &dimension);//, Engine::Subsystem::DebugSubsystem &debug);

    void physicsStep();

    // Event handlers
    void onEntityAdd(Entity *entity);
    void onEntityRemove(Entity *entity);
    private:

    // Provided
    Dimension &dimension;
    std::vector<Entity*> physicsEntities;
    // Engine::Subsystem::DebugSubsystem &debug;

    // Configuration
    glm::highp_dvec3 gravity = {0, 0, -25};

    // Internal state
    std::chrono::time_point<std::chrono::high_resolution_clock> lastStepTime;

    private:
    void testBoundingBoxes(const BoundingBox &testArea, std::function<bool(BoundingBox)> testFunction);
    void testFluidBoundingBoxes(const BoundingBox &testArea, std::function<bool(BoundingBox)> testFunction);
};

}