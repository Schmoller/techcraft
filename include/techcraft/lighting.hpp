#pragma once

// Forward declarations
namespace Techcraft {
    class LightingUpdater;
}

#include "common.hpp"
#include "tiles/base.hpp"
#include "settings.hpp"
#include <glm/glm.hpp>
#include <optional>

namespace Techcraft {

// Prototype in lieu of include
class Dimension;

#define TILE_LIGHT_MAX_INTENSITY 31.0f
#define SKY_LIGHT_MAX_INTENSITY 31.0f
#define SKY_LIGHT_INTENSITY 31

#define toLightBaked(intensity, direction) ((intensity & 0b11111) << 3) | (static_cast<uint8_t>(direction) & 0b111)
#define getIntensity(baked) ((baked & 0b11111000) >> 3)
#define getDirectionInt(baked) (baked & 0b111)
#define getDirection(baked) static_cast<TileDirection>(baked & 0b111)
/**
 * Handles recalculating tile lighting
 */
class LightingUpdater {
    struct FloodState {
        tcsize x;
        tcsize y;
        tcsize z;

        uint8_t tileLight[TILE_LIGHT_PALLET_COUNT];
        uint8_t skyLight;
        bool seed;
        bool check;
        bool process[TILE_LIGHT_PALLET_COUNT] = {false};
        bool processSky = false;
    };

    public:
    LightingUpdater(Dimension &dimension);

    /**
     * Recalculates lighting in an entire chunk. This may update surrounding chunks
     */
    void recalculateLighting(const ChunkPosition &chunkCoord);
    /**
     * Recalculates lighting in response to a change in the given tile
     */
    void recalculateLightingFromTile(const TilePosition &position, std::optional<TilePosition> highest);

    /**
     * Recalculates sky lighting in a column, in response to a change of the top block
     */
    void recalculateSkyForColumn(tcsize x, tcsize y, tcsize zMin, tcsize zMax, bool raised);

    /**
     * Performs lighting updates.
     */
    void update();

    private:
    void floodLight(const FloodState &state, bool positive);
    void floodLight(const std::deque<FloodState> &positive, const std::deque<FloodState> &negative, bool singleChunkOnly = false, const ChunkPosition &limit = {});

    // Provided    
    Dimension &dimension;
};

glm::vec3 blendTileLighting(const uint8_t *tileLight);
glm::vec3 blendSkyLighting(uint8_t skyLight);

}