#pragma once

#include "engine/engine.hpp"

#include "techcraft/universe.hpp"
#include "techcraft/keybinding.hpp"
#include "utilities/workers.hpp"
#include "techcraft/items/registry.hpp"
#include "techcraft/commands/executor.hpp"
#include "techcraft/resources/manager.hpp"
#include "techcraft/rendering/item/helper.hpp"
#include "techcraft/gui/window.hpp"

namespace Techcraft {

class Game {
    public:
    static Game &getInstance() { return *instance; };

    Game();

    /**
     * Launches the game (yay)
     * @returns false if an error occurrs
     */
    bool launch();

    void reloadSettings();
    void loadNewUniverse(const std::string &name, size_t seed);

    ClientUniverse *getUniverse() const { return universe.get(); }

    /**
     * Gets the currently displayed GUI window if any.
     */
    std::shared_ptr<Window> getCurrentGUI() const;
    /**
     * Sets the currently displayed GUI.
     * If there was already a shown window, that will be closed.
     */
    void setCurrentGUI(std::shared_ptr<Window> window);

    double getFPS() const { return instantFPS; }
    double getFPSAverage() const { return rollingFPS; }

    BindingManager &getBindingManager() { return *bindingManager; }

    Rendering::ItemRenderHelper &getItemRenderHelper() const { return *itemRenderHelper; }

    Commands::Executor &getCommandExecutor() const { return *commandExecutor; }

    private:
    static Game *instance;
    bool initEngine();

    void initMisc();

    void loadResources();

    void initBindings();
    void initSubtiles();
    void initItems();
    void initCommands();

    void eventLoop();

    void onInputUpdate(Engine::Key key, Engine::Action action, const Engine::Modifier &modifers);
    void onCharInputUpdate(wchar_t ch);
    void onMouseInputUpdate(double x, double y, Engine::Action action, Engine::MouseButton button, Engine::Modifier modifiers);
    void onMouseMove(double x, double y, Engine::MouseButtons buttons, Engine::Modifier modifiers);
    void onMouseScroll(double scrollX, double scrollY);

    Engine::RenderEngine engine;
    Utilities::WorkerFleet fleet;

    std::shared_ptr<Window> currentWindow;
    int currentWindowId { -1 };
    bool captureMouse { true };

    std::unique_ptr<BindingManager> bindingManager;
    std::unique_ptr<ItemRegistry> itemRegistry;
    std::unique_ptr<Commands::Executor> commandExecutor;
    std::unique_ptr<ResourceManager> resourceManager;

    std::shared_ptr<ClientUniverse> universe;
    std::unique_ptr<Rendering::ItemRenderHelper> itemRenderHelper;

    std::chrono::high_resolution_clock::time_point lastFrameTime;
    
    // Diagnostics
    const double fpsRollingWeight {0.99};
    double instantFPS {0};
    double rollingFPS {0};
};

}