#pragma once

#include "common.hpp"
#include "tiles/base.hpp"
#include "fluid/base.hpp"
#include "subtiles/base.hpp"

#include <memory>
#include <vector>

namespace Techcraft {

/**
 * A region is some area that contain tiles.
 * This might be a chunk but can also be some other tile containing object.
 */
class Region {
public:
    /**
     * The size of the region
     */
    virtual Extent size() const = 0;

    virtual void setTile(const TilePosition& location, const Tile &tile) = 0;
    virtual const Tile &getTile(const TilePosition& location) const = 0;

    virtual void setFluid(const TilePosition& location, const FluidTile &fluid) = 0;
    virtual const FluidTile &getFluid(const TilePosition& location) const = 0;

    virtual void setTileLight(const TilePosition &location, const TileLighting &light) = 0;
    virtual const TileLighting &getTileLight(const TilePosition &location) const = 0;

    virtual void setSubtile(const TilePosition &location, const SubtileSlot &slot, const Subtile &subtile) = 0;
    virtual const Subtile &getSubtile(const TilePosition &location, const SubtileSlot &slot) const = 0;
};

}