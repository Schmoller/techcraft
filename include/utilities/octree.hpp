#pragma once

#include <cstdint>
#include <memory>
#include <optional>
#include <type_traits>
#include <array>

namespace Utilities {

template <typename T, size_t Depth = 16, typename CoordUnit = int64_t>
class Octree {
    using TPointer = std::shared_ptr<T>;
    using NodeType = Octree<T, Depth-1, CoordUnit>;
    using NodePointer = std::shared_ptr<NodeType>;

    friend class Octree<T, Depth+1, CoordUnit>;
public:
    constexpr static bool isLeaf() { return Depth <= 1; }

    using ChildType = typename std::conditional<isLeaf(), TPointer, NodePointer>::type;

    inline const TPointer *find(CoordUnit x, CoordUnit y, CoordUnit z) const {
        auto &child = children[cellIndex(x, y, z)];

        if (!child) {
            return {};
        }

        if constexpr (isLeaf()) {
            return &child;
        } else {
            return child->find(x, y, z);
        }
    }

    inline const TPointer &put(CoordUnit x, CoordUnit y, CoordUnit z, const TPointer &value) {
        if constexpr (isLeaf()) {
            children[cellIndex(x, y, z)] = value;
            return children[cellIndex(x, y, z)];
        } else {
            auto &child = children[cellIndex(x, y, z)];
            if (child) {
                return child->put(x, y, z, value);
            } else {
                children[cellIndex(x, y, z)] = std::make_shared<NodeType>();
                return children[cellIndex(x, y, z)]->put(x, y, z, value);
            }
        }
    }

    inline void remove(CoordUnit x, CoordUnit y, CoordUnit z) {
        removeInternal(x, y, z);
    }

    template <typename F>
    inline void forEach(const F &callback) {
        for (auto &child : children) {
            if (!child) {
                continue;
            }

            if constexpr (isLeaf()) {
                callback(child);
            } else {
                child->forEach(callback);
            }
        }
    }

private:
    inline CoordUnit pickBit(CoordUnit value) const {
        // Each child node has a value of Depth that is 1 lower than its parent.
        // This allows us to scan through the coords to find the right node.

        constexpr size_t shift = (Depth - 1);
        constexpr size_t mask = 1 << shift;

        return (value & mask) >> shift;
    }

    inline CoordUnit cellIndex(CoordUnit x, CoordUnit y, CoordUnit z) const {
        return pickBit(x) << 2 | pickBit(y) << 1 | pickBit(z);
    }

    bool removeInternal(CoordUnit x, CoordUnit y, CoordUnit z) {
        if constexpr (isLeaf()) {
            children[cellIndex(x, y, z)] = {};

        } else {
            auto &child = children[cellIndex(x, y, z)];
            if (child) {
                if (!child->removeInternal(x, y, z)) {
                    children[cellIndex(x, y, z)] = {};
                }
            }
        }

        for (size_t i = 0; i < 8; ++i) {
            if (children[i]) {
                return true;
            }
        }

        return false;
    }

    std::array<ChildType, 8> children;
};
}