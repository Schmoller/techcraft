# Techcraft (Working title)

## Building

This project uses cmake to build the executable.

The following external libraries are required:

- Vulkan
- glfw3
- glm
- libnoise
- [JSON for Modern C++](https://github.com/nlohmann/json)

C++ 20 support is required
