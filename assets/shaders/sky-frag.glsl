#version 450
#pragma shader_stage(fragment)
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 inScreenCoord;

layout(binding = 0) uniform CameraUBO {
    mat4 view;
    mat4 proj;
} cam;

layout(location = 0) out vec4 outColor;

layout(binding = 1) uniform SkyUBO {
    float sunAngularDiameter;
    vec3 sunDirection;
} sky;

const vec3 up = vec3(0.0, 0.0, 1.0);

const vec3 baseSkyColour = vec3(0.3961, 0.8078, 0.9451);
const vec3 baseSunsetColour = vec3(0.8753, 0.5443, 0.2271);

void main() {
    // Pixel direction to virtual skydome
    mat4 inverseCamera = inverse(cam.proj * cam.view);
    vec4 far = inverseCamera * vec4(inScreenCoord, 100, 1);
    vec4 near = inverseCamera * vec4(inScreenCoord, 0, 1);
    far = far / far.w;
    near = near / near.w;

    vec3 directionVector = normalize(near.xyz - far.xyz);
	float height = dot(up, normalize(directionVector));

	float cosTheta = dot(directionVector, sky.sunDirection);

	// The sun
	float sunAngularDiameterCos = cos(sky.sunAngularDiameter);
	float sundisk = smoothstep(sunAngularDiameterCos, sunAngularDiameterCos + 0.00002, cosTheta);
	vec3 sun = vec3(sundisk, sundisk, sundisk * 0.3);
	
	// Sunset / sunrise
	float horizonContribution = clamp(pow(1-abs(height), 2) * pow(1-abs(dot(up, sky.sunDirection)), 3), 0, 1);
	float sunsetIntensity = clamp((1 - clamp(height * 5, 0.0, 1.0)) * pow(cosTheta, 1.5), 0, 1);
	sunsetIntensity = pow(sunsetIntensity, 1.5) * horizonContribution;
	vec3 sunset = baseSunsetColour;

	// Sky and composition
	float skyBrightness = clamp((dot(up, sky.sunDirection) + 0.09) * 2.4, 0, 1);
	vec3 skyColour = baseSkyColour * skyBrightness;
	vec3 result = (skyColour * (1 - sunsetIntensity) + sunset * sunsetIntensity) + sun;

	outColor = vec4(result, 1.0);
}