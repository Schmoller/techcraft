#version 450
#pragma shader_stage(vertex)
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform CameraUBO {
    mat4 view;
    mat4 proj;
} cam;

layout(set = 1, binding = 1) uniform LightUBO {
    vec3 globalSkyLight;
    vec3 ambientLight;
} light;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inTileLight;
layout(location = 2) in vec3 inSkyTint;
layout(location = 3) in vec3 inOcclusion;
layout(location = 4) in vec3 inNormal;
layout(location = 5) in vec3 inTexCoord;

layout(location = 0) out vec3 fragCombinedLight;
layout(location = 1) out vec3 fragNormal;
layout(location = 2) out vec3 fragTexCoord;


void main() {
    gl_Position = cam.proj * cam.view * vec4(inPosition, 1.0);
    fragNormal = inNormal;

    fragTexCoord = inTexCoord;

    fragCombinedLight = (inTileLight + inSkyTint * light.globalSkyLight + light.ambientLight) * inOcclusion;
}