#version 450
#pragma shader_stage(fragment)
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 outColor;

layout(location = 0) in vec3 fragCombinedLight;
layout(location = 1) in vec3 fragNormal;
layout(location = 2) in vec3 fragTexCoord;
layout(set = 2, binding = 2) uniform sampler2DArray texSampler;

void main() {
    vec4 diffuse = texture(texSampler, fragTexCoord);

    outColor = vec4(fragCombinedLight, 1.0) * diffuse;

    if (outColor.a < 1) {
        discard;
    }
}