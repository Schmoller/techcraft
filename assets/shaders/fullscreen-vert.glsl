#version 450
#pragma shader_stage(vertex)
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec2 outScreenCoord;

const vec4 VERTICES_POS[4] = {
    vec4(-1, -1, 1, 1),
    vec4(1, -1, 1, 1),
    vec4(1, 1, 1, 1),
    vec4(-1, 1, 1, 1)
};

const vec2 VERTICES_SCREEN[4] = {
    vec2(-1, -1),
    vec2(1, -1),
    vec2(1, 1),
    vec2(-1, 1)
};

void main() {
    if (gl_VertexIndex == 0) {
        gl_Position = VERTICES_POS[0];
        outScreenCoord = VERTICES_SCREEN[0];
    } else if (gl_VertexIndex == 1) {
        gl_Position = VERTICES_POS[2];
        outScreenCoord = VERTICES_SCREEN[2];
    } else if (gl_VertexIndex == 2) {
        gl_Position = VERTICES_POS[1];
        outScreenCoord = VERTICES_SCREEN[1];
    } else if (gl_VertexIndex == 3) {
        gl_Position = VERTICES_POS[0];
        outScreenCoord = VERTICES_SCREEN[0];
    } else if (gl_VertexIndex == 4) {
        gl_Position = VERTICES_POS[3];
        outScreenCoord = VERTICES_SCREEN[3];
    } else if (gl_VertexIndex == 5) {
        gl_Position = VERTICES_POS[2];
        outScreenCoord = VERTICES_SCREEN[2];
    }
}